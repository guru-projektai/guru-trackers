<?php

class BaseDatabaseService
{
    protected $wpdb;
    protected $transactionStarted = false;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function startTransaction()
    {
        $this->setTransactionStarted(true);
        $this->wpdb->query('START TRANSACTION');
    }

    public function commit()
    {
        $this->setTransactionStarted(false);
        $this->wpdb->query('COMMIT');
    }

    public function rollback()
    {
        $this->setTransactionStarted(false);
        $this->wpdb->query('ROLLBACK');
    }

    public function setTransactionStarted($value)
    {
        $this->transactionStarted = $value;
    }

    public function isTransactionStarted()
    {
        return $this->transactionStarted;
    }
}
