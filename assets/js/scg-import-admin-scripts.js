jQuery(document).ready(function ($) {

  $( document.body ).on( 'submit', '.scg-trackers-import-form', function(e) {
     e.preventDefault();

     var submitButton = $(this).find( 'input[type="submit"]' );

     if ( ! submitButton.hasClass( 'button-disabled' ) ) {

      var data = $(this).serialize();

      submitButton.addClass( 'button-disabled' );
      submitButton.attr( 'value', 'Vyksta importavimas!' );
      // $(this).find('.notice-wrap').remove();
      // $(this).append( '<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>' );

      var sheet_select_value = $(this).find('#casino-trackers-import-select option:selected').val();
      var import_type_value = $(this).find('#casino-trackers-import-type option:selected').val();

      $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
          form: data,
          action: 'scg_manual_schedule_wrapper_import_action',
          sheet_select: sheet_select_value,
          import_type: import_type_value,
          security: SCGajax.security,
        },
        dataType: 'json',
        success: function( response ) {}
      }).fail(function (response) {
        if ( window.console && window.console.log ) {
          console.log( response );
        }
      });

      // trebPoll();

    }

  });

  $('.cancel-import').on('click', function () {
    $.ajax({
      type: 'POST',
      url: ajaxurl,
      data: {
        action: 'scg_trackers_import_cancel',
      },
      dataType: 'json',
      success: function( response ) {
        response.success && location.reload();
      }
    }).fail(function (response) {
      if ( window.console && window.console.log ) {
        console.log( response );
      }
    });

  });

  $( document.body ).on( 'submit', '.scg-restricted-countries-import-form', function(e) {
     e.preventDefault();

     var submitButton = $(this).find( 'input[type="submit"]' );

     if ( ! submitButton.hasClass( 'button-disabled' ) ) {

      var data = $(this).serialize();

      submitButton.addClass( 'button-disabled' );
      submitButton.attr( 'value', 'Vyksta importavimas!' );
      // $(this).find('.notice-wrap').remove();
      // $(this).append( '<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>' );

      var casino_value = $(this).find('#restricted-countries-import-select').val();

      $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
          form: data,
          action: 'scg_restricted_countries_import',
          restricted_countries_import_select: casino_value,
          restricted_countries_security: SCGajax.restricted_countries_security,
        },
        dataType: 'json',
        success: function( response ) {}
      }).fail(function (response) {
        if ( window.console && window.console.log ) {
          console.log( response );
        }
      });

      // restrictedPoll();

    }

  });

  $( document.body ).on( 'submit', '.scg-scrape-data-import-form', function(e) {
     e.preventDefault();

     var submitButton = $(this).find( 'input[type="submit"]' );

     if ( ! submitButton.hasClass( 'button-disabled' ) ) {

      var data = $(this).serialize();

      submitButton.addClass( 'button-disabled' );
      submitButton.attr( 'value', 'Vyksta importavimas!' );
      $(this).find('.notice-wrap').remove();
      $(this).append( '<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>' );

      var casino_value = $(this).find('#scrape-data-import-select').val();

      $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
          form: data,
          action: 'scg_scrape_data_import',
          scrape_data_import_select: casino_value,
          scrape_data_security: SCGajax.scrape_data_security,
        },
        dataType: 'json',
        success: function( response ) {}
      }).fail(function (response) {
        if ( window.console && window.console.log ) {
          console.log( response );
        }
      });

      scrapeDataPoll();

    }

  });


  $( document.body ).on( 'submit', '.scg-import-casino-all-info-import-form', function(e) {
     e.preventDefault();

     var submitButton = $(this).find( 'input[type="submit"]' );

     if ( ! submitButton.hasClass( 'button-disabled' ) ) {

      var data = $(this).serialize();

      submitButton.addClass( 'button-disabled' );
      submitButton.attr( 'value', 'Vyksta importavimas!' );
      // $(this).find('.notice-wrap').remove();
      // $(this).append( '<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>' );

      var casino_value = $(this).find('#casino-all-info-import-select').val();
      var import_logos = $(this).find('#casino-all-info-import-logos').val();

      $.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
          form: data,
          action: 'scg_manual_schedule_casino_all_info_import_action',
          casino_all_info_import_select: casino_value,
          casino_all_info_import_logos: import_logos,
          casino_all_info_security: SCGajax.casino_all_info_security,
        },
        dataType: 'json',
        success: function( response ) {}
      }).fail(function (response) {
        if ( window.console && window.console.log ) {
          console.log( response );
        }
      });

    }

  });


  var percentage = 0;
  var counter = 0;
  var sheet_select_value = $('.scg-trackers-import-form #casino-trackers-import-select option:selected').val();

  // Progress bar
  var data = {
    'action': 'scg_trackers_import_progress',
    'sheet_select': sheet_select_value,
    'progress': percentage,
    'counter': counter,
  };

  var trebPoll = function(run) {

    data['counter'] = run > 0 ? run : counter;
    data['progress'] = percentage;

    counter++;


    $.post(ajaxurl, data, function(response) {

      percentage = response == 'complete' ? 100 : response;

      $('.edd-progress div').animate({
        width: percentage + '%',
      }, 50, function() {
        // Animation complete.
      });

      if (response == 100 || response == 'complete') {
        $('.scg-trackers-import-form .notice-wrap .spinner').removeClass('is-active');

        $('.scg-trackers-import-form input[type="submit"], .scg-trackers-import-form select').remove();

        if (!$('.scg-trackers-import-form .current-status').length) {
          $('.scg-trackers-import-form').prepend('<div class="current-status hide"></div>');
        }

        $('.scg-trackers-import-form .current-status')
          .removeClass('importing').removeClass('hide')
          .addClass('done')
          .text('Importavimas baigtas!');

        counter = 0;
      }

      // if (response != 'complete') {
      //   setTimeout( trebPoll, 2000 );
      // }
    });
  }

  // Restricted variables.
  var restricted_percentage = 0;
  var restricted_counter = 0;

  // Progress bar
  var restricted_data = {
    'action': 'scg_restricted_countries_import_progress',
    'progress': restricted_percentage,
    'counter': restricted_counter,
  };

  // Restricted poll.
  var restrictedPoll = function(run) {

    restricted_data['counter'] = run > 0 ? run : restricted_counter;
    restricted_data['progress'] = restricted_percentage;

    restricted_counter++;


    $.post(ajaxurl, restricted_data, function(response) {

      restricted_percentage = response == 'complete' || response == 100 ? 100 : response;

      $('.edd-progress div').animate({
        width: restricted_percentage + '%',
      }, 50, function() {
        // Animation complete.
      });

      if (response == 100 || response == 'complete') {
        $('.scg-restricted-countries-import-form .notice-wrap .spinner').removeClass('is-active');

        $('.scg-restricted-countries-import-form input[type="submit"], .scg-restricted-countries-import-form select').remove();

        if (!$('.scg-restricted-countries-import-form .current-status').length) {
          $('.scg-restricted-countries-import-form').prepend('<div class="current-status hide"></div>');
        }

        $('.scg-restricted-countries-import-form .current-status')
          .removeClass('importing').removeClass('hide')
          .addClass('done')
          .text('Importavimas baigtas!');

        restricted_counter = 0;
      }

      if (response != 100) {
        setTimeout( restrictedPoll, 2000 );
      }
    });
  }

  // Scrape data variables.
  var scrape_data_percentage = 0;
  var scrape_counter = 0;

  // Progress bar
  var scrape_data = {
    'action': 'scg_scrape_data_import_progress',
    'progress': scrape_data_percentage,
    'counter': scrape_counter,
  };

  // Scrape data poll.
  var scrapeDataPoll = function(run) {

    scrape_data['counter'] = run > 0 ? run : scrape_counter;
    scrape_data['progress'] = scrape_data_percentage;

    scrape_counter++;


    $.post(ajaxurl, scrape_data, function(response) {

      scrape_data_percentage = response == 'complete' || response == 100 ? 100 : response;

      $('.edd-progress div').animate({
        width: scrape_data_percentage + '%',
      }, 50, function() {
        // Animation complete.
      });

      if (response == 100 || response == 'complete') {
        $('.scg-scrape-data-import-form .notice-wrap .spinner').removeClass('is-active');

        $('.scg-scrape-data-import-form input[type="submit"], .scg-scrape-data-import-form select').remove();

        if (!$('.scg-scrape-data-import-form .current-status').length) {
          $('.scg-scrape-data-import-form').prepend('<div class="current-status hide"></div>');
        }

        $('.scg-scrape-data-import-form .current-status')
          .removeClass('importing').removeClass('hide')
          .addClass('done')
          .text('Importavimas baigtas!');

        scrape_counter = 0;
      }

      if (response != 100) {
        setTimeout( scrapeDataPoll, 2000 );
      }
    });
  }

  // Detect when current status div is present, and then execute polling.
  if ($('.scg-trackers-import-form .current-status.importing').length) {

    $('.scg-trackers-import-form').append('<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>');

    // Execute poll - simulate not first request.
    trebPoll(2);
  }

  // Restricted - Detect when current status div is present, and then execute polling.
  if ($('.scg-restricted-countries-import-form .current-status.importing').length) {

    $('.scg-restricted-countries-import-form').append('<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>');

    // Execute poll - simulate not first request.
    restrictedPoll(2);
  }

  // Scrape data - Detect when current status div is present, and then execute polling.
  if ($('.scg-scrape-data-import-form .current-status.importing').length) {

    $('.scg-scrape-data-import-form').append('<div class="notice-wrap"><div class="edd-progress"><div></div></div><span class="spinner is-active"></span></div>');

    // Execute poll - simulate not first request.
    scrapeDataPoll(2);
  }

  $('.toggle-show-with-casinos')

  // Toggle show slots only with casinos.
  $('.toggle-show-with-casinos').click(function() {
    $("#post-body .wp-list-table").find("td.slot_casinos:empty").parent('tr').toggle();
    var row_length = $("#post-body .wp-list-table").find("tr:visible").length - 2;

    $('p span.total-count').text(row_length);

    return false;
  });

});
