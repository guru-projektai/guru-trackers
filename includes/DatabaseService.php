<?php

$autoload_path = __DIR__ . '/vendor/autoload.php';

if ( file_exists( $autoload_path ) ) {
    require_once( $autoload_path );
}

require_once plugin_dir_path(__FILE__) . '/BaseDatabaseService.php';

use SQLBuilder\ArgumentArray;
use SQLBuilder\Driver\MySQLDriver;
use SQLBuilder\Universal\Query\InsertQuery;
use SQLBuilder\Universal\Query\SelectQuery;
use SQLBuilder\Universal\Query\UpdateQuery;

class DatabaseService extends BaseDatabaseService
{
    /** @var SelectQuery */
    private $query;
    private $dialect;
    private $type;
    private $column;

    const UPDATE_PODS_FIELDS = 'UPDATE_PODS_FIELDS';
    const PODS_FIELDS_TRANSIENT = 'PODS_FIELDS_TRANSIENT';
    const GET_ITEM = 'GET_ITEM';
    const GET_DATA = 'GET_DATA';
    const REL_TABLE = 'wp_podsrel';

    public function __construct($query = null, $type = self::GET_DATA, $column = null)
    {
        parent::__construct();
        $this->setupFieldTransients();

        $this->dialect = new MySQLDriver;
        $this->query = $query;
        $this->type = $type;
        $this->column = $column;
    }

    public function setupFieldTransients($manualResave = false)
    {
        if (! $manualResave && get_option(self::UPDATE_PODS_FIELDS, false) === '1') return;

        $pods = pods_api()->load_pods(['fields' => true]);

        $data = [];
        foreach ($pods as $pod) {
            foreach($pod['fields'] as $field) {
                $data[$pod['name']][$field['name']] = $field['id'];

                if ($field['name'] == 'name') {
                    // Save pod id from every pod.
                    $data[$pod['name']]['pod_id'] = $field['pod_id'];
                }
            }
        }

        set_transient(self::PODS_FIELDS_TRANSIENT, $data);

        update_option(self::UPDATE_PODS_FIELDS, true);
    }

    public function build()
    {
        $sql = $this->query->toSql($this->dialect, new ArgumentArray);
        // var_dump($sql);

        switch ($this->type) {
            case self::GET_ITEM:
                $item = $this->getRow($sql);

                if ($item && $this->column) return $item->{$this->column};

                return $item;
            case self::GET_DATA:
                return $this->getResults($sql);
        }
    }

    public function setUp()
    {
        $casino_cache_table_sql = "
            CREATE TABLE IF NOT EXISTS {$this->wpdb->prefix}casino_cache (
                id         BIGINT       NOT NULL auto_increment,
                name       VARCHAR(255) NOT NULL,
                data       LONGTEXT,
                updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (id)
            ) {$this->wpdb->get_charset_collate()}
          ";

        $this->wpdb->query($casino_cache_table_sql);

        $casino_cache_rel_table_sql = "
            CREATE TABLE IF NOT EXISTS {$this->wpdb->prefix}casino_cache_rel (
                id              BIGINT NOT NULL auto_increment,
                casino_cache_id BIGINT NOT NULL,
                casino_id       BIGINT(20) UNSIGNED NOT NULL,
                country_id      BIGINT(20) UNSIGNED NOT NULL,
                game_type_id    BIGINT(20) UNSIGNED,
                extra_data      LONGTEXT,
                PRIMARY KEY (id),
                FOREIGN KEY (casino_cache_id) REFERENCES wp_casino_cache (id) ON DELETE CASCADE,
                FOREIGN KEY (casino_id) REFERENCES wp_pods_casino (id) ON DELETE CASCADE,
                FOREIGN KEY (country_id) REFERENCES wp_pods_country (id) ON DELETE CASCADE,
                FOREIGN KEY (game_type_id) REFERENCES wp_pods_casino_game (id) ON DELETE CASCADE
            ) {$this->wpdb->get_charset_collate()}
          ";

        $this->wpdb->query($casino_cache_rel_table_sql);
    }

    public static function getField($table_name, $field)
    {
        return get_transient(self::PODS_FIELDS_TRANSIENT)[$table_name][$field];
    }

    public function getRow($sql)
    {
        return $this->wpdb->get_row($sql);
    }

    public function getResults($sql)
    {
        return $this->wpdb->get_results($sql);
    }

    public function query($sql)
    {
        $this->wpdb->query($sql);
    }

    public function insert($table, $values, $relation = null, $return = false)
    {
        $query = new InsertQuery;

        if (! $this->isTransactionStarted()) $this->startTransaction();

        try {
            $query->insert($values)->into($table);

            $result = $this->wpdb->query($query->toSql($this->dialect, new ArgumentArray));

            if (! $result) throw new Exception('Something is wrong with database insertion');

            if ($relation) {
                $this->insert(
                    $relation['table'],
                    array_merge([
                        $relation['parent_column'] => $this->wpdb->insert_id
                    ], $relation['values'])
                );
            }

            $this->commit();

            if ($return) return $this->wpdb->insert_id;
        } catch (Exception $e) {
            var_dump($e);
            $this->rollback();
        }
    }

    public function createOrUpdate($table, $pairs, $values)
    {
        $query = new SelectQuery;

        $query->select(['id'])->from($table);

        foreach ($pairs as $column => $value) {
            if (! $value) {
                $query->where()->is($column, null);
                continue;
            }
            $query->where()->equal($column, $value);
        }

        $resultID = $this->wpdb->get_var($query->toSql($this->dialect, new ArgumentArray));

        if ($resultID) return $this->update($table, $resultID, $values);

        $this->insert($table, $values);
    }

    public function update($table, $id, $pairs)
    {
        $query = new UpdateQuery;

        $query->update($table)->set($pairs)->where()->equal('id', $id);

        $result = $this->wpdb->query($query->toSql($this->dialect, new ArgumentArray));

        if ($result) return $this->wpdb->insert_id;

        return null;
    }

    private function first($table, $column, $value)
    {
        $selectQuery = new SelectQuery;

        $selectQuery->select(['id'])->from($table)->where()->equal($column, $value);

        return $this->wpdb->get_var($selectQuery->toSql($this->dialect, new ArgumentArray));
    }

    private function create($table, $values)
    {
        $insertQuery = new InsertQuery;

        $insertQuery->insert($values)->into($table);

        $result = $this->wpdb->query($insertQuery->toSql($this->dialect, new ArgumentArray));

        if ($result) return $this->wpdb->insert_id;

        return null;
    }

    public function firstOrCreate($table, $column, $value, $values)
    {
        if ($existsId = $this->first($table, $column, $value)) return (int)$existsId;

        return $this->create($table, $values);
    }
}

add_action('pods_cache_flushed', 'scg_database_pods_fields_resave');

function scg_database_pods_fields_resave() {
    (new DatabaseService)->setupFieldTransients(true);
}

add_action('admin_init', 'scg_database_pods_field_admin_resave');

function scg_database_pods_field_admin_resave() {
    (new DatabaseService)->setupFieldTransients();
}
