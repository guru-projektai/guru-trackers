<?php
/**
 * Plugin Name: Guru Trackers
 * Description: Guru Trackers
 * Version: 1.0.0
 * Author: Guru
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use SQLBuilder\Universal\Query\SelectQuery;

$autoload_path = __DIR__ . '/vendor/autoload.php';

if ( file_exists( $autoload_path ) ) {
    require_once( $autoload_path );
}

require_once plugin_dir_path(__FILE__) . 'includes/DatabaseService.php';

define( 'Guru_Trackers_FILE', __FILE__ );
define( 'Guru_Trackers_PATH', plugin_dir_path( Guru_Trackers_FILE ) );
define( 'Guru_Trackers_URL', plugin_dir_url( Guru_Trackers_FILE ) );

// Set Google Drive API credentials file path
// by environment - one for dev and other for LIVE and stage.
if (WP_ENV == 'development') {
    putenv('GOOGLE_APPLICATION_CREDENTIALS=/app/guru-projektai-lt-a85a901ae1d7.json');
} else {
    putenv('GOOGLE_APPLICATION_CREDENTIALS=/www/wwwroot/casinoguru-lt/staging/current/guru-projektai-lt-a85a901ae1d7.json');
}

$urlparts = parse_url(home_url());
$domain = $urlparts['host'];
$is_sbingg = strstr($domain, 'bingoguide') !== FALSE || strstr($domain, 'sbingg') !== FALSE;

define( 'Guru_Trackers_IS_BINGO', $is_sbingg );

if ($is_sbingg) {
    if (WP_ENV == 'development') {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=/app/guru-projektai-20a6ed51ff7b.json');
    } else {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=/www/wwwroot/smartbingoguide/staging/current/guru-projektai-20a6ed51ff7b.json');
    }
}

register_deactivation_hook( Guru_Trackers_FILE, array( 'Guru_Trackers', 'Guru_Trackers_deactivate' ) );

final class Guru_Trackers
{

    /**
     * Plugin instance.
     *
     * @var Guru_Trackers
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Trackers
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }



    /**
     * SCG Casino Trackers constructor.
     */
    public function __construct()
    {
        register_activation_hook(Guru_Trackers_FILE, array($this , 'Guru_Trackers_activate'));

        add_action('admin_menu', array($this, 'admin_add_submenu'), 11);
        add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts'), 100);
        add_action('admin_init', array($this, 'register_admin_settings'));
        add_action('admin_bar_menu', array($this, 'scg_add_country_switcher_menu_item_bar'), 100);
    }

    public function scg_add_country_switcher_menu_item_bar($admin_bar) {
        global $wp;

        $custom_permalink = get_post_meta(get_the_ID(), 'custom_permalink', TRUE);

        $current_url = !empty($custom_permalink) ? $custom_permalink : str_replace(home_url(), '',home_url( $wp->request ));

        $usa_code = scg_get_country_code_from_name('United States');
        $uk_code = scg_get_country_code_from_name('United Kingdom');
        $india_code = scg_get_country_code_from_name('India');
        $gambia_code = scg_get_country_code_from_name('Gambia');
        $australia_code = scg_get_country_code_from_name('Australia');
        $china_code = scg_get_country_code_from_name('China');
        $netherlands_code = scg_get_country_code_from_name('Netherlands');
        $turkey_code = scg_get_country_code_from_name('Turkey');
        $italy_code = scg_get_country_code_from_name('Italy');
        $lithuania_code = scg_get_country_code_from_name('Lithuania');
        $sweden_code = scg_get_country_code_from_name('Sweden');
        $poland_code = scg_get_country_code_from_name('Poland');
        $norway_code = scg_get_country_code_from_name('Norway');
        $germany_code = scg_get_country_code_from_name('Germany');

        $kenya_code = scg_get_country_code_from_name('Kenya');
        $south_africa_code = scg_get_country_code_from_name('South Africa');
        $nigeria_code = scg_get_country_code_from_name('Nigeria');
        $indonesia_code = scg_get_country_code_from_name('Indonesia');
        $singapore_code = scg_get_country_code_from_name('Singapore');

        $current_url_uk = home_url(add_query_arg(array('switch-country' => $uk_code), $current_url));
        $current_url_usa = home_url(add_query_arg(array('switch-country' => $usa_code), $current_url));
        $current_url_india = home_url(add_query_arg(array('switch-country' => $india_code), $current_url));
        $current_url_se = home_url(add_query_arg(array('switch-country' => $sweden_code), $current_url));
        $current_url_pl = home_url(add_query_arg(array('switch-country' => $poland_code), $current_url));
        $current_url_no = home_url(add_query_arg(array('switch-country' => $norway_code), $current_url));

        $current_url_gm = home_url(add_query_arg(array('switch-country' => $gambia_code), $current_url));
        $current_url_au = home_url(add_query_arg(array('switch-country' => $australia_code), $current_url));
        $current_url_cn = home_url(add_query_arg(array('switch-country' => $china_code), $current_url));

        $current_url_de = home_url(add_query_arg(array('switch-country' => $germany_code), $current_url));
        $current_url_nl = home_url(add_query_arg(array('switch-country' => $netherlands_code), $current_url));
        $current_url_tr = home_url(add_query_arg(array('switch-country' => $turkey_code), $current_url));
        $current_url_lt = home_url(add_query_arg(array('switch-country' => $lithuania_code), $current_url));

        $current_url_ke = home_url(add_query_arg(array('switch-country' => $kenya_code), $current_url));
        $current_url_za = home_url(add_query_arg(array('switch-country' => $south_africa_code), $current_url));
        $current_url_ng = home_url(add_query_arg(array('switch-country' => $nigeria_code), $current_url));
        $current_url_id = home_url(add_query_arg(array('switch-country' => $indonesia_code), $current_url));
        $current_url_sg = home_url(add_query_arg(array('switch-country' => $singapore_code), $current_url));
        $current_url_it = home_url(add_query_arg(array('switch-country' => $italy_code), $current_url));

        $admin_bar->add_menu( array(
            'id'    => 'country-switcher',
            'title' => 'Change Country',
            'href'  => '#',
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-lithuania',
            'title'  => 'Lithuania (All Countries)',
            'href'   => $current_url_lt,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-de',
            'title'  => 'Germany (DE)',
            'href'   => $current_url_de,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-poland',
            'title'  => 'Poland (All Countries)',
            'href'   => $current_url_pl,
        ));


        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-norway',
            'title'  => 'Norway (NO)',
            'href'   => $current_url_no,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-uk',
            'title'  => 'United Kingdom (UK)',
            'href'   => $current_url_uk,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-usa',
            'title'  => 'United States (USA)',
            'href'   => $current_url_usa,
        ));

        $admin_bar->add_menu(array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-se',
            'title'  => 'Sweden (SE)',
            'href'   => $current_url_se,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-india',
            'title'  => 'India (IN)',
            'href'   => $current_url_india,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-gambia',
            'title'  => 'Gambia (Africa)',
            'href'   => $current_url_gm,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-kenya',
            'title'  => 'Kenya (Africa)',
            'href'   => $current_url_ke,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-nigeria',
            'title'  => 'Nigeria (Africa)',
            'href'   => $current_url_ng,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-south-africa',
            'title'  => 'South Africa (Africa)',
            'href'   => $current_url_za,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-australia',
            'title'  => 'Australia (AU)',
            'href'   => $current_url_au,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-indonesia',
            'title'  => 'Indonesia (Asia)',
            'href'   => $current_url_id,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-singapore',
            'title'  => 'Singapore (Asia)',
            'href'   => $current_url_sg,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-china',
            'title'  => 'China (Asia)',
            'href'   => $current_url_cn,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-netherlands',
            'title'  => 'Netherlands (NL)',
            'href'   => $current_url_nl,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-turkey',
            'title'  => 'Turkey (TR)',
            'href'   => $current_url_tr,
        ));

        $admin_bar->add_menu( array(
            'parent' => 'country-switcher',
            'id'     => 'country-switcher-italy',
            'title'  => 'Italy (IT)',
            'href'   => $current_url_it,
        ));

        return $admin_bar;
    }

    /**
     * Add Import progress option.
     */
    public function register_admin_settings()
    {
        // Import progress option
        add_option('scg-casino-trackers-import-progress', 'complete', '', 'yes');
        add_option('scg-scrape-data-import-progress', 'complete', '', 'yes');

        add_option('scg-casino-ratings-last-import-timestamp', 1, '', 'yes');

        $list_of_sheets = get_option('scg-casino-trackers-settings-list-sheets');

        if (empty($list_of_sheets)) {
            $sheets = scg_casino_trackers_get_sheets_list_from_file();

            if (!empty($sheets)) {
                add_option('scg-casino-trackers-settings-list-sheets', $sheets);
            }
        }

        // Add selected sheets option.
        add_option('scg-casino-trackers-settings-selected-sheets', '');

        // Restricted Countries Import progress option
        add_option('scg-restricted-countries-import-progress', 'complete', '', 'yes');
    }

    /**
     * Creates the admin submenu page
     * under "Kazino reitingas" menu.
     */
    public function admin_add_submenu()
    {
        // Add Casino Trackers sub-menu.
        $casino_trackers_import_page = add_submenu_page(
            'options-general.php',
            __('Casino tracker\'ių ir reitingų importavimas', 'scg-casino-trackers'),
            __('Casino tracker\'ių + reitingų importavimas', 'scg-casino-trackers'),
            'pods_edit_casino_info', 'scg-casino-trackers',
            array(&$this, 'casino_trackers_import_selection_page'));

        // Add Casino Trackers settings sub-menu.
        $casino_trackers_import_settings_page = add_submenu_page(
            'options-general.php',
            __('Casino tracker\'ių importavimo nustatymai', 'scg-casino-trackers'),
            __('Casino tracker\'ių importavimo nustatymai', 'scg-casino-trackers'),
            'manage_options', 'scg-casino-trackers-settings',
            array(&$this, 'casino_trackers_import_settings_page'));

        // Add Restricted Countries import sub-menu.
        $restricted_countries_import_page = add_submenu_page(
            'options-general.php',
            __('Restricted Countries importavimas', 'scg-casino-trackers'),
            __('Restricted Countries importavimas', 'scg-casino-trackers'),
            'pods_edit_casino_info', 'scg-restricted-countries',
            array(&$this, 'scg_restricted_countries_import_selection_page'));

        // Add Casino All Info import sub-menu.
        $casino_all_info_import_page = add_submenu_page(
            'options-general.php',
            __('Casino All Info importavimas', 'scg-casino-trackers'),
            __('Casino All Info importavimas', 'scg-casino-trackers'),
            'pods_edit_casino_info', 'scg-casino-all-infos',
            array(&$this, 'scg_casino_all_info_import_selection_page'));

        // Add TOP rating dropdown sub-menu.
        $top_rating_dropdowns_page = add_submenu_page(
            'casino-infos',
            __('TOP reitingų dropdown\'ai', 'scg-casino-trackers'),
            __('TOP reitingų dropdown\'ai', 'scg-casino-trackers'),
            'pods_edit_casino_info', 'scg-top-rating-dropdowns',
            array(&$this, 'scg_top_rating_dropdown_redirect_page'));
    }

    /**
     * TOP rating dropdown redirect.
     */
    function scg_top_rating_dropdown_redirect_page()
    {
        header('Location:' . admin_url('admin.php?page=pods-manage-dropdown_list'));
    }

    /**
     * Casino Trackers Import Settings page content.
     */
    public function casino_trackers_import_settings_page()
    {
        if (isset($_GET['refresh_sheets_list'])) {
            scg_casino_trackers_get_sheets_list_from_file();
        }

        $sheets_list = get_option('scg-casino-trackers-settings-list-sheets');
        $selected_sheets = array();

        if (!empty($_POST['selected_sheets'])) {
            foreach ($_POST['selected_sheets'] as $selected_sheet_id) {
                $selected_sheet_key = scg_casino_trackers_search_for_array_item($selected_sheet_id, $sheets_list);

                $selected_sheets[] = $sheets_list[$selected_sheet_key];
            }
        }

        if (!empty($selected_sheets)) {
            update_option('scg-casino-trackers-settings-selected-sheets', $selected_sheets);

            // Get sheets with countries list.
            scg_casino_trackers_get_sheets_with_countries_list();
        }

        // Logic to save no bonus sheets.
        $no_bonus_sheets = array();

        if (!empty($_POST['sheets_no_bonus'])) {
            foreach ($_POST['sheets_no_bonus'] as $selected_sheet_id) {
                $selected_sheet_key = scg_casino_trackers_search_for_array_item($selected_sheet_id, $sheets_list);

                $no_bonus_sheets[] = $sheets_list[$selected_sheet_key];
            }
        }

        if (!empty($no_bonus_sheets)) {
            update_option('scg-casino-trackers-settings-sheets-no-bonus', $no_bonus_sheets);
        }

        ?>
        <div class="scg-casino-trackers-settings">
            <div class="inside">
                <h3><span><?php _e('Importuoti Casino Tracker\'ius', 'scg-casino-trackers'); ?></span></h3>
                <p>Pasirinkite, kuriuos sheet'us leidžiame importuoti iš <a
                        href="https://docs.google.com/spreadsheets/d/1JacebZJk5_NtEW0eqfEgVMV8DHYRJVsJguqzkzO840g/edit?usp=sharing"
                        target="_blank">mūsų Google Sheet'o.</a></p>
                <p><a href="/wp/wp-admin/options-general.php?page=scg-casino-trackers-settings&refresh_sheets_list">Atnaujinti
                        sheet'ų sąrašą</a></p>
                <form class="scg-casino-trackers-settings" method="post"
                      action="<?php print admin_url('options-general.php?page=scg-casino-trackers-settings') ?>">
          <span>
            <h2>Importuojami Sheet'ai (šalys):</h2>
            <div class="checkboxes">
              <?php
              $selected_sheets = get_option('scg-casino-trackers-settings-selected-sheets');

              if (!empty($sheets_list)) {
                  foreach ($sheets_list as $sheet) {
                      $sheet_key = scg_casino_trackers_search_for_array_item($sheet['sheetId'], $selected_sheets);

                      if (isset($sheet_key)) {
                          $selected = ' checked="checked"';
                      } else {
                          $selected = '';
                      }

                      print
                          '<div class="checkbox">' .
                          '<label>' .
                          '<input name="selected_sheets[]" value="' . $sheet['sheetId'] . '"' . $selected . ' type="checkbox" /> ' . '<span>' . $sheet['title'] . '</span>' .
                          '</label>' .
                          '</div>';
                  }
              }
              ?>
            </div>
          </span>
          <span>
            <h2>Sheet'ai (šalys), kur nerodyti Bonusų:</h2>
            <div class="checkboxes">
              <?php
              $sheets_no_bonus = get_option('scg-casino-trackers-settings-sheets-no-bonus');

              if (!empty($sheets_list)) {
                  foreach ($sheets_list as $sheet) {
                      $sheet_key = scg_casino_trackers_search_for_array_item($sheet['sheetId'], $sheets_no_bonus);

                      if (isset($sheet_key)) {
                          $selected = ' checked="checked"';
                      } else {
                          $selected = '';
                      }

                      print
                          '<div class="checkbox">' .
                          '<label>' .
                          '<input name="sheets_no_bonus[]" value="' . $sheet['sheetId'] . '"' . $selected . ' type="checkbox" /> ' . '<span>' . $sheet['title'] . '</span>' .
                          '</label>' .
                          '</div>';
                  }
              }
              ?>
            </div>
            <?php submit_button(); ?>
          </span>
                </form>

            </div><!-- .inside -->
        </div><!-- .postbox -->

        <?php
    }

    /**
    * Get casino import actions pending count.
    */
    public static function casino_import_actions_count() {
        $query = new SelectQuery;
        $query
            ->from('wp_actionscheduler_actions', 't')
            ->select(['t.action_id', 't.hook', 't.schedule']);

        $query
            ->join('wp_actionscheduler_groups', 'action_group')
            ->on('action_group.group_id = t.group_id');

        $query->where()
            ->group()
            ->equal('t.status', 'pending')
            ->or()
            ->equal('t.status', 'in-progress')
            ->endGroup()
            ->and()
            ->notEqual('t.hook', 'sbg_auto_schedule_ratings_trackers_import_action')
            ->notEqual('t.hook', 'sbg_auto_schedule_restricted_countries_import_action');

        $query->where()
            ->group()
            ->equal('action_group.slug', 'scg_import_sheets_data')
            ->or()
            ->equal('action_group.slug', 'casinos_transient_cache');

        $results = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

        if (!empty($results)) {
            foreach ($results as $key => $entry) {
                $schedule = unserialize($entry->schedule);

                // Remove interval actions from count.
                if (get_class($schedule) == 'ActionScheduler_IntervalSchedule') {
                    unset($results[$key]);
                }
            }
        }

        $count = !empty($results) ? count($results) : 0;

        return $count;
    }


    /**
    * Get restricted import actions pending count.
    */
    public static function restricted_import_actions_count() 
    {
        $query = new SelectQuery;
        $query
            ->from('wp_actionscheduler_actions', 't')
            ->select(['t.action_id', 't.hook']);

        $query
            ->join('wp_actionscheduler_groups', 'action_group')
            ->on('action_group.group_id = t.group_id');

        $query->where()
            ->group()
            ->equal('t.status', 'pending')
            ->or()
            ->equal('t.status', 'in-progress')
            ->endGroup()
            ->and()
            ->notEqual('t.hook', 'sbg_auto_schedule_ratings_trackers_import_action')
            ->notEqual('t.hook', 'sbg_auto_schedule_restricted_countries_import_action');

        $query->where()
            ->group()
            ->equal('action_group.slug', 'restricted_countries')
            ->or()
            ->equal('action_group.slug', 'scg_import_sheets_data');

        $results = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

        $count = !empty($results) ? count($results) : 0;

        return $count;
    }


    /**
    * Get restricted import actions pending count.
    */
    public static function casino_all_info_import_actions_count() 
    {
        $query = new SelectQuery;
        $query
            ->from('wp_actionscheduler_actions', 't')
            ->select(['t.action_id', 't.hook']);

        $query
            ->join('wp_actionscheduler_groups', 'action_group')
            ->on('action_group.group_id = t.group_id');

        $query->where()
            ->group()
            ->equal('t.status', 'pending')
            ->or()
            ->equal('t.status', 'in-progress');

        $query->where()
            ->group()
            ->equal('action_group.slug', 'all_info')
            ->or()
            ->equal('action_group.slug', 'all_info_data');

        $results = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

        $count = !empty($results) ? count($results) : 0;

        return $count;
    }


    /**
     * Casino Trackers Import page content.
     */
    public function casino_trackers_import_selection_page()
    {
        // Get import logs for casino trackers.
        // $import_logs_table = new SCG_Import_Logs_List_Table();
        // $import_logs_table->set_import_log_type('casino_trackers');
        $import_pending_count = $this->casino_import_actions_count();

        $selected_sheets = get_option('scg-casino-trackers-settings-selected-sheets');
        // $casino_ratings_actions_count = scg_get_casino_ratings_actions_count();
        ?>
        <div class="postbox edd-export-payment-history casino-trackers-ratings-import">
            <div class="inside">
                <h3>
                    <span><?php _e('Importuoti Casino Tracker\'ius ir Reitingus pagal šalis', 'scg-casino-trackers'); ?></span>
                </h3>
                <p>Trackers + ratings by country <strong>auto import every ~24 hours.</strong> - at 3 AM every night.</p>
                <p><?php _e('Will import all casino trackers + ratings (including different game types, Crypto, Index page aff. links) from', 'scg-casino-trackers' ); ?> <a href="hhttps://docs.google.com/spreadsheets/d/1JacebZJk5_NtEW0eqfEgVMV8DHYRJVsJguqzkzO840g/edit?usp=sharing" target="_blank">Google Sheet</a>.</p>
                <p>If you want to import right now, just click button "<strong>Import now</strong>" and wait until import is finished.</p>

                <form id="scg-import-trackers" class="scg-import-form scg-trackers-import-form" method="post">
                    <?php wp_nonce_field('scg_trackers_import', 'scg_trackers_import'); ?>
                    <span>
            <?php if (!scg_casino_trackers_import_is_active()): ?>
                <select name="casino_trackers_import_select" id="casino-trackers-import-select">
                <option value="all_sheets">All Sheets (will be longest)</option>
                <?php if (!empty($selected_sheets)): ?>
                    <?php foreach ($selected_sheets as $sheet): ?>
                        <option value="<?php print $sheet['sheetId']; ?>"><?php print $sheet['title']; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
                </select>
                <input type="submit" value="<?php _e('Importuoti dabar', 'scg-casino-trackers'); ?>"
                       class="button-secondary"/>
            <?php else: ?>
                <div class="current-status importing">
                Vyksta importavimo procesas!
              </div>
            <?php endif; ?>
            <span class="spinner"></span>
            <div class="import-pending-count">
                <strong>Waiting in import queue count: <?php print $import_pending_count; ?></strong>
                <div>Need to wait when this count reaches zero. <br />Refresh page to see updated number.
                </div>
            </div>
          </span>
                </form>



            </div><!-- .inside -->
        </div><!-- .postbox -->

        <?php
    }

    /**
     * Casino Trackers Import page content.
     */
    public function scg_restricted_countries_import_selection_page()
    {
        // Get import logs for casino trackers.
        // $import_logs_table = new SCG_Import_Logs_List_Table();
        // $import_logs_table->set_import_log_type('restricted_countries');
        $import_pending_count = $this->restricted_import_actions_count();

        $casinos_list = scg_get_current_casinos_list();
        ?>
        <div class="postbox edd-export-payment-history">
            <div class="inside">
                <h3><span><?php _e('Importuoti Restricted Countries', 'scg-casino-trackers'); ?></span></h3>
                <p><?php _e('Importuosime Restricted Countries iš', 'scg-casino-trackers'); ?> <a
                        href="https://docs.google.com/spreadsheets/d/1Hy8wtjIUaZR8Br7nNlau2_UUQonaQhNbRWO3daVrWQ4/edit#gid=0"
                        target="_blank">mūsų Google Sheet'o.</a></p>
                <p>Restricted Countries <strong>automatiškai importuojasi kas 7 dienas</strong> pirmadieniais 04:00 val.
                    naktį.</p>
                <p>Jeigu nenorite laukti, tai spauskite mygtuką "<strong>Importuoti dabar</strong>" ir palaukite kol
                    pasibaigs importavimo procesas.</p>

                <form id="scg-import-restricted-countries" class="scg-import-form scg-restricted-countries-import-form"
                      method="post">
                    <?php wp_nonce_field('scg_restricted_countries_import', 'scg_restricted_countries_import'); ?>
                    <span>
            <?php if (!scg_restricted_countries_import_is_active()): ?>
                <select name="restricted_countries_import_select" id="restricted-countries-import-select">
                <option value="all">Visiems kazino</option>
                <?php if (!empty($casinos_list)): ?>
                    <?php foreach ($casinos_list as $casino): ?>
                        <option value="<?php print $casino['id']; ?>"><?php print $casino['name']; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
              </select>
                <input type="submit" value="<?php _e('Importuoti dabar', 'scg-casino-trackers'); ?>"
                       class="button-secondary"/>
            <?php else: ?>
                <div class="current-status importing">
                Vyksta importavimo procesas!
              </div>
            <?php endif; ?>
            <span class="spinner"></span>
            <div class="import-pending-count">
                <strong>Waiting in import queue count: <?php print $import_pending_count; ?></strong>
                <div>Need to wait when this count reaches zero. <br />Refresh page to see updated number.
                </div>
            </div>
          </span>
                </form>

            </div><!-- .inside -->
        </div><!-- .postbox -->

        <?php
    }


    /**
     * Casino All Info Import page content.
     */
    public function scg_casino_all_info_import_selection_page()
    {
        // Get import logs for casino trackers.
        // $import_logs_table = new SCG_Import_Logs_List_Table();
        // $import_logs_table->set_import_log_type('restricted_countries');
        $import_pending_count = $this->casino_all_info_import_actions_count();

        $casinos_list = scg_get_current_casinos_list();
        ?>
        <div class="postbox edd-export-payment-history">
            <div class="inside">
                <h3><span><?php _e('Importuoti Casino All Info', 'scg-casino-trackers'); ?></span></h3>
                <p><?php _e('Importuosime Casino All Info iš', 'scg-casino-trackers'); ?> <a
                        href="https://docs.google.com/spreadsheets/d/1Hy8wtjIUaZR8Br7nNlau2_UUQonaQhNbRWO3daVrWQ4/edit#gid=1881160158"
                        target="_blank">mūsų Google Sheet'o.</a></p>
                <p style="text-decoration: line-through;">Casino All Info <strong>automatiškai importuojasi kas 7 dienas</strong> pirmadieniais 04:00 val.
                    naktį.</p>
                <p>Jeigu nenorite laukti, tai spauskite mygtuką "<strong>Importuoti dabar</strong>" ir palaukite kol
                    pasibaigs importavimo procesas.</p>

            <form id="scg-import-casino-all-info" class="scg-import-form scg-import-casino-all-info-import-form"
                  method="post">
                <?php wp_nonce_field('scg_casino_all_info_import', 'scg_casino_all_info_import'); ?>
                <span>
            <select name="casino_all_info_import_select" id="casino-all-info-import-select">
            <option value="all">Visiems kazino</option>
            <?php if (!empty($casinos_list)): ?>
                <?php foreach ($casinos_list as $casino): ?>
                    <option value="<?php print $casino['id']; ?>"><?php print $casino['name']; ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
            </select>
            <select name="casino_all_info_import_logos" id="casino-all-info-import-logos">
                <option value="no">Neimportuoti logos (Payment methods, Cryptos, Game Providers)</option>
                <option value="yes">Taip, importuoti visus logos iš dokumento (užtruks ~15-30 min.)</option>
            </select>
            <br /><br />
            <input type="submit" value="<?php _e('Importuoti dabar', 'scg-casino-trackers'); ?>" class="button-secondary"/>
            <div class="import-pending-count">
                <strong>Waiting in import queue count: <?php print $import_pending_count; ?></strong>
                <div>Need to wait when this count reaches zero. <br />Refresh page to see updated number.
                </div>
            </div>
          </span>
                </form>

            </div><!-- .inside -->
        </div><!-- .postbox -->

        <?php
    }



    public function load_admin_scripts()
    {
        $js_dir_url = plugin_dir_url(__FILE__) . 'assets/js/';
        $css_dir_url = plugin_dir_url(__FILE__) . 'assets/css/';
        $js_dir_path = plugin_dir_path(__FILE__) . 'assets/js/';
        $css_dir_path = plugin_dir_path(__FILE__) . 'assets/css/';

        wp_enqueue_script('jquery-form');

        wp_register_script('scg-import-admin-scripts', $js_dir_url . 'scg-import-admin-scripts.js', array('jquery', 'jquery-form'), filemtime($js_dir_path . 'scg-import-admin-scripts.js'), false);
        wp_enqueue_script('scg-import-admin-scripts');

        wp_localize_script('scg-import-admin-scripts', 'SCGajax', array(
            'security' => wp_create_nonce('scg_trackers_import'),
            'restricted_countries_security' => wp_create_nonce('scg_restricted_countries_import'),
            'casino_all_info_security' => wp_create_nonce('scg_casino_all_info_import'),
        ));

        wp_register_style('scg-import-admin-style', $css_dir_url . 'scg-import-admin-style.css', array(), filemtime($css_dir_path . 'scg-import-admin-style.css'));
        wp_enqueue_style('scg-import-admin-style');
    }










    public function migrate()
    {
        $migrations_folder = WP_CONTENT_DIR . '/migrations';
        $src_directory = new RecursiveDirectoryIterator($migrations_folder);

        $filtered_file_list = new GuruFilenameFilter($src_directory, '/(?:php)$/i');
        $filtered_file_list = new GuruFilenameFilter($filtered_file_list, '/^(?!.*(Complex|Exception)\.php).*$/i');

        foreach (new RecursiveIteratorIterator($filtered_file_list) as $file) {
            if ($file->isFile()) {
                include_once $file;

                $option = $guru_migration_date_updated . '_' . pathinfo($file->getFileName(), PATHINFO_FILENAME);
                $old_options = get_option('Guru_Trackers', []);
                if ($old_options[$option]) continue;

                $guru_migration_call();
                $this->Guru_Trackers_update_option($option);
            }
        }
    }

    public function Guru_Trackers_update_option($key)
    {
        $old_options = get_option('Guru_Trackers', []);

        $old_options[$key] = true;

        update_option('Guru_Trackers', $old_options);
    }

    /**
     * Run when deactivate plugin.
     */
    public static function Guru_Trackers_deactivate()
    {
        require_once Guru_Trackers_PATH . 'includes/guru-trackers-deactivator.php';
        Guru_Trackers_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     */
    public function Guru_Trackers_activate()
    {
        require_once Guru_Trackers_PATH . 'includes/guru-trackers-activator.php';
        Guru_Trackers_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    // public function Guru_Trackers_includes()
    // {
    //     require_once Guru_Trackers_PATH . 'includes/guru-trackers-functions.php';
    // }
}

function Guru_Trackers()
{
    return Guru_Trackers::get_instance();
}

$GLOBALS['Guru_Trackers'] = Guru_Trackers();

/**
 * Get casino tracker Google Sheet file ID.
 */
function guru_get_tracker_sheet_file_id()
{
    $sheetFileId = '1UPA3cFk1sWz2lsOHg6dyOkFqIokCed5xMPsP9fG7y4c';

    $urlparts = parse_url(home_url());
    $domain = $urlparts['host'];
    $is_sbingg = strstr($domain, 'bingoguide') !== FALSE || strstr($domain, 'sbingg') !== FALSE;

    // Use other sheet file ID when its bingoguide.
    if ($is_sbingg) {
        $sheetFileId = '1_SJdhaQzYulStCWA2ZzcJfWbVCwAfsIimVGQ3VhG85M';
    }

    return $sheetFileId;
}

function testing_now_ok() {
    $file_id = guru_get_tracker_sheet_file_id();
    die();
}

// add_action('admin_init', 'testing_now_ok');

/**
 * Custom XlSX folder to upload downloaded file.
 */
function guru_trackers_get_xlsx_file_upload_dir($arr)
{
    $folder = '/xlsx'; // No trailing slash at the end.

    $arr['path'] = str_replace($arr['subdir'], '', $arr['path']);
    $arr['url'] = str_replace($arr['subdir'], '', $arr['url']);

    $arr['path'] .= $folder;
    $arr['url'] .= $folder;
    $arr['subdir'] = $folder;

    return $arr;
}

/**
 * Get latest trackers sheet file from Google Drive.
 */
function guru_trackers_and_ratings_get_google_drive_file()
{
    $filename = 'guru-trackers-ratings.xlsx';
    $upload_dir = wp_upload_dir();
    $xlsx_file_dir = $upload_dir['basedir'] . '/' . 'xlsx';

    // Delete file firstly if it exists.
    wp_delete_file($xlsx_file_dir . '/' . $filename);

    try {
        // Tell the Google client to use your service account credentials.
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();

        // Set the scopes required for the API.
        $client->addScope(Google_Service_Drive::DRIVE);

        // Make Drive Service.
        $service = new Google_Service_Drive($client);

        $fileId = guru_get_tracker_sheet_file_id();
        $response = $service->files->export($fileId, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', array(
            'alt' => 'media'));

        $content = $response->getBody()->getContents();
        $optParams = array('fields' => 'modifiedTime');
        $filedata = $service->files->get($fileId, $optParams);
        $modifiedTime = $filedata->getModifiedTime();

        add_filter('upload_dir', 'guru_trackers_get_xlsx_file_upload_dir');

        $ret = wp_upload_bits($filename, null, $content);
        $uploaded_filepath = $ret['file'];

        remove_filter('upload_dir', 'guru_trackers_get_xlsx_file_upload_dir');

        return $client;
    } catch (Exception $e) {
        error_log('An error occurred downloading Google Drive Sheets File: ' . $e->getMessage());
    }
}

// add_action('admin_init', 'guru_trackers_and_ratings_get_google_drive_file');

/**
 * Get the absolute path to the XLSX uploads sub-directory,
 * with a trailing slash.
 */
function guru_get_xlsx_uploads_directory_path()
{
    $upload_dir = wp_upload_dir();

    return trailingslashit($upload_dir['basedir']) . 'xlsx/';
}


/**
 * Get casino game types list.
 */
function guru_get_casino_game_types_list()
{
    $query = new SelectQuery;

    $query
        ->select(['t.*'])
        ->from('wp_pods_casino_game', 't');

    $query
        ->orderBy('t.name', 'ASC');

    $data = (new DatabaseService($query))->build();

    $game_types = [];

    if ($data) {
        foreach ($data as $item) {
            $game_types[] = array(
                'id' => (int)$item->id,
                'name' => $item->name,
            );
        }
    }

    return $game_types;
}


/**
 * Get countries list.
 */
function guru_get_countries_list()
{
    $query = new SelectQuery;

    $query
        ->select(['t.*'])
        ->from('wp_pods_country', 't');

    $query
        ->orderBy('t.name', 'ASC');

    $data = (new DatabaseService($query))->build();

    $countries = [];

    if ($data) {
        foreach ($data as $item) {
            $countries[] = array(
                'id' => (int)$item->id,
                'name' => $item->name,
            );
        }
    }

    return $countries;
}

/**
 * Find if array value exists.
 */
function guru_find_array_key_value($array, $key, $val)
{
    foreach ($array as $item) {
        if (isset($item[$key]) && $item[$key] == $val) {
            return TRUE;
        }
    }

    return FALSE;
}


/**
 * Parse guru trackers sheets file
 * and prepare data for import.
 */
function guru_trackers_parse_file($sheet_name)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = guru_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'guru-trackers-ratings.xlsx';

    try {
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly([$sheet_name]);
        $reader->setReadEmptyCells(false);
        $spreadsheet = $reader->load($file_path);
        //$spreadsheet = new Spreadsheet();

        $worksheetData = $reader->listWorksheetInfo($file_path);

        // d($file_path);
        // d($sheet_name);
        // d($worksheetData);
        // die();

        foreach ($worksheetData as $worksheet) {
            if ($worksheet['worksheetName'] == $sheet_name) {
                $sheet_lastColumnIndex = $worksheet['lastColumnIndex'];
                $sheet_lastColumnLetter = $worksheet['lastColumnLetter'];

                $sheet_lastRange = $sheet_lastColumnLetter . $sheet_lastColumnIndex;

                // d($worksheet);
                // die();

            }
        }

        $active_sheet = $spreadsheet->getActiveSheet();
        $maxCell = $active_sheet->getHighestRowAndColumn();

        $highestRow = 100;
        $highestCol = $active_sheet->getHighestColumn();
        $highestCol = !empty($maxCell['column']) ? $maxCell['column'] : $highestCol;

        $casino_trackers = $active_sheet->rangeToArray(
            'A1:' . $highestCol . $highestRow
        );

        // $casino_trackers = array_filter(array_map('array_filter', $casino_trackers));

        // d($casino_trackers);

        // Get last column that has countries
        // starting from second row.
        $casino_countries_range_array = $active_sheet->rangeToArray(
            $highestCol . '2:' . $highestCol . '100'
        );

        // var_dump($highestCol . '2:' . $highestCol . $highestRow);
        // d($casino_countries_range_array);

        // Filter array to only non-empty items.
        $casino_countries_array = array_shift(array_filter(array_map('array_filter', $casino_countries_range_array)));

        // Get Countries array.
        // $casino_country_value = reset($casino_countries_array);
        $casino_country_value = !empty($casino_countries_array) ? $casino_countries_array : FALSE;

        // d($highestCol . '2:' . $highestCol . $highestRow);

        // d($casino_countries_array);
        // d($casino_country_value);

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    $count = 0;
    $casinos = array();

    // Dynamic list of game types.
    $game_types_list = guru_get_casino_game_types_list();
    $game_types_data = array();

    // d($casino_trackers);
    // die();

    foreach ($casino_trackers as $key => $casino_tracker) {

        // d($key);

        if ($key == 0) {
            foreach ($casino_tracker as $row_key => $entry) {
                if ($entry == 'Pavadinimas' || $entry == 'Name') {
                    $name_key = $row_key;
                }

                if ($entry == 'Top' || $entry == 'TOP') {
                    $top_link_key = $row_key;
                }

                if ($entry == 'TOP reitinge - mygtuko tekstas' || $entry == 'TOP rating - button text') {
                    $top_button_text_key = $row_key;
                }

                if ($entry == 'TOP - regulation tekstas' || $entry == 'TOP rating - regulation text') {
                    $top_button_regulation_key = $row_key;
                }

                if ($entry == 'Review') {
                    $review_link_key = $row_key;
                }

                // Bonus text.
                if ($entry == 'Review - bonuso tekstas' || $entry == 'Review - bonus text') {
                    $review_bonus_text = $row_key;
                }

                // Bonus button text.
                if ($entry == 'Review - bonus mygtuko tekstas' || $entry == 'Review - bonus button text') {
                    $review_button_text_key = $row_key;
                }

                // Bonus regulation text.
                if ($entry == 'Review - regulation tekstas' || $entry == 'Review - regulation text') {
                    $review_button_regulation_key = $row_key;
                }

                if ($entry == 'Shortcode\'o - mygtuko tekstas' || $entry == 'Shortcode - button text') {
                    $shortcode_button_text_key = $row_key;
                }

                if ($entry == 'Shortcode\'o - regulation tekstas' || $entry == 'Shortcode - regulation text') {
                    $shortcode_button_regulation_text_key = $row_key;
                }

                if (strpos($entry, 'Bonus shortcode\'o tekstas') !== false || strpos($entry, 'Bonus shortcode text') !== false) {
                    $bonus_text_key = $row_key;
                }

                if ($entry == 'Bonus suma' || $entry == 'Bonus sum') {
                    $bonus_sum_key = $row_key;
                }

                if ($entry == 'Bonus currency') {
                    $bonus_currency_key = $row_key;
                }

                if ($entry == 'Country 1' || $entry == 'Countries') {
                    $country_1_key = $row_key;
                }

                if ($entry == 'Įjungta' || $entry == 'Enabled') {
                    $enabled_global_key = $row_key;
                }

                // Roulette
                // Įjungta Roulette

                if ($entry == 'Roulette') {
                    $roulette_key = $row_key;
                }

                if ($entry == 'Įjungta Roulette' || $entry == 'Enabled Roulette') {
                    $roulette_enabled_key = $row_key;
                }

                // BlackJack
                // Įjungta BlackJack

                if ($entry == 'BlackJack') {
                    $blackjack_key = $row_key;
                }

                if ($entry == 'Įjungta BlackJack' || $entry == 'Enabled BlackJack') {
                    $blackjack_enabled_key = $row_key;
                }

                // Baccarat
                // Įjungta Baccarat

                if ($entry == 'Baccarat') {
                    $baccarat_key = $row_key;
                }

                if ($entry == 'Įjungta Baccarat' || $entry == 'Enabled Baccarat') {
                    $baccarat_enabled_key = $row_key;
                }

                // Slots
                // Įjungta Slots

                if ($entry == 'Slots') {
                    $slots_key = $row_key;
                }

                if ($entry == 'Įjungta Slots' || $entry == 'Enabled Slots') {
                    $slots_enabled_key = $row_key;
                }

                // Card Games
                // Įjungta Card Games

                if ($entry == 'Card Games') {
                    $card_games_key = $row_key;
                }

                if ($entry == 'Įjungta Card Games' || $entry == 'Enabled Card Games') {
                    $card_games_enabled_key = $row_key;
                }

                // Video Poker
                // Įjungta Video Poker

                if ($entry == 'Video Poker') {
                    $video_poker_key = $row_key;
                }

                if ($entry == 'Įjungta Video Poker' || $entry == 'Enabled Video Poker') {
                    $video_poker_enabled_key = $row_key;
                }

                // Live Casino
                // Įjungta Live Casino

                if ($entry == 'Live Casino') {
                    $live_casino_key = $row_key;
                }

                if ($entry == 'Įjungta Live Casino' || $entry == 'Enabled Live Casino') {
                    $live_casino_enabled_key = $row_key;
                }

                // Other Games
                // Įjungta Other Games

                if ($entry == 'Other Games') {
                    $other_games_key = $row_key;
                }

                if ($entry == 'Įjungta Other Games' || $entry == 'Enabled Other Games') {
                    $other_games_enabled_key = $row_key;
                }

                // Bingo
                // Įjungta Bingo

                if ($entry == 'Bingo') {
                    $bingo_key = $row_key;
                }

                if ($entry == 'Įjungta Bingo' || $entry == 'Enabled Bingo') {
                    $bingo_enabled_key = $row_key;
                }

                // Keno
                // Įjungta Keno

                if ($entry == 'Keno') {
                    $keno_key = $row_key;
                }

                if ($entry == 'Įjungta Keno' || $entry == 'Enabled Keno') {
                    $keno_enabled_key = $row_key;
                }

                // Scratch Cards
                // Įjungta Scratch Cards

                if ($entry == 'Scratch Cards') {
                    $scratch_cards_key = $row_key;
                }

                if ($entry == 'Įjungta Scratch Cards' || $entry == 'Enabled Scratch Cards') {
                    $scratch_cards_enabled_key = $row_key;
                }

                // Index Page
                // Įjungta Index Page

                if ($entry == 'Index Page') {
                    $index_page_key = $row_key;
                }

                if ($entry == 'Įjungta Index Page' || $entry == 'Enabled Index Page') {
                    $index_page_enabled_key = $row_key;
                }

                // PayPal casinos
                // Įjungta PayPal casinos

                if ($entry == 'Payments') {
                    $paypal_casinos_key = $row_key;
                }

                if ($entry == 'Įjungta Payments' || $entry == 'Enabled Payments') {
                    $paypal_casinos_enabled_key = $row_key;
                }

                // Dynamic game types columns search in sheet.
                if (!empty($game_types_list)) {
                    foreach ($game_types_list as $game_type) {
                        if ($entry == $game_type['name']) {
                            $key_enabled = $row_key + 1;
                            $game_types_data[] = array(
                                'key' => $row_key,
                                'key_enabled' => $key_enabled,
                                'name' => $entry,
                            );
                        }
                    }
                }

                // Crypto
                // Įjungta Crypto
                // Crypto - mygtuko tekstas
                // Crypto - regulation tekstas

                if ($entry == 'Crypto') {
                    $crypto_key = $row_key;
                }

                if ($entry == 'Įjungta Crypto' || $entry == 'Enabled Crypto') {
                    $crypto_enabled_key = $row_key;
                }

                if ($entry == 'Crypto - mygtuko tekstas' || $entry == 'Crypto - button text') {
                    $crypto_button_text_key = $row_key;
                }

                if ($entry == 'Crypto - regulation tekstas' || $entry == 'Crypto - regulation text') {
                    $crypto_regulation_text_key = $row_key;
                }
            }
        }

        if ($key > 0) {
            $casino_name = trim($casino_tracker[$name_key]);
            $casino_id = (int) scg_get_casino_id_from_name($casino_name);
            $casino_name = trim($casino_tracker[$name_key]);
            $casino_top_link = trim($casino_tracker[$top_link_key]);
            $casino_top_button_text = trim($casino_tracker[$top_button_text_key]);
            $casino_top_button_regulation_text = trim($casino_tracker[$top_button_regulation_key]);
            $casino_review_link = trim($casino_tracker[$review_link_key]);

            // Bonus text.
            $casino_review_bonus_text = trim($casino_tracker[$review_bonus_text]);

            // Bonus button text.
            $casino_review_bonus_button_text = trim($casino_tracker[$review_button_text_key]);

            // Bonus regulation text.
            $casino_review_button_regulation_text = trim($casino_tracker[$review_button_regulation_key]);

            $casino_shortcode_button_text = trim($casino_tracker[$shortcode_button_text_key]);
            $casino_shortcode_button_regulation_text = trim($casino_tracker[$shortcode_button_regulation_text_key]);
            $casino_bonus_enabled_globally = trim($casino_tracker[$enabled_global_key]);
            $casino_bonus_enabled = $casino_bonus_enabled_globally == 'Taip' || $casino_bonus_enabled_globally == 'Yes' ? 1 : 0;

            $casino_bonus_text = trim(str_replace(',', ' ', $casino_tracker[$bonus_text_key]));
            $casino_bonus_sum = !empty($casino_tracker[$bonus_sum_key]) ? trim($casino_tracker[$bonus_sum_key]) : FALSE;
            $casino_bonus_currency = !empty($casino_tracker[$bonus_currency_key]) ? trim($casino_tracker[$bonus_currency_key]) : FALSE;

            $casino_country_1 = $casino_country_value;

            // Roulette
            // Įjungta Roulette

            $casino_roulette_link = trim($casino_tracker[$roulette_key]);
            $casino_roulette_enabled = trim($casino_tracker[$roulette_enabled_key]);

            // BlackJack
            // Įjungta BlackJack

            $casino_blackjack_link = trim($casino_tracker[$blackjack_key]);
            $casino_blackjack_enabled = trim($casino_tracker[$blackjack_enabled_key]);

            // Baccarat
            // Įjungta Baccarat

            $casino_baccarat_link = trim($casino_tracker[$baccarat_key]);
            $casino_baccarat_enabled = trim($casino_tracker[$baccarat_enabled_key]);

            // Slots
            // Įjungta Slots

            $casino_slots_link = trim($casino_tracker[$slots_key]);
            $casino_slots_enabled = trim($casino_tracker[$slots_enabled_key]);

            // Card Games
            // Įjungta Card Games

            $casino_card_games_link = trim($casino_tracker[$card_games_key]);
            $casino_card_games_enabled = trim($casino_tracker[$card_games_enabled_key]);

            // Video Poker
            // Įjungta Video Poker

            $casino_video_poker_link = trim($casino_tracker[$video_poker_key]);
            $casino_video_poker_enabled = trim($casino_tracker[$video_poker_enabled_key]);

            // Live Casino
            // Įjungta Live Casino

            $casino_live_casino_link = trim($casino_tracker[$live_casino_key]);
            $casino_live_casino_enabled = trim($casino_tracker[$live_casino_enabled_key]);

            // Other Games
            // Įjungta Other Games

            $casino_other_games_link = trim($casino_tracker[$other_games_key]);
            $casino_other_games_enabled = trim($casino_tracker[$other_games_enabled_key]);

            // Bingo
            // Įjungta Bingo

            $casino_bingo_link = trim($casino_tracker[$bingo_key]);
            $casino_bingo_enabled = trim($casino_tracker[$bingo_enabled_key]);

            // Keno
            // Įjungta Keno

            $casino_keno_link = trim($casino_tracker[$keno_key]);
            $casino_keno_enabled = trim($casino_tracker[$keno_enabled_key]);

            // Scratch Cards
            // Įjungta Scratch Cards

            $casino_scratch_cards_link = trim($casino_tracker[$scratch_cards_key]);
            $casino_scratch_cards_enabled = trim($casino_tracker[$scratch_cards_enabled_key]);

            // Index Page
            // Įjungta Index Page

            // var_dump($index_page_enabled_key);
            // var_dump($casino_tracker[$index_page_enabled_key]);

            // var_dump($casino_tracker[$index_page_key]);
            // die();

            $casino_index_page_link = trim($casino_tracker[$index_page_key]);
            $casino_index_page_enabled = trim($casino_tracker[$index_page_enabled_key]);
            $casino_index_page_enabled = $casino_index_page_enabled == 'Taip' || $casino_index_page_enabled == 'Yes';

            // PayPal casinos
            // Įjungta PayPal casinos

            $paypal_casinos_link = trim($casino_tracker[$paypal_casinos_key]);
            $paypal_casinos_enabled = trim($casino_tracker[$paypal_casinos_enabled_key]);

            // Crypto
            // Įjungta Crypto
            // Crypto - mygtuko tekstas
            // Crypto - regulation tekstas

            $casino_crypto_link = trim($casino_tracker[$crypto_key]);
            $casino_crypto_enabled = trim($casino_tracker[$crypto_enabled_key]);
            $casino_crypto_button_text = trim($casino_tracker[$crypto_button_text_key]);
            $casino_crypto_regulation_text = trim($casino_tracker[$crypto_regulation_text_key]);

            $casino_games_links = array();

            if (!empty($casino_index_page_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Index Page',
                    'aff_link' => $casino_index_page_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_index_page_enabled,
                );
            }

            if (!empty($casino_roulette_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Roulette',
                    'aff_link' => $casino_roulette_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_blackjack_enabled,
                );
            }

            if (!empty($casino_blackjack_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'BlackJack',
                    'aff_link' => $casino_blackjack_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_blackjack_enabled,
                );
            }

            if (!empty($casino_baccarat_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Baccarat',
                    'aff_link' => $casino_baccarat_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_baccarat_enabled,
                );
            }

            if (!empty($casino_slots_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Slots',
                    'aff_link' => $casino_slots_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_slots_enabled,
                );
            }

            if (!empty($casino_card_games_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Card Games',
                    'aff_link' => $casino_card_games_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_card_games_enabled,
                );
            }

            if (!empty($casino_video_poker_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Video Poker',
                    'aff_link' => $casino_video_poker_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_video_poker_enabled,
                );
            }

            if (!empty($casino_live_casino_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Live Casino',
                    'aff_link' => $casino_live_casino_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_live_casino_enabled,
                );
            }

            if (!empty($casino_other_games_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Other Games',
                    'aff_link' => $casino_other_games_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_other_games_enabled,
                );
            }

            if (!empty($casino_bingo_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Bingo',
                    'aff_link' => $casino_bingo_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_bingo_enabled,
                );
            }

            if (!empty($casino_keno_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Keno',
                    'aff_link' => $casino_keno_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_keno_enabled,
                );
            }

            if (!empty($casino_scratch_cards_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Scratch Cards',
                    'aff_link' => $casino_scratch_cards_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $casino_scratch_cards_enabled,
                );
            }

            if (!empty($paypal_casinos_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Payments',
                    'aff_link' => $paypal_casinos_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_shortcode_button_text,
                    'regulation_text' => $casino_shortcode_button_regulation_text,
                    'enabled' => $paypal_casinos_enabled,
                );
            }

            if (!empty($casino_crypto_link)) {
                $casino_games_links[] = array(
                    'casino_id' => $casino_id,
                    'game_type' => 'Crypto',
                    'aff_link' => $casino_crypto_link,
                    'country_name' => $casino_country_1,
                    'button_text' => $casino_crypto_button_text,
                    'regulation_text' => $casino_crypto_regulation_text,
                    'enabled' => $casino_crypto_enabled,
                );
            }

            // Add all casino game types
            // links be imported dynamically.
            if (!empty($game_types_data)) {
                foreach ($game_types_data as $game_type_data_key => $game_type_data) {
                    if (isset($casino_tracker[$game_type_data['key']])) {
                        $game_type_name = $game_type_data['name'];
                        $game_type_aff_link = trim($casino_tracker[$game_type_data['key']]);
                        $game_type_enabled = trim($casino_tracker[$game_type_data['key_enabled']]);
                        $game_type_array_exists = scg_find_array_key_value($casino_games_links, 'game_type', $game_type_name);

                        if (!empty($game_type_aff_link) && !$game_type_array_exists) {
                            $casino_games_links[] = array(
                                'casino_id' => $casino_id,
                                'game_type' => $game_type_name,
                                'aff_link' => $game_type_aff_link,
                                'country_name' => $casino_country_1,
                                'button_text' => $casino_shortcode_button_text,
                                'regulation_text' => $casino_shortcode_button_regulation_text,
                                'enabled' => $game_type_enabled,
                            );
                        }
                    }
                }
            }

            if (!empty($casino_id)) {
                $casinos[] = array(
                    'casino_id' => $casino_id,
                    'casino_sheet_name' => $sheet_name,
                    'casino_name' => $casino_name,
                    'casino_top_link' => $casino_top_link,
                    'casino_top_button_text' => $casino_top_button_text,
                    'casino_top_button_regulation_text' => $casino_top_button_regulation_text,
                    'casino_review_link' => $casino_review_link,
                    'casino_review_bonus_text' => $casino_review_bonus_text,
                    'casino_review_bonus_button_text' => $casino_review_bonus_button_text,
                    'casino_review_button_regulation_text' => $casino_review_button_regulation_text,
                    'casino_shortcode_button_text' => $casino_shortcode_button_text,
                    'casino_shortcode_button_regulation_text' => $casino_shortcode_button_regulation_text,
                    'casino_bonus_text' => $casino_bonus_text,
                    'casino_bonus_sum' => $casino_bonus_sum,
                    'casino_bonus_currency' => $casino_bonus_currency,
                    'casino_country_1' => $casino_country_1,
                    'casino_country_ids' => $casino_country_value,
                    'casino_bonus_enabled_globally' => $casino_bonus_enabled,

                    // Array of games affiliate links.
                    'casino_games_links' => $casino_games_links,
                );
            }
        }

        $count++;
    }

    // d($casinos);

    return $casinos;
}


/**
 * Parse casino ratings sheets file
 * and prepare data for import.
 */
function guru_ratings_parse_file($sheet_name)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = guru_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'guru-trackers-ratings.xlsx';

    $sheet_name = $sheet_name . '-Rate';

    // var_dump($sheet_name);

    // d($file_path);
    // die();

    try {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly([$sheet_name]);
        $reader->setReadEmptyCells(false);
        $spreadsheet = $reader->load($file_path);

        $worksheetData = $reader->listWorksheetInfo($file_path);

        // d($spreadsheet);
        // d($worksheetData);

        // die();

        foreach ($worksheetData as $worksheet) {
            if ($worksheet['worksheetName'] == $sheet_name) {
                $sheet_lastColumnIndex = $worksheet['lastColumnIndex'];
                $sheet_lastColumnLetter = $worksheet['lastColumnLetter'];

                $sheet_lastRange = $sheet_lastColumnLetter . $sheet_lastColumnIndex;
            }
        }

        $active_sheet = $spreadsheet->getActiveSheet();
        $maxCell = $active_sheet->getHighestRowAndColumn();

        $highestRow = $active_sheet->getHighestRow();
        $highestCol = $active_sheet->getHighestColumn();

        $casino_ratings = $active_sheet->rangeToArray(
            'A1:' . $highestCol . $highestRow
        );

        // Get last column that has countries
        // starting from second row.
        $casino_countries_range_array = $active_sheet->rangeToArray(
            $highestCol . '2:' . $highestCol . $highestRow
        );

        // Filter array to only non-empty items.
        $casino_countries_array = array_filter(call_user_func_array('array_merge', $casino_countries_range_array));

        // Get Countries array.
        // $casino_country_value = reset($casino_countries_array);
        $casino_country_value = !empty($casino_countries_array) ? $casino_countries_array : FALSE;

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    $count = 0;
    $casinos = array();

    $game_types_list = guru_get_casino_game_types_list();
    $game_types_data = array();

    foreach ($casino_ratings as $key => $casino_rating) {

        if ($key == 0) {
            foreach ($casino_rating as $row_key => $entry) {
                if ($entry == 'TOP') {
                    $general_rating_key = $row_key;
                }

                if ($entry == 'Countries') {
                    $country_1_key = $row_key;
                }

                if (Guru_Trackers_IS_BINGO) {
                    if ($entry == 'Bingo') {
                        $name_key = $row_key;
                    }
                } else {
                    if ($entry == 'Name') {
                        $name_key = $row_key;
                    }
                }

                // Dynamic game types columns search in sheet.
                if (!empty($game_types_list)) {
                    foreach ($game_types_list as $game_type) {
                        if ($entry == $game_type['name']) {
                            $game_types_data[] = array(
                                'key' => $row_key,
                                'name' => $entry,
                            );
                        }
                    }
                }

            }
        }

        if ($key > 0) {
            $casino_name = trim($casino_rating[$name_key]);
            $casino_id = (int) scg_get_casino_id_from_name($casino_name);
            $casino_rating = $casino_rating[$general_rating_key];
            $casino_rating = (float) str_replace(',', '.', $casino_rating);
            $casino_country_1 = $casino_country_value;

            $casino_games_ratings = array();

            if (!empty($game_types_data)) {
                foreach ($game_types_data as $game_type_data_key => $game_type_data) {
                    if (!empty($casino_ratings[$key][$game_type_data['key']])) {
                        $casino_game_rating = (float) str_replace(',', '.', $casino_ratings[$key][$game_type_data['key']]);
                        $casino_games_ratings[] = array(
                            'casino_id'   => $casino_id,
                            'game_type'   => $game_type_data['name'],
                            'country_ids' => $casino_country_1,
                            'rating'      => $casino_game_rating,
                        );
                    }
                }
            }

            if (!empty($casino_id) && (!empty($casino_rating) || !empty($casino_games_ratings))) {
                $casinos[] = array(
                    'casino_id' => $casino_id,
                    'casino_name' => $casino_name,
                    'casino_sheet_name' => $sheet_name,
                    'casino_rating' => $casino_rating,
                    'casino_country_1' => $casino_country_1,

                    // Array of casino games ratings.
                    'casino_games_ratings' => $casino_games_ratings,
                );
            }
        }

        $count++;
    }

    return $casinos;
}



function new_test() {
    // $info = scg_casino_trackers_get_casino_info(70);
    // d($info);
    // die();

    // Import All Casinos Restricted Countries.
    $sheet_name = 'Restricted Countries';
    $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name);
    d($casinos);
    die();

    $payment_method = array(
        'name' => '2Click',
        'id' => 37,
        'image_url' => 'https://drive.google.com/uc?id=1eJmKHOKAe6muRSDN1qJ6ye1m5P4Qiamr',
        'image_file_id' => '1eJmKHOKAe6muRSDN1qJ6ye1m5P4Qiamr',
    );
    $attachment_id = scg_save_image_file_to_folder($payment_method);
    // d($payment_method);
    d($attachment_id);
    die();

    // guru_trackers_and_ratings_get_google_drive_file();

    $sheet_name = 'ALL';
    $casino_trackers = guru_trackers_parse_file($sheet_name);
    $casino = $casino_trackers[31];
    // $sheet_name_lower = strtolower(str_replace(' ', '-', $sheet_name));
    // d($casino);
    // d($casino_trackers);
    // die();


    $country_ids = array(1);
    $casino_id = 94;

    if (!empty($country_ids) && !empty($casino_id)) {
        $bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');
        $bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');

        $query = new SelectQuery;
        $query
            ->from('wp_pods_casino_bonus', 't')
            ->select(['t.id'])
            ->limit(1);

        $query
            ->join(DatabaseService::REL_TABLE, 'bonus_casino')
            ->on("bonus_casino.field_id = $bonus_casino_field_id AND bonus_casino.item_id = t.id");

        $query
            ->join(DatabaseService::REL_TABLE, 'bonus_countries')
            ->on("bonus_countries.field_id = $bonus_countries_field_id AND bonus_countries.item_id = t.id");

        $query->where()
            ->equal('bonus_casino.related_item_id', $casino_id);

        $query->where()
            ->in('bonus_countries.related_item_id', $country_ids);

        $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
        $existing_casino_bonus_id = !empty($results) ? (int) $results->id : FALSE;
        // d($existing_casino_bonus_id);

        // Adjust bonus sum text and currency when its empty.
        $bonus_sum_currency_text = !empty($casino['casino_bonus_sum']) && !empty($casino['casino_bonus_currency']) ? ' - ' . $casino['casino_bonus_sum'] . ' ' . $casino['casino_bonus_currency'] : ' - Nėra bonuso sumos';

        $country_code = 'ALL';

        $casino_bonus_name = $casino['casino_name'] . $bonus_sum_currency_text . ' - ' . $country_code;
        $permalink = strtolower($casino_bonus_name);
        $permalink = str_replace('---', '-', strtolower($permalink));
        $permalink = str_replace(' ', '', strtolower($permalink));

        $current_timestamp = current_time('mysql');

        // Prepare data for insertion/update.
        $data = array(
            'name' => $casino_bonus_name,

            'id'        => $existing_casino_bonus_id,
            'permalink' => $permalink,
            'modified'  => $current_timestamp,

            'bonus_text' => $casino['casino_bonus_text'],
            'bonus_sum' => $casino['casino_bonus_sum'],

            'casino_id' => $casino['casino_id'],
            'bonus_countries' => $country_ids,

            'bonus_list_button_text' => $casino['casino_top_button_text'],
            'bonus_list_regulation_text' => $casino['casino_top_button_regulation_text'],

            // Bonus text.
            'bonus_review_bonus_text' => $casino['casino_review_bonus_text'],

            // Bonus button text.
            'bonus_review_button_text' => $casino['casino_review_bonus_button_text'],

            // Bonus regulation text.
            'bonus_review_regulation_text' => $casino['casino_review_button_regulation_text'],

            'bonus_shortcode_button_text' => $casino['casino_shortcode_button_text'],
            'bonus_shortcode_regulation_text' => $casino['casino_shortcode_button_regulation_text'],

            'bonus_enabled' => $casino['casino_bonus_enabled_globally'],
        );

        // Insert / update wp_pods_casino_review record.
        $casino_bonus_id = scg_casino_bonus_insert_or_update($data, $casino, $country_code);

        d($data);
        d($casino_bonus_id);

        die();
    }


    // $sheet_name = 'NO';
    // $trackers = guru_trackers_parse_file($sheet_name);
    // $ratings = guru_ratings_parse_file($sheet_name);

    // d($trackers);
    // d($ratings);
    // die();

    // Check if have casinos to import.
    if (!empty($casino_trackers)) {
      $count = 0;
      foreach ($casino_trackers as $casino) {
          // if ($count < 1) {
            // d($casino);
            // die();

            $sync_name = 'scg-casino-import-data-trackers-' . $sheet_name_lower . '-' . $casino['casino_name'];

            d($sync_name);
            d($casino);
            die();

            set_transient($sync_name, $casino, 25 * 60 * 60);

            // if ($sheet_name == 'UK' && $casino['casino_name'] == '888casino') {
            //     $import_trackers_data = casino_trackers_import_data($sync_name, $sheet_name);
            // }

            // Create a "schedule action" for the data
            // as_schedule_single_action(
            //     time(),
            //     'scg_batch_import_casino_trackers',
            //     array(
            //       'sync_name' => $sync_name,
            //       'sheet_name' => $sheet_name,
            //     ),
            //     'scg_import_sheets_data'
            // );


          // }

          $count++;
      }
    }


    // $sheet_name = 'SE';
    // $trackers = guru_trackers_parse_file($sheet_name);
    // $ratings = guru_ratings_parse_file($sheet_name);

    // d($trackers);
    // d($ratings);
    // die();

    // Get casino import data from transient.
    // $sync_name = 'scg-casino-import-data-ratings-all-VegazCasino';
    // $casino = get_transient($sync_name);

    // // Import Casino Ratings.
    // $casino_rating = $casino['casino_rating'];
    // $casino_game_type_ratings = $casino['casino_games_ratings'];

    // // d($casino_rating);
    // // d($casino_game_type_ratings);
    // // die();

    // // Import those bookmaker ratings
    // // including sport type ratings.
    // $import = scg_casino_ratings_import_casino_ratings($casino['casino_id'], $casino['casino_name'], $sheet_name, $casino_rating, $casino['casino_country_1'], $casino_game_type_ratings);

    // $selected_sheets = get_option('scg-casino-trackers-settings-selected-sheets');
    // $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');
    // d($selected_sheets);
    // d($sheets_with_countries);


    // if (empty($sheets_with_countries)) {
    //     // Get latest Google Drive file.
    //     guru_trackers_and_ratings_get_google_drive_file();

    //     if (!empty($selected_sheets)) {
    //         foreach ($selected_sheets as $sheet) {
    //             $sheet_countries = scg_casino_trackers_get_countries_from_sheet($sheet['title']);

    //             $sheets_with_countries[] = array(
    //                 'title' => $sheet['title'],
    //                 'sheetId' => $sheet['sheetId'],
    //                 'countries' => $sheet_countries,
    //             );
    //         }
    //     }

    //     if (!empty($sheets_with_countries)) {
    //         update_option('scg-casino-trackers-sheets-with-countries', $sheets_with_countries, false);
    //     }
    // }

    // $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');
    // d($sheets_with_countries);

    // $sheet_countries = scg_casino_trackers_get_countries_from_sheet('ALL');
    // d($sheet_countries);
    // die();

    // Import All Casinos Restricted Countries.
    // $sheet_name = 'Restricted Countries';
    // $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name);
    // d($casinos);
}

// add_action('init', 'new_test');


/**
 * Sheets to show without casino bonus.
 */
function scg_sheets_to_show_without_bonus()
{
    $sheets_no_bonus = get_option('scg-casino-trackers-settings-sheets-no-bonus');

    $is_from_uk = scg_user_is_from_country('GB');
    $is_from_ireland = scg_user_is_from_country('IE');

    $country_name = scg_casino_trackers_get_user_country();
    $country_code = scg_get_country_code_from_name($country_name);

    $show_without_bonus = FALSE;
    if (!empty($sheets_no_bonus)) {
        foreach ($sheets_no_bonus as $sheet) {
            if ($sheet['title'] == $country_code) {
                $show_without_bonus = TRUE;
            }
        }
    }

    if ($is_from_uk || $is_from_ireland) {
        $show_without_bonus = TRUE;
    }

    return $show_without_bonus;
}


/**
 * Custom XlSX folder to upload downloaded file.
 */
function scg_casino_trackers_get_xlsx_file_upload_dir($arr)
{
    $folder = '/xlsx'; // No trailing slash at the end.

    $arr['path'] = str_replace($arr['subdir'], '', $arr['path']);
    $arr['url'] = str_replace($arr['subdir'], '', $arr['url']);

    $arr['path'] .= $folder;
    $arr['url'] .= $folder;
    $arr['subdir'] = $folder;

    return $arr;
}


function testing_now() {
    scg_casino_trackers_get_all_info_csv_files();
}

// add_action('admin_init', 'testing_now');



/**
 * Get latest restricted countries sheet file from Google Drive.
 */
function scg_casino_trackers_get_all_info_csv_files()
{

    $url = 'https://hook.integromat.com/muwg177v415taq35men4gtu3kfmzhkj4';
    $response = wp_remote_post( $url, array(
      'method'      => 'POST',
      'timeout'     => 45,
      'redirection' => 5,
      // 'blocking'    => false,
      // 'headers'     => array(),
      // 'cookies'     => array(),
    ) );
    $csv_files = !empty($response['body']) ? json_decode($response['body']) : FALSE;

    // echo "<pre>" . print_r($response, true) . "</pre>";
    // echo "<pre>" . print_r($csv_files, true) . "</pre>";
    $fileId = scg_get_all_info_sheet_file_id();

    // echo "<pre>" . print_r($csv_files, true) . "</pre>";
    // echo "<pre>" . print_r($fileId, true) . "</pre>";

    if (!empty($csv_files)) {
        foreach ($csv_files as $file) {
            $filename   = strtolower(str_replace(' ', '-', $file->title)) . '.csv';
            $sheetId    = $file->sheetId;

            // echo "<pre>" . print_r($filename, true) . "</pre>";
            // echo "<pre>" . print_r($sheetId, true) . "</pre>";

            $upload_dir = wp_upload_dir();
            $xlsx_file_dir = $upload_dir['basedir'] . '/' . 'xlsx';

            // Delete file firstly if it exists.
            wp_delete_file($xlsx_file_dir . '/' . $filename);

            try {
                $file_url = 'https://docs.google.com/spreadsheets/d/' . $fileId . '/export?gid=' . $sheetId . '&format=csv';

                // $response = wp_remote_get($file_url, 
                //     array(
                //         'method' => 'GET',
                //         'headers' => ['Authorization' => "Bearer "]
                //     ),
                // );
                // $content = $response['body'];
                // var_dump($content);
                // die();


                // create the Google client
                $client = new Google_Client();

                /**
                 * Set your method for authentication. Depending on the API, This could be
                 * directly with an access token, API key, or (recommended) using
                 * Application Default Credentials.
                 */
                $client->useApplicationDefaultCredentials();
                $client->addScope(Google_Service_Drive::DRIVE);

                // returns a Guzzle HTTP Client
                $httpClient = $client->authorize();

                // make an HTTP request
                // $response = $httpClient->get('https://www.googleapis.com/plus/v1/people/me');
                $response = $httpClient->get($file_url, ['save_to' => $xlsx_file_dir . '/' . $filename]);
                // echo "<pre>" . print_r($response, true) . "</pre>";
                // die();


                // Tell the Google client to use your service account credentials.
                // $client = new Google_Client();
                // $client->useApplicationDefaultCredentials();

                // // Set the scopes required for the API.
                // $client->addScope(Google_Service_Drive::DRIVE);

                // // Make Drive Service.
                // $service = new Google_Service_Drive($client);

                // $fileId = scg_get_restricted_countries_sheet_file_id();
                // $response = $service->files->export($fileId, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', array(
                //     'alt' => 'media'));
                $content = $response->getBody()->getContents();

                // $optParams = array('fields' => 'modifiedTime');
                // $filedata = $service->files->get($fileId, $optParams);
                // $modifiedTime = $filedata->getModifiedTime();

                if (!empty($filename) && !empty($content)) {
                    add_filter('upload_dir', 'scg_casino_trackers_get_xlsx_file_upload_dir');

                    $ret = wp_upload_bits($filename, null, $content);
                    $uploaded_filepath = $ret['file'];

                    remove_filter('upload_dir', 'scg_casino_trackers_get_xlsx_file_upload_dir');
                }

                // return $client;
            } catch (Exception $e) {
                error_log('An error occurred downloading Google Drive Sheets File: ' . $e->getMessage());
            }
        }
    }
}


/**
 * Get latest restricted countries sheet file from Google Drive.
 */
function scg_casino_trackers_get_restricted_countries_file()
{
    $filename = 'casinos-restricted-countries.xlsx';
    $upload_dir = wp_upload_dir();
    $xlsx_file_dir = $upload_dir['basedir'] . '/' . 'xlsx';

    // Delete file firstly if it exists.
    wp_delete_file($xlsx_file_dir . '/' . $filename);

    try {
        // Tell the Google client to use your service account credentials.
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();

        // Set the scopes required for the API.
        $client->addScope(Google_Service_Drive::DRIVE);

        // Make Drive Service.
        $service = new Google_Service_Drive($client);

        $fileId = scg_get_restricted_countries_sheet_file_id();
        $response = $service->files->export($fileId, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', array(
            'alt' => 'media'));
        $content = $response->getBody()->getContents();

        $optParams = array('fields' => 'modifiedTime');
        $filedata = $service->files->get($fileId, $optParams);
        $modifiedTime = $filedata->getModifiedTime();

        add_filter('upload_dir', 'scg_casino_trackers_get_xlsx_file_upload_dir');

        $ret = wp_upload_bits($filename, null, $content);
        $uploaded_filepath = $ret['file'];

        remove_filter('upload_dir', 'scg_casino_trackers_get_xlsx_file_upload_dir');

        return $client;
    } catch (Exception $e) {
        error_log('An error occurred downloading Google Drive Sheets File: ' . $e->getMessage());
    }
}


/**
 * Parse casinos restricted countries file.
 */
function scg_casino_trackers_parse_restricted_countries_csv_file($data, $single_casino_info = FALSE)
{
    $count = 0;
    $casinos = array();

    foreach ($data as $key => $restricted_country) {
        // Prepare Casino Data in first row.
        if ($key == 0) {
            foreach ($restricted_country as $row_key => $entry) {

                if ($row_key > 1) {
                    $casino_column_id = $row_key;
                    $casino_name = $restricted_country[$casino_column_id];

                    // Adjust casino enabled based on project.
                    if (Guru_Trackers_IS_BINGO) {
                        $casino_restricted_enabled_raw = trim($data[5][$casino_column_id]);
                        $casino_enabled_raw = trim($data[5][$casino_column_id]);
                    } else {
                        $casino_restricted_enabled_raw = trim($data[2][$casino_column_id]);
                        $casino_enabled_raw = trim($data[2][$casino_column_id]);
                    }

                    $casino_restricted_enabled = $casino_restricted_enabled_raw == 'Taip';
                    $casino_enabled = $casino_enabled_raw == 'Taip';
                    $casino_closed = $casino_enabled_raw == 'Closed';

                    $casinos[$casino_column_id] = array(
                        'casino_column_id' => $casino_column_id,
                        'casino_name' => $casino_name,
                        'casino_restricted_enabled' => $casino_restricted_enabled,
                        'casino_enabled' => $casino_enabled,
                        'casino_closed'  => $casino_closed,
                        'casino_restricted_countries' => array(),
                    );

                }
            }
        }

        // Restricted countries info:
        // 0 ROW - Casino Names
        // 1 ROW - SCG Casino enabled
        // 2 ROW - CG Casino enabled
        // 3 ROW - SBG Casino enabled
        // 4 ROW - LG Casino enabled
        // 5 ROW - SBIG Bingo enabled
        // 5+ ROWS - Restricted Countries info 
        // (X value means that country is restricted)
        if ($key > 5) {
            $country_name = trim($restricted_country[0]);
            $country_code = trim($restricted_country[1]);

            foreach ($casinos as $casino_key => $casino) {
                $country_is_restricted = !empty($restricted_country[$casino['casino_column_id']]);

                if ($country_is_restricted) {
                    $casinos[$casino_key]['casino_restricted_countries'][] = array(
                        'country_name' => $country_name,
                        'country_code' => $country_code,
                    );
                }
            }
        }

        $count++;
    }

    // echo "<pre>" . print_r($casinos, true) . "</pre>";

    if (!empty($single_casino_info)) {
        foreach ($casinos as $casino) {
            if (
                $casino['casino_id'] == $single_casino_info['casino_id'] ||
                $casino['casino_name'] == $single_casino_info['casino_name']
            ) {
                $single_casino = $casino;

                continue;
            }
        }

        $casinos = array();

        // Import single casino.
        if (!empty($single_casino)) {
            $casinos[] = $single_casino;
        }
    }

    return $casinos;
}



/**
 * Parse casinos restricted countries file.
 */
function scg_casino_trackers_parse_restricted_countries_file($sheet_name, $single_casino_info = FALSE)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = scg_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'casinos-restricted-countries.xlsx';

    try {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly([$sheet_name]);
        $reader->setReadEmptyCells(false);
        $spreadsheet = $reader->load($file_path);

        $worksheetData = $reader->listWorksheetInfo($file_path);

        // var_dump($worksheetData);
        // die();

        foreach ($worksheetData as $worksheet) {
            if ($worksheet['worksheetName'] == $sheet_name) {
                $sheet_lastColumnIndex = $worksheet['lastColumnIndex'];
                $sheet_lastColumnLetter = $worksheet['lastColumnLetter'];

                $sheet_lastRange = $sheet_lastColumnLetter . $sheet_lastColumnIndex;
            }
        }

        $active_sheet = $spreadsheet->getActiveSheet();

        $highestRow = $active_sheet->getHighestRow();
        $highestCol = $active_sheet->getHighestColumn();

        // var_dump($highestRow);
        // var_dump($highestCol);

        // // Get last column that has countries
        // // starting from second row.
        // $casino_countries_range_array = $active_sheet->rangeToArray(
        //     $highestCol . '2:' . $highestCol . $highestRow
        // );

        $restricted_countries_data = $active_sheet->rangeToArray(
            'A1:' . $highestCol . $highestRow
        );

        // var_dump($restricted_countries_data);
        // die();

        // Filter array to only non-empty items.
        $restricted_countries_data_array = array_filter(array_map('array_filter', $restricted_countries_data));

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    $count = 0;
    $casinos = array();

    foreach ($restricted_countries_data_array as $key => $restricted_country) {

        // Prepare Casino Data in first row.
        if ($key == 0) {
            foreach ($restricted_country as $row_key => $entry) {

                if ($row_key > 1) {
                    $casino_column_id = $row_key;

                    // d($restricted_countries_data_array);
                    // d($row_key);

                    $casino_id = $restricted_countries_data_array[2][$casino_column_id];
                    $casino_name = $restricted_country[$casino_column_id];
                    $casino_restricted_enabled_raw = trim($restricted_countries_data_array[2][$casino_column_id]);
                    $casino_restricted_enabled = $casino_restricted_enabled_raw != 'Ne';
                    // $casino_restricted_enabled = TRUE;

                    $casino_enabled_raw = trim($restricted_countries_data_array[3][$casino_column_id]);
                    $casino_enabled = $casino_enabled_raw != 'Ne';

                    if (!empty($casino_id)) {
                        $casinos[$casino_column_id] = array(
                            'casino_column_id' => $casino_column_id,
                            'casino_id' => $casino_id,
                            'casino_enabled' => $casino_enabled,
                            'casino_name' => $casino_name,
                            'casino_restricted_enabled' => $casino_restricted_enabled,
                            'casino_restricted_countries' => array(),
                        );
                    }

                }
            }
        }

        if ($key > 2) {
            $country_name = trim($restricted_country[0]);
            $country_code = trim($restricted_country[1]);

            foreach ($casinos as $casino_key => $casino) {
                $country_is_restricted = !empty($restricted_country[$casino['casino_column_id']]);

                if ($country_is_restricted && !empty($country_name) && !empty($country_code)) {
                    $casinos[$casino_key]['casino_restricted_countries'][] = array(
                        'country_name' => $country_name,
                        'country_code' => $country_code,
                    );
                }
            }
        }

        $count++;
    }

    if (!empty($single_casino_info)) {
        foreach ($casinos as $casino) {
            if (
                $casino['casino_id'] == $single_casino_info['casino_id'] ||
                $casino['casino_name'] == $single_casino_info['casino_name']
            ) {
                $single_casino = $casino;

                continue;
            }
        }

        $casinos = array();

        // Import single casino.
        if (!empty($single_casino)) {
            $casinos[] = $single_casino;
        }
    }

    return $casinos;
}


/**
 * Get current casinos list.
 */
function scg_get_current_casinos_list($enabled_only = TRUE)
{
    // $casinos_pods = pods('casino');

    // $enabled_only_filter = !empty($enabled_only) ? 't.casino_enabled = 1' : '';

    // $params = array(
    //     'distinct' => FALSE,
    //     'limit' => 0,
    //     'orderby' => 't.name ASC',
    //     'where' => $enabled_only_filter,
    // );
    // $casinos_pods->find($params);
    // $casinos_list = $casinos_pods->data();

    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino')
        ->select(['name', 'id'])
        ->limit(0)
        ->orderBy('name', 'ASC');

    if ($enabled_only) {
      $query
        ->where()
        ->equal('casino_enabled', 1);
    }

    $casinos_list = (new DatabaseService($query, DatabaseService::GET_DATA))->build();
    $casinos = array();

    if ($casinos_list) {
        foreach ($casinos_list as $entry) {
            $casinos[] = array(
                'id' => (int)$entry->id,
                'name' => $entry->name,
            );
        }
    }

    return $casinos;
}


/**
 * Get the absolute path to the XLSX uploads sub-directory,
 * with a trailing slash.
 */
function scg_get_xlsx_uploads_directory_path()
{
    $upload_dir = wp_upload_dir();

    return trailingslashit($upload_dir['basedir']) . 'xlsx/';
}


/**
 * Find if array value exists.
 */
function scg_find_array_key_value($array, $key, $val)
{
    foreach ($array as $item) {
        if (isset($item[$key]) && $item[$key] == $val) {
            return TRUE;
        }
    }

    return FALSE;
}


/**
 * Get sheets list from casino trackers file.
 */
function scg_casino_trackers_get_sheets_list_from_file()
{
    $sheets = array();

    try {
        // Tell the Google client to use your service account credentials.
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();

        // Set the scopes required for the API.
        $client->addScope(Google_Service_Drive::DRIVE);
        $client->addScope(Google_Service_Sheets::SPREADSHEETS);

        $spreadsheetID = guru_get_tracker_sheet_file_id();

        $sheetService = new Google_Service_Sheets($client);
        $spreadSheet = $sheetService->spreadsheets->get($spreadsheetID);
        $sheets_list = $spreadSheet->getSheets();
    } catch (Exception $e) {
        error_log('An error occurred while getting sheets list: ' . $e->getMessage());
    }

    if (!empty($sheets_list)) {
        foreach ($sheets_list as $sheet) {

            if (!strpos($sheet->properties->title, '-Rate')) {
                $sheets[] = array(
                    'title' => $sheet->properties->title,
                    'sheetId' => $sheet->properties->sheetId,
                );
            }
        }
    }

    if (!empty($sheets)) {
        update_option('scg-casino-trackers-settings-list-sheets', $sheets);
    }

    return $sheets;
}


/**
 * Import casino rating data from our Google Sheet.
 */
function scg_casino_ratings_import_data($casino)
{
    // Get hash of current import.
    $hash = get_option('scg-casino-trackers-import-hash');

    // Sheet name for further use.
    $sheet_name = trim($casino['casino_sheet_name']);

    // Prepare log entry data for further use.
    $log_entry_data = array(
        'log_entry_casino_id' => $casino['casino_id'],
        'log_entry_sheet_name' => $casino['casino_sheet_name'],
        'log_entry_type' => 'casino_trackers',
    );

    $casino_matches = scg_check_if_casino_and_id_matches($casino['casino_id'], $casino['casino_name']);

    if (!$casino_matches) {
        // Stop executing further code if no match is found.
        return FALSE;
    }

    // Get country ID and code using pods query.
    $country_name = trim($casino['casino_country_1']);
    $country_id = scg_get_country_id_from_name($country_name);
    $country_code = scg_get_country_code_from_name($country_name);

    // Get language code by country code.
    $language_code = scg_get_language_code_by_country_code($country_code);

    $casino_info_id = FALSE;

    // Let's check if this rating exists first.
    if (!empty($country_id) && !empty($casino['casino_id']) && !empty($language_code)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'rating_casino.id = "' . $casino['casino_id'] . '"' . ' AND ' .
                'rating_countries.id IN (' . $country_id . ')' . ' AND ' .
                'language = "' . $language_code . '"',
        );
        $casino_review = pods('casino_review');
        $casino_review->find($params);
        $data = $casino_review->data();

      $query_casino_review = new SelectQuery;
      $query_casino_review
          ->from('wp_pods_casino_review', 't')
          ->select(['id'])
          ->limit(1);

      $query_casino_review->where()
          ->equal('t.casino_id', $casino['casino_id'])
          ->equal('t.sport_type_id', $sport_type_id)
          ->equal('t.country_id', $country_id)
          ->notEqual('t.sport_review_id', 0)
          ->or()
          ->notEqual('t.sport_review_id', NULL);

      $sport_review_results = (new DatabaseService($query_sport_review, DatabaseService::GET_ITEM))->build();
      $existing_sport_review_id = !empty($sport_review_results) ? (int) $sport_review_results->sport_review_id : FALSE;


        // Casino info ID.
        $casino_info_id = scg_casino_trackers_get_casino_info_id($casino['casino_id'], $language_code);
    }

    $existing_casino_review_id = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $existing_casino_review_id = $data[0]->id;
    }

    // Select casino review pod.
    if (!empty($existing_casino_review_id)) {
        $casino_review_pod = pods('casino_review', $existing_casino_review_id);
    } else {
        $casino_review_pod = pods('casino_review');
    }

    if (!empty($existing_casino_review_id)) {
        $params = array(
            'distinct' => FALSE,
            'where' => 't.id = ' . $existing_casino_review_id,
        );

        $casino_review_pod->find($params);
    }

    // Prepare data for insertion/update.
    if (!empty($language_code) && !empty($casino['casino_name']) && !empty($casino['casino_id']) && !empty($country_id) && !empty($casino['casino_rating'])) {
        $data_to_save = array(
            'name' => $casino['casino_name'] . ' - ' . $casino['casino_rating'],

            'language' => $language_code,
            'rating_casino' => $casino['casino_id'],
            'rating_casino_info' => $casino_info_id,
            'rating_countries' => $country_id,
            'rating_total' => $casino['casino_rating'],
        );

        if (!empty($data_to_save)) {
            if (!empty($existing_casino_review_id)) {
                // Save data to existing casino review item.
                $casino_review_pod->save($data_to_save);
            } else {
                // Otherwise create new casino review item.
                $new_casino_review_id = $casino_review_pod->add($data_to_save);
            }
        }

        $casino_review_id = !empty($existing_casino_review_id) ? $existing_casino_review_id : $new_casino_review_id;

        if (!empty($casino_review_id)) {
            $params = array(
                'distinct' => false,
                'where' => "t.id = '" . $casino['casino_id'] . "'",
            );

            $casino_pod = pods('casino', $casino['casino_id']);
            $casino_pod->find($params);

            // Add casino bonus ID to casino item.
            $casino_pod->add_to('casino_review_list', $casino_review_id);

            // Save all the data to casino item.
            $casino_pod->save();
        }
    }

    // Deal with multiple casino games ratings.
    $casino_games_ratings = $casino['casino_games_ratings'];

    // error_log('<pre>' . print_r($casino, TRUE) . '</pre>');

    if (!empty($casino_games_ratings)) {
        foreach ($casino_games_ratings as $casino_game_rating) {
            $casino_id = $casino_game_rating['casino_id'];
            $casino_name = $casino['casino_name'];
            $game_type = trim($casino_game_rating['game_type']);
            $game_type_rating = trim($casino_game_rating['rating']);
            $country_id = $country_id;

            if (!empty($game_type_rating)) {
                // Set game type ratings from sheet data
                // and attach to parent casino review pod.
                $casino_game_type_rating_id = scg_set_casino_game_type_ratings_from_sheet(
                    $casino_review_id,
                    $casino_id,
                    $casino_name,
                    $game_type,
                    $game_type_rating,
                    $country_id,
                    $language_code
                );
            }
        }

    }

    return TRUE;
}


/**
 * Import Log logic to keep track of what was imported
 * and what went wrong (TODO) - bad links, no data provided.
 */
function scg_casino_trackers_create_or_update_import_log($hash_code, $entry = array())
{
    // Check if import log exists first.
    $params = array(
        'distinct' => FALSE,
        'where' =>
            't.import_log_code = "' . $hash_code . '"' . ' AND ' .
            't.import_log_type = "' . $entry['import_log_type'] . '"',
    );

    $import_log = pods('import_log');
    $import_log->find($params);
    $data = $import_log->data();

    $existing_import_log_id = FALSE;
    $existing_author = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $existing_import_log_id = $data[0]->id;
        $existing_author = $data[0]->import_log_author;
    }

    // Select import log pod.
    if (!empty($existing_import_log_id)) {
        $import_log_pod = pods('import_log', $existing_import_log_id);
    } else {
        $import_log_pod = pods('import_log');
    }

    $current_time = current_time('mysql');
    $current_user_info = get_currentuserinfo();

    if (empty($existing_author)) {
        $log_author = !empty($current_user_info) ? $current_user_info->display_name : 'Automatika';
    }

    // Prepare data for insertion/update.
    $data_to_save = array(
        'import_log_code' => $hash_code,
    );

    if (!empty($log_author) && empty($existing_author)) {
        $data_to_save['import_log_author'] = $log_author;
    }

    if (!empty($entry['first_save'])) {
        $data_to_save['name'] = $current_time;
        $data_to_save['import_log_datetime'] = $current_time;
    }

    if (!empty($entry['import_log_type'])) {
        $data_to_save['import_log_type'] = $entry['import_log_type'];
    }

    if (!empty($entry['author_cron'])) {
        $data_to_save['import_log_author'] = 'CRON (Automatiškai)';
    }

    if (!empty($entry['finished'])) {
        $data_to_save['import_log_end_datetime'] = $current_time;

        // Calculate duration between start and end.
        $start = date_create($data[0]->import_log_datetime);
        $end = date_create($current_time);
        $diff = date_diff($end, $start);
        $duration = sprintf('%02d', $diff->i) . ':' . sprintf('%02d', $diff->s);

        $data_to_save['import_log_duration'] = $duration;
        $data_to_save['import_log_finished'] = 1;
    }

    if (!empty($entry['new_link_added'])) {
        $data_to_save['import_log_total_links'] = !empty($data[0]) ? $data[0]->import_log_total_links + 1 : 1;
    }

    if (!empty($entry['new_link_imported'])) {
        $data_to_save['import_log_total_imported_links'] = !empty($data[0]) ? $data[0]->import_log_total_imported_links + 1 : 1;
    }

    if (!empty($existing_import_log_id)) {
        // Save data to import log.
        $import_log_pod->save($data_to_save);
    } else {
        // Otherwise create new import log.
        $new_import_log_id = $import_log_pod->add($data_to_save);
    }

}


/**
 * Get import log ID by hash.
 */
function scg_casino_trackers_get_import_log_id_by_hash($hash_code, $log_type)
{
    // Check if import log exists first.
    $params = array(
        'distinct' => FALSE,
        'where' =>
            't.import_log_code = "' . $hash_code . '"' . ' AND ' .
            't.import_log_type = "' . $log_type . '"',
    );

    $import_log = pods('import_log');
    $import_log->find($params);
    $data = $import_log->data();

    $import_log_id = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $import_log_id = $data[0]->id;
    }

    return $import_log_id;
}


/**
 * Import Log entry logic on what went wrong:
 * bad links, no crypto data provided,
 * wrong casino ID or casino name.
 */
function scg_casino_trackers_create_log_entry($hash_code, $entry = array())
{
    $log_entry_pod = pods('log_entry');

    $current_time = current_time('mysql');

    $name = $current_time . ' - ' . $entry['log_entry_reason'];
    $log_entry_reason = $entry['log_entry_reason'];
    $log_entry_text = $entry['log_entry_text'];
    $log_entry_datetime = $current_time;
    $log_entry_casino_id = !empty($entry['log_entry_casino_id']) ? $entry['log_entry_casino_id'] : FALSE;
    $log_entry_sheet_name = !empty($entry['log_entry_sheet_name']) ? $entry['log_entry_sheet_name'] : FALSE;
    $log_entry_note = !empty($entry['log_entry_note']) ? $entry['log_entry_note'] : FALSE;

    // Prepare data for insertion/update.
    $data_to_save = array(
        'name' => $name,
        'log_entry_code' => $hash_code,
        'log_entry_reason' => $log_entry_reason,
        'log_entry_text' => $log_entry_text,
        'log_entry_datetime' => $log_entry_datetime,
        'log_entry_casino_id' => $log_entry_casino_id,
        'log_entry_sheet_name' => $log_entry_sheet_name,
        'log_entry_note' => $log_entry_note,
    );

    // Create new log entry item.
    $new_log_entry_id = $log_entry_pod->add($data_to_save);

    // Add log entry into current import log.
    $import_log_id = scg_casino_trackers_get_import_log_id_by_hash($hash_code, $entry['log_entry_type']);

    if (!empty($import_log_id)) {
        $import_log_pod = pods('import_log', $import_log_id);
        $import_log_pod->add_to('import_log_items', $new_log_entry_id);

        // Save log entry data to import log.
        $import_log_pod->save();
    }
}


function scg_manual_schedule_wrapper_import_action() {
  $sheet_select = $_POST['sheet_select'];

  // Schedule wrapper import action.
  as_schedule_single_action(
      time(),
      'scg_batch_schedule_wrapper_import_action',
      array(
        'sheet_select' => $sheet_select
      ),
      'scg_import_sheets_data'
  );

  echo TRUE;
  
  wp_die();
}

add_action( 'wp_ajax_scg_manual_schedule_wrapper_import_action', 'scg_manual_schedule_wrapper_import_action' );


function new_trackers_import_test() {
  // Execute only if option is empty.
  if (empty(get_option('new-import-data-done'))) {
    // Get latest file.
    // scg_casino_trackers_get_google_drive_file();
    // scg_casino_ratings_get_google_drive_file();

    guru_trackers_and_ratings_get_google_drive_file();

    $sheet_name = 'ALL';

    // d($casino_trackers);

    $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');
    $sheets = array();
    $sheets_countries = array();

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $sheets[] = $sheet['title'];
        }
    }

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $country_count = 1;
            foreach ($sheet['countries'] as $country) {
                if ($country_count == 1) {
                    if ($country == 'All Countries') {
                        $country_name = 'Lithuania';
                    } else {
                        $country_name = $country;
                    }

                    $sheets_countries[] = $country_name;
                }

                $country_count++;
            }
        }
    }

    // d($sheets);
    // d($sheets_countries);
    // die();

    $count = 1;
    foreach ($sheets as $sheet_name) {
        // Trigger function used to load bookmakers by sport.
        // This make trigger transient cache and makes it fast loaded.
        // if ($sheet_name == 'FI') {
            // as_schedule_single_action(
            //     time(),
            //     'scg_batch_import_casino_country',
            //     array(
            //         'sheet_name' => $sheet_name,
            //     ),
            //     'scg_import_sheets_data'
            // );

            // $count++;
        // }

        if ($sheet_name == 'ALL') {
          scg_import_casino_country($sheet_name);        
        }
    }

    // Generate transients cache.
    // $sheet_select = 'all_sheets';
    // scg_generate_casinos_transients_cache($sheet_select);

    // Mark done.
    update_option('new-import-data-done', TRUE);
  }

}

// add_action('admin_init', 'new_trackers_import_test');


function scg_schedule_wrapper_import_action($sheet_select) {
    // Get latest Google Drive files.
    // scg_casino_trackers_get_google_drive_file();
    // scg_casino_ratings_get_google_drive_file();

    guru_trackers_and_ratings_get_google_drive_file();

    $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');
    $sheets = array();
    $sheets_countries = array();

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $sheets[] = $sheet['title'];
        }
    }

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $country_count = 1;
            foreach ($sheet['countries'] as $country) {
                if ($country_count == 1) {
                    if ($country == 'All Countries') {
                        $country_name = 'Lithuania';
                    } else {
                        $country_name = $country;
                    }

                    $sheets_countries[] = $country_name;
                }

                $country_count++;
            }
        }
    }

    // d($sheets);
    // d($sheets_countries);
    // die();

    $count = 1;
    foreach ($sheets as $sheet_name) {
        // Trigger function used to load bookmakers by sport.
        // This make trigger transient cache and makes it fast loaded.
        // if ($sheet_name == 'FI') {
            as_schedule_single_action(
                time(),
                'scg_batch_import_casino_country',
                array(
                    'sheet_name' => $sheet_name,
                ),
                'scg_import_sheets_data'
            );

            // $count++;
        // }

        // if ($sheet_name == 'All Countries') {
        //   scg_import_casino_country($sheet_name);        
        // }
    }

    // Generate transients cache.
    scg_generate_casinos_transients_cache($sheet_select);
}

add_action('scg_batch_schedule_wrapper_import_action', 'scg_schedule_wrapper_import_action', 10, 1);


function scg_casino_ratings_single_casino_data($casino_id, $rating_data) {
  $single_casino_data = FALSE;

  foreach ($rating_data as $key => $casino) {
    if ($casino['casino_id'] == $casino_id) {
      $single_casino_data = $casino;
    }
  }

  return $single_casino_data;
}

function scg_import_casino_country($sheet_name) {

  $casino_trackers = guru_trackers_parse_file($sheet_name);
  $sheet_name_lower = strtolower(str_replace(' ', '-', $sheet_name));

  // Check if have casinos to import.
  if (!empty($casino_trackers)) {
      $count = 0;
      foreach ($casino_trackers as $casino) {
          // if ($count < 1) {
            // d($casino);
            // die();

            $sync_name = 'scg-casino-import-data-trackers-' . $sheet_name_lower . '-' . $casino['casino_name'];

            set_transient($sync_name, $casino, 25 * 60 * 60);

            if ($sheet_name == 'ALL' && $casino['casino_name'] == '888casino') {
              // $import_trackers_data = casino_trackers_import_data($sync_name, $sheet_name);
            }

            // Create a "schedule action" for the data
            as_schedule_single_action(
                time(),
                'scg_batch_import_casino_trackers',
                array(
                  'sync_name' => $sync_name,
                  'sheet_name' => $sheet_name,
                ),
                'scg_import_sheets_data'
            );


          // }

          $count++;
      }
  }

  $casino_ratings = guru_ratings_parse_file($sheet_name);

  if (!empty($casino_ratings)) {
      $count = 0;
      foreach ($casino_ratings as $casino) {
          // if ($count < 1) {
            // d($casino);
            // die();

            $sync_name = 'scg-casino-import-data-ratings-' . $sheet_name_lower . '-' . $casino['casino_name'];

            set_transient($sync_name, $casino, 25 * 60 * 60);

            // Create a "schedule action" for the data
            as_schedule_single_action(
                time(),
                'scg_batch_import_casino_ratings',
                array(
                  'sync_name' => $sync_name,
                  'sheet_name' => $sheet_name,
                ),
                'scg_import_sheets_data'
            );

            // if ($sheet_name == 'All Countries') {
            //   $import_ratings_data = casino_new_ratings_import_data($sync_name, $sheet_name);
            // }



          // }

          $count++;
      }
  }

}

// add_action('scg_batch_import_casino_country', 'scg_import_casino_country', 10, 1);



/**
 * Import casino data from our Google Sheet.
 */
function casino_trackers_import_data($sync_name, $sheet_name)
{
    // Get casino import data from transient.
    $casino = get_transient($sync_name);

    // // Get hash of current import.
    // $hash = get_option('scg-casino-trackers-import-hash');

    // // Sheet name for further use.
    // $sheet_name = trim($casino['casino_sheet_name']);

    // // Prepare log entry data for further use.
    // $log_entry_data = array(
    //     'log_entry_casino_id' => $casino['casino_id'],
    //     'log_entry_sheet_name' => $casino['casino_sheet_name'],
    //     'log_entry_type' => 'casino_trackers',
    // );


    $current_timestamp = current_time('mysql');

    // d($casino['casino_country_ids']);

    // Get country IDs.
    $country_ids = scg_get_country_ids_from_country_names($casino['casino_country_ids']);
    $country_ids_count = count($country_ids);

    // d($country_ids);

    if ($country_ids_count == 1) {
      $country_id = reset($country_ids);
      $country_code = scg_get_country_code_from_country_id($country_id);
    }

    if ($country_ids_count > 1) {
      $country_id = $country_ids;
      $country_code = $sheet_name;
    }

    // Get language code by country code.
    $language_code = scg_get_language_code_by_country_code($country_code);

    $casino_id = $casino['casino_id'];
    $casino_matches = scg_check_if_casino_and_id_matches($casino['casino_id'], $casino['casino_name']);

    // error_log('<pre>' . print_r($casino, TRUE) . '</pre>');
    // error_log('<pre>' . print_r($casino_matches, TRUE) . '</pre>');

    if (!$casino_matches) {
        // Log entry data for bad casino name/ID.
        $log_entry_data['log_entry_reason'] = 'Neatitinka kazino';
        $log_entry_data['log_entry_text'] = 'Blogas kazino ID arba pavadinimas';

        // Insert log entry about bad casino name/ID.
        scg_casino_trackers_create_log_entry($hash, $log_entry_data);

        $cell_info = scg_google_sheet_get_row_info_by_casino_value($casino['casino_name'], $sheet_name);
        $cell_row = $cell_info['row'];
        $cell_column = $cell_info['column_index'];
        $mark_type = 'Casino ID/Name';

        // if (isset($cell_row) && isset($cell_column)) {
        //     $mark_cell_red = scg_google_sheet_red_color_cell($cell_row, $cell_column, $sheet_name, $mark_type);
        // }

        // Stop executing further code if no match is found.
        return FALSE;
    }

    // Casino rating and info existance check.
    // $casino_rating_and_info_check = scg_casino_trackers_check_casino_rating_and_info($casino['casino_id'], $language_code);

    if (!empty($casino_rating_and_info_check) && !empty($casino['casino_bonus_enabled_globally'])) {
        $casino_info_exists = $casino_rating_and_info_check['casino_info_exists'];
        $casino_review_exists = $casino_rating_and_info_check['casino_review_exists'];

        // Log entry data for missing casino info/review data.
        if (empty($casino_info_exists) || empty($casino_review_exists)) {
            if (empty($casino_info_exists) && !empty($casino_review_exists)) {
                $log_entry_text = 'Trūksta įjungto kazino info';
            }
            if (!empty($casino_info_exists) && empty($casino_review_exists)) {
                $log_entry_text = 'Trūksta kazino reitingo';
            }
            if (empty($casino_info_exists) && empty($casino_review_exists)) {
                $log_entry_text = 'Trūksta įjungto kazino info ir reitingo';
            }

            $log_entry_data['log_entry_reason'] = 'Nepilnas kazino';
            $log_entry_data['log_entry_text'] = $log_entry_text;

            // Insert log entry about missing casino info/review data.
            // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
        }
    }

    // Get currency ID using pods query.
    $currency_sign = trim($casino['casino_bonus_currency']);
    $currency_id = !empty($currency_sign) ? scg_get_currency_id_from_currency_sign($currency_sign) : FALSE;

    // Let's check if this bonus exists first.
    if (!empty($country_ids) && !empty($casino_id)) {
        $bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');
        $bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');

        $query = new SelectQuery;
        $query
            ->from('wp_pods_casino_bonus', 't')
            ->select(['t.id'])
            ->limit(1);

        $query
            ->join(DatabaseService::REL_TABLE, 'bonus_casino')
            ->on("bonus_casino.field_id = $bonus_casino_field_id AND bonus_casino.item_id = t.id");

        $query
            ->join(DatabaseService::REL_TABLE, 'bonus_countries')
            ->on("bonus_countries.field_id = $bonus_countries_field_id AND bonus_countries.item_id = t.id");

        $query->where()
            ->equal('bonus_casino.related_item_id', $casino_id);

        $query->where()
            ->in('bonus_countries.related_item_id', $country_ids);

        $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
        $existing_casino_bonus_id = !empty($results) ? (int) $results->id : FALSE;
    }

    // Adjust bonus sum text and currency when its empty.
    $bonus_sum_currency_text = !empty($casino['casino_bonus_sum']) && !empty($casino['casino_bonus_currency']) ? ' - ' . $casino['casino_bonus_sum'] . ' ' . $casino['casino_bonus_currency'] : ' - Nėra bonuso sumos';

    // Check main casino affiliate links.
    if (!empty($casino['casino_top_link']) && !empty($casino['casino_bonus_enabled_globally'])) {
        $casino_top_link_valid = scg_check_affiliate_link_is_valid($casino['casino_top_link']);

        // Log data for new link added.
        $import_log_data = array(
            'new_link_added' => TRUE,
            'import_log_type' => 'casino_trackers',
        );

        // Mark this cell red if aff. link is invalid.
        if (!$casino_top_link_valid) {
            $cell_info = scg_google_sheet_get_row_info_by_casino_value($casino['casino_top_link'], $sheet_name);
            $cell_row = $cell_info['row'];
            $cell_column = $cell_info['column_index'];
            $mark_type = 'Bad Link';

            // if (isset($cell_row) && isset($cell_column)) {
            //     $mark_cell_red = scg_google_sheet_red_color_cell($cell_row, $cell_column, $sheet_name, $mark_type);
            // }

            // Log entry data for bad link.
            $log_entry_data['log_entry_reason'] = 'Bloga nuoroda';
            $log_entry_data['log_entry_text'] = $casino['casino_top_link'];
            $log_entry_data['log_entry_note'] = 'TOP linkas';

            // Insert log entry about bad link.
            // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
        } else {
            // Log data for new link imported.
            $import_log_data['new_link_imported'] = TRUE;
        }

        // Import link to import log for tracking.
        // scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);
    }

    if (!empty($casino['casino_review_link']) && !empty($casino['casino_bonus_enabled_globally'])) {
        $casino_review_link_valid = scg_check_affiliate_link_is_valid($casino['casino_review_link']);

        // Log data for new link added.
        $import_log_data = array(
            'new_link_added' => TRUE,
            'import_log_type' => 'casino_trackers',
        );

        // Mark this cell red if aff. link is invalid.
        if (!$casino_review_link_valid) {
            $cell_info = scg_google_sheet_get_row_info_by_casino_value($casino['casino_review_link'], $sheet_name);
            $cell_row = $cell_info['row'];
            $cell_column = $cell_info['column_index'];
            $mark_type = 'Bad Link';

            // if (isset($cell_row) && isset($cell_column)) {
            //     $mark_cell_red = scg_google_sheet_red_color_cell($cell_row, $cell_column, $sheet_name, $mark_type);
            // }

            // Log entry data for bad link.
            $log_entry_data['log_entry_reason'] = 'Bloga nuoroda';
            $log_entry_data['log_entry_text'] = $casino['casino_review_link'];
            $log_entry_data['log_entry_note'] = 'Review linkas';

            // Insert log entry about bad link.
            // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
        } else {
            // Log data for new link imported.
            $import_log_data['new_link_imported'] = TRUE;
        }

        // Import link to import log for tracking.
        // scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);
    }


    $casino_bonus_name = $casino['casino_name'] . $bonus_sum_currency_text . ' - ' . $country_code;
    $permalink = strtolower($casino_bonus_name);
    $permalink = str_replace('---', '-', strtolower($permalink));
    $permalink = str_replace(' ', '', strtolower($permalink));

    // Prepare data for insertion/update.
    $data = array(
        'name' => $casino_bonus_name,

        'id'        => $existing_casino_bonus_id,
        'permalink' => $permalink,
        'modified'  => $current_timestamp,

        'bonus_text' => $casino['casino_bonus_text'],
        'bonus_sum' => $casino['casino_bonus_sum'],

        'casino_id' => $casino['casino_id'],
        'bonus_countries' => $country_ids,

        'bonus_list_button_text' => $casino['casino_top_button_text'],
        'bonus_list_regulation_text' => $casino['casino_top_button_regulation_text'],

        // Bonus text.
        'bonus_review_bonus_text' => $casino['casino_review_bonus_text'],

        // Bonus button text.
        'bonus_review_button_text' => $casino['casino_review_bonus_button_text'],

        // Bonus regulation text.
        'bonus_review_regulation_text' => $casino['casino_review_button_regulation_text'],

        'bonus_shortcode_button_text' => $casino['casino_shortcode_button_text'],
        'bonus_shortcode_regulation_text' => $casino['casino_shortcode_button_regulation_text'],

        'bonus_enabled' => $casino['casino_bonus_enabled_globally'],
    );

    // Import TOP link only when it's valid.
    // If TOP link is empty - update it to remove.
    if (!empty($casino_top_link_valid) || empty($casino_top_link_valid) && empty($casino['casino_top_link'])) {
        $data['bonus_list_affiliate_link'] = $casino['casino_top_link'];
    }

    // Import Review link only when it's valid.
    // If Review link is empty - update it to remove.
    if (!empty($casino_review_link_valid) || empty($casino_review_link_valid) && empty($casino['casino_review_link'])) {
        $data['bonus_review_affiliate_link'] = $casino['casino_review_link'];
    }

    if (!empty($currency_id)) {
        $data['currency_id'] = $currency_id;
    }

    // error_log('<pre>' . print_r($data, TRUE) . '</pre>');

    // if (!empty($existing_bonus_id)) {
    //     // Save data to existing bonus item.
    //     $casino_bonus_pod->save($data);
    // } else {
    //     // Otherwise create new bonus item.
    //     $new_casino_bonus_id = $casino_bonus_pod->add($data);
    // }

    // $casino_bonus_id = !empty($existing_bonus_id) ? $existing_bonus_id : $new_casino_bonus_id;


    if (empty($data['id'])) {
      $data['created'] = $current_timestamp;
    }    

    // d($data);
    // die();

    // d($data_to_save);
    // die();

    // if ($casino['casino_id'] == 14 || $casino['casino_id'] == '14') {
        // var_dump($data);
        // var_dump($casino);
        // var_dump($country_code);
        // die();
    // }

    // Insert / update wp_pods_casino_review record.
    $casino_bonus_id = scg_casino_bonus_insert_or_update($data, $casino, $country_code);


    // TODO:

    // $params = array(
    //     'distinct' => false,
    //     'where' => "t.id = '" . $casino['casino_id'] . "'",
    // );

    // $casino_pod = pods('casino', $casino['casino_id']);
    // $casino_pod->find($params);

    // // Add casino bonus ID to casino item.
    // if (!empty($casino_bonus_id)) {
    //     $casino_pod->add_to('casino_bonuses', $casino_bonus_id);
    // }

    // // Save all the data to casino item.
    // $casino_pod->save();

    // Deal with multiple games affiliate links.
    $casino_games_links = $casino['casino_games_links'];

    if (!empty($casino_games_links)) {
        foreach ($casino_games_links as $casino_game_link) {
            $casino_id = $casino_game_link['casino_id'];
            $game_type = trim($casino_game_link['game_type']);
            $aff_link = trim($casino_game_link['aff_link']);
            $country_id = $country_id;
            $button_text = trim($casino_game_link['button_text']);
            $regulation_text = trim($casino_game_link['regulation_text']);
            $enabled = trim($casino_game_link['enabled']);

            // Let's exclude some game types for now.
            $excluded_game_types_from_checks = array('Index Page');

            // Leave only main game types to check on.
            $checkable_game_type = !in_array($game_type, $excluded_game_types_from_checks);

            if (!empty($casino['casino_bonus_enabled_globally']) && !empty($casino_game_link['aff_link']) && ($enabled == 'Taip' || $enabled == 'Yes') && !empty($checkable_game_type)) {
                // Casino rating and info existance check.
                // $casino_game_rating_check = scg_casino_trackers_check_casino_rating_and_info($casino_id, $language_code, $game_type);

                if (!empty($casino_rating_and_info_check)) {
                    $casino_game_review_exists = $casino_game_rating_check['casino_game_review_exists'];

                    // Log entry data for missing casino game review data.
                    if (empty($casino_game_review_exists)) {
                        $log_entry_text = 'Trūksta ' . $game_type . ' žaidimo tipo reitingo';

                        $log_entry_data['log_entry_reason'] = 'Nepilnas kazino';
                        $log_entry_data['log_entry_text'] = $log_entry_text;

                        // Insert log entry about missing casino info/review data.
                        // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
                    }
                }
            }

            // Validate casino games affiliate links.
            if (!empty($casino['casino_bonus_enabled_globally']) && !empty($casino_game_link['aff_link']) && ($enabled == 'Taip' || $enabled == 'Yes')) {
                $casino_game_link_valid = scg_check_affiliate_link_is_valid($aff_link);

                // Log data for new link.
                $import_log_data = array(
                    'new_link_added' => TRUE,
                    'import_log_type' => 'casino_trackers',
                );

                // Mark this cell red if aff. link is invalid.
                if (!$casino_game_link_valid) {
                    $cell_info = scg_google_sheet_get_row_info_by_casino_value($aff_link, $sheet_name);
                    $cell_row = $cell_info['row'];
                    $cell_column = $cell_info['column_index'];
                    $mark_type = 'Bad Link';

                    // if (isset($cell_row) && isset($cell_column)) {
                    //     $mark_cell_red = scg_google_sheet_red_color_cell($cell_row, $cell_column, $sheet_name, $mark_type);
                    // }

                    // Log entry data for bad link.
                    $log_entry_data['log_entry_reason'] = 'Bloga nuoroda';
                    $log_entry_data['log_entry_text'] = $aff_link;
                    $log_entry_data['log_entry_note'] = $game_type;

                    // Insert log entry about bad link.
                    // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
                } else {
                    // Log data for new link imported.
                    $import_log_data['new_link_imported'] = TRUE;
                }

                // Import link to import log for tracking.
                // scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);

                // Special logic for Crypto game type info.
                if ($game_type == 'Crypto') {
                    if (!empty($casino_game_link['aff_link']) && ($enabled == 'Taip' || $enabled == 'Yes') && empty($button_text) && empty($regulation_text)) {
                        // Use TOP texts if Crypto texts are empty.
                        $button_text = $casino['casino_top_button_text'];
                        $regulation_text = $casino['casino_top_button_regulation_text'];
                    }

                    if (empty($button_text) && empty($regulation_text)) {
                        // Log entry data for no Crypto texts.
                        $log_entry_data['log_entry_reason'] = 'Nėra Crypto info';
                        $log_entry_data['log_entry_text'] = 'mygtuko ir regulation teksto';

                        // Insert log entry about Crypto.
                        // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
                    }

                    if (empty($button_text) && !empty($regulation_text)) {
                        // Log entry data for no Crypto text.
                        $log_entry_data['log_entry_reason'] = 'Nėra Crypto info';
                        $log_entry_data['log_entry_text'] = 'mygtuko teksto';

                        // Insert log entry about Crypto.
                        // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
                    }

                    if (!empty($button_text) && empty($regulation_text)) {
                        // Log entry data for no Crypto text.
                        $log_entry_data['log_entry_reason'] = 'Nėra Crypto info';
                        $log_entry_data['log_entry_text'] = 'regulation teksto';

                        // Insert log entry about Crypto.
                        // scg_casino_trackers_create_log_entry($hash, $log_entry_data);
                    }
                }
            }

            // d($casino_bonus_id);
            // die();
            $casino_link_id = scg_set_casino_game_type_links_from_sheet($casino_bonus_id, $casino_id, $game_type, $aff_link, $country_id, $button_text, $regulation_text, $enabled, $casino_game_link_valid, $country_ids);
        }

    }

}

add_action('scg_batch_import_casino_trackers', 'casino_trackers_import_data', 10, 2);


function casino_new_ratings_import_data($sync_name, $sheet_name)
{
    // Get casino import data from transient.
    $casino = get_transient($sync_name);

    // Import Casino Ratings.
    $casino_rating = $casino['casino_rating'];
    $casino_game_type_ratings = $casino['casino_games_ratings'];

    // d($casino_rating);
    // d($casino_game_type_ratings);

    // Import those bookmaker ratings
    // including sport type ratings.
    $import = scg_casino_ratings_import_casino_ratings($casino['casino_id'], $casino['casino_name'], $sheet_name, $casino_rating, $casino['casino_country_1'], $casino_game_type_ratings);

}

add_action('scg_batch_import_casino_ratings', 'casino_new_ratings_import_data', 10, 2);


function scg_casino_bonus_insert_or_update($data_to_save, $casino, $country_code) {
    // $data_to_save = array(
    //     'name' => $casino_name . ' - ' . $casino_rating,

    //     'language' => $language_code,
    //     'rating_casino' => $casino_id,
    //     'rating_casino_info' => $casino_info_id,
    //     'rating_countries' => $country_id,
    //     'rating_total' => $casino_rating,
    // );

    // 'bonus_countries' => $country_ids,
    //

    $bonus_countries = $data_to_save['bonus_countries'];
    unset($data_to_save['bonus_countries']);

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_casino_bonus',
        [
            'id' => $data_to_save['id'],
        ],
        $data_to_save
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $bonus_id = !empty($data_to_save['id']) ? $data_to_save['id'] : $last_insert_id;

    // d($bonus_id);
    // die();

    // TODO update/insert on Casino review:
    // 1. rating_casino_info
    // + 2. other rating fields - 0.00
    // + 3. created, modified, permalink
    // 4. attach game_type_ratings (after adding/updating them)

    if (!empty($bonus_countries)) {
      $bonus_pod_id = DatabaseService::getField('casino_bonus', 'pod_id');
      $bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');
      $bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');
      $bonus_currency_field_id = DatabaseService::getField('casino_bonus', 'bonus_currency');
      $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
      $casino_bonuses_field_id = DatabaseService::getField('casino', 'casino_bonuses');

      // d($casino_review_pod_id);
      // d($casino_pod_id);
      // die();

      // Casino Bonuses.
      $casino_bonuses_field_info = array(
          'pod_id' => $casino_pod_id,
          'field_id' => $casino_bonuses_field_id,
          'item_id' => $data_to_save['casino_id'],
          'related_item_id' => $bonus_id,
      );

      if (!empty($casino_bonuses_field_info) && !empty($data_to_save['casino_id'])) {
          // sbg_remove_referenced_data_from_pod($bookmaker_reviews_field_info);

          $casino_bonuses_data = array();
          $casino_bonuses_data[] = $casino_bonuses_field_info;

          if (!empty($casino_bonuses_data)) {
            // error_log('<pre>' . print_r($bookmaker_reviews_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_bonuses_data);
          }
      }

      // d($casino_bonuses_field_info);
      // d($casino_bonuses_data);
      // die();

      // Bonus Casino.
      $bonus_casino_field_info = array(
          'pod_id' => $bonus_pod_id,
          'field_id' => $bonus_casino_field_id,
          'item_id' => $bonus_id,
          'related_item_id' => $data_to_save['casino_id'],
      );

      // d($bonus_casino_field_info);
      // die();

      if (!empty($bonus_casino_field_info) && !empty($data_to_save['casino_id'])) {
          // sbg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $casino_data = array();
          $casino_data[] = $bonus_casino_field_info;

          if (!empty($casino_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_data);
          }
      }

      // Bonus Currency.
      $bonus_currency_field_info = array(
          'pod_id' => $bonus_pod_id,
          'field_id' => $bonus_currency_field_id,
          'item_id' => $bonus_id,
          'related_item_id' => $data_to_save['currency_id'],
      );

      if (!empty($bonus_currency_field_info) && !empty($data_to_save['currency_id'])) {
          // sbg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $casino_currency_data = array();
          $casino_currency_data[] = $bonus_currency_field_info;

          if (!empty($casino_currency_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_currency_data);
          }
      }

      // Bonus Countries.
      $bonus_countries_field_info = array(
          'pod_id' => $bonus_pod_id,
          'field_id' => $bonus_countries_field_id,
          'item_id' => $bonus_id,
      );

      // d($bonus_casino_field_info);
      // die();

      if (!empty($bonus_countries_field_info) && !empty($bonus_countries)) {
          // scg_remove_referenced_data_from_pod($rating_countries_field_info);

          $countries_data = array();
          foreach ($bonus_countries as $country_id) {
              $countries_data[] = array(
                  'pod_id' => $bonus_pod_id,
                  'field_id' => $bonus_countries_field_id,
                  'item_id' => $bonus_id,
                  'related_item_id' => $country_id,
              );
          }

          if (!empty($countries_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($countries_data);
          }
      }
    }

    // error_log('<pre>' . print_r($data_to_save['rating_bookmaker'] . ' - ' . $review_id, TRUE) . '</pre>');

    $data_to_save['countries'] = $bonus_countries;


    $data_to_save['casino_name']     = $casino['casino_name'];
    $data_to_save['country_code_lower'] = strtolower(str_replace(' ', '-', $country_code));

    // Map ALL to All Countries.
    if ($data_to_save['country_code_lower'] == 'all') {
      $data_to_save['country_code_lower'] = 'all';
    }

    // Map GB to UK.
    if ($data_to_save['country_code_lower'] == 'gb') {
      $data_to_save['country_code_lower'] = 'uk';
    }

    // Map US to USA.
    if ($data_to_save['country_code_lower'] == 'us') {
      $data_to_save['country_code_lower'] = 'usa';
    }

    if (!empty($bonus_id)) {
      $data_to_save['bonus_id'] = $bonus_id;
    }

    // d($data_to_save);
    // die();

    // Custom Country relation. 
    $country_rel_id = scg_casino_bonus_country_relation_insert_or_update($data_to_save);

    return $bonus_id;
}



function scg_casino_bonus_country_relation_insert_or_update($data_to_save) {
  // Custom Country Relation.
  $current_timestamp = current_time('mysql');

  foreach ($data_to_save['countries'] as $country_id) {
    $country_id = (int) $country_id;

    // d($country_id);
    // die();

    // Let's check if this rating exists first.
    $query = new SelectQuery;
    $query
        ->from('wp_pods_country_relation', 't')
        ->select(['id', 'country_id'])
        ->limit(1);

    $query->where()
        ->equal('t.casino_id', $data_to_save['casino_id'])
        ->equal('t.bonus_id', $data_to_save['bonus_id'])
        ->equal('t.country_id', $country_id);

    $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $existing_country_rel_id = !empty($results) && $results->country_id == $country_id ? (int) $results->id : FALSE;

    // d($existing_country_rel_id);
    // die();

    // Let's check if main review exists.
    $query = new SelectQuery;
    $query
        ->from('wp_pods_casino_review', 't')
        ->select(['id', 'casino_id'])
        ->limit(1);

    $country_casino_filter = strtolower($data_to_save['casino_name']) . '-' . $data_to_save['country_code_lower'];

    $query->where()
        ->equal('t.casino_id', $data_to_save['casino_id'])
        ->equal('t.permalink', $country_casino_filter);

    $main_review_results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $main_review_id = !empty($main_review_results->id) ? (int) $main_review_results->id : FALSE;

    // d($country_casino_filter);
    // d($main_review_id);
    // die();

    $data_to_update = [
        'id'             => $existing_country_rel_id,
        'name'           => $data_to_save['name'],
        'permalink'      => $data_to_save['permalink'],
        'country_id'     => $country_id,
        'modified'       => $current_timestamp,
        'casino_id'      => $data_to_save['casino_id'],
        'main_review_id' => $main_review_id,
        'bonus_id'       => $data_to_save['bonus_id'],
    ];

    if (empty($existing_country_rel_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // d($data_to_update);
    // die();

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_country_relation',
        [
            'id' => $existing_country_rel_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $country_rel_id = !empty($existing_country_rel_id) ? $existing_country_rel_id : $last_insert_id;
    // d($country_rel_id);
    // die();
  }
}







/**
 * Import/update game type links for casino
 * and for particular bonus ID based on country.
 */
function scg_set_casino_game_type_links_from_sheet($casino_bonus_id, $casino_id, $game_type, $aff_link, $country_id, $button_text = '', $regulation_text = '', $enabled, $is_valid_link, $country_ids)
{

    $game_type_id = scg_get_casino_game_type_id_from_name($game_type);

    // Exit early if no
    // game type ID is found.
    if (empty($game_type_id)) {
        return;
    }

    // d($game_type_id);
    // die();

    // Let's check if this casino link (for this country) exists first.
    // $params = array(
    //     'distinct' => FALSE,
    //     'where' =>
    //         "casino_link_casino.id = " . $casino_id . "" . ' AND ' .
    //         'casino_link_countries.id IN (' . $country_id . ')' . ' AND ' .
    //         "casino_link_game.id = " . $game_type_id . "",
    // );

    // $casino_link = pods('casino_link');
    // $casino_link->find($params);
    // $data = $casino_link->data();

    // d($country_ids);
    // die();

    // Get language code by country code.
    $country_code = scg_get_country_code_from_id($country_id);
    $language_code = scg_get_language_code_by_country_code($country_code);

    $casino_link_casino_field_id = DatabaseService::getField('casino_link', 'casino_link_casino');
    $casino_link_countries_field_id = DatabaseService::getField('casino_link', 'casino_link_countries');
    $casino_link_game_type_field_id = DatabaseService::getField('casino_link', 'casino_link_game');

    $query = new SelectQuery;
    $query
        ->from('wp_pods_casino_link', 't')
        ->select(['t.id', 't.casino_link'])
        ->limit(1);

    $query
        ->join(DatabaseService::REL_TABLE, 'link_casino')
        ->on("link_casino.field_id = $casino_link_casino_field_id AND link_casino.item_id = t.id");

    $query
        ->leftJoin(DatabaseService::REL_TABLE, 'link_countries')
        ->on("link_countries.field_id = $casino_link_countries_field_id AND link_countries.item_id = t.id");

    $query
        ->join(DatabaseService::REL_TABLE, 'link_game_type')
        ->on("link_game_type.field_id = $casino_link_game_type_field_id AND link_game_type.item_id = t.id");

    $query->where()
        ->equal('link_casino.related_item_id', $casino_id);

    $query->where()
        ->group()
        ->in('link_countries.related_item_id', $country_ids)
        ->or()
        ->is('link_countries.related_item_id', NULL);

    $query->where()
        ->equal('link_game_type.related_item_id', $game_type_id);

    $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $existing_casino_link_data = !empty($results) ? $results : FALSE;

    // d($results);
    // die();

    $existing_casino_link_id = FALSE;

    if (!empty($existing_casino_link_data)) {
        $existing_casino_link_id = $existing_casino_link_data->id;
        $existing_casino_link = $existing_casino_link_data->casino_link;
    }

    $enabled_status = $enabled == 1;

    // Name generation.
    // $casino_game_pod = pods('casino_game', $game_type_id);
    // $casino_game_name = $casino_game_pod->field('name');

    $site_url = 'https://play.smartcasinoguide.com';
    $casino_game = !empty($game_type) ? ' - ' . $game_type : FALSE;

    // Retain existing casino link if current is invalid.
    if (!$is_valid_link) {
        $aff_link = $existing_casino_link;
    }

    $casino_link_string = $aff_link;
    $casino_link_path_only = str_replace($site_url, '', $casino_link_string);

    $casino_link_status = !empty($enabled_status) ? '' : ' - Išjungta';

    $country_name_value = scg_get_country_name_from_id($country_id);
    $country_name = !empty($country_name_value) ? ' - ' . $country_name_value : '';

    // Generate title.
    $custom_name = $casino_link_path_only . $casino_game . $country_name . $casino_link_status;

    // Prepare data for insertion/update.
    $data = array(
        'name' => $custom_name,
        'casino_link' => $aff_link,
        'casino_link_casino' => $casino_id,
        'casino_link_game' => $game_type_id,
        'casino_link_countries' => $country_ids,
        'casino_link_button_text' => $button_text,
        'casino_link_regulation_text' => $regulation_text,
        'casino_link_status' => $enabled_status,
    );

    // d($data);

    // Disable invalid link.
    if (!$is_valid_link) {
        $data['casino_link_status'] = FALSE;
    }

    // if (!empty($existing_casino_link_id)) {
    //     $casino_link = pods('casino_link', $existing_casino_link_id);

    //     // Save data to existing bonus item.
    //     $casino_link->save($data);
    // } else {
    //     // Otherwise create new bonus item.
    //     $new_casino_link_id = $casino_link->add($data);
    // }

    $permalink = strtolower($data['name']);
    $permalink = str_replace('---', '-', strtolower($permalink));
    $permalink = str_replace(' ', '', strtolower($permalink));
    $current_timestamp = current_time('mysql');


    $data_to_update = [
        'id'                  => $existing_casino_link_id,
        'name'                => $data['name'],
        'permalink'           => $permalink,
        'modified'            => $current_timestamp,
        'casino_link'         => $aff_link,
        'casino_link_status'  => $enabled_status,
        'language'            => $language_code,
        'casino_link_button_text'     => $button_text,
        'casino_link_regulation_text' => $regulation_text,

        // 'casino_id'      => $casino_id,
        // 'game_type_id'   => $game_type_id,
        // 'game_type_name' => $game_type,
    ];

    // d($data_to_update);
    // die();

    if (empty($existing_casino_link_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_casino_link',
        [
            'id' => $existing_casino_link_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $casino_link_id = !empty($existing_casino_link_id) ? $existing_casino_link_id : $last_insert_id;
    $casino_link_id = (int) $casino_link_id;

    // d($casino_link_id);
    // die();

    // 'casino_link_casino' => integer36
    // 'casino_link_game' => integer2
    // 'casino_link_countries' => array(1)

    if (!empty($country_ids)) {

     // $casino_link_casino_field_id = DatabaseService::getField('casino_link', 'casino_link_casino');
     //  $casino_link_countries_field_id = DatabaseService::getField('casino_link', 'casino_link_countries');
     //  $casino_link_game_type_field_id = DatabaseService::getField('casino_link', 'casino_link_game');
      $casino_link_pod_id = DatabaseService::getField('casino_link', 'pod_id');
      $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
      $casino_links_field_id = DatabaseService::getField('casino', 'casino_link_list');

      // d($casino_review_pod_id);
      // d($casino_pod_id);
      // die();

      // Casino Links.
      $casino_links_field_info = array(
          'pod_id' => $casino_pod_id,
          'field_id' => $casino_links_field_id,
          'item_id' => $casino_id,
          'related_item_id' => $casino_link_id,
      );

      // d($casino_links_field_info);
      // die();

      if (!empty($casino_links_field_info) && !empty($casino_id)) {
          // sbg_remove_referenced_data_from_pod($bookmaker_reviews_field_info);

          $casino_links_data = array();
          $casino_links_data[] = $casino_links_field_info;

          if (!empty($casino_links_data)) {
            // error_log('<pre>' . print_r($bookmaker_reviews_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_links_data);
          }
      }

      // Link Casino.
      $link_casino_field_info = array(
          'pod_id' => $casino_link_pod_id,
          'field_id' => $casino_link_casino_field_id,
          'item_id' => $casino_link_id,
          'related_item_id' => $casino_id,
      );

      // d($link_casino_field_info);
      // die();

      if (!empty($link_casino_field_info) && !empty($casino_id)) {
          // sbg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $casino_data = array();
          $casino_data[] = $link_casino_field_info;

          if (!empty($casino_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_data);
          }
      }

      // Link Countries.
      $link_countries_field_info = array(
          'pod_id' => $casino_link_pod_id,
          'field_id' => $casino_link_countries_field_id,
          'item_id' => $casino_link_id,
      );

      // d($link_countries_field_info);
      // die();

      if (!empty($link_countries_field_info) && !empty($country_ids)) {
          // scg_remove_referenced_data_from_pod($rating_countries_field_info);

          $countries_data = array();
          foreach ($country_ids as $country_id) {
              $countries_data[] = array(
                  'pod_id' => $casino_link_pod_id,
                  'field_id' => $casino_link_countries_field_id,
                  'item_id' => $casino_link_id,
                  'related_item_id' => $country_id,
              );
          }

          if (!empty($countries_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($countries_data);
          }
      }

      // Link Game Type.
      $link_game_type_field_info = array(
          'pod_id' => $casino_link_pod_id,
          'field_id' => $casino_link_game_type_field_id,
          'item_id' => $casino_link_id,
          'related_item_id' => $game_type_id,
      );

      if (!empty($link_game_type_field_info) && !empty($game_type_id)) {
          // scg_remove_referenced_data_from_pod($link_game_type_field_info);

          $link_game_type_data = array();
          $link_game_type_data[] = $link_game_type_field_info;

          if (!empty($link_game_type_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($link_game_type_data);
          }
      }
    }

    // d($countries_data);
    // die();

    // error_log('<pre>' . print_r($data_to_save['rating_bookmaker'] . ' - ' . $review_id, TRUE) . '</pre>');

    // $data_to_save['review_id'] = $review_id;

    // Custom Country relation.
    $data_to_save = array(
      'name'         => $data_to_update['name'],
      'permalink'    => $data_to_update['permalink'],
      'link_id'      => $casino_link_id,
      'casino_id'    => $casino_id,
      'game_type_id' => $game_type_id,
      'bonus_id'     => $casino_bonus_id,
    );

    // d($data_to_save);

    $country_rel_id = scg_casino_link_country_relation_insert_or_update($data_to_save, $country_ids);


    // Get casino pod.
    // $params = array(
    //     'distinct' => false,
    //     'where' => "t.id = '" . $casino_bonus_id . "'",
    // );

    // $casino_bonus_pod = pods('casino_bonus', $casino_bonus_id);
    // $casino_bonus_pod->find($params);

    // // Add casino link ID to casino item.
    // if (!empty($casino_link_id)) {
    //     $casino_bonus_pod->add_to('bonus_shortcode_affiliate_links', $casino_link_id);
    // }

  return $casino_link_id;
}


function scg_casino_link_country_relation_insert_or_update($data_to_save, $country_ids) {
  // Custom Country Relation.
  $current_timestamp = current_time('mysql');

  foreach ($country_ids as $country_id) {
    // Let's check if this rating exists first.
    $query = new SelectQuery;
    $query
        ->from('wp_pods_country_relation', 't')
        ->select(['id', 'country_id'])
        ->limit(1);

    $query->where()
        ->equal('t.casino_id', $casino_id)
        ->equal('t.link_id', $data_to_save['link_id'])
        ->equal('t.game_type_id', $data_to_save['game_type_id'])
        ->equal('t.bonus_id', $data_to_save['bonus_id'])
        ->equal('t.country_id', $country_id);

    $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $existing_country_rel_id = !empty($results) && $results->country_id == $country_id ? (int) $results->id : FALSE;

    // d($existing_country_rel_id);
    // die();

    $query_game_type_review = new SelectQuery;
    $query_game_type_review
        ->from('wp_pods_country_relation', 't')
        ->select(['id, game_type_review_id'])
        ->limit(1);

    $query_game_type_review->where()
        ->equal('t.casino_id', $data_to_save['casino_id'])
        ->equal('t.game_type_id', $data_to_save['game_type_id'])
        ->equal('t.country_id', $country_id)
        ->notEqual('t.game_type_review_id', 0)
        ->or()
        ->notEqual('t.game_type_review_id', NULL);

    $game_type_review_results = (new DatabaseService($query_game_type_review, DatabaseService::GET_ITEM))->build();
    $existing_game_type_review_id = !empty($game_type_review_results) ? (int) $game_type_review_results->game_type_review_id : FALSE;

    // d($existing_game_type_review_id);

    $data_to_update = [
        'id'            => $existing_country_rel_id,
        'name'          => $data_to_save['name'],
        'permalink'     => $data_to_save['permalink'],
        'country_id'    => $country_id,
        'modified'      => $current_timestamp,
        'casino_id'     => $data_to_save['casino_id'],
        'bonus_id'      => $data_to_save['bonus_id'],
        'link_id'       => $data_to_save['link_id'],
        'game_type_id'        => $data_to_save['game_type_id'],
        'game_type_review_id' => $existing_game_type_review_id,
    ];

    if (empty($existing_country_rel_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // d($data_to_update);
    // die();

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_country_relation',
        [
            'id' => $existing_country_rel_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $country_rel_id = !empty($existing_country_rel_id) ? $existing_country_rel_id : $last_insert_id;
    // d($country_rel_id);
    // die();
  }
}


/**
 * Import/update game type ratings for casino
 * based on country.
 */
function scg_set_casino_game_type_ratings_from_sheet($casino_review_id, $casino_id, $casino_name, $game_type, $game_type_rating, $country_id, $language_code)
{

    $game_type_id = scg_get_casino_game_type_id_from_name($game_type);

    // Let's check if this casino link (for this country) exists first.
    $params = array(
        'distinct' => FALSE,
        'where' =>
            'casino_game_review_casino.id = "' . $casino_id . '"' . ' AND ' .
            'casino_game_review_countries.id IN (' . $country_id . ')' . ' AND ' .
            "casino_game_review_game.id = " . $game_type_id . "" . ' AND ' .
            'language = "' . $language_code . '"',
    );

    $casino_game_review = pods('casino_game_review');
    $casino_game_review->find($params);
    $data = $casino_game_review->data();

    $existing_casino_game_review_id = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $existing_casino_game_review_id = $data[0]->id;
    }

    $country_name_value = scg_get_country_name_from_id($country_id);
    $language_code_upper = strtoupper($language_code);

    $custom_name = $game_type . '  - ' . $game_type_rating . ' (' . $casino_name . ')' . ' - ' . $language_code_upper;

    // Prepare data for insertion/update.
    $data = array(
        'name' => $custom_name,
        'language' => $language_code,
        'casino_game_review_casino' => $casino_id,
        'casino_game_review_game' => $game_type_id,
        'casino_game_review_countries' => $country_id,
        'casino_game_review_rating' => $game_type_rating,
    );

    // var_dump($data);

    if (!empty($existing_casino_game_review_id)) {
        $casino_game_review = pods('casino_game_review', $existing_casino_game_review_id);

        // Save data to existing game review item.
        $casino_game_review->save($data);
    } else {
        // Otherwise create new game review item.
        $new_casino_game_review_id = $casino_game_review->add($data);
    }

    $casino_game_review_id = !empty($existing_casino_game_review_id) ? $existing_casino_game_review_id : $new_casino_game_review_id;

    // Get casino review pod.
    $casino_review_pod = pods('casino_review', $casino_review_id);

    // Add casino game review ID to casino review pod.
    if (!empty($casino_game_review_id)) {
        $casino_review_pod->add_to('rating_casino_game_reviews', $casino_game_review_id);
    }

    // Get casino pod.
    $casino_pod = pods('casino', $casino_id);

    // Add casino game review ID to casino pod.
    if (!empty($casino_game_review_id)) {
        $casino_pod->add_to('casino_game_review_list', $casino_game_review_id);
    }

    return $casino_game_review_id;
}


/**
 * Get casino review language by user IP.
 */
function scg_casino_trackers_get_casino_review_language($game_type = FALSE)
{
    // Make sure IP2Location database is exist.
    if (!is_file(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'))) {
        return;
    }

    if (!class_exists('IP2Location\\Database')) {
        require_once IP2LOCATION_REDIRECTION_ROOT . 'class.IP2Location.php';
    }

    $ip = custom_get_user_ip_address();

    // Create IP2Location object.
    $db = new \IP2Location\Database(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'), \IP2Location\Database::FILE_IO);

    // Get geolocation by IP address.
    $response = $db->lookup($ip, \IP2Location\Database::ALL);

    // Country variables.
    $country_code = !empty($response['countryCode']) && $response['countryCode'] != '-' ? $response['countryCode'] : FALSE;
    $country_name = !empty($response['countryName']) && $response['countryName'] != '-' ? $response['countryName'] : FALSE;

    // Check if country name has United Kingdom 
    // and shorten it because it's now giving long version.
    $country_name = strpos($country_name, 'United Kingdom') !== FALSE ? 'United Kingdom' : $country_name;

    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    // If we dont have this country bonuses, then show
    // casinos from 'All Countries' (English) rating.
    if (!$have_this_country_bonuses) {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses, then show
    // casinos from this country rating and language.
    if ($have_this_country_bonuses) {
        $casino_review_language = strtolower($country_code);
    }

    // If we have this country bonuses and user's country
    // is SE, then show casinos from Sweden and in English.
    if ($have_this_country_bonuses && $country_code == 'SE') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'TR') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'BG') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is UK, then show casinos from UK and in English.
    if ($have_this_country_bonuses && $country_code == 'GB') {
        $casino_review_language = 'uk';
    }

    // Map EE (Estonia) code to our ET code.
    if ($have_this_country_bonuses && $country_code == 'EE') {
        $casino_review_language = 'et';
    }

    // Set English language as default.
    if (empty($casino_review_language)) {
        $casino_review_language = 'en';
    }

    return $casino_review_language;
}


/**
 * Get user country name by user IP.
 */
function scg_casino_trackers_get_user_country($game_type = FALSE, $ip = FALSE)
{
    // Make sure IP2Location database is exist.
    if (!is_file(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'))) {
        return;
    }

    if (!class_exists('IP2Location\\Database')) {
        require_once IP2LOCATION_REDIRECTION_ROOT . 'class.IP2Location.php';
    }

    if (!$ip) {
        $ip = custom_get_user_ip_address();
    }

    // Create IP2Location object.
    $db = new \IP2Location\Database(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'), \IP2Location\Database::FILE_IO);

    // Get geolocation by IP address.
    $response = $db->lookup($ip, \IP2Location\Database::ALL);

    // Country variables.
    $country_code = !empty($response['countryCode']) && $response['countryCode'] != '-' ? $response['countryCode'] : FALSE;
    $country_name = !empty($response['countryName']) && $response['countryName'] != '-' ? $response['countryName'] : FALSE;

    // Get proper country name.
    $country_name = trim(scg_get_country_name_from_country_code($country_code));

    return $country_name;
}


/**
 * Get sheet row info from casino value.
 */
function scg_google_sheet_get_row_info_by_casino_value($value, $sheetName)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = scg_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'guru-trackers-ratings.xlsx';

    if ($sheetName == 'Restricted Countries') {
        $file_path = $xlsx_directory_path . 'casinos-restricted-countries.xlsx';
    }

    try {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly([$sheetName]);
        $spreadsheet = $reader->load($file_path);

        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $ws = $worksheet->getTitle();

            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                // $cellIterator->setIterateOnlyExistingCells(true);

                foreach ($cellIterator as $cell) {
                    // Check if our value is in cell.
                    if ($cell->getValue() == $value) {
                        $column_index = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($cell->getColumn());
                        $row_index = $cell->getRow() - 1;

                        $foundInCell = array(
                            'row' => $row_index,
                            'column_index' => $column_index,
                        );
                    }
                }
            }
        }

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred: ' . $e->getMessage());
    }

    return $foundInCell;
}


/**
 * Make particular Google Sheet cell red color.
 */
function scg_google_sheet_red_color_cell($row, $column, $sheetName, $mark_type)
{
    // Return early when we are not in production environment.
    // Prevents from confusing errors when production
    // differs from other environments.
    if (WP_ENV == 'staging' || WP_ENV == 'development') {
        return FALSE;
    }

    // Red color codes.
    $r = 230 / 255;
    $g = 184 / 255;
    $b = 175 / 255;
    $a = 1.0;

    // Tell the Google client to use your service account credentials.
    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();

    // Set the scopes required for the API.
    $client->addScope(Google_Service_Drive::DRIVE);
    $client->addScope(Google_Service_Sheets::SPREADSHEETS);

    $spreadsheetID = scg_get_casino_tracker_sheet_file_id();

    if ($sheetName == 'Restricted Countries') {
        $spreadsheetID = scg_get_restricted_countries_sheet_file_id();
    }

    $sheetService = new Google_Service_Sheets($client);
    $spreadSheet = $sheetService->spreadsheets->get($spreadsheetID);
    $sheets_list = $spreadSheet->getSheets();
    $sheets = array();

    foreach ($sheets_list as $sheet) {
        $sheets[] = array(
            'title' => $sheet->properties->title,
            'sheetId' => $sheet->properties->sheetId,
        );
    }

    $sheetId = FALSE;

    if (!empty($sheets)) {
        foreach ($sheets as $sheet) {
            if ($sheet['title'] == $sheetName) {
                $sheetId = $sheet['sheetId'];

                // Stop iterating.
                continue;
            }
        }
    }

    if (isset($sheetId)) {
        $myRange = [
            'sheetId' => $sheetId,
            'startRowIndex' => $row,
            'endRowIndex' => $row + 1,
            'startColumnIndex' => $column - 1,
            'endColumnIndex' => $column,
        ];
    }

    $format = [
        "backgroundColor" => [
            "red" => $r,
            "green" => $g,
            "blue" => $b,
            "alpha" => $a,
        ],
    ];

    // Note text by mark type.
    $note_text_by_type = array(
        'Bad Link' => 'Bloga nuoroda! Tikrinta automatiškai importuojant per SCG: ' . current_time('mysql'),
        'Casino ID/Name' => 'Blogas kazino ID/pavadinimas. Tikrinta automatiškai importuojant per SCG: ' . current_time('mysql'),
        'Crypto Info' => 'Nėra Crypto tekstų. Tikrinta automatiškai importuojant per SCG: ' . current_time('mysql'),
    );

    $note_text = $note_text_by_type[$mark_type];

    try {
        if (!empty($myRange)) {
            $requests = [
                new Google_Service_Sheets_Request([
                    'repeatCell' => [
                        'fields' => 'userEnteredFormat.backgroundColor,note',
                        'range' => $myRange,
                        'cell' => [
                            'userEnteredFormat' => $format,
                            'note' =>
                                $note_text,
                        ],
                    ],
                ]),
            ];
        }

        if (!empty($requests)) {
            $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
                'requests' => $requests
            ]);
        }

        if (!empty($spreadsheetID) && !empty($batchUpdateRequest)) {
            $response = $sheetService->spreadsheets->batchUpdate($spreadsheetID, $batchUpdateRequest);

            if ($response->valid()) {
                // Success - cell has been changed.
                return TRUE;
            }
        }
    } catch (Exception $e) {
        // Something went wrong
        error_log($e->getMessage());
    }

    return FALSE;
}


/**
 * Check if affiliate link is valid.
 */
function scg_check_affiliate_link_is_valid($affiliate_link)
{
    // $ch = curl_init();

    // curl_setopt($ch, CURLOPT_URL, $affiliate_link);

    // // Tell cURL that it should only spend 10 seconds
    // // trying to connect to the URL in question.
    // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

    // // A given cURL operation should only take
    // // 30 seconds max.
    // curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    // curl_setopt($ch, CURLOPT_HEADER, 1);
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $res = curl_exec($ch);

    // $url_opens_properly = FALSE;
    // $url_has_scg_domain = strstr($affiliate_link, 'https://play.smartcasinoguide.com/') !== FALSE;

    // if (curl_getinfo($ch)['http_code'] == 200) {
    //     $url_opens_properly = TRUE;
    // }

    // $url_is_valid = $url_opens_properly && $url_has_scg_domain;

    // return $url_is_valid;
    return TRUE;
}


/**
 * Get casino tracker Google Sheet file ID.
 */
function scg_get_casino_tracker_sheet_file_id()
{
    $sheetFileId = '1894J3iJVdKmNzxRzhkw0BN7s_8w8nuhTMWrQIX_4SeM';

    return $sheetFileId;
}


/**
 * Get casino ratings Google Sheet file ID.
 */
function scg_get_casino_ratings_sheet_file_id()
{
    $sheetFileId = '1XL1U_2F32_-KI4vw4nzV1w9sC7Xf-KhxaYAV2ltP19U';

    return $sheetFileId;
}


/**
 * Get restricted countries Google Sheet file ID.
 */
function scg_get_restricted_countries_sheet_file_id()
{
    $sheetFileId = '1Hy8wtjIUaZR8Br7nNlau2_UUQonaQhNbRWO3daVrWQ4';

    return $sheetFileId;
}


/**
 * Get SxG All Info Google Sheet file ID.
 */
function scg_get_all_info_sheet_file_id()
{
    $sheetFileId = '10PsuzKXBYSwS6HFPZycficDi-cJJeta-wo2vn2af5J8';

    return $sheetFileId;
}


function game_type_test() {
    $game_type = 'Ruletė';
    $game_type_id = scg_get_casino_game_type_id_from_name($game_type);
    var_dump($game_type_id);
    die();
}

// add_action('init', 'game_type_test');


/**
 * Get game type ID from game type name.
 */
function scg_get_casino_game_type_id_from_name($game_type)
{
    if ($game_type == 'TOP') {
        return 99999;
    }

    // Re-map old LT values to EN.
    $game_type_lt_map = array(
        'Ruletė'            => 'Roulette',
        'Blackjack'         => 'BlackJack',
        'Lošimų automatai'  => 'Slots',
        'Others'            => 'Other Games',
        'Loterija'          => 'Lotto',
        'Video poker'       => 'Video Poker',
        'Poker games'       => 'Card Games',
        'Live casino'       => 'Live Casino',
        'Others'            => 'Other Games',
    );

    // Use mapped values if LT, otherwise use EN original value.
    $game_type = isset($game_type_lt_map[$game_type]) ? $game_type_lt_map[$game_type] : $game_type;

    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino_game')
        ->select(['name', 'id'])
        ->limit(1)
        ->where()
        ->equal('name', $game_type);

    return (int) (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Get game type ID from game type name.
 */
function scg_get_casino_game_type_requirement($game_type_id)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino_game')
        ->select(['casino_no_tracker_rating'])
        ->limit(1)
        ->where()
        ->equal('id', $game_type_id);

    return (bool) (new DatabaseService($query, DatabaseService::GET_ITEM, 'casino_no_tracker_rating'))
        ->build() ?: false;
}


/**
 * Get game type link from game type ID.
 */
function scg_get_casino_game_type_link_from_id($game_type_id)
{
    $params = array(
        'distinct' => FALSE,
        'where' =>
            "t.id = '" . $game_type_id . "'",
    );

    $casino_game_type = pods('casino_game');
    $casino_game_type->find($params);
    $data = $casino_game_type->data();

    $game_type_page_id = FALSE;

    if ($casino_game_type->total_found()) {
        while ($casino_game_type->fetch()) {
            $game_type_page = $casino_game_type->field('casino_game_top_rating_page');
            $game_type_page_id = !empty($game_type_page) ? $game_type_page['ID'] : FALSE;
        }
    }

    return $game_type_page_id;
}


/**
 * Get game node type from casino game name.
 */
function scg_get_casino_game_type_data_from_casino_game($casino_game_id)
{
    $casino_game_type_data = FALSE;

    if (!empty($casino_game_id)) {
        $fields = array(
            'casino_game_type.name AS casino_game_type_name',
            'casino_game_type.id AS casino_game_type_id',
        );
        $fields = implode(', ', $fields);
        $params = array(
            'select' => $fields,
            'distinct' => FALSE,
            'where' =>
                "t.id = '" . $casino_game_id . "'",
        );

        $casino_game_node = pods('casino_game_node', $params);
        $data = $casino_game_node->data();

        if ($casino_game_node->total_found()) {
            while ($casino_game_node->fetch()) {
                $casino_game_type_id = $casino_game_node->field('casino_game_type_id');
                $casino_game_type = $casino_game_node->field('casino_game_type_name');

                $casino_game_type_data = array(
                    'casino_game_type_id' => $casino_game_type_id,
                    'casino_game_type' => $casino_game_type,
                );
            }
        }

    }

    return $casino_game_type_data;
}


/**
 * Get game node type from casino game name.
 */
function scg_get_casinos_list_from_casino_game($casino_game_id)
{
    $casino_game_node_casinos = FALSE;

    if (!empty($casino_game_id)) {
        $params = array(
            // 'select' => $fields,
            // 'join' => $sql,
            'distinct' => FALSE,
            'where' =>
                "t.id = '" . $casino_game_id . "'",
        );

        $casino_game_node = pods('casino_game_node', $params);
        $data = $casino_game_node->data();

        if ($casino_game_node->total_found()) {
            while ($casino_game_node->fetch()) {
                $casino_game_node_casinos_raw = $casino_game_node->field('casino_game_node_casinos');

                if (!empty($casino_game_node_casinos_raw)) {
                    foreach ($casino_game_node_casinos_raw as $casino) {
                        $casino_game_node_casinos[] = (int)$casino['id'];
                    }
                }
            }
        }

    }

    return $casino_game_node_casinos;
}


/**
 * Get game node ID from game name.
 */
function scg_get_casino_game_node_id_from_name($game_name)
{
    $game_node_id = FALSE;

    if (!empty($game_name)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                "t.name = '" . $game_name . "'",
        );

        $casino_game_node = pods('casino_game_node');
        $casino_game_node->find($params);
        $data = $casino_game_node->data();

        if (!empty($data) && !empty($data[0])) {
            $game_node_id = $data[0]->id;
        }
    }

    return $game_node_id;
}


/**
 * Get payment method ID from method name.
 */
function scg_get_payment_method_id_from_name($name)
{
    // Escape single quote.
    $name = str_replace("'", "\'", $name);

    $query = new SelectQuery;

    $query
        ->from('wp_pods_payment_method')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $name);

    $payment_method_id = (int) (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;

    return $payment_method_id;
}


/**
 * Get crypto ID from name.
 */
function scg_get_crypto_id_from_name($name)
{
    $params = array(
        'distinct' => FALSE,
        'where' =>
            "t.name = '" . $name . "'" . " OR " . "t.short_name = '" . $name . "'"
    );

    $crypto = pods('cryptocurrency');
    $crypto->find($params);
    $data = $crypto->data();

    $crypto_id = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $crypto_id = (int)$data[0]->id;
    }

    return $crypto_id;
}


/**
 * Get game provider ID from name.
 */
function scg_get_game_provider_id_from_name($name)
{
    $name = str_replace("'", "\'", $name);

    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino_game_provider')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $name);

    $id = (int) (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build();

    return !empty($id) ? $id : false;
}


/**
* Get country IDs from country names.
*/
function scg_get_country_ids_from_country_names($countries) {
  $query = new SelectQuery;
  $query
      ->from('wp_pods_country')
      ->select(['id'])
      ->limit(0)
      ->where()
      ->in('name', $countries);

  $results = (new DatabaseService($query, DatabaseService::GET_DATA))->build();
  $country_ids = array();

  if (!empty($results)) {
    foreach ($results as $entry) {
      $country_ids[] = (int) $entry->id;
    }
  }

  return $country_ids;
}


/**
 * Get country ID from country name.
 */
function scg_get_country_id_from_name($country_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['name', 'id', 'name_other'])
        ->orderBy('name', 'ASC')
        ->orderBy('id', 'ASC')
        ->limit(1)
        ->where()
        ->equal('name', $country_name)
        ->or()
        ->equal('name_other', $country_name);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Get country ID from country code.
 */
function scg_get_country_id_from_country_code($country_code)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('country_code', $country_code);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
* Get country code from country ID.
*/
function scg_get_country_code_from_country_id($country_id) {
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['country_code'])
        ->limit(1)
        ->where()
        ->equal('id', $country_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'country_code'))
        ->build() ?: false;
}


/**
 * Get country code from country name.
 */
function scg_get_country_code_from_name($country_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['name', 'country_code'])
        ->orderBy('name', 'ASC')
        ->orderBy('id', 'ASC')
        ->limit(1)
        ->where()
        ->equal('name', $country_name);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'country_code'))
        ->build() ?: false;
}


/**
 * Get country name from country code.
 */
function scg_get_country_name_from_country_code($country_code)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['name'])
        ->limit(1)
        ->where()
        ->equal('country_code', $country_code);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'name'))
        ->build() ?: false;
}


/**
 * Get country code from country ID.
 */
function scg_get_country_code_from_id($country_id)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['country_code'])
        ->limit(1)
        ->where()
        ->equal('id', $country_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'country_code'))
        ->build() ?: false;
}


/**
 * Get country name from country ID.
 */
function scg_get_country_name_from_id($country_id)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_country')
        ->select(['name', 'id'])
        ->orderBy('name', 'ASC')
        ->orderBy('id', 'ASC')
        ->limit(1)
        ->where()
        ->equal('id', $country_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'name'))
        ->build() ?: false;
}


/**
 * Get language ID from name.
 */
function scg_get_language_id_from_name($language_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_language')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $language_name);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Get/create license ID from name.
 */
function scg_get_or_create_license_id_from_name($license_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_license')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $license_name);

    $license_id = (int) (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;

    // Create license if license ID not found.
    if (empty($license_id)) {
        $license_add = pods('license');

        // To add a new item, let's set the data first
        $data = array(
            'name' => $license_name,
        );

        // Add the new item now and get the new ID
        $license_id = $license_add->add($data);
    }

    return $license_id;
}


/**
 * Get country IDs based on bonus country.
 * If we that country bonuses - then use them.
 * Otherwise, return 'All Countries' country ID.
 */
function scg_get_country_ids_based_on_bonus_country($country_name = FALSE)
{
    $country_name = empty($country_name) ? scg_casino_trackers_get_user_country() : $country_name;
    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    // var_dump($country_name);
    // var_dump($have_this_country_bonuses);

    $country_id = scg_get_country_id_from_name($country_name);
    $original_country_id = $country_id;

    if (!empty($have_this_country_bonuses)) {
        // Get country ID using pods query.
        $country_id = $country_id;
    } else {
        // Use "All Countries" country.
        $country_id = scg_get_country_id_from_name('All Countries');
    }

    // Use "All Countries" ID when country_id is empty.
    $country_id = empty($country_id) ? scg_get_country_id_from_name('All Countries') : $country_id;
    $original_country_id = empty($original_country_id) ? scg_get_country_id_from_name('All Countries') : $original_country_id;
    $original_country_id = (string)$original_country_id;

    $country_ids = array(
        'country_id' => $country_id,
        'original_country_id' => $original_country_id,
    );

    return $country_ids;
}


/**
 * Get currency sign from currency ID.
 */
function scg_get_currency_sign_from_currency_id($currency_id)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_currency')
        ->select(['currency_sign'])
        ->limit(1)
        ->where()
        ->equal('id', $currency_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'currency_sign'))
        ->build() ?: false;
}


/**
 * Get currency ID from currency sign.
 */
function scg_get_currency_id_from_currency_sign($currency_sign)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_currency')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('currency_sign', $currency_sign);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Get currency ID from currency short name.
 */
function scg_get_currency_id_from_currency_name($currency_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_currency')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $currency_name);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Get casino name from casino ID.
 */
function scg_get_casino_name_from_id($casino_id)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino')
        ->select(['name'])
        ->limit(1)
        ->where()
        ->equal('id', $casino_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'name'))
        ->build() ?: false;
}


/**
 * Get casino ID from name.
 */
function scg_get_casino_id_from_name($casino_name)
{
    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino')
        ->select(['id'])
        ->limit(1)
        ->where()
        ->equal('name', $casino_name);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}


/**
 * Change casino (by ID) pod value.
 */
function scg_change_casino_value($casino_id, $data)
{
    $casino_pods = pods('casino', $casino_id);

    if (!empty($data)) {
        $casino_pods->save($data);
    }
}


/**
 * Get language code by country code.
 */
function scg_get_language_code_by_country_code($country_code)
{
    // Custom language code map.
    $language_code_map = array(
        'ALL' => 'en',
        'EE' => 'et',
        'GB' => 'uk',
        'SE' => 'en',
        'TR' => 'en',
        'BG' => 'en',
    );

    $language_code = !empty($language_code_map[$country_code]) ? $language_code_map[$country_code] : strtolower($country_code);

    return $language_code;
}


/**
 * Get languages list from pod.
 */
function scg_get_languages_list_from_pod()
{
    // Get languages field from casino_info pod.
    $casino_info_pod = pods('casino_info');
    $languages_field = $casino_info_pod->fields('language');

    // Language field options.
    $languages_options_raw = $languages_field['options']['pick_custom'];
    $languages_options_tmp = explode("\n", $languages_options_raw);

    $languages = array();
    if (!empty($languages_options_tmp)) {
        foreach ($languages_options_tmp as $language) {
            $language_array = explode("|", $language);

            $language_code = !empty($language_array) ? $language_array[0] : FALSE;

            $languages[] = $language_code;
        }
    }

    return $languages;
}


/**
 * Check if we actually have casino
 * with given name and ID.
 */
function scg_check_if_casino_and_id_matches($casino_id, $casino_name)
{
    $casino_id = trim($casino_id);
    $casino_name = trim($casino_name);

    if (!empty($casino_id) && !empty($casino_name)) {
      $query = new SelectQuery;

      $query
          ->from('wp_pods_casino')
          ->select(['id'])
          ->limit(1)
          ->where()
          ->equal('id', $casino_id)
          ->and()
          ->equal('name', $casino_name);

      $casino_item = (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
          ->build() ?: false;

    }

    $casino_found = !empty($casino_item);

    return $casino_found;
}


/**
 * Get casino trackers sheet name by select value (ID).
 */
function scg_get_casino_trackers_sheet_name_by_value($sheet_value)
{
    $selected_sheets = get_option('scg-casino-trackers-settings-selected-sheets');
    $sheet_key = scg_casino_trackers_search_for_array_item($sheet_value, $selected_sheets);
    $sheet_name = $selected_sheets[$sheet_key]['title'];

    return $sheet_name;
}


/**
 * Casino trackers search array key by sheet ID.
 */
function scg_casino_trackers_search_for_array_item($sheetId, $array)
{
    $sheetId = (int)$sheetId;

    if (!empty($array)) {
        foreach ($array as $key => $val) {
            if ($val['sheetId'] === $sheetId) {
                return $key;
            }
        }
    }

    return null;
}


/**
 * Get casino bonus link by Casino ID,
 * Current country, link type and game type.
 */
function scg_casino_trackers_get_casino_bonus_link($casino_id, $country, $link_type, $game_type = FALSE)
{
    // Get country ID using pods query.
    $country_id = scg_get_country_id_from_name(trim($country));

    // Let's check if this bonus exists first.
    if (!empty($country_id) && !empty($casino_id)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                "bonus_casino.id = " . $casino_id . "" . ' AND ' .
                'bonus_countries.id IN (' . $country_id . ')',
            // "bonus_currency.id = " . $currency_id . "",
        );

        $casino_bonus = pods('casino_bonus');
        $casino_bonus->find($params);
        $bonus_data = $casino_bonus->data();
    }

    $link_data = FALSE;

    if (!empty($bonus_data) && !empty($bonus_data[0])) {
        $top_casino_link = $bonus_data[0]->bonus_list_affiliate_link;
        $top_casino_button_text = $bonus_data[0]->bonus_list_button_text;
        $top_casino_regulation_text = $bonus_data[0]->bonus_list_regulation_text;

        $review_casino_link = $bonus_data[0]->bonus_review_affiliate_link;
        $review_casino_button_text = $bonus_data[0]->bonus_review_button_text;
        $review_casino_regulation_text = $bonus_data[0]->bonus_review_regulation_text;

        $bonus_text = $bonus_data[0]->bonus_text;
        $bonus_enabled = $bonus_data[0]->bonus_enabled;

        if ($link_type == 'TOP') {
            $link_data = array(
                'link' => $top_casino_link,
                'button_text' => $top_casino_button_text,
                'regulation_text' => $top_casino_regulation_text,
            );
        }

        if ($link_type == 'Review') {
            $link_data = array(
                'link' => $review_casino_link,
                'button_text' => $review_casino_button_text,
                'regulation_text' => $review_casino_regulation_text,
            );
        }

        if ($link_type == 'Game type' && !empty($game_type)) {
            $game_type_id = scg_get_casino_game_type_id_from_name($game_type);

            $params = array(
                'distinct' => FALSE,
                'where' =>
                    "casino_link_casino.id = " . $casino_id . "" . ' AND ' .
                    'casino_link_countries.id IN (' . $country_id . ')' . ' AND ' .
                    "casino_link_game.id = " . $game_type_id . "",
            );

            $casino_link = pods('casino_link');
            $casino_link->find($params);
            $game_type_link_data = $casino_link->data();

            $game_type_link = FALSE;
            $game_type_button_text = FALSE;
            $game_type_regulation_text = FALSE;

            if (!empty($game_type_link_data) && !empty($game_type_link_data[0])) {
                $game_type_link = $game_type_link_data[0]->casino_link;
                $game_type_button_text = $game_type_link_data[0]->casino_link_button_text;
                $game_type_regulation_text = $game_type_link_data[0]->casino_link_regulation_text;

                $link_data = array(
                    'link' => $game_type_link,
                    'button_text' => $game_type_button_text,
                    'regulation_text' => $game_type_regulation_text,
                );
            }
        }

        // Add bonus text and enabled field values.
        if (!empty($link_data)) {
            $link_data['bonus_text'] = $bonus_text;
            $link_data['bonus_enabled'] = $bonus_enabled;
        }
    }

    return $link_data;
}


/**
 * Get casino info ID by language.
 */
function scg_casino_trackers_get_casino_info_id_old($casino_id, $language)
{
    $casino_info_id = FALSE;

    // Let's check if casino info is enabled.
    if (!empty($casino_id) && !empty($language)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'casino_info_casino.id = "' . $casino_id . '"' . ' AND ' .
                't.language = "' . $language . '"',
            'limit' => 1,
        );

        $casino_info = pods('casino_info');
        $casino_info->find($params);
        $casino_info_data = $casino_info->data();

        if (!empty($casino_info_data) && !empty($casino_info_data[0])) {
            $casino_info_id = (int)$casino_info_data[0]->id;
        }

    }

    return $casino_info_id;
}

/**
* Get casino info ID.
*/
function scg_casino_trackers_get_casino_info_id($casino_id, $language = FALSE) {
    $query = new SelectQuery;

    $query
        ->from('wp_pods_casino_info', 't')
        ->select(['id']);
      $query
        ->limit(1)
        ->where()
        ->equal('t.casino_id', $casino_id);

    return (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
        ->build() ?: false;
}

/**
 * Get casino link by casino_id, game type, country_id.
 */
// function scg_casino_trackers_get_casino_link($casino_id, $game_type_id, $country_id)
// {
//     $casino_link_data = FALSE;

//     if (!empty($casino_id) && !empty($game_type_id)) {

//         $query = new SelectQuery;

//         $query
//             ->select(['casino_link_list.casino_link as casino_link'])
//             ->from('wp_pods_casino', 't');

//         $casino_link_list_field_id = DatabaseService::getField('casino', 'casino_link_list');
//         $query
//             ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list')
//             ->on("rel_casino_link_list.field_id = $casino_link_list_field_id AND rel_casino_link_list.item_id = t.id");
//         $query
//             ->leftJoin('wp_pods_casino_link', 'casino_link_list')
//             ->on("casino_link_list.id = rel_casino_link_list.related_item_id");

//         $casino_link_countries_field_id = DatabaseService::getField('casino_link', 'casino_link_countries');
//         $query
//             ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_countries')
//             ->on("rel_casino_link_list_casino_link_countries.field_id = $casino_link_countries_field_id AND rel_casino_link_list_casino_link_countries.item_id = casino_link_list.id");
//         $query
//             ->leftJoin('wp_pods_country', 'casino_link_list_casino_link_countries')
//             ->on("casino_link_list_casino_link_countries.id = rel_casino_link_list_casino_link_countries.related_item_id");

//         $casino_link_game_field_id = DatabaseService::getField('casino_link', 'casino_link_game');
//         $query
//             ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_game')
//             ->on("rel_casino_link_list_casino_link_game.field_id = $casino_link_game_field_id AND rel_casino_link_list_casino_link_game.item_id = casino_link_list.id");
//         $query
//             ->leftJoin('wp_pods_casino_game', 'casino_link_list_casino_link_game')
//             ->on("casino_link_list_casino_link_game.id = rel_casino_link_list_casino_link_game.related_item_id");

//         $query
//             ->where()
//             ->equal('t.id', $casino_id)
//             ->equal('casino_link_list_casino_link_countries.id', $country_id)
//             ->equal('casino_link_list.casino_link_status', 1)
//             ->equal('casino_link_list_casino_link_game.id', $game_type_id);

//         $query
//             ->groupBy('t.id')
//             ->orderBy('t.name')
//             ->orderBy('t.id');

//         $data = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

//         if ($data) {
//             $casino_link_data = $data->casino_link;
//         }
//     }

//     return $casino_link_data;
// }


/**
 * Get casino link by casino_id, game type, country_id.
 */
function scg_casino_trackers_get_casino_link($casino_id, $game_type_id, $country_id)
{
    $casino_link_data = FALSE;
    $game_type_id = (int) $game_type_id;

    if (!empty($casino_id) && !empty($game_type_id)) {

        $query = new SelectQuery;

        $query
            ->select([
                'game_type_link.casino_link AS casino_link',
            ])
            ->from('wp_pods_country_relation', 't');

        $query
            ->leftJoin('wp_pods_casino_link', 'game_type_link')
            ->on('t.link_id = game_type_link.id');

        $query
            ->where()
            ->equal('t.game_type_id', $game_type_id)
            ->equal('t.country_id', $country_id)
            ->isNot('t.game_type_review_id', NULL)
            ->isNot('t.link_id', NULL)
            ->equal('game_type_link.casino_link_status', 1);

        if (!empty($casino_id)) {
          $query
              ->where()
              ->equal('t.casino_id', $casino_id);
        }

        $data = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

        if ($data) {
            $casino_link_data = $data->casino_link;
        }
    }

    return $casino_link_data;
}



/**
 * Get casino main info.
 */
function scg_casino_trackers_get_casino_main_info($casino_id)
{
    if (!empty($casino_id)) {
        $casino_main_info = get_transient('scg_casino_main_info_' . $casino_id);

        if (!empty($casino_main_info)) {
          return $casino_main_info;
        }

        $query = new SelectQuery;

        $query
            ->select([
                't.*',
                'casino.id AS casino_id',
            ])
            ->from('wp_pods_casino_main_info', 't');

        $casino_main_info_casino_field_id = DatabaseService::getField('casino_main_info', 'casino');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino')
            ->on("rel_casino.field_id = $casino_main_info_casino_field_id AND rel_casino.item_id = t.id");
        $query
            ->leftJoin('wp_pods_casino', 'casino')
            ->on("casino.id = rel_casino.related_item_id");

        $query
            ->where()
            ->equal('casino.id', $casino_id);

        $data = (new DatabaseService($query))->build();

        $casino_main_info = FALSE;

        if ($data) {
            $casino_main_info_id = (int) $data[0]->id;
            $casino_established_date = (int) $data[0]->established_date;
            $casino_video_review_link = $data[0]->video_review_link;
            $casino_background_color = $data[0]->background_color;

            // TOP countries.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'country.name',
                    'country.country_code',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $top_countries_field_id = DatabaseService::getField('casino_main_info', 'top_countries');

            $query
                ->leftJoin('wp_pods_country', 'country')
                ->on("country.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $top_countries_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $casino_top_countries_raw = (new DatabaseService($query))->build();
            $casino_top_countries = array();

            if (!empty($casino_top_countries_raw)) {
                foreach ($casino_top_countries_raw as $country) {
                    $casino_top_countries[] = array(
                        'country_name' => $country->name,
                        'country_code' => $country->country_code,
                        'country_code_lower' => strtolower($country->country_code),
                    );
                }
            }

            // Licenses.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'license.name',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $licenses_field_id = DatabaseService::getField('casino_main_info', 'licenses');

            $query
                ->leftJoin('wp_pods_license', 'license')
                ->on("license.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $licenses_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $licenses_raw = (new DatabaseService($query))->build();
            $licenses = array();

            if (!empty($licenses_raw)) {
                foreach ($licenses_raw as $license) {
                    $top_rating_page_url = scg_casino_trackers_get_top_rating_page_url_from_pod('license', $license->id);

                    $licenses[] = array(
                        'license_id' => (int) $license->id,
                        'license_name' => $license->name,
                        'top_rating_page_url' => $top_rating_page_url,
                    );
                }
            }

            // Game Providers.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'game_provider.name',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $game_providers_field_id = DatabaseService::getField('casino_main_info', 'game_providers');

            $query
                ->leftJoin('wp_pods_casino_game_provider', 'game_provider')
                ->on("game_provider.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $game_providers_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $game_providers_raw = (new DatabaseService($query))->build();
            $game_providers = array();

            if (!empty($game_providers_raw)) {
                foreach ($game_providers_raw as $entry) {
                    $game_providers[] = array(
                        'id' => (int) $entry->id,
                        'name' => $entry->name,
                    );
                }
            }

            // Main Rating ID.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $main_rating_field_id = DatabaseService::getField('casino_main_info', 'main_rating');

            $query
                ->where()
                ->equal('rel.field_id', $main_rating_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $main_rating_raw_id = (int) (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))->build();
            $main_rating_id = !empty($main_rating_raw_id) ? $main_rating_raw_id : FALSE;

            // Products
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'product.name',
                    'product.description',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $products_field_id = DatabaseService::getField('casino_main_info', 'products');

            $query
                ->leftJoin('wp_pods_product', 'product')
                ->on("product.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $products_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $products_raw = (new DatabaseService($query))->build();
            $products = array();

            if (!empty($products_raw)) {
                foreach ($products_raw as $entry) {
                    $products[] = array(
                        'id' => (int) $entry->id,
                        'name' => $entry->name,
                        'description' => $entry->description,
                    );
                }
            }

            // Deposit Methods.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'payment_method.name',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $deposit_methods_field_id = DatabaseService::getField('casino_main_info', 'deposit_methods');

            $query
                ->leftJoin('wp_pods_payment_method', 'payment_method')
                ->on("payment_method.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $deposit_methods_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $deposit_methods_raw = (new DatabaseService($query))->build();
            $deposit_methods = array();

            if (!empty($deposit_methods_raw)) {
                foreach ($deposit_methods_raw as $entry) {
                    $deposit_methods[] = array(
                        'id' => (int) $entry->id,
                        'name' => $entry->name,
                    );
                }
            }

            // Cryptos deposits.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'cryptocurrency.name',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $crypto_deposits_field_id = DatabaseService::getField('casino_main_info', 'crypto_deposits');

            $query
                ->leftJoin('wp_pods_cryptocurrency', 'cryptocurrency')
                ->on("cryptocurrency.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $crypto_deposits_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $crypto_methods_raw = (new DatabaseService($query))->build();
            $crypto_methods = array();

            if (!empty($crypto_methods_raw)) {
                foreach ($crypto_methods_raw as $entry) {
                    $crypto_methods[] = array(
                        'id' => (int) $entry->id,
                        'name' => $entry->name,
                    );
                }
            }

            // Owner top rating page.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $owner_top_rating_field_id = DatabaseService::getField('casino_main_info', 'owner_top_rating_page');

            $query
                ->where()
                ->equal('rel.field_id', $owner_top_rating_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $owner_top_rating_page_raw = (new DatabaseService($query))->build();

            $owner_top_rating_page_url = FALSE;

            if (!empty($owner_top_rating_page_raw)) {
                $owner_top_rating_page_url = get_permalink($owner_top_rating_page_raw[0]->id);
            }

            // Email Languages.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'language.name',
                    'language.language_code',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $email_languages_field_id = DatabaseService::getField('casino_main_info', 'email_support_languages');

            $query
                ->leftJoin('wp_pods_language', 'language')
                ->on("language.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $email_languages_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $email_languages_raw = (new DatabaseService($query))->build();
            $email_languages = array();

            if (!empty($email_languages_raw)) {
                foreach ($email_languages_raw as $language) {
                    $language_id = (int)$language->id;
                    $language_countries = scg_casino_trackers_get_language_countries($language_id);
                    $language_country_id = !empty($language_countries) ? reset($language_countries) : FALSE;
                    $language_country_code = scg_get_country_code_from_id($language_country_id);
                    $language_country_code = strtolower($language_country_code);

                    $email_languages[] = array(
                        'language_name' => $language->name,
                        'language_code' => $language->language_code,
                        'language_code_lower' => strtolower($language->language_code),
                        'language_countries' => $language_countries,
                        'language_country_code' => $language_country_code,
                    );
                }
            }

            // Website Languages.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'language.name',
                    'language.language_code',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $website_languages_field_id = DatabaseService::getField('casino_main_info', 'websites_languages');

            $query
                ->leftJoin('wp_pods_language', 'language')
                ->on("language.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $website_languages_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $website_languages_raw = (new DatabaseService($query))->build();
            $website_languages = array();

            if (!empty($website_languages_raw)) {
                foreach ($website_languages_raw as $language) {
                    $language_id = (int)$language->id;
                    $language_countries = scg_casino_trackers_get_language_countries($language_id);
                    $language_country_id = !empty($language_countries) ? reset($language_countries) : FALSE;
                    $language_country_code = scg_get_country_code_from_id($language_country_id);
                    $language_country_code = strtolower($language_country_code);

                    $website_languages[] = array(
                        'language_name' => $language->name,
                        'language_code' => $language->language_code,
                        'language_code_lower' => strtolower($language->language_code),
                        'language_countries' => $language_countries,
                        'language_country_code' => $language_country_code,
                    );
                }
            }

            // Chat Languages.
            $query = new SelectQuery;

            $query
                ->select([
                    'rel.related_item_id AS id',
                    'language.name',
                    'language.language_code',
                ])
                ->from(DatabaseService::REL_TABLE, 'rel');

            $chat_languages_field_id = DatabaseService::getField('casino_main_info', 'chat_languages');

            $query
                ->leftJoin('wp_pods_language', 'language')
                ->on("language.id = rel.related_item_id");

            $query
                ->where()
                ->equal('rel.field_id', $chat_languages_field_id)
                ->equal('rel.item_id', $casino_main_info_id);

            $query
                ->orderBy('rel.weight', 'ASC');

            $chat_languages_raw = (new DatabaseService($query))->build();
            $chat_languages = array();

            if (!empty($chat_languages_raw)) {
                foreach ($chat_languages_raw as $language) {
                    $language_id = (int)$language->id;
                    $language_countries = scg_casino_trackers_get_language_countries($language_id);
                    $language_country_id = !empty($language_countries) ? reset($language_countries) : FALSE;
                    $language_country_code = scg_get_country_code_from_id($language_country_id);
                    $language_country_code = strtolower($language_country_code);

                    $chat_languages[] = array(
                        'language_name' => $language->name,
                        'language_code' => $language->language_code,
                        'language_code_lower' => strtolower($language->language_code),
                        'language_countries' => $language_countries,
                        'language_country_code' => $language_country_code,
                    );
                }
            }

            $casino_main_info = array(
                'owner_top_rating_page_url' => $owner_top_rating_page_url,
                'casino_established_date' => $casino_established_date,
                'casino_licenses' => $licenses,
                'casino_email_languages' => $email_languages,
                'casino_website_languages' => $website_languages,
                'casino_chat_languages' => $chat_languages,
                'casino_top_countries' => $casino_top_countries,
                'casino_game_providers' => $game_providers,
                'casino_deposit_methods' => $deposit_methods,
                'casino_crypto_deposits' => $crypto_methods,
                'casino_products' => $products,
                'casino_video_review_link' => $casino_video_review_link,
                'casino_background_color' => $casino_background_color,
                'casino_main_rating_id' => $main_rating_id,
            );


            // $bookmaker_main_info = array(
            //     + 'owner_top_rating_page_url' => $owner_top_rating_page_url,
            //     + 'bookmaker_established_date' => $bookmaker_established_date,
            //     + 'bookmaker_licenses' => $licenses,
            //     + 'bookmaker_email_languages' => $email_languages,
            //     + 'bookmaker_website_languages' => $website_languages,
            //     + 'bookmaker_chat_languages' => $chat_languages,
            //     + 'bookmaker_top_countries' => $bookmaker_top_countries,
            //     + 'bookmaker_deposit_methods' => $deposit_methods,
            //     + 'bookmaker_crypto_deposits' => $crypto_methods,
            //     'bookmaker_wallpaper_image' => $wallpaper_image_src,
            //     'bookmaker_video_review_link' => $bookmaker_video_review_link,
            // );


        // + $game_provider_id = !empty($game_provider_name) ? scg_get_game_provider_id_from_name($game_provider_name) : FALSE;
        // + $license_id = !empty($license_name) ? scg_get_or_create_license_id_from_name($license_name) : FALSE;
        // + $deposit_method_id = !empty($deposit_method) ? scg_get_payment_method_id_from_name($deposit_method) : FALSE;
        // + $crypto_deposit_id = !empty($crypto_deposit) ? scg_get_crypto_id_from_name($crypto_deposit) : FALSE;
 
        // ATSKIRAI: $tracker_game_type_id = !empty($tracker_game_type) ? (int) scg_get_casino_game_type_id_from_name($tracker_game_type) : FALSE;
        // ATSKIRAI: $established_from = $extra_data['scg_all_info']['established_from'];



        }

        $cache_casino_main_info = set_transient('scg_casino_main_info_' . $casino_id, $casino_main_info, 31 * 24 * 60 * 60);
    }

    return $casino_main_info;
}


/**
 * Get language countries.
 */
function scg_casino_trackers_get_language_countries($id)
{
    $params = array(
        'distinct' => FALSE,
        'where' =>
            "t.id = '" . $id . "'",
    );

    $pods = pods('language');
    $pods->find($params);

    $countries = array();

    if ($pods->total_found()) {
        while ($pods->fetch()) {
            $countries_raw = $pods->field('countries');

            if (!empty($countries_raw)) {
                foreach ($countries_raw as $country) {
                    $countries[] = (int)$country['id'];
                }
            }
        }
    }

    return $countries;
}


/**
 * Get active languages.
 */
function scg_get_active_languages()
{
    $languages = apply_filters('wpml_active_languages', NULL, 'orderby=id&order=desc');

    return $languages;
}


/**
 * Get casino EN review information.
 */
function scg_get_casino_en_review_post($casino_id)
{
    global $wpdb;

//  $fields = array(
//    't.id AS review_id',
//  );

//  $fields = implode( ', ', $fields );

//  $args = array(
//    'select' => $fields,
//    'limit' => '1',
//    'where' =>
//      'casino.id = "' . $casino_id . '"',
//    'distinct' => FALSE,
//    'orderby'  => null,
//  );

    $sql = "
       SELECT
            t.id AS review_id
            FROM `wp_posts` AS `t`

                LEFT JOIN `wp_podsrel` AS `rel_casino` ON
                    `rel_casino`.`field_id` = 19900
                    AND `rel_casino`.`item_id` = `t`.`ID`

                LEFT JOIN `wp_pods_casino` AS `casino` ON
                    `casino`.`id` = `rel_casino`.`related_item_id`


                    -- LEFT JOIN `wp_icl_translations` AS `wpml_translations`
                    --     ON `wpml_translations`.`element_id` = `t`.`ID`
                    --         AND `wpml_translations`.`element_type` = 'post_video_review'
                    --         AND `wpml_translations`.`language_code` = 'en'


                    -- LEFT JOIN `wp_icl_languages` AS `wpml_languages`
                    --     ON `wpml_languages`.`code` = `wpml_translations`.`language_code` AND `wpml_languages`.`active` = 1

            LEFT JOIN `wp_pods_video_review` AS `d` ON `d`.`id` = `t`.`ID`
            WHERE ( ( `casino`.`id` = $casino_id ) AND ( `t`.`post_type` = 'video_review' ) AND ( `t`.`post_status` IN ( 'publish' ) ) )


            ORDER BY `t`.`menu_order`, `t`.`post_title`, `t`.`post_date`
            LIMIT 0, 1;
  ";

    $data = $wpdb->get_results($sql);

    $review_id = FALSE;
    if ($data) {
        foreach ($data as $item) {
            $review_id = (int)$item->review_id;
        }
    }

    if (!empty($review_id)) {
        $review_en_id = !empty($review_id) ? icl_object_id($review_id, 'post', true, 'en') : FALSE;
    }

    $review_en_data = FALSE;
    if (!empty($review_en_id)) {
        $review_en_link = !empty($review_en_id) ? get_permalink($review_en_id) : FALSE;
        $review_en_with_video = !empty($review_en_link) ? scg_casino_info_review_with_video($review_en_id) : FALSE;

        $review_en_data = array(
            'review_en_id' => $review_en_id,
            'review_en_link' => $review_en_link,
            'review_en_with_video' => $review_en_with_video,
        );
    }

    return $review_en_data;
}


/**
 * Get casino info.
 */
function scg_casino_trackers_get_casino_info($casino_info_id)
{
    $query = new SelectQuery;

    if (!empty($casino_info_id)) {

        $query
            ->select([
                't.*',
                'casino.id AS casino_id',
                'video_review.id as video_review_id',
            ])
            ->from('wp_pods_casino_info', 't');

        $casino_info_casino_field_id = DatabaseService::getField('casino_info', 'casino_info_casino');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino')
            ->on("rel_casino.field_id = $casino_info_casino_field_id AND rel_casino.item_id = t.id");
        $query
            ->leftJoin('wp_pods_casino', 'casino')
            ->on("casino.id = rel_casino.related_item_id");

        $casino_info_video_review_field_id = DatabaseService::getField('casino_info', 'casino_info_video_review');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_video_review')
            ->on("rel_video_review.field_id = $casino_info_video_review_field_id AND rel_video_review.item_id = t.id");
        $query
            ->leftJoin('wp_pods_video_review', 'video_review')
            ->on("video_review.id = rel_video_review.related_item_id");

        $query
            ->where()
            ->equal('t.id', $casino_info_id);

        $query
            ->orderBy('t.name', 'ASC')
            ->orderBy('t.id', 'ASC')
            ->limit(15);

        $data = (new DatabaseService($query))->build();

        // d($data);
        // die();

        $casino_info = FALSE;

        if ($data) {
            foreach ($data as $item) {
                $casino_info_new_casino = (bool)$item->casino_info_new_casino;
                $casino_info_new_text = $item->casino_info_new_text;
                $casino_info_new_text = !empty($casino_info_new_text) ? str_replace('"', '\'', $casino_info_new_text) : FALSE;
                $casino_info_new_text = !empty($casino_info_new_text) ? wpautop($casino_info_new_text) : FALSE;

                $casino_info_featured = $item->casino_info_featured;
                $casino_info_terms = $item->casino_info_terms;
                $casino_info_color = $item->casino_info_color;
                $casino_info_button_text_color = $item->casino_info_button_text_color;
                $casino_info_pros = $item->casino_info_pros;
                $casino_info_cons = $item->casino_info_cons;
                $casino_info_open_year = $item->casino_info_open_date;
                $casino_info_rating_description = $item->casino_info_rating_description;
                $casino_info_rating_description = !empty($casino_info_rating_description) ? str_replace('"', '\'', $casino_info_rating_description) : FALSE;
                $casino_info_rating_description = !empty($casino_info_rating_description) ? wpautop($casino_info_rating_description) : FALSE;

                $casino_info_button_text = $item->casino_info_button_text;
                $casino_info_button_text_list = $item->casino_info_button_text_list;
                $casino_info_affiliate_platforms = !empty($item->affiliate_platforms) ? $item->affiliate_platforms : FALSE;

                $casino_id = (int)$item->casino_id;

                // Get language code by country code.
                $language_code = $item->language;

                $review_post_id = $item->video_review_id;
                $review_id = $review_post_id;
                // $review_id = !empty($review_post_id) ? icl_object_id($review_post_id, 'post', true, $language_code) : FALSE;
                // $review_lang_info = !empty($review_id) ? apply_filters('wpml_post_language_details', NULL, $review_id) : FALSE;
                // $review_lang = !empty($review_lang_info) ? strtoupper($review_lang_info['language_code']) : FALSE;
                // $review_different_lang = !empty($review_lang_info) ? !$review_lang_info['different_language'] : FALSE;

                // Get EN casino review post
                // for use in other languages.
                // $casino_review_post_en_data = scg_get_casino_en_review_post($casino_id);

                $casino_info_video_review_link = !empty($review_id) ? get_permalink($review_id) : FALSE;
                $casino_info_review_button_text = $item->casino_info_review_button_text;
                $casino_info_review_with_video = !empty($casino_info_video_review_link) ? scg_casino_info_review_with_video($casino_id) : FALSE;

                $casino_info_left_button_link = $item->casino_info_left_button_link;
                $casino_info_left_button_text = $item->casino_info_left_button_text;
                $casino_info_left_button_text_mobile = $item->casino_info_left_button_text_mobile;
                $casino_info_left_button_color = $item->casino_info_left_button_color;
                $casino_info_left_button_hover_color = $item->casino_info_left_button_hover_color;
                $casino_info_left_button_text_color = $item->casino_info_left_button_text_color;
                $casino_info_left_button_text_hover_color = $item->casino_info_left_button_text_hover_color;

                if (!empty($casino_info_pros)) {
                    $dom = new DOMDocument;
                    $dom->loadHTML('<?xml encoding="utf-8" ?>' . $casino_info_pros);
                    $casino_pros_array = array();
                    foreach ($dom->getElementsByTagName('li') as $node) {
                        $casino_pros_array[] = $dom->saveHTML($node);
                    }

                    $casino_pros_split = array_chunk($casino_pros_array, 3);

                    $casino_pros_html = '';

                    if (!empty($casino_pros_split)) {
                        foreach ($casino_pros_split as $casino_pros) {
                            $casino_pros_html .= '<ul>';
                            foreach ($casino_pros as $entry) {
                                $casino_pros_html .= $entry;
                            }
                            $casino_pros_html .= '</ul>';
                        }
                    }
                }

                if (!empty($casino_info_cons)) {
                    $dom = new DOMDocument;
                    $dom->loadHTML('<?xml encoding="utf-8" ?>' . $casino_info_cons);
                    $casino_cons_array = array();
                    foreach ($dom->getElementsByTagName('li') as $node) {
                        $casino_cons_array[] = $dom->saveHTML($node);
                    }

                    $casino_cons_split = array_chunk($casino_cons_array, 3);

                    $casino_cons_html = '';

                    if (!empty($casino_cons_split)) {
                        foreach ($casino_cons_split as $casino_cons) {
                            $casino_cons_html .= '<ul>';
                            foreach ($casino_cons as $entry) {
                                $casino_cons_html .= $entry;
                            }
                            $casino_cons_html .= '</ul>';
                        }
                    }
                }

                $casino_info_logo_field_id = DatabaseService::getField('casino_info', 'casino_info_logo');
                global $wpdb;
                $logo_attachment_ID = !empty($casino_info_id) ? $wpdb->get_var("SELECT related_item_id FROM wp_podsrel WHERE item_id = $casino_info_id AND field_id = $casino_info_logo_field_id") : FALSE;
                $logo_source = pods_image($logo_attachment_ID, $size = 'list_crop_thumb', $default = 0, array('class' => 'casino-item-logo'), $force = false);
                $logo_source_image_info = !empty($logo_attachment_ID) ? wp_get_attachment_image_src($logo_attachment_ID, $size = 'list_crop_thumb') : FALSE;
                $logo_source_raw = !empty($logo_source_image_info) ? $logo_source_image_info[0] : FALSE;

                $casino_info = array(
                    'casino_info_new_casino' => $casino_info_new_casino,
                    'casino_info_new_text' => $casino_info_new_text,
                    'casino_info_terms' => $casino_info_terms,
                    'casino_info_color' => $casino_info_color,
                    'casino_info_button_text_color' => $casino_info_button_text_color,
                    'casino_info_pros' => $casino_info_pros,
                    'casino_info_cons' => $casino_info_cons,
                    'casino_info_logo' => $logo_source,
                    'casino_info_logo_src' => $logo_source_raw,
                    'casino_info_featured' => $casino_info_featured,
                    'casino_info_open_year' => $casino_info_open_year,
                    'casino_info_rating_description' => $casino_info_rating_description,
                    'casino_info_button_text' => $casino_info_button_text,
                    'casino_info_button_text_list' => $casino_info_button_text_list,
                    'casino_info_affiliate_platforms' => $casino_info_affiliate_platforms,
                    'casino_info_video_review_link' => $casino_info_video_review_link,
                    'casino_info_review_button_text' => $casino_info_review_button_text,
                    'casino_info_review_with_video' => $casino_info_review_with_video,

                    // 'casino_info_review_lang' => $review_lang,
                    // 'casino_info_review_lang_different' => $review_different_lang,
                    // 'casino_review_post_en_data' => $casino_review_post_en_data,

                    'casino_info_left_button_link' => $casino_info_left_button_link,
                    'casino_info_left_button_text' => $casino_info_left_button_text,
                    'casino_info_left_button_text_mobile' => $casino_info_left_button_text_mobile,
                    'casino_info_left_button_color' => $casino_info_left_button_color,
                    'casino_info_left_button_hover_color' => $casino_info_left_button_hover_color,
                    'casino_info_left_button_text_color' => $casino_info_left_button_text_color,
                    'casino_info_left_button_text_hover_color' => $casino_info_left_button_text_hover_color,
                );
            }
        }

    }

    return $casino_info;
}


/**
 * Check if video review has video link filled.
 */
function scg_casino_info_review_with_video($casino_id)
{
    $query = new SelectQuery;

    $query
        ->select('video_review_link AS video_review_link')
        ->from('wp_pods_casino_main_info', 't');

    $query
        ->where()
        ->equal('t.casino_id', $casino_id);

    $data = (new DatabaseService($query))->build();

    $video_review_link = $data[0]->video_review_link;
    $has_video_review_link = !empty($video_review_link);
   
    return $has_video_review_link;
}


/**
 * Get casino review top at data.
 */
// function scg_casino_trackers_get_casino_review_top_at($casino_id, $country_id, $original_country_id, $language)
// {
//     $casino_game_type_reviews = scg_casino_trackers_get_casino_game_type_review_overall_fast($casino_id, $country_id, $original_country_id, $language);

//     return $casino_game_type_reviews;
// }


/**
 * Get bookmaker review top at data.
 */
function scg_casino_trackers_get_casino_review_top_at($casino_id, $country_id, $original_country_id, $language_code)
{
    $casino_grouped_game_type_reviews = FALSE;

    $country_name = scg_get_country_name_from_id($country_id);
    $transient_sheet_name = scg_casino_trackers_get_casinos_transient_sheet_name($country_name);
    // $language_code = scg_casino_trackers_get_casino_review_language(FALSE);
    // $casino_grouped_game_type_reviews = get_transient('scg_casino_game_type_reviews' . '_' . $transient_sheet_name);

    // var_dump('scg_casino_game_type_reviews' . '_' . $transient_sheet_name);

    // var_dump($country_id);
    // var_dump($country_name);
    // var_dump($original_country_id);
    // var_dump($language);

    if (empty($casino_grouped_game_type_reviews)) {
      $groupedItems = array();
      $game_types = scg_get_casino_game_types_list();

      $count = 1;
      // d($game_types);
      // die();
      foreach ($game_types as $game_type) {
        // if ($count > 5) {
        //     continue;
        // }

        // Skip these game types.
        if ($game_type['name'] == 'Index Page' || $game_type['name'] == 'Article' || $game_type['name'] == 'Bonus šone') {
            continue;
        }

        // Get TOP rating for every game type.
        $casinos = scg_casino_trackers_get_top_rating($game_type['name'], $country_name, $language_code);

        $groupedItems[] = $casinos;

        $count++;
      }

      // d($groupedItems);
      // die();

      if (!empty($groupedItems)) {
        foreach ($groupedItems as $game_key => $game_type_ratings) {
          $position = 1;

          foreach ($game_type_ratings as $key => $entry) {
            // unset($groupedItems[$game_key][$key]['casino_info']);
            // unset($groupedItems[$game_key][$key]['casino_info_crypto_deposits']);
            // unset($groupedItems[$game_key][$key]['casino_info_crypto_withdrawals']);

            // Mark restricted casino in this country.
            $casino_restricted = FALSE;
            foreach ($entry['casino_restricted_countries'] as $restricted_country) {
              if ($restricted_country == $original_country_id) {
                  $casino_restricted = TRUE;
                  unset($groupedItems[$game_key][$key]);
              }
            }

            // Count only when restricted bookmaker is removed.
            if (empty($casino_restricted) && $entry['casino_info']['casino_info_new_casino'] == FALSE) {
              $groupedItems[$game_key][$key] = $entry['casino_game_type_review'];

              $groupedItems[$game_key][$key]['position'] = $position;
              $groupedItems[$game_key][$key]['position_suffix'] = scg_format_ordinal_number($position, $only_suffix = TRUE);

              $position++;
            }

            if ($entry['casino_id'] !== $casino_id) {
              unset($groupedItems[$game_key][$key]);
            }
          }
        }
      }

      if (!empty($groupedItems)) {
          $game_type_reviews = array();

          foreach ($groupedItems as $game_key => $game_type_data) {
              foreach ($game_type_data as $key => $entry) {
                  if ($entry['casino_game_type_name'] == 'UK') {
                    continue;
                  }

                  $game_type_reviews[] = $entry;
              }
          }
      }

      // Sort by position and then rating.
      usort($game_type_reviews, function ($a, $b) {
          if ($a['position'] == $b['position']) {
              // Top position is the same, sort by higher game review rating.
              if ($b['casino_game_type_review']['casino_game_review_rating'] > $a['casino_game_type_review']['casino_game_review_rating']) return 1;
          }

          // Sort the top position first.
          return $a['position'] <=> $b['position'];
      });

      // Re-index keys.
      $game_type_reviews = array_values($game_type_reviews);

      // d($game_type_reviews);
      // die();

      // var_dump($groupedItems);
      // var_dump($sport_type_reviews);
      // die();

      return $game_type_reviews;
    }
}



/**
 * Get casino game type review data - fast.
 */
function scg_casino_trackers_get_casino_game_type_review_overall_fast($casino_id, $country_id, $original_country_id, $language)
{
    $casino_grouped_game_type_reviews = FALSE;

    $country_name = scg_get_country_name_from_id($country_id);
    $transient_sheet_name = scg_casino_trackers_get_casinos_transient_sheet_name($country_name);
    // $casino_grouped_game_type_reviews = get_transient('scg_casino_game_type_reviews' . '_' . $transient_sheet_name);

    // var_dump('scg_casino_game_type_reviews' . '_' . $transient_sheet_name);

    // var_dump($country_id);
    // var_dump($country_name);
    // var_dump($original_country_id);
    // var_dump($language);

    if (empty($casino_grouped_game_type_reviews) && !empty($country_id) && !empty($original_country_id) && !empty($language)) {

        $game_review_pod_id = DatabaseService::getField('casino_game_review', 'pod_id');
        $game_review_countries_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_countries');
        $game_review_casino_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_casino');
        $game_review_game_type_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_game');

        // $game_review_pod = pods('casino_game_review');
        // $game_review_countries_field = $game_review_pod->fields('casino_game_review_countries');
        // $game_review_casino_field = $game_review_pod->fields('casino_game_review_casino');
        // $game_review_game_type_field = $game_review_pod->fields('casino_game_review_game');

        $bonus_pod_id = DatabaseService::getField('casino_bonus', 'pod_id');
        $bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');
        $bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');

        // $bonus_pod = pods('casino_bonus');
        // $bonus_countries_field = $bonus_pod->fields('bonus_countries');
        // $bonus_casino_field = $bonus_pod->fields('bonus_casino');

        $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
        $accepting_countries_field_id = DatabaseService::getField('casino', 'casino_accepting_countries');

        // $casino_pod = pods('casino');
        // $accepting_countries_field = $casino_pod->fields('casino_accepting_countries');

        $casino_game_pod_id = DatabaseService::getField('casino_game', 'pod_id');
        $game_page_field_id = DatabaseService::getField('casino_game', 'casino_game_top_rating_page');

        // $casino_game_pod = pods('casino_game');
        // $game_page_field = $casino_game_pod->fields('casino_game_top_rating_page');

        $casino_link_pod_id = DatabaseService::getField('casino_link', 'pod_id');
        $casino_link_game_field_id = DatabaseService::getField('casino_link', 'casino_link_game');

        // $casino_link_pod = pods('casino_link');
        // $casino_link_game_field = $casino_link_pod->fields('casino_link_game');

        if (!empty($casino_id) && !empty($language) && !empty($country_id) && !empty($original_country_id)) {
            $casino_field_info = array(
                'pod_id' => $game_review_pod_id,
                'field_id' => $game_review_casino_field_id,
                'related_item_id' => $casino_id,
            );

            $countries_field_info = array(
                'pod_id' => $game_review_pod_id,
                'field_id' => $game_review_countries_field_id,
                'related_item_id' => $country_id,
            );

            $game_type_field_info = array(
                'pod_id' => $game_review_pod_id,
                'field_id' => $game_review_game_type_field_id,
                'related_item_id' => $game_type_id,
            );

            $bonus_countries_field_info = array(
                'pod_id' => $bonus_pod_id,
                'field_id' => $bonus_countries_field_id,
                'related_item_id' => $country_id,
            );

            $bonus_casino_field_info = array(
                'pod_id' => $bonus_pod_id,
                'field_id' => $bonus_casino_field_id,
            );

            $accepting_countries_field_info = array(
                'pod_id' => $casino_pod_id,
                'field_id' => $accepting_countries_field_id,
            );

            $game_page_field_info = array(
                'pod_id' => $casino_game_pod_id,
                'field_id' => $game_page_field_id,
            );

            $casino_link_game_field_info = array(
                'pod_id' => $casino_link_pod_id,
                'field_id' => $casino_link_game_field_id,
            );

            global $wpdb;
            $casino_game_reviews_ids_tmp = $wpdb->get_results("
          SELECT r.id, r.name, r.casino_game_review_rating AS rating, r.language, review_country.related_item_id AS country_id, review_casino.related_item_id AS casino_id, review_game_type.related_item_id AS game_type_id, casino_game_type.name AS game_type_name
          FROM wp_pods_casino_game_review r
          LEFT JOIN wp_podsrel AS review_country ON r.id = review_country.item_id
          LEFT JOIN wp_podsrel AS review_casino ON r.id = review_casino.item_id
          LEFT JOIN wp_podsrel AS review_game_type ON r.id = review_game_type.item_id
          LEFT JOIN wp_podsrel AS bonus_casino ON review_casino.related_item_id = bonus_casino.related_item_id
          LEFT JOIN wp_pods_casino_game AS casino_game_type ON review_game_type.related_item_id = casino_game_type.id
          LEFT JOIN wp_pods_casino AS casino ON review_casino.related_item_id = casino.id
          LEFT JOIN wp_pods_casino_bonus AS casino_bonus ON bonus_casino.item_id = casino_bonus.id
          LEFT JOIN wp_podsrel AS bonus_countries ON casino_bonus.id = bonus_countries.item_id
          WHERE (review_casino.pod_id = " . $casino_field_info['pod_id'] . " AND review_casino.field_id = " . $casino_field_info['field_id'] . ") AND
          (review_country.pod_id = " . $countries_field_info['pod_id'] . " AND review_country.related_item_id = " . $countries_field_info['related_item_id'] . " AND review_country.field_id = " . $countries_field_info['field_id'] . ")" . " AND (review_game_type.pod_id = " . $game_type_field_info['pod_id'] . " AND review_game_type.field_id = " . $game_type_field_info['field_id'] . ")" . " AND (bonus_casino.pod_id = " . $bonus_casino_field_info['pod_id'] . " AND bonus_casino.field_id = " . $bonus_casino_field_info['field_id'] . ")" . " AND (bonus_countries.pod_id = " . $bonus_countries_field_info['pod_id'] . " AND bonus_countries.related_item_id = " . $bonus_countries_field_info['related_item_id'] . " AND bonus_countries.field_id = " . $bonus_countries_field_info['field_id'] . ")" . " AND (r.language = '" . $language . "')" . '
            AND (casino.casino_enabled = 1 AND casino.casino_restricted_enabled = 1 AND casino_bonus.bonus_enabled = 1)' . ' ORDER BY rating DESC',
                ARRAY_A);

            // var_dump($wpdb->last_query);
            d($casino_game_reviews_ids_tmp);
            die();

            if (!empty($casino_game_reviews_ids_tmp)) {
                $casino_game_reviews = array();
                foreach ($casino_game_reviews_ids_tmp as $game_review) {
                    $game_review_id = (int)$game_review['id'];
                    $name = $game_review['name'];
                    $rating = $game_review['rating'];
                    $language = $game_review['language'];
                    $casino = (int)$game_review['casino_id'];
                    $game_type = (int)$game_review['game_type_id'];
                    $game_type_name = $game_review['game_type_name'];
                    $game_type_slug = strtolower(str_replace(' ', '-', $game_type_name));
                    $game_type_page_id = scg_get_casino_game_type_link_from_id($game_type);
                    $game_type_link = !empty($game_type_page_id) ? get_permalink($game_type_page_id) : FALSE;
                    $game_type_enabled = scg_casino_trackers_check_if_casino_link_is_enabled($casino, $country_id, $game_type);
                    $casino_restricted_countries = !empty($casino) ? scg_casino_trackers_get_casino_restricted_countries($casino) : array();

                    $casino_game_reviews[] = array(
                        'game_review_id' => $game_review_id,
                        'name' => $name,
                        'rating' => $rating,
                        'language' => $language,
                        'casino_id' => $casino,
                        'game_type_id' => $game_type,
                        'game_type_name' => $game_type_name,
                        'game_type_slug' => $game_type_slug,
                        'game_type_link' => $game_type_link,
                        'game_type_enabled' => $game_type_enabled,
                        'casino_restricted_countries' => $casino_restricted_countries,
                    );
                }
            }

            if (!empty($casino_game_reviews)) {
                $groupedItems = array();
                foreach ($casino_game_reviews as $item) {
                    // Skip adding "Payments" game type.
                    if ($item['game_type_id'] == 18) {
                        continue;
                    }

                    $groupedItems[$item['game_type_id']][] = $item;
                }

                $casino_grouped_game_type_reviews = array_values($groupedItems);
            }

            // 25 hours - actually it's not expiring as we
            // re-generate transients every 24 hours after
            // all casino trackers import (in the morining).
            set_transient('scg_casino_game_type_reviews' . '_' . $transient_sheet_name, $casino_grouped_game_type_reviews, 25 * 60 * 60);
        }
    }

    if (!empty($casino_grouped_game_type_reviews)) {
        $groupedItems = $casino_grouped_game_type_reviews;
        // var_dump($groupedItems);

        foreach ($groupedItems as $game_key => $game_type_data) {
            $count = 1;
            $casino_restricted = FALSE;
            foreach ($game_type_data as $key => $entry) {
                if (empty($entry['game_type_enabled'])) {
                    unset($groupedItems[$game_key][$key]);
                    continue;
                }

                foreach ($entry['casino_restricted_countries'] as $restricted_country) {
                    // Mark restricted casino in this country.
                    if ($restricted_country == $original_country_id && $casino_id != $entry['casino_id']) {
                        $casino_restricted = TRUE;
                    }
                }

                // // Remove restricted casino in this country.
                if (!empty($casino_restricted)) {
                    unset($groupedItems[$game_key][$key]);
                    continue;
                }

                if (!$casino_restricted) {
                    $groupedItems[$game_key][$key]['position'] = $count;
                    $groupedItems[$game_key][$key]['position_suffix'] = scg_format_ordinal_number($count, $only_suffix = TRUE);
                }

                // var_dump($groupedItems);

                if ($casino_id != $entry['casino_id']) {
                    unset($groupedItems[$game_key][$key]);
                }

                if (!empty($entry['game_type_enabled']) && !$casino_restricted) {
                    //if (!empty($entry['game_type_enabled'])) {
                    $count++;
                }
            }
        }

        if (!empty($groupedItems)) {
            $casino_game_type_reviews = array();

            foreach ($groupedItems as $game_key => $game_type_data) {
                foreach ($game_type_data as $key => $entry) {
                    $casino_game_type_reviews[] = $entry;
                }
            }
        }

        // Sort by position and then rating.
        usort($casino_game_type_reviews, function ($a, $b) {
            if ($a['position'] == $b['position']) {
                // Top position is the same, sort by higher game review rating.
                if ($b['rating'] > $a['rating']) return 1;
            }

            // Sort the top position first.
            return $a['position'] <=> $b['position'];
        });

        // Re-index keys.
        $casino_game_type_reviews = array_values($casino_game_type_reviews);
    }

    // var_dump($casino_game_type_reviews);

    return $casino_game_type_reviews;
}


/**
 * Get casino game providers.
 */
function scg_casino_trackers_get_casino_game_providers($casino_id)
{
    $casino_game_providers = FALSE;

    if (!empty($casino_id)) {

        // Get main info fields info.
        global $wpdb;
        $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

        $casino_main_info_pod = pods('casino_main_info', $casino_main_info_id);
        $game_providers_tmp = $casino_main_info_pod->field('game_providers');

        $game_providers = array();

        if (!empty($game_providers_tmp)) {
            foreach ($game_providers_tmp as $entry) {
                $id = (int)$entry['id'];
                $name = $entry['name'];
                $logo_url = scg_casino_trackers_get_logo_from_pod('casino_game_provider', $entry['id']);
                $top_rating_page_url = scg_casino_trackers_get_top_rating_page_url_from_pod('casino_game_provider', $entry['id']);

                $game_providers[] = array(
                    'id' => $id,
                    'name' => $name,
                    'logo_url' => $logo_url,
                    'top_rating_page_url' => $top_rating_page_url,
                );
            }
        }

    }

    return $game_providers;
}


/**
 * Get casino game provider logo.
 */
function scg_casino_trackers_get_casino_game_provider_logo($id)
{
    $game_provider_pod = pods('casino_game_provider', $id);
    $game_provider_logo = $game_provider_pod->field('logo');

    $game_provider_logo_url = FALSE;
    if (!empty($game_provider_logo)) {
        $game_provider_logo_url = wp_get_attachment_url($game_provider_logo['ID']);
    }

    return $game_provider_logo_url;
}


/**
 * Get casino payment methods.
 */
function scg_casino_trackers_get_casino_payment_methods($casino_id)
{
    $deposit_methods = FALSE;

    if (!empty($casino_id)) {

        // Get main info fields info.
        global $wpdb;
        $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

        $casino_main_info_pod = pods('casino_main_info', $casino_main_info_id);
        $deposit_methods_tmp = $casino_main_info_pod->field('deposit_methods');
        $crypto_deposits_tmp = $casino_main_info_pod->field('crypto_deposits');

        $deposit_methods = array();

        if (!empty($deposit_methods_tmp)) {
            foreach ($deposit_methods_tmp as $entry) {
                $id = (int)$entry['id'];
                $name = $entry['name'];
                $logo_url = scg_casino_trackers_get_logo_from_pod('payment_method', $entry['id']);
                $top_rating_page_url = scg_casino_trackers_get_top_rating_page_url_from_pod('payment_method', $entry['id']);

                $deposit_methods[] = array(
                    'id' => $id,
                    'name' => $name,
                    'logo_url' => $logo_url,
                    'top_rating_page_url' => $top_rating_page_url,
                );
            }
        }

        if (!empty($crypto_deposits_tmp)) {
            $crypto_deposit_methods = array();
            foreach ($crypto_deposits_tmp as $entry) {
                $id = (int)$entry['id'];
                $name = $entry['name'];
                $logo_url = scg_casino_trackers_get_logo_from_pod('cryptocurrency', $entry['id']);
                $top_rating_page_url = scg_casino_trackers_get_top_rating_page_url_from_pod('cryptocurrency', $entry['id']);

                $crypto_deposit_methods[] = array(
                    'id' => $id,
                    'name' => $name,
                    'logo_url' => $logo_url,
                    'top_rating_page_url' => $top_rating_page_url,
                );
            }
        }

    }

    $data = array(
        'deposit_methods' => $deposit_methods,
        'crypto_deposits' => $crypto_deposit_methods,
    ); 

    return $data;
}


/**
 * Get casino logo from pod.
 */
function scg_casino_trackers_get_logo_from_pod($pod_type, $id)
{
    global $wpdb;

    $logo_url = FALSE;

    if ($pod_type == 'casino_game_provider') {
        $pod_id = DatabaseService::getField('casino_game_provider', 'pod_id');
        $field_id = DatabaseService::getField('casino_game_provider', 'logo');
    } else if ($pod_type == 'cryptocurrency') {
        $pod_id = DatabaseService::getField('cryptocurrency', 'pod_id');
        $field_id = DatabaseService::getField('cryptocurrency', 'logo');
    } else if ($pod_type == 'payment_method') {
        $pod_id = DatabaseService::getField('payment_method', 'pod_id');
        $field_id = DatabaseService::getField('payment_method', 'logo');
    }

    $sql = "
    SELECT related_item_id FROM wp_podsrel
      WHERE wp_podsrel.pod_id = $pod_id AND
            wp_podsrel.item_id = $id AND
            wp_podsrel.field_id = $field_id
    ";

    $data = $wpdb->get_row($sql);

    if (!empty($data)) {
        $logo_url = wp_get_attachment_url($data->related_item_id);
    }

    return $logo_url;
}


/**
 * Get top rating page ID from url.
 */
function scg_casino_trackers_get_top_rating_page_id_from_url($url)
{
    $url_parse = parse_url($url);
    $url_path = !empty($url_parse) ? ltrim(rtrim($url_parse['path'], '/'), '/') : FALSE;
    $url_path_1 = !empty($url_parse) ? ltrim($url_parse['path'], '/') : FALSE;
    $url_path_2 = !empty($url_parse) ? rtrim($url_parse['path'], '/') : FALSE;
    $page = get_page_by_path($url_path, OBJECT, 'page'); 
    $page_id = !empty($page) && $page->post_status != 'draft' ? $page->ID : FALSE;

    // Check in custom permalink using
    // all the slash combinations.
    if (!empty($url) && empty($page_id)) {
        global $wpdb;
        $sql = $wpdb->prepare(
            "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'custom_permalink' AND (meta_value = %s OR meta_value = %s OR meta_value = %s)",
            $url_path,
            $url_path_1,
            $url_path_2
        );

        $page_id = $wpdb->get_var($sql);
    }

    return $page_id;
}


/**
 * Get top rating page url from pod.
 */
function scg_casino_trackers_get_top_rating_page_url_from_pod($pod_type, $id)
{
    $pod = pods($pod_type, $id);
    $top_rating_page = $pod->field('top_rating_page');

    $top_rating_page_url = FALSE;
    if (!empty($top_rating_page)) {
        $top_rating_page_url = get_permalink($top_rating_page['ID']);
    }

    return $top_rating_page_url;
}


/**
 * Get casino game type review data.
 */
function scg_casino_trackers_get_casino_game_type_review($casino_id, $game_type_id, $country_id, $language)
{
    $casino_game_type_review_data = FALSE;

    if (!empty($casino_id) && !empty($game_type_id) && !empty($country_id) && !empty($language)) {

        $query = new SelectQuery;

        $query
            ->select([
                'casino_game_review_list_casino_game_review_game.id as casino_game_type_id',
                'casino_game_review_list.casino_game_review_rating AS casino_game_review_rating',
                'casino_link_list.casino_link AS casino_game_link',
                'casino_link_list.casino_link_button_text AS casino_game_link_button_text',
                'casino_link_list.casino_link_regulation_text AS casino_game_link_regulation_text',
            ])
            ->from('wp_pods_casino', 't');

        $casino_game_review_list_field_id = DatabaseService::getField('casino', 'casino_game_review_list');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list')
            ->on("rel_casino_game_review_list.field_id = $casino_game_review_list_field_id AND rel_casino_game_review_list.item_id = t.id");
        $query
            ->leftJoin('wp_pods_casino_game_review', 'casino_game_review_list')
            ->on("casino_game_review_list.id = rel_casino_game_review_list.related_item_id");

        $casino_game_review_game_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_game');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list_casino_game_review_game')
            ->on("rel_casino_game_review_list_casino_game_review_game.field_id = $casino_game_review_game_field_id AND rel_casino_game_review_list_casino_game_review_game.item_id = casino_game_review_list.id");
        $query
            ->leftJoin('wp_pods_casino_game', 'casino_game_review_list_casino_game_review_game')
            ->on("casino_game_review_list_casino_game_review_game.id = rel_casino_game_review_list_casino_game_review_game.related_item_id");

        $casino_link_list_field_id = DatabaseService::getField('casino', 'casino_link_list');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list')
            ->on("rel_casino_link_list.field_id = $casino_link_list_field_id AND rel_casino_link_list.item_id = t.id");
        $query
            ->leftJoin('wp_pods_casino_link', 'casino_link_list')
            ->on("casino_link_list.id = rel_casino_link_list.related_item_id");

        $casino_game_review_countries_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_countries');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list_casino_game_review_countries')
            ->on("rel_casino_game_review_list_casino_game_review_countries.field_id = $casino_game_review_countries_field_id AND rel_casino_game_review_list_casino_game_review_countries.item_id = casino_game_review_list.id");
        $query
            ->leftJoin('wp_pods_country', 'casino_game_review_list_casino_game_review_countries')
            ->on("casino_game_review_list_casino_game_review_countries.id = rel_casino_game_review_list_casino_game_review_countries.related_item_id");


        $casino_link_countries_field_id = DatabaseService::getField('casino_link', 'casino_link_countries');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_countries')
            ->on("rel_casino_link_list_casino_link_countries.field_id = $casino_link_countries_field_id AND rel_casino_link_list_casino_link_countries.item_id = casino_link_list.id");
        $query
            ->leftJoin('wp_pods_country', 'casino_link_list_casino_link_countries')
            ->on("casino_link_list_casino_link_countries.id = rel_casino_link_list_casino_link_countries.related_item_id");


        $casino_link_game_field_id = DatabaseService::getField('casino_link', 'casino_link_game');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_game')
            ->on("rel_casino_link_list_casino_link_game.field_id = $casino_link_game_field_id AND rel_casino_link_list_casino_link_game.item_id = casino_link_list.id");
        $query
            ->leftJoin('wp_pods_casino_game', 'casino_link_list_casino_link_game')
            ->on("casino_link_list_casino_link_game.id = rel_casino_link_list_casino_link_game.related_item_id");

        $query
            ->where()
            ->equal('t.id', $casino_id)
            ->equal('casino_game_review_list_casino_game_review_game.id', $game_type_id)
            ->equal('casino_game_review_list_casino_game_review_countries.id', $country_id)
            ->equal('casino_game_review_list.language', $language);

        if ($game_type_id < 22 && !preg_match('~[0-9]+~', $game_type_name)) {
            $query
                ->where()
                ->equal('casino_link_list_casino_link_countries.id', $country_id)
                ->equal('casino_link_list.casino_link_status', 1)
                ->equal('casino_link_list_casino_link_game.id', $game_type_id);
        }

        $query
            ->groupBy('t.id')
            ->orderBy('t.name', 'ASC')
            ->orderBy('t.id', 'ASC');

        $item = (new DatabaseService($query, DatabaseService::GET_ITEM))->build() ?: false;

        if ($item) {
            $casino_game_type_id = (int)$item->casino_game_type_id;
            $casino_game_review_rating = (float) number_format($item->casino_game_review_rating, 2);
            $casino_game_link = $item->casino_game_link;
            $casino_game_button_text = $item->casino_game_link_button_text;
            $casino_game_regulation_text = $item->casino_game_link_regulation_text;

            $casino_game_type_review_data = array(
                'casino_game_type_id' => $casino_game_type_id,
                'casino_game_review_rating' => $casino_game_review_rating,
                'casino_game_link' => $casino_game_link,
                'casino_game_button_text' => $casino_game_button_text,
                'casino_game_regulation_text' => $casino_game_regulation_text,
            );
        }

    }

    return $casino_game_type_review_data;
}


/**
 * Get casino game type review data.
 */
function scg_casino_trackers_get_casino_game_type_review_single($casino_id = FALSE, $game_type_id, $country_id, $tracker_game_type_id = FALSE)
{
    // Override country for Asia and UK.
    $game_type_id = (int) $game_type_id;

    // Game type no tracker logic.
    // $game_type_no_tracker = scg_get_casino_game_type_requirement($game_type_id);
    // $game_type_needs_tracker_rating = empty($game_type_no_tracker) ? TRUE : FALSE;

    if (!empty($sport_type)) {
      if ($sport_type == 'Asia') {
        // China.
        $country = 'China';
        $country_id = scg_get_country_id_from_name($country);
      }

      if ($sport_type == 'UK') {
        // United Kingdom.
        $country = 'United Kingdom';
        $country_id = scg_get_country_id_from_name($country);
      }
    }

    $casino_game_type_review_data = FALSE;
    // $casino_game_type_review_data = get_transient('scg_casino_game_type_review_data_' . $game_type_id . '_' . $country_id);

    if (empty($casino_game_type_review_data) && !empty($game_type_id) && !empty($country_id)) {

        $query = new SelectQuery;

        $query
            ->select([
                't.casino_id AS casino_id',
                't.game_type_id AS casino_game_type_id',
                'game_type_review.game_type_name AS casino_game_type_name',
                'game_type_review.casino_game_review_rating AS casino_game_type_review_rating',
                'casino_link.casino_link AS casino_game_type_link',
                'casino_link.casino_link_button_text AS casino_game_type_link_button_text',
                'casino_link.casino_link_regulation_text AS casino_game_type_link_regulation_text',
                'casino_link.casino_link_status AS casino_game_type_link_status',
                // 't.casino as id',
            ])
            ->from('wp_pods_country_relation', 't');
        $query
            ->leftJoin('wp_pods_casino_game_review', 'game_type_review')
            ->on('t.game_type_review_id = game_type_review.id');
        $query
            ->leftJoin('wp_pods_casino_link', 'casino_link')
            ->on('t.link_id = casino_link.id');

        $query
            ->where()
            ->equal('t.game_type_id', $game_type_id)
            ->equal('t.country_id', $country_id);

        // If game type
        // if ($game_type_needs_tracker_rating) {
        // }

        // Require game type review ID and rating by default.
        $query
            ->where()           
            ->isNot('t.game_type_review_id', NULL)
            ->greaterThan('game_type_review.casino_game_review_rating', '0.00');

        // Require tracker only when no tracker game type ID ir provided.
        if (empty($tracker_game_type_id)) {
            $query
                ->where()           
                ->isNot('t.link_id', NULL)
                ->equal('casino_link.casino_link_status', 1)
                ->isNot('casino_link.casino_link', NULL);
        }

        if (!empty($casino_id)) {
          $query
              ->where()
              ->equal('t.casino_id', $casino_id);
        }

        $query
             ->groupBy('t.casino_id')
             ->orderBy('t.id', 'DESC');

        $data = (new DatabaseService($query, DatabaseService::GET_DATA))->build() ?: false;

        $casino_game_type_review_data = array();

        if ($data) {
          foreach ($data as $item) {
            $casino_id = (int) $item->casino_id;

            $casino_game_type_id = (int) $item->casino_game_type_id;
            $casino_game_type_name = $item->casino_game_type_name;

            $page_ID = !empty($casino_game_type_id) ? scg_get_casino_game_type_link_from_id($casino_game_type_id) : FALSE;
            $casino_game_type_permalink = !empty($page_ID) ? get_permalink($page_ID) : FALSE;
            $casino_game_type_slug = strtolower(str_replace(' ', '-', $casino_game_type_name));

            $casino_game_type_review_rating = (float) $item->casino_game_type_review_rating;
            $casino_game_type_link = $item->casino_game_type_link;
            $casino_game_type_button_text = $item->casino_game_type_link_button_text;
            $casino_game_type_regulation_text = $item->casino_game_type_link_regulation_text;

            $casino_game_type_review_data[] = array(
                'casino_id' => $casino_id,

                'casino_game_type_id' => $casino_game_type_id,

                'casino_game_type_name' => $casino_game_type_name,
                'casino_game_type_slug' => $casino_game_type_slug,
                'casino_game_type_permalink' => $casino_game_type_permalink,

                'casino_game_review_rating' => $casino_game_type_review_rating,
                'casino_game_link' => $casino_game_type_link,
                'casino_game_button_text' => $casino_game_type_button_text,
                'casino_game_regulation_text' => $casino_game_type_regulation_text,
            );
          }
        }

        // d($casino_game_type_review_data);
        // die();

        $casino_data = set_transient('scg_casino_game_type_review_data_' . $game_type_id . '_' . $country_id, $casino_game_type_review_data, 25 * 60 * 60 );

    }

    return $casino_game_type_review_data;
}


function scg_casino_single_casino_game_type_review($casino_id, $game_type_review_data) {
  $single_game_type_review_data = FALSE;

  // var_dump($sport_type_review_data);

  foreach ($game_type_review_data as $key => $casino) {
    if ($casino['casino_id'] == $casino_id) {
      $single_game_type_review_data = $casino;
    }
  }

  return $single_game_type_review_data;
}



/**
 * Check if we have casinos bonuses from
 * provided country and at least one is enabled.
 */
function scg_casino_trackers_check_if_casino_bonus_country_imported($country_name, $casino_id = FALSE)
{
    $country_exists = FALSE;

    // Get country ID using pods query.
    $country_id = scg_get_country_id_from_name(trim($country_name));

    $query = new SelectQuery;
    $query
        ->select(['t.*'])
        ->from('wp_pods_casino_bonus', 't')
        ->orderBy('name', 'ASC')
        ->orderBy('id', 'ASC')
        ->limit(1);

    $casino_bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');
    $casino_bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');

    $query
        ->join(DatabaseService::REL_TABLE, 'rel_bonus_countries')
        ->on("rel_bonus_countries.field_id = $casino_bonus_countries_field_id AND rel_bonus_countries.item_id = t.id");

    $query
        ->join('wp_pods_country', 'bonus_countries')
        ->on('bonus_countries.id = rel_bonus_countries.related_item_id');

    if (!empty($casino_id)) {
        $query
            ->join(DatabaseService::REL_TABLE, 'rel_bonus_casino')
            ->on("rel_bonus_casino.field_id = $casino_bonus_casino_field_id AND rel_bonus_casino.item_id = t.id");

        $query
            ->join('wp_pods_casino', 'bonus_casino')
            ->on('bonus_casino.id = rel_bonus_casino.related_item_id');

        $query->where()
            ->equal('bonus_casino.id', $casino_id);
    }

    $query->where()
        ->in('bonus_countries.id', [$country_id]);


    // Let's check if one enabled
    // bonus exists in this country.
    if (! empty($country_id)) {
        $item = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    }

    // die();

    if (! empty($item)) $country_exists = TRUE;

    return $country_exists;
}


/**
 * Check if we have casinos bonuses from
 * provided country and at least one is enabled.
 */
function scg_casino_trackers_list_casino_bonus_countries($casino_id)
{
    $query = new SelectQuery;
    $query
        ->select(['bonus_countries.*'])
        ->from('wp_pods_casino_bonus', 't')
        ->orderBy('name', 'ASC')
        ->orderBy('id', 'ASC')
        ->limit(0);

    $casino_bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');
    $casino_bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');

    $query
        ->join(DatabaseService::REL_TABLE, 'rel_bonus_countries')
        ->on("rel_bonus_countries.field_id = $casino_bonus_countries_field_id AND rel_bonus_countries.item_id = t.id");

    $query
        ->join('wp_pods_country', 'bonus_countries')
        ->on('bonus_countries.id = rel_bonus_countries.related_item_id');

    if (!empty($casino_id)) {
        $query
            ->join(DatabaseService::REL_TABLE, 'rel_bonus_casino')
            ->on("rel_bonus_casino.field_id = $casino_bonus_casino_field_id AND rel_bonus_casino.item_id = t.id");

        $query
            ->join('wp_pods_casino', 'bonus_casino')
            ->on('bonus_casino.id = rel_bonus_casino.related_item_id');

        $query->where()
            ->equal('bonus_casino.id', $casino_id);
    }

    // $query->where()
    //     ->in('bonus_countries.id', [$country_id]);
    $query
        ->groupBy('bonus_countries.id');

    $data = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

    $countries = array(); 
    if (!empty($data)) {
        foreach ($data as $country) {
            $countries[] = $country->id;
        }
    }

    return $countries;
}


/**
 * Check if casino is enabled, including restricted countries field.
 */
function scg_casino_trackers_check_if_casino_is_enabled($casino_id)
{
    $casino_enabled = FALSE;

    // Let's check if casino is enabled.
    if (!empty($casino_id)) {
      $query = new SelectQuery;

      $query
          ->from('wp_pods_casino')
          ->select(['id'])
          ->limit(1)
          ->where()
          ->equal('id', $casino_id)
          ->and()
          ->equal('casino_enabled', 1);

      $casino_item = (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
          ->build() ?: false;

    }

    $casino_enabled = !empty($casino_item);


    return $casino_enabled;
}


/**
 * Check if casino is closed.
 */
function scg_casino_trackers_check_if_casino_is_closed($casino_id)
{

    if (!empty($casino_id)) {
      $query = new SelectQuery;

      $query
          ->from('wp_pods_casino')
          ->select(['id', 'casino_closed'])
          ->limit(1)
          ->where()
          ->equal('id', $casino_id)
          ->and()
          ->equal('casino_closed', 1);

      $casino_item = (new DatabaseService($query, DatabaseService::GET_ITEM, 'id'))
          ->build() ?: false;

    }

    $casino_found = !empty($casino_item);

    return $casino_found;
}


/**
 * Check if bonus data is enabled by language.
 */
function scg_casino_trackers_check_if_bonus_data_enabled($casino_id, $country_id)
{
    $bonus_enabled = FALSE;

    // Let's check if this bonus exists first.
    // if (!empty($casino_id) && !empty($country_id)) {
    //     $params = array(
    //         'distinct' => FALSE,
    //         'where' =>
    //             'bonus_casino.id = "' . $casino_id . '"' . ' AND ' .
    //             'bonus_countries.id IN (' . $country_id . ')',
    //         'limit' => 1,
    //     );

    //     $casino_bonus = pods('casino_bonus');
    //     $casino_bonus->find($params);
    //     $bonus_data = $casino_bonus->data();
    // }

    // if (!empty($bonus_data) && !empty($bonus_data[0])) {
    //     $bonus_enabled = !empty($bonus_data[0]->bonus_enabled);
    // }

    // d($casino_bonus->sql);

    // SELECT
    //     `t`.*
    //     FROM `wp_pods_casino_bonus` AS `t`
        
    //         LEFT JOIN `wp_podsrel` AS `rel_bonus_casino` ON
    //             `rel_bonus_casino`.`field_id` = 17629
    //             AND `rel_bonus_casino`.`item_id` = `t`.`id`

    //         LEFT JOIN `wp_pods_casino` AS `bonus_casino` ON
    //             `bonus_casino`.`id` = `rel_bonus_casino`.`related_item_id`
        
        
    //         LEFT JOIN `wp_podsrel` AS `rel_bonus_countries` ON
    //             `rel_bonus_countries`.`field_id` = 17630
    //             AND `rel_bonus_countries`.`item_id` = `t`.`id`

    //         LEFT JOIN `wp_pods_country` AS `bonus_countries` ON
    //             `bonus_countries`.`id` = `rel_bonus_countries`.`related_item_id`
        
    //     WHERE ( `bonus_casino`.`id` = "40" AND `bonus_countries`.`id` IN (1) AND t.bonus_enabled = 1 )
        
        
    //     ORDER BY `t`.`name`, `t`.`id`
    //     LIMIT 0, 1

    $bonus_casino_field_id = DatabaseService::getField('casino_bonus', 'bonus_casino');
    $bonus_country_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');

    // $query = new SelectQuery;
    // $query
    //     ->select([
    //         't.id'
    //     ])
    //     ->from('wp_pods_casino_bonus', 't');

    // LEFT JOIN `wp_podsrel` AS `rel_bonus_casino` ON
    //     `rel_bonus_casino`.`field_id` = 17629
    //     AND `rel_bonus_casino`.`item_id` = `t`.`id`

    // LEFT JOIN `wp_pods_casino` AS `bonus_casino` ON
    //     `bonus_casino`.`id` = `rel_bonus_casino`.`related_item_id`

    // $query
    //   ->leftJoin('wp_podsrel', 'bonus_casino_field')
    //   ->on("t.id = bonus_casino_field.item_id AND bonus_casino_field.field_id = $bonus_casino_field_id");
    // $query
    //     ->leftJoin('wp_pods_casino', 'bonus_casino')
    //     ->on('bonus_casino_field.related_item_id = bonus_casino.id');

    // LEFT JOIN `wp_podsrel` AS `rel_bonus_countries` ON
    //     `rel_bonus_countries`.`field_id` = 17630
    //     AND `rel_bonus_countries`.`item_id` = `t`.`id`

    // LEFT JOIN `wp_pods_country` AS `bonus_countries` ON
    //     `bonus_countries`.`id` = `rel_bonus_countries`.`related_item_id`

    // $query
    //   ->leftJoin('wp_podsrel', 'bonus_country_field')
    //   ->on("t.id = bonus_country_field.item_id AND bonus_country_field.field_id = $bonus_country_field_id");
    // $query
    //     ->leftJoin('wp_pods_country', 'bonus_countries')
    //     ->on('bonus_country_field.related_item_id = bonus_countries.id');

    // WHERE ( `bonus_casino`.`id` = "40" AND `bonus_countries`.`id` IN (1) AND t.bonus_enabled = 1 )

    // $query
    //     ->where()
    //     ->equal('t.bonus_enabled', 1)
    //     ->equal('bonus_casino.id', $casino_id)
    //     ->in('bonus_countries.id', $country_id);

    // $data = (new DatabaseService($query, DatabaseService::GET_DATA))->build();
    // $data = !empty($data) ? reset($data) : FALSE;

    // d($data);
    // die();

    if (!empty($casino_id) && !empty($country_id)) {
        global $wpdb;

        $sql = "
            SELECT
                `t`.*
                FROM `wp_pods_casino_bonus` AS `t`
                
                    LEFT JOIN `wp_podsrel` AS `rel_bonus_casino` ON
                        `rel_bonus_casino`.`field_id` = $bonus_casino_field_id
                        AND `rel_bonus_casino`.`item_id` = `t`.`id`

                    LEFT JOIN `wp_pods_casino` AS `bonus_casino` ON
                        `bonus_casino`.`id` = `rel_bonus_casino`.`related_item_id`
                
                
                    LEFT JOIN `wp_podsrel` AS `rel_bonus_countries` ON
                        `rel_bonus_countries`.`field_id` = $bonus_country_field_id
                        AND `rel_bonus_countries`.`item_id` = `t`.`id`

                    LEFT JOIN `wp_pods_country` AS `bonus_countries` ON
                        `bonus_countries`.`id` = `rel_bonus_countries`.`related_item_id`
                
                WHERE ( `bonus_casino`.`id` = $casino_id AND `bonus_countries`.`id` IN ($country_id) AND t.bonus_enabled = 1 )
                
                ORDER BY `t`.`name`, `t`.`id`
                LIMIT 0, 1
        ";

        $data = $wpdb->get_results($sql);
        $bonus_enabled = !empty($data);
    }

    return $bonus_enabled;
}


/**
 * Check if casino info is enabled by language.
 */
function scg_casino_trackers_check_if_casino_info_is_enabled($casino_id, $language)
{
    $casino_info_enabled = FALSE;

    // Let's check if casino info is enabled.
    if (!empty($casino_id) && !empty($language)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'casino_info_casino.id = "' . $casino_id . '"',
                // ' AND ' . 't.language = "' . $language . '"',
            'limit' => 1,
        );

        $casino_info = pods('casino_info');
        $casino_info->find($params);
        $casino_info_data = $casino_info->data();

        if (!empty($casino_info_data) && !empty($casino_info_data[0])) {
            $casino_info_enabled = !empty($casino_info_data[0]->casino_info_enabled);
        }

    }

    return $casino_info_enabled;
}


/**
 * Check if casino rating by country and language exists.
 */
function scg_casino_trackers_check_if_casino_rating_country_imported($casino_id, $country_id, $language)
{
    $params = array(
        'distinct' => FALSE,
        'where' =>
            'rating_casino.id = "' . $casino_id . '"' . ' AND ' .
            'rating_countries.id IN (' . $country_id . ')',
            // ' AND ' . 'language = "' . $language . '"',
        'limit' => 1,
    );
    $casino_review = pods('casino_review');
    $casino_review->find($params);
    $casino_review_data = $casino_review->data();

    $casino_review_exists = FALSE;
    if (!empty($casino_review_data) && !empty($casino_review_data[0])) {
        $casino_review_exists = !empty($casino_review_data[0]->id);
    }

    return $casino_review_exists;
}


/**
 * Get casino link by country ID and game name.
 */
function scg_casino_trackers_check_if_casino_link_is_enabled($casino_id, $country_id, $game_type_id)
{
    $casino_link_enabled = FALSE;

    // Let's check if casino link is enabled.
    if (!empty($casino_id) && !empty($country_id) && !empty($game_type_id)) {
        // $game_type_id = scg_get_casino_game_type_id_from_name($game_name);

        // Let's check if this casino link (for this country) exists first.
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'casino_link_casino.id = "' . $casino_id . '"' . ' AND ' .
                'casino_link_countries.id IN (' . $country_id . ')' . ' AND ' .
                'casino_link_game.id = "' . $game_type_id . '"',
        );

        $casino_link = pods('casino_link');
        $casino_link->find($params);
        $casino_link_data = $casino_link->data();
        // d($casino_link->sql);

        if (!empty($casino_link_data) && !empty($casino_link_data[0])) {
            $casino_link_enabled = !empty($casino_link_data[0]->casino_link_status);
        }
    }

    return $casino_link_enabled;

}


/**
 * Get casino game review by game name and language.
 */
function scg_casino_trackers_get_casino_review_by_game($casino_id, $language, $game_name)
{
    $game_review_exists = FALSE;

    if (!empty($casino_id) && !empty($language) && !empty($game_name)) {
        $game_type_id = scg_get_casino_game_type_id_from_name($game_name);

        // Get casino game review.
        $params = array(
            'distinct' => false,
            'where' =>
                'casino_game_review_casino.id = "' . $casino_id . '"' . ' AND ' .
                't.language = "' . $language . '"' . ' AND ' .
                'casino_game_review_game.id = "' . $game_type_id . '"',
        );

        $game_review = pods('casino_game_review', $params);
        $game_review->find($params);
        $game_review_data = $game_review->data();

        if (!empty($game_review_data) && !empty($game_review_data[0])) {
            $game_review_exists = !empty($game_review_data[0]->casino_game_review_rating);
        }
    }

    return $game_review_exists;
}

/**
 * Get casinos by game.
 */
function scg_casino_trackers_get_casinos_by_game($game_name, $language)
{
    $game_type_id = scg_get_casino_game_type_id_from_name($game_name);

    // Get casino game review sorted by total rating by that game.
    $params = array(
        'distinct' => false,
        'where' => 'casino_game_review_game.id = "' . $game_type_id . '"' . ' AND ' .
            '`t` . `language` = "' . $language . '"',
        'orderby' => 't.casino_game_review_rating DESC',
        'offset' => 0,
    );

    $game_review_pods = pods('casino_game_review', $params);
    $game_review_pods->find($params);
    $data = $game_review_pods->data();

    $casinos = array();

    if (!empty($data) && !empty($data[0])) {

        foreach ($data as $entry) {
            $params = array(
                'distinct' => false,
                'where' => 't.id = "' . $entry->id . '"' . ' AND ' . '`t` . `language` = "' . $language . '"',
            );
            $game_review_pods = pods('casino_game_review', $params);

            $casino_id = $game_review_pods->field('casino_game_review_casino')['id'];

            $casinos[] = $casino_id;
        }
    }

    return $casinos;
}


function scg_casino_trackers_get_limited_casinos_list_by_game_node($game_node_name) {
        // $game_node_query = new SelectQuery;

        // $game_node_query
        //     ->select([
        //         'id'
        //     ])
        //     ->from('wp_pods_casino_game_node', 't');

        // $game_node_query
        //     ->where()
        //     ->equal('t.name', $game_node_name);

        // $game_node_data = (new DatabaseService($game_node_query, DatabaseService::GET_DATA))->build();

        global $wpdb;
        // $game_node_name = utf8_decode($game_node_name);
        $sql = "SELECT id FROM wp_pods_casino_game_node WHERE name = '$game_node_name'";
        $game_node_data = $wpdb->get_results($sql);
        $game_node_id = !empty($game_node_data) ? $game_node_data[0]->id : FALSE;

        if (!$game_node_id) {
            return;
        }

        $query = new SelectQuery;

        $query
            ->select([
                'rel_casino_game_node.related_item_id',
            ])
            ->from(DatabaseService::REL_TABLE, 'rel_casino_game_node');

        $casino_game_node_casinos_field_id = DatabaseService::getField('casino_game_node', 'casino_game_node_casinos');

        $query
            ->where()
            ->equal('rel_casino_game_node.field_id', $casino_game_node_casinos_field_id)
            ->equal('rel_casino_game_node.item_id', $game_node_id);

        $data = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

        $limited_casinos = array();
        if ($data) {
            foreach ($data as $entry) {
                $limited_casinos[] = (int) $entry->related_item_id;
            }
        }

        return $limited_casinos;
}

/**
 * Get limited casinos by game.
 */
function scg_casino_trackers_get_limited_casinos_by_game($game_name, $country_name, $language, $limited_casinos = array(), $top1 = FALSE, $extra_data = array())
{
    (new DatabaseService())->setUp();
    $from_cache = true;
    $casinos = [];
    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    $country_id = scg_get_country_id_from_name(trim($country_name));
    $original_country_id = $country_id;

    if (!empty($have_this_country_bonuses)) {
        // Get country ID using pods query.
        $country_id = $country_id;
    } else {
        // Use "All Countries" country.
        $country_id = scg_get_country_id_from_name('All Countries');
    }

    // Use "All Countries" ID when country_id is empty.
    $country_id = empty($country_id) ? scg_get_country_id_from_name('All Countries') : $country_id;
    $original_country_id = empty($original_country_id) ? scg_get_country_id_from_name('All Countries') : $original_country_id;
    $original_country_id = (string)$original_country_id;

    $game_name_lower = strtolower(str_replace(' ', '_', $game_name));

    $transient_sheet_name = scg_casino_trackers_get_casinos_transient_sheet_name($country_name);
    $transient_name = 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name;

    $casinos_query = new SelectQuery;

    $casinos_query->select(['data', 'name'])->from('wp_casino_cache')
        ->where()
        ->equal('name', 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name);

    if (isset($_GET['no-cache'])) {
        $casinos = false;
        $from_cache = false;
    }

    if (! empty($extra_data['scg_all_info'])) {
        $casinos_query
            ->leftJoin('wp_casino_cache_rel', 'cache_rel')
            ->on("cache_rel.casino_cache_id = wp_casino_cache.id");

        $casinos_query->groupBy('casino_cache_id');

        $game_provider_name = $extra_data['scg_all_info']['game_provider'];
        $game_provider_id = !empty($game_provider_name) ? scg_get_game_provider_id_from_name($game_provider_name) : FALSE;

        $license_name = $extra_data['scg_all_info']['license'];
        $license_id = !empty($license_name) ? scg_get_or_create_license_id_from_name($license_name) : FALSE;

        $deposit_method = $extra_data['scg_all_info']['deposit_method'];
        $deposit_method_id = !empty($deposit_method) ? scg_get_payment_method_id_from_name($deposit_method) : FALSE;

        $crypto_deposit = $extra_data['scg_all_info']['crypto_deposit'];
        $crypto_deposit_id = !empty($crypto_deposit) ? scg_get_crypto_id_from_name($crypto_deposit) : FALSE;

        // Custom tracker game type.
        $tracker_game_type = $extra_data['scg_all_info']['tracker_game_type'];
        $tracker_game_type_id = !empty($tracker_game_type) ? (int) scg_get_casino_game_type_id_from_name($tracker_game_type) : FALSE;

        // Established from
        $established_from = $extra_data['scg_all_info']['established_from'];

        if ($game_provider_id)
            $casinos_query
                ->where()
                ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $game_provider_id . "', '$.game_provider')");

        if ($license_id)
            $casinos_query
                ->where()
                ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $license_id . "', '$.license')");

        if ($deposit_method_id)
            $casinos_query
                ->where()
                ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $deposit_method_id . "', '$.deposit_method')");
    }

    if ($from_cache) {
        $casinos_data = (new DatabaseService($casinos_query, DatabaseService::GET_ITEM, 'data'))->build();

        if ($casinos_data) $casinos = json_decode($casinos_data);
    }

    if (empty($casinos)) {
        if (!empty($game_name) && !empty($country_id) && !empty($language)) {
            $game_type_id_index = scg_get_casino_game_type_id_from_name($game_name);
            $casinos = [];

            $query = new SelectQuery;

            $query
                ->select([
                    't.id AS casino_id',
                    'casino_info_list.id AS casino_info_id',
                    'casino_bonuses.bonus_text AS casino_bonus_text',
                    'casino_bonuses.bonus_sum AS casino_bonus_sum',
                    'casino_bonuses_bonus_currency.currency_sign AS casino_bonus_currency',
                    'casino_link_list.casino_link_status AS casino_link_status',
                    'casino_link_list.casino_link AS casino_link',
                    'casino_link_list.casino_link_button_text AS casino_link_button_text',
                    'casino_link_list.casino_link_regulation_text AS casino_link_regulation_text',
                    'casino_game_review_list.casino_game_review_rating AS casino_game_review_rating'
                ])
                ->from('wp_pods_casino', 't');

            $casino_info_list_field_id = DatabaseService::getField('casino', 'casino_info_list');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_info_list')
                ->on("rel_casino_info_list.field_id = $casino_info_list_field_id AND rel_casino_info_list.item_id = t.id");
            $query
                ->leftJoin('wp_pods_casino_info', 'casino_info_list')
                ->on("casino_info_list.id = rel_casino_info_list.related_item_id");

            $casino_bonuses_field_id = DatabaseService::getField('casino', 'casino_bonuses');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_bonuses')
                ->on("rel_casino_bonuses.field_id = $casino_bonuses_field_id AND rel_casino_bonuses.item_id = t.id");
            $query
                ->leftJoin('wp_pods_casino_bonus', 'casino_bonuses')
                ->on("casino_bonuses.id = rel_casino_bonuses.related_item_id");

            $casino_bonus_bonus_currency_field_id = DatabaseService::getField('casino_bonus', 'bonus_currency');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_bonuses_bonus_currency')
                ->on("rel_casino_bonuses_bonus_currency.field_id = $casino_bonus_bonus_currency_field_id AND rel_casino_bonuses_bonus_currency.item_id = casino_bonuses.id");
            $query
                ->leftJoin('wp_pods_currency', 'casino_bonuses_bonus_currency')
                ->on("casino_bonuses_bonus_currency.id = rel_casino_bonuses_bonus_currency.related_item_id");

            $casino_link_list_field_id = DatabaseService::getField('casino', 'casino_link_list');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list')
                ->on("rel_casino_link_list.field_id = $casino_link_list_field_id AND rel_casino_link_list.item_id = t.id");
            $query
                ->leftJoin('wp_pods_casino_link', 'casino_link_list')
                ->on("casino_link_list.id = rel_casino_link_list.related_item_id");

            $casino_game_review_list_field_id = DatabaseService::getField('casino', 'casino_game_review_list');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list')
                ->on("rel_casino_game_review_list.field_id = $casino_game_review_list_field_id AND rel_casino_game_review_list.item_id = t.id");
            $query
                ->leftJoin('wp_pods_casino_game_review', 'casino_game_review_list')
                ->on("casino_game_review_list.id = rel_casino_game_review_list.related_item_id");

            $casino_game_review_game_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_game');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list_casino_game_review_game')
                ->on("rel_casino_game_review_list_casino_game_review_game.field_id = $casino_game_review_game_field_id AND rel_casino_game_review_list_casino_game_review_game.item_id = casino_game_review_list.id");
            $query
                ->leftJoin('wp_pods_casino_game', 'casino_game_review_list_casino_game_review_game')
                ->on("casino_game_review_list_casino_game_review_game.id = rel_casino_game_review_list_casino_game_review_game.related_item_id");

            $casino_game_review_game_review_countries_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_countries');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_game_review_list_casino_game_review_countries')
                ->on("rel_casino_game_review_list_casino_game_review_countries.field_id = $casino_game_review_game_review_countries_field_id AND rel_casino_game_review_list_casino_game_review_countries.item_id = casino_game_review_list.id");
            $query
                ->leftJoin('wp_pods_country', 'casino_game_review_list_casino_game_review_countries')
                ->on("casino_game_review_list_casino_game_review_countries.id = rel_casino_game_review_list_casino_game_review_countries.related_item_id");

            $casino_link_countries_field_id = DatabaseService::getField('casino_link', 'casino_link_countries');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_countries')
                ->on("rel_casino_link_list_casino_link_countries.field_id = $casino_link_countries_field_id AND rel_casino_link_list_casino_link_countries.item_id = casino_link_list.id");
            $query
                ->leftJoin('wp_pods_country', 'casino_link_list_casino_link_countries')
                ->on("casino_link_list_casino_link_countries.id = rel_casino_link_list_casino_link_countries.related_item_id");

            $casino_link_game_field_id = DatabaseService::getField('casino_link', 'casino_link_game');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_link_list_casino_link_game')
                ->on("rel_casino_link_list_casino_link_game.field_id = $casino_link_game_field_id AND rel_casino_link_list_casino_link_game.item_id = casino_link_list.id");
            $query
                ->leftJoin('wp_pods_casino_game', 'casino_link_list_casino_link_game')
                ->on("casino_link_list_casino_link_game.id = rel_casino_link_list_casino_link_game.related_item_id");

            $casino_bonus_countries_field_id = DatabaseService::getField('casino_bonus', 'bonus_countries');
            $query
                ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_bonuses_bonus_countries')
                ->on("rel_casino_bonuses_bonus_countries.field_id = $casino_bonus_countries_field_id AND rel_casino_bonuses_bonus_countries.item_id = casino_bonuses.id");
            $query
                ->leftJoin('wp_pods_country', 'casino_bonuses_bonus_countries')
                ->on("casino_bonuses_bonus_countries.id = rel_casino_bonuses_bonus_countries.related_item_id");

            // SCG All Info - Game Providers, Licenses, Deposit Methods.
            if (!empty($extra_data['scg_all_info'])) {
                // Custom sort by.
                $sort_by = $extra_data['scg_all_info']['sort_by'];
                $sort_by_year_desc = !empty($sort_by) && $sort_by == '<year';
                $sort_by_year_asc = !empty($sort_by) && $sort_by == '>year';
                $sort_by_rating_asc = !empty($sort_by) && $sort_by == '>rating';

                $casino_main_info_field_id = DatabaseService::getField('casino', 'casino_main_info');
                $query
                    ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_main_info')
                    ->on("rel_casino_main_info.field_id = $casino_main_info_field_id AND rel_casino_main_info.item_id = t.id");
                $query
                    ->leftJoin('wp_pods_casino_main_info', 'casino_main_info')
                    ->on("casino_main_info.id = rel_casino_main_info.related_item_id");

                if (!empty($game_provider_id)) {
                    $game_providers_field_id = DatabaseService::getField('casino_main_info', 'game_providers');
                    $query
                        ->leftJoin(DatabaseService::REL_TABLE, 'rel_game_providers')
                        ->on("rel_game_providers.field_id = $game_providers_field_id AND rel_game_providers.item_id = casino_main_info.id");
                    $query
                        ->leftJoin('wp_pods_casino_game_provider', 'game_provider')
                        ->on("game_provider.id = rel_game_providers.related_item_id");
                }

                if (!empty($license_id)) {
                    $licenses_field_id = DatabaseService::getField('casino_main_info', 'licenses');
                    $query
                        ->leftJoin(DatabaseService::REL_TABLE, 'rel_licenses')
                        ->on("rel_licenses.field_id = $licenses_field_id AND rel_licenses.item_id = casino_main_info.id");
                    $query
                        ->leftJoin('wp_pods_license', 'license')
                        ->on("license.id = rel_licenses.related_item_id");
                }

                if (!empty($deposit_method_id)) {
                    $deposit_methods_field_id = DatabaseService::getField('casino_main_info', 'deposit_methods');
                    $query
                        ->leftJoin(DatabaseService::REL_TABLE, 'rel_deposit_methods')
                        ->on("rel_deposit_methods.field_id = $deposit_methods_field_id AND rel_deposit_methods.item_id = casino_main_info.id");
                    $query
                        ->leftJoin('wp_pods_payment_method', 'payment_method')
                        ->on("payment_method.id = rel_deposit_methods.related_item_id");
                }

                if (!empty($crypto_deposit_id)) {
                    $crypto_deposits_field_id = DatabaseService::getField('casino_main_info', 'crypto_deposits');
                    $query
                        ->leftJoin(DatabaseService::REL_TABLE, 'rel_crypto_deposits')
                        ->on("rel_crypto_deposits.field_id = $crypto_deposits_field_id AND rel_crypto_deposits.item_id = casino_main_info.id");
                    $query
                        ->leftJoin('wp_pods_cryptocurrency', 'cryptocurrency')
                        ->on("cryptocurrency.id = rel_crypto_deposits.related_item_id");
                }
            }

            $query
                ->where()
                ->equal('t.casino_enabled', 1)
                ->equal('casino_info_list.language', $language)
                ->equal('casino_info_list.casino_info_enabled', 1)
                ->equal('casino_game_review_list_casino_game_review_game.id', $game_type_id_index)
                ->equal('casino_game_review_list_casino_game_review_countries.id', $country_id)
                ->equal('casino_game_review_list.language', $language)
                ->equal('casino_link_list_casino_link_countries.id', $country_id)
                ->equal('casino_link_list.casino_link_status', 1)
                ->equal('casino_link_list_casino_link_game.id', $game_type_id_index)
                ->equal('casino_bonuses_bonus_countries.id', $country_id)
                ->equal('casino_bonuses.bonus_enabled', 1);

            // Game provider filter.
            if (!empty($game_provider_id)) {
                $query
                    ->where()
                    ->equal('game_provider.id', $game_provider_id);
            }

            // License filter.
            if (!empty($license_id)) {
                $query
                    ->where()
                    ->equal('license.id', $license_id);
            }

            // Deposit method filter.
            if (!empty($deposit_method_id)) {
                $query
                    ->where()
                    ->equal('payment_method.id', $deposit_method_id);
            }

            // Crypto deposit filter.
            if (!empty($crypto_deposit_id)) {
                $query
                    ->where()
                    ->equal('cryptocurrency.id', $crypto_deposit_id);
            }

            // Established from year filter.
            if (!empty($established_from)) {
                $query
                    ->where()
                    ->greaterThanOrEqual('casino_main_info.established_date', $established_from);
            }

            $query->groupBy('t.id');

            $query
                ->orderBy('casino_game_review_rating', 'DESC')
                ->orderBy('t.name', 'ASC')
                ->orderBy('t.id', 'ASC');

            $data = (new DatabaseService($query))->build();

            if ($data) {
                $casino_cache_id = (new DatabaseService)->firstOrCreate(
                    'wp_casino_cache',
                    'name', 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name,
                    [
                        'name' => 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name
                    ]
                );

                foreach ($data as $item) {
                    $casino_id = (int)$item->casino_id;
                    $casino_info_id = (int)$item->casino_info_id;
                    $casino_info_enabled = (bool)$item->casino_info_enabled;
                    $casino_info = !empty($casino_info_id) ? scg_casino_trackers_get_casino_info($casino_info_id) : FALSE;
                    $casino_logo_id = scg_casino_trackers_get_casino_logo_id($casino_info_id);
                    $casino_review_id = (int)$item->casino_review_id;
                    $casino_game_review_rating = $item->casino_game_review_rating;

                    // Override default tracker when
                    // tracker-game-type is provided.
                    $tracker_game_type_link = !empty($tracker_game_type_id) ? scg_casino_trackers_get_casino_link($casino_id, $tracker_game_type_id, $country_id) : FALSE;

                    $casino_link = $item->casino_link;

                    // Override default tracker link.
                    if (!empty($tracker_game_type_link)) {
                        $casino_link = $tracker_game_type_link;
                    }

                    $casino_button_text = $item->casino_link_button_text;
                    $casino_regulation_text = $item->casino_link_regulation_text;
                    $casino_bonus_text = $item->casino_bonus_text;
                    $casino_bonus_sum = $item->casino_bonus_sum;
                    $casino_bonus_currency = $item->casino_bonus_currency;

                    $casino_restricted_countries = !empty($casino_id) ? scg_casino_trackers_get_casino_restricted_countries($casino_id) : array();

                    $casinos[] = array(
                        'casino_id' => $casino_id,
                        'casino_info_id' => $casino_info_id,
                        'casino_info' => $casino_info,
                        'casino_logo_id' => $casino_logo_id,
                        'casino_rating' => $casino_game_review_rating,
                        'casino_link' => $casino_link,
                        'casino_button_text' => $casino_button_text,
                        'casino_regulation_text' => $casino_regulation_text,
                        'casino_bonus_text' => $casino_bonus_text,
                        'casino_bonus_sum' => $casino_bonus_sum,
                        'casino_bonus_currency' => $casino_bonus_currency,
                        'casino_restricted_countries' => $casino_restricted_countries,
                    );

                    (new DatabaseService)->createOrUpdate(
                        'wp_casino_cache_rel',
                        [
                            'casino_cache_id' => $casino_cache_id,
                            'casino_id' => $casino_id,
                            'country_id' => $country_id,
                            'game_type_id' => $game_type_id_index ?: null,
                        ],
                        [
                            'casino_cache_id' => $casino_cache_id,
                            'casino_id' => $casino_id,
                            'country_id' => $country_id,
                            'game_type_id' => $game_type_id_index ?: null,
                            'extra_data' => json_encode([
                                'game_provider' => $game_provider_id,
                                'license' => $license_id,
                                'deposit_method' => $deposit_method_id,
                            ])
                        ]
                    );
                }

                (new DatabaseService)
                    ->update('wp_casino_cache', $casino_cache_id, ['data' => json_encode($casinos)]);
            }

            // 25 hours - actually it's not expiring as we
            // re-generate transients every 24 hours after
            // all casino trackers import (in the morining).
            // set_transient('scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name, $casinos, 25 * 60 * 60);
        }

    }

    $casinos = json_decode(json_encode($casinos), true);

    if (!empty($casinos)) {
        foreach ($casinos as $key => $casino) {
            $casino_restricted = FALSE;
            foreach ($casino['casino_restricted_countries'] as $restricted_country) {
                // Mark restricted casino in this country.
                if ($restricted_country == $original_country_id) {
                    $casino_restricted = TRUE;
                    $casinos[$key]['casino_restricted'] = $casino_restricted;
                }
            }

            // Remove restricted casino in this country.
            if (!empty($casino_restricted) && empty($extra_data['scg_all_info']['show_restricted'])) {
                unset($casinos[$key]);
            }
        }
    }

    if (!empty($casinos) && !empty($limited_casinos)) {
        // Transform string to array with integers from Slot.
        $limited_casinos = is_string($limited_casinos) ? array_map('intval', explode(',', $limited_casinos)) : $limited_casinos;

        foreach ($casinos as $key => $casino) {
            $casino_is_in_limited_array = scg_casino_trackers_has_casino_in_limited_array($limited_casinos, $casino['casino_id']);

            if (empty($casino_is_in_limited_array)) {
                unset($casinos[$key]);
            }
        }
    }

    // Default sort restricted casinos
    // in bottom when show_restricted="TRUE".
    if (!empty($extra_data['scg_all_info']['show_restricted'])) {
        usort($casinos, function ($a, $b) {
            // Order TOP casinos by:
            // casino_restricted (bool) DESC
            // casino_rating (float) DESC
            if ($a['casino_restricted'] === $b['casino_restricted']) {
                return $b['casino_rating'] <=> $a['casino_rating'];
            }
            return $a['casino_restricted'] <=> $b['casino_restricted'];
        });
    }

    // Override default sort
    // by custom provided.
    if (!empty($sort_by)) {
        if ($sort_by_year_desc) {
            usort($casinos, function ($a, $b) {
                // Order TOP casinos by:
                // casino_info_open_year  (int)  DESC
                return $b['casino_info']['casino_info_open_year'] <=> $a['casino_info']['casino_info_open_year'];
            });
        }

        if ($sort_by_year_asc) {
            usort($casinos, function ($a, $b) {
                // Order TOP casinos by:
                // casino_info_open_year  (int)  ASC
                return $a['casino_info']['casino_info_open_year'] <=> $b['casino_info']['casino_info_open_year'];
            });
        }

        if ($sort_by_rating_asc) {
            usort($casinos, function ($a, $b) {
                // Order TOP casinos by:
                // casino_rating (float) ASC
                return $a['casino_rating'] <=> $b['casino_rating'];
            });
        }
    }

    if (empty($top1) && !empty($casinos)) {
        // Offset TOP1 casino as we are showing Other Casinos.
        $casinos = array_splice($casinos, 1);
    }

    return $casinos;
}


function new_review_testing() {
  $casino_id = 3;
  $casino_id = FALSE;
  $game_type_id = 27;
  $tracker_game_type_id = 20;
  $country_id = 1;

  $review = scg_casino_trackers_get_casino_game_type_review_single(
  $casino_id, $game_type_id, $country_id, $tracker_game_type_id);

  d($review);
  die();
}

// add_action('init', 'new_review_testing');


/**
 * Get casino top rating - All or by Game type,
 * also by country name and language.
 * Uses transients to cache data.
 */
function scg_casino_trackers_get_top_rating($game_type = FALSE, $country_name, $language, $extra_data = array())
{
    // (new DatabaseService)->setUp();
    // $casinos = [];
    // $from_cache = true;

    // d($country_name);
    // d($language);
    // d($extra_data);

    // Map olds LT values with new ones.
    // $game_type_lt_map = array(
    //     'Ruletė'            => 'Roulette',
    //     'Blackjack'         => 'BlackJack',
    //     'Lošimų automatai'  => 'Slots',
    //     'Others'            => 'Other Games',
    //     'Loterija'          => 'Lotto',
    //     'Video poker'       => 'Video Poker',
    //     'Poker games'       => 'Card Games',
    //     'Live casino'       => 'Live Casino',
    //     'Others'            => 'Other Games',
    // );

    // Map olds LT values with new ones.
    // $game_type_lt_map = array(
    //     'Roulette'      => 'Ruletė',            // UPDATE
    //     'Blackjack'     => 'BlackJack',
    //     'Slots'         => 'Lošimų automatai',  // UPDATE
    //     'Other Games'   => 'Others',            // UPDATE
    //     'Lotto'         => 'Loterija',          // UPDATE
    //     // Video Poker          - UPDATE
    //     // Poker games > Poker  - UPDATE
    // );

    // $game_type_mapped = $game_type_lt_map[$game_type];
    // $game_type = !empty($game_type_mapped) ? $game_type_mapped : $game_type;

    $country_ids = scg_get_country_ids_based_on_bonus_country($country_name);
    $country_id = !empty($country_ids) ? $country_ids['country_id'] : FALSE;
    $original_country_id = !empty($country_ids) ? (int)$country_ids['original_country_id'] : FALSE;
    $country_name = scg_get_country_name_from_id($country_id);

    // var_dump($country_ids);
    // var_dump($country_id);
    // var_dump($original_country_id);
    // var_dump($country_name);

    $game_name_lower = !empty($game_type) ? strtolower(str_replace(' ', '_', $game_type)) : 'main';
    $game_type_id = !empty($game_type) ? scg_get_casino_game_type_id_from_name($game_type) : FALSE;
    $game_type_id_index = scg_get_casino_game_type_id_from_name('Index Page');

    $transient_sheet_name = scg_casino_trackers_get_casinos_transient_sheet_name($country_name);

    // d($transient_sheet_name);
    // d('scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name);
    // die();

    // var_dump($country_name);
    // var_dump($country_ids);
    // var_dump('scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name);

    $tracker_game_type_id = FALSE;

    if (! empty($extra_data['scg_all_info'])) {
        // $casinos_query
        //     ->leftJoin('wp_casino_cache_rel', 'cache_rel')
        //     ->on("cache_rel.casino_cache_id = wp_casino_cache.id");

        // $casinos_query->groupBy('casino_cache_id');

        // Single casino.
        $single_casino_name = isset($extra_data['scg_all_info']['single_casino']) ? $extra_data['scg_all_info']['single_casino']['casino_name'] : FALSE;

        $game_provider_name = $extra_data['scg_all_info']['game_provider'];
        $game_provider_id = !empty($game_provider_name) ? scg_get_game_provider_id_from_name($game_provider_name) : FALSE;

        $license_name = $extra_data['scg_all_info']['license'];
        $license_id = !empty($license_name) ? scg_get_or_create_license_id_from_name($license_name) : FALSE;

        $deposit_method = $extra_data['scg_all_info']['deposit_method'];
        $deposit_method_id = !empty($deposit_method) ? scg_get_payment_method_id_from_name($deposit_method) : FALSE;

        $crypto_deposit = $extra_data['scg_all_info']['crypto_deposit'];
        $crypto_deposit_id = !empty($crypto_deposit) ? scg_get_crypto_id_from_name($crypto_deposit) : FALSE;

        // Custom tracker game type.
        $tracker_game_type = $extra_data['scg_all_info']['tracker_game_type'];
        $tracker_game_type_id = !empty($tracker_game_type) ? (int) scg_get_casino_game_type_id_from_name($tracker_game_type) : FALSE;

        // d($tracker_game_type);
        // d($tracker_game_type_id);
        // die();

        // Established from
        $established_from = $extra_data['scg_all_info']['established_from'];

        // Custom sort by.
        $sort_by = !empty($extra_data['scg_all_info']['sort_by']) ? $extra_data['scg_all_info']['sort_by'] : FALSE;
        $sort_by_year_desc = !empty($sort_by) && $sort_by == '<year';
        $sort_by_year_asc = !empty($sort_by) && $sort_by == '>year';
        $sort_by_rating_asc = !empty($sort_by) && $sort_by == '>rating';

        // if ($game_provider_id)
        //     $casinos_query
        //         ->where()
        //         ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $game_provider_id . "', '$.game_provider')");

        // if ($license_id)
        //     $casinos_query
        //         ->where()
        //         ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $license_id . "', '$.license')");

        // if ($deposit_method_id)
        //     $casinos_query
        //         ->where()
        //         ->raw("JSON_CONTAINS(cache_rel.extra_data, '" . $deposit_method_id . "', '$.deposit_method')");
    }

    // Add tracker game type to cache.
    if (!empty($tracker_game_type) && !empty($tracker_game_type_id)) {
        $game_name_lower .= '_tracker_' . $tracker_game_type;
    }

    $transient_name = 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name;

    // Get from transient cache.
    $casinos = get_transient('scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name);

    // d($casinos);
    // die();

    // if (!empty($game_type)) {
    //     d($game_type);
    //     d($game_type_id);
    //     die();
    // }

    if (empty($casinos)) {
        if (!empty($country_id)) {
        // NEW
        // Using Country Relation.
        $query = new SelectQuery;
        $query
            ->select([
                't.casino_id AS casino_id',
                'casino.name AS casino_name',
                
                'info.id AS casino_info_id',
                'info.casino_info_enabled AS casino_info_enabled',
                'info.casino_info_new_casino AS casino_new_casino',
                
                'bonus_country.bonus_text AS casino_bonus_text',
                'bonus_country.bonus_sum AS casino_bonus_sum',
                'currency.currency_sign AS casino_bonus_currency',
                'bonus_country.bonus_list_affiliate_link AS casino_link',
                'bonus_country.bonus_list_button_text AS casino_link_button_text',
                'bonus_country.bonus_list_regulation_text AS casino_link_regulation_text',
                'bonus_country.bonus_shortcode_button_text AS casino_shortcode_button_text',
                'bonus_country.bonus_shortcode_regulation_text AS casino_shortcode_regulation_text',
                'bonus_country.bonus_review_button_text AS casino_bonus_review_button_text',
                'bonus_country.bonus_review_affiliate_link AS casinor_bonus_review_affiliate_link',
                'bonus_country.bonus_review_regulation_text AS casino_bonus_review_regulation_text',

                'main_review.rating_total AS casino_top_rating',
            ])
            ->from('wp_pods_country_relation', 't');
        $query
            ->leftJoin('wp_pods_casino', 'casino')
            ->on('t.casino_id = casino.id');
        $query
            ->leftJoin('wp_pods_casino_review', 'main_review')
            ->on('t.main_review_id = main_review.id');
        $query
            ->leftJoin('wp_pods_casino_info', 'info')
            ->on('t.casino_id = info.casino_id');
        $query
            ->leftJoin('wp_pods_casino_bonus', 'bonus_country')
            ->on('t.bonus_id = bonus_country.id');
        $query
            ->leftJoin('wp_pods_currency', 'currency')
            ->on('bonus_country.currency_id = currency.id');

        $query
            ->where()
            ->equal('t.country_id', $country_id)
            ->isNot('t.main_review_id', NULL)
            ->isNot('t.bonus_id', NULL)
            ->equal('casino.casino_enabled', 1)
            ->equal('casino.casino_restricted_enabled', 1)
            ->equal('bonus_country.bonus_enabled', 1)
            ->equal('info.casino_info_enabled', 1);
            // ->equal('info.language', $language);

        // Special query logic when TOP rating. 
        if (empty($game_type) && empty($single_casino_name)) {
            $query
                ->where()
                ->greaterThan('main_review.rating_total', '0.00') 
                ->greaterThan('bonus_country.bonus_list_affiliate_link', '')
                ->greaterThan('bonus_country.bonus_list_button_text', '')
                ->greaterThan('bonus_country.bonus_list_regulation_text', '');
        }

        // Sort by.
        $query
          ->groupBy('t.casino_id')
          ->orderBy('casino_new_casino', 'DESC')
          ->orderBy('casino_top_rating', 'DESC')
          ->orderBy('casino_name', 'ASC');

        $data = (new DatabaseService($query))->build();

        if ($game_type_id == 13) {
            // d($casinos);
            // die();
        }


        // if ($game_type_id == 13) {
        //     d($game_type);
        //     d($game_type_id);
        //     d($country_id);
        //     d($tracker_game_type_id);
        //     // die();
        // }

        // All game type reviews for game type.
        $casino_game_type_reviews = !empty($game_type) && !empty($game_type_id) ? scg_casino_trackers_get_casino_game_type_review_single(FALSE, $game_type_id, $country_id, $tracker_game_type_id) : FALSE;

        // d($data);
        // d($casino_game_type_reviews);
        // if ($game_type_id == 13) {
        //     die();
        // }

            if ($data) {
                foreach ($data as $item) {
                    $casino_id = (int)$item->casino_id;

                    $casino_name = $item->casino_name;
                    $casino_info_id = (int)$item->casino_info_id;
                    $casino_info_enabled = (bool)$item->casino_info_enabled;
                    $casino_info = !empty($casino_info_id) ? scg_casino_trackers_get_casino_info($casino_info_id) : FALSE;

                    // d($casino_info);

                    $casino_logo_id = scg_casino_trackers_get_casino_logo_id($casino_info_id);

                    // Override default tracker when
                    // tracker-game-type is provided.
                    $tracker_game_type_link = !empty($tracker_game_type_id) ? scg_casino_trackers_get_casino_link($casino_id, $tracker_game_type_id, $country_id) : FALSE;

                    // Allow using "TOP" for easier use. 
                    // if ($tracker_game_type == 'TOP') {
                    //     $tracker_game_type_link = $casino_link;
                    // } 

                    $casino_game_type_review = !empty($casino_game_type_reviews) && !empty($game_type_id) ?  scg_casino_single_casino_game_type_review($casino_id, $casino_game_type_reviews) : FALSE;

                    // if ($casino_id == 3) {
                    //   d($casino_game_type_reviews);
                    //   d($casino_id);
                    //   d($casino_game_type_review);
                    //   die();
                    // }

                    // Override default tracker link.
                    if (!empty($tracker_game_type_link) && !empty($casino_game_type_review)) {
                        $casino_game_type_review['casino_game_link'] = $tracker_game_type_link;
                    }

                    // Remove game type review if game type ID
                    // is provided but game type link is not found.
                    if (
                        !empty($tracker_game_type_id) && 
                        $tracker_game_type_id != 99999 && 
                        empty($tracker_game_type_link) && 
                        !empty($casino_game_type_review)
                    ) {
                        $casino_game_type_review = FALSE;
                    }

                    $casino_review_button_text = $item->casino_bonus_review_button_text;
                    $casino_review_affiliate_link = $item->casino_bonus_review_affiliate_link;
                    $casino_review_regulation_text = $item->casino_bonus_review_regulation_text;

                    $casino_top_rating = (float)$item->casino_top_rating;
                    $casino_link = $item->casino_link;

                    $casino_link = !empty($tracker_game_type_link) ? $tracker_game_type_link : $casino_link;

                    $casino_link_index = !empty($casino_id) ? scg_casino_trackers_get_casino_link($casino_id, $game_type_id_index, $country_id) : FALSE;
                    $casino_button_text = $item->casino_link_button_text;
                    $casino_regulation_text = $item->casino_link_regulation_text;
                    $casino_shortcode_button_text = $item->casino_shortcode_button_text;
                    $casino_shortcode_regulation_text = $item->casino_shortcode_regulation_text;
                    $casino_restricted_countries = !empty($casino_id) ? scg_casino_trackers_get_casino_restricted_countries($casino_id) : array();

                    $casino_main_info = scg_casino_trackers_get_casino_main_info($casino_id);

                    $casino_bonus_text = $item->casino_bonus_text;
                    $casino_bonus_sum = $item->casino_bonus_sum;
                    $casino_bonus_currency = $item->casino_bonus_currency;

                    if (!empty($casino_id)) {
                        $casinos[] = array(
                            'casino_id' => $casino_id,
                            'casino_name' => $casino_name,
                            'casino_info_id' => $casino_info_id,
                            'casino_logo_id' => $casino_logo_id,
                            'casino_rating' => $casino_top_rating,
                            'tracker_game_type_link' => $tracker_game_type_link,
                            'casino_link' => $casino_link,
                            'casino_link_index' => $casino_link_index,
                            'casino_button_text' => $casino_button_text,
                            'casino_regulation_text' => $casino_regulation_text,
                            'casino_short_button_text' => $casino_shortcode_button_text,
                            'casino_short_regulation_text' => $casino_shortcode_regulation_text,
                            'casino_review_button_text' => $casino_review_button_text,
                            'casino_review_affiliate_link' => $casino_review_affiliate_link,
                            'casino_review_regulation_text' => $casino_review_regulation_text,
                            'casino_bonus_text' => $casino_bonus_text,
                            'casino_bonus_sum' => $casino_bonus_sum,
                            'casino_bonus_currency' => $casino_bonus_currency,
                            'casino_info' => $casino_info,
                            'casino_main_info' => $casino_main_info,
                            'casino_game_type_review' => $casino_game_type_review,
                            'casino_restricted_countries' => $casino_restricted_countries,
                        );

                        // (new DatabaseService)->createOrUpdate(
                        //     'wp_casino_cache_rel',
                        //     [
                        //         'casino_cache_id' => $casino_cache_id,
                        //         'casino_id' => $casino_id,
                        //         'country_id' => $country_id,
                        //         'game_type_id' => $game_type_id ?: null,
                        //     ],
                        //     [
                        //         'casino_cache_id' => $casino_cache_id,
                        //         'casino_id' => $casino_id,
                        //         'country_id' => $country_id,
                        //         'game_type_id' => $game_type_id ?: null,
                        //         'extra_data' => json_encode([
                        //             'game_provider' => $game_provider_id,
                        //             'license' => $license_id,
                        //             'deposit_method' => $deposit_method_id,
                        //         ])
                        //     ]
                        // );
                    }
                }

                // (new DatabaseService)
                //     ->update('wp_casino_cache', $casino_cache_id, ['data' => json_encode($casinos)]);
            }

            // 25 hours - actually it's not expiring as we
            // re-generate transients every 24 hours after
            // all casino trackers import (in the morining).
            // set_transient('scg_casinos_filter_top_rating_' . $game_name_lower . '_' . $transient_sheet_name, $casinos, 25 * 60 * 60);
            set_transient('scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name, $casinos, 25 * 60 * 60);
        }
    }

    // d($casinos);
    // die();

        // if ($game_type_id == 13) {
        //     d($casinos);
        //     // die();
        // }

    // $casinos = json_decode(json_encode($casinos), true);

    if (!empty($casinos)) {
        foreach ($casinos as $key => $casino) {
            $casino_restricted = FALSE;
            foreach ($casino['casino_restricted_countries'] as $restricted_country) {
                // Mark restricted casino in this country.
                if ($restricted_country == $original_country_id) {
                    $casino_restricted = TRUE;
                    $casinos[$key]['casino_restricted'] = $casino_restricted;
                }
            }

            // var_dump($casino_restricted);
            // var_dump($casinos[$key]['casino_id']);

            // Remove restricted casino in this country.
            if (!empty($casino_restricted) && empty($extra_data['scg_all_info']['show_restricted'])) {
                unset($casinos[$key]);
            }

            if (!empty($extra_data['limited_casinos'])) {
                // Remove casino, which casino ID
                // is not in limited casinos list.
                if (!in_array($casino['casino_id'], $extra_data['limited_casinos'])) {
                    unset($casinos[$key]);
                }
            }

            // Remove casinos with empty casino link.
            if (!empty($tracker_game_type) && !empty($tracker_game_type_id) && empty($casino['tracker_game_type_link'])) {
              unset($casinos[$key]);
            }

            // Remove new casinos from [top-rated-casino] and [other-rated-casinos] shortcodes.
            if ($casino['casino_info']['casino_info_new_casino'] == TRUE && isset($extra_data['top1'])) {
              unset($casinos[$key]);
            }

            // License ID match.
            if (!empty($license_id)) {

              $license_matches = FALSE;

              if (!empty($casino['casino_main_info']['casino_licenses'])) {
                foreach ($casino['casino_main_info']['casino_licenses'] as $license) {
                  // Mark that license matches.
                  if ($license_id == $license['license_id']) {
                    $license_matches = TRUE;
                  }
                }
              }

              // Remove non-matched license bookmakers.
              if (empty($license_matches)) {
                unset($casinos[$key]);
              }
            }

            // Crypto Deposit ID match.
            if (!empty($crypto_deposit_id)) {

              $crypto_matches = FALSE;

              if (!empty($casino['casino_main_info']['casino_crypto_deposits'])) {
                foreach ($casino['casino_main_info']['casino_crypto_deposits'] as $crypto) {
                  // Mark that crypto matches.
                  if ($crypto_deposit_id == $crypto['id']) {
                    $crypto_matches = TRUE;
                  }
                }
              }

              // Remove non-matched crypto bookmakers.
              if (empty($crypto_matches)) {
                unset($casinos[$key]);
              }
            }

            // // Deposit Method ID match.
            if (!empty($deposit_method_id)) {

              $deposit_matches = FALSE;

              if (!empty($casino['casino_main_info']['casino_deposit_methods'])) {
                foreach ($casino['casino_main_info']['casino_deposit_methods'] as $deposit_method) {
                  // Mark that deposit method matches.
                  if ($deposit_method_id == $deposit_method['id']) {
                    $deposit_matches = TRUE;
                  }
                }
              }

              // Remove non-matched deposit method bookmakers.
              if (empty($deposit_matches)) {
                unset($casinos[$key]);
              }
            }

            // Game provider ID match.
            if (!empty($game_provider_id)) {

              $casino_game_provider_matches = FALSE;

              if (!empty($casino['casino_main_info']['casino_game_providers'])) {
                foreach ($casino['casino_main_info']['casino_game_providers'] as $entry) {
                  // Mark that bookmaker game provider matches.
                  if ($game_provider_id == $entry['id']) {
                    $casino_game_provider_matches = TRUE;
                  }
                }
              }

              // Remove non-matched game provider casinos.
              if (empty($casino_game_provider_matches)) {
                unset($casinos[$key]);
              }
            }

            // Establish from year match.
            if (!empty($established_from)) {

              $established_from_matches = FALSE;

              if (!empty($casino['casino_main_info']['casino_established_date'])) {
                if ($casino['casino_main_info']['casino_established_date'] >= $established_from) {
                  $established_from_matches = TRUE;
                }
              }

              // Remove non-matched established from bookmakers.
              if (empty($established_from_matches)) {
                unset($casinos[$key]);
              }
            }

            // Single casino match.
            if (!empty($single_casino_name)) {

              $casino_name_matches = FALSE;

              if (!empty($casino['casino_name'])) {
                if ($casino['casino_name'] == $single_casino_name) {
                  $casino_name_matches = TRUE;
                }
              }

              // Remove non-matched casinos.
              if (empty($casino_name_matches)) {
                unset($casinos[$key]);
              }
            }


            // Limited Casinos match.
            if (!empty($extra_data['limited_casinos'])) {

              $limited_casinos_matches = FALSE;

              // Mark that Casino ID matches.
              foreach ($extra_data['limited_casinos'] as $limited_casino_id) {
                if ($casino['casino_id'] == $limited_casino_id) {
                  $limited_casinos_matches = TRUE;
                }
              }

              // Remove non-matched casinos.
              if (empty($limited_casinos_matches)) {
                unset($casinos[$key]);
              }
            }


            // Remove if casino rating is 0 or 0.00.
            if (empty($game_type_id) && (empty($casino['casino_rating']) || $casino['casino_rating'] == '0.00')) {
              unset($casinos[$key]);
            }



        }
    }

    // var_dump($casinos);
    // die();

    // Sort when no game type is provided.
    if (!empty($casinos) && empty($game_type_id)) {
        usort($casinos, function ($a, $b) {
            // Order TOP casinos by:
            // new_casino     (bool)   ASC
            // casino_rating  (float)  DESC
            // casino_name    (string) ASC
            if ($a['casino_info']['casino_info_new_casino'] === $b['casino_info']['casino_info_new_casino']) {
                  if ($a['casino_rating'] === $b['casino_rating']) {
                      return $a['casino_name'] <=> $b['casino_name'];
                  }
                return $b['casino_rating'] <=> $a['casino_rating'];
            }
            return $b['casino_info']['casino_info_new_casino'] <=> $a['casino_info']['casino_info_new_casino'];

        });
    }

    if (!empty($casinos) && !empty($game_type_id)) {
        foreach ($casinos as $key => $casino) {
            $game_type_id_mismatches = FALSE;
            if (empty($casino['casino_game_type_review'])) {
                $game_type_id_mismatches = TRUE;
            }

            // Remove non-matched casino.
            if (!empty($game_type_id_mismatches)) {
                unset($casinos[$key]);
            }

            // Remove casino game review rating if 0 or 0.00.
            if (empty($casino['casino_game_type_review']['casino_game_review_rating']) || $casino['casino_game_type_review']['casino_game_review_rating'] == '0.00') {
              unset($casinos[$key]);
            }
        }

        usort($casinos, function ($a, $b) {
            // Order TOP casinos (by game type) by:
            // new_casino                 (bool)   ASC
            // casino_game_review_rating  (float)  DESC
            // casino_name                (string) ASC
            if ($a['casino_info']['casino_info_new_casino'] === $b['casino_info']['casino_info_new_casino']) {
                  if ($a['casino_game_type_review']['casino_game_review_rating'] === $b['casino_game_type_review']['casino_game_review_rating']) {
                      return $a['casino_name'] <=> $b['casino_name'];
                  }
                return $b['casino_game_type_review']['casino_game_review_rating'] <=> $a['casino_game_type_review']['casino_game_review_rating'];
            }
            return $b['casino_info']['casino_info_new_casino'] <=> $a['casino_info']['casino_info_new_casino'];
        });

    }

    // Override default sort
    // by custom provided.
    if (!empty($sort_by)) {

        if ($sort_by_year_desc) {
            usort($casinos, function ($a, $b) {
                // Order TOP casinos by:
                // casino_info_open_year  (int)  DESC
                return $b['casino_info']['casino_info_open_year'] <=> $a['casino_info']['casino_info_open_year'];
            });
        }

        if ($sort_by_year_asc) {
            usort($casinos, function ($a, $b) {
                // Order TOP casinos by:
                // casino_info_open_year  (int)  ASC
                return $a['casino_info']['casino_info_open_year'] <=> $b['casino_info']['casino_info_open_year'];
            });
        }

        if ($sort_by_rating_asc) {
            if (!empty($game_type_id)) {
                usort($casinos, function ($a, $b) {
                    // Order TOP casinos by:
                    // casino_game_review_rating (float) ASC
                    return $a['casino_game_type_review']['casino_game_review_rating'] <=> $b['casino_game_type_review']['casino_game_review_rating'];
                });
            } else {
                usort($casinos, function ($a, $b) {
                    // Order TOP casinos by:
                    // casino_rating (float) ASC
                    return $a['casino_rating'] <=> $b['casino_rating'];
                });
            }
        }
    }

    // d($game_type_id);
    // die();

    // d($casinos);
    // die();

    // Default sort restricted casinos
    // in bottom when show_restricted="TRUE".
    if (!empty($extra_data['scg_all_info']['show_restricted'])) {
            if (!empty($game_type_id)) {
                usort($casinos, function ($a, $b) {
                    // Order TOP casinos by:
                    // casino_restricted (bool) DESC
                    // casino_game_review_rating (float) DESC
                    if ($a['casino_restricted'] === $b['casino_restricted']) {
                        return $b['casino_game_type_review']['casino_game_review_rating'] <=> $a['casino_game_type_review']['casino_game_review_rating'];
                    }
                    return $a['casino_restricted'] <=> $b['casino_restricted'];
                });
            } else {
                usort($casinos, function ($a, $b) {
                    // Order TOP casinos by:
                    // casino_restricted (bool) DESC
                    // casino_rating (float) DESC
                    if ($a['casino_restricted'] === $b['casino_restricted']) {
                        return $b['casino_rating'] <=> $a['casino_rating'];
                    }
                    return $a['casino_restricted'] <=> $b['casino_restricted'];
                });
            }
    }

    // d($casinos);
    // die();

    // Only offset when not TOP1 and not crypto filtering.
    if (isset($extra_data['top1']) && empty($extra_data['top1']) && !empty($casinos) && empty($extra_data['crypto_filters'])) {
      // Offset TOP1 casino as we are showing Other casinos.
      $casinos = array_splice($casinos, 1);
    }

    // d($casinos);

    return $casinos;
}


/**
 * Check if casino is in limited array.
 */
function scg_casino_trackers_has_casino_in_limited_array($array, $value)
{
    $in_limited_array = FALSE;

    foreach ($array as $key => $entry) {
        if ($entry === $value) {
            $in_limited_array = TRUE;
        }
    }

    return $in_limited_array;
}


/**
 * Get casino logo ID.
 */
function scg_casino_trackers_get_casino_logo_id($casino_info_id)
{
    $casino_logo_id = false;
    if (!empty($casino_info_id)) {

        $query = new SelectQuery;

        $query
            ->select(['casino_info_list_casino_info_logo_smaller.id as casino_info_logo_id'])
            ->from('wp_pods_casino', 't');

        $casino_info_list_field_id = DatabaseService::getField('casino', 'casino_info_list');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_info_list')
            ->on("rel_casino_info_list.field_id = $casino_info_list_field_id AND rel_casino_info_list.item_id = t.id");
        $query
            ->leftJoin('wp_pods_casino_info', 'casino_info_list')
            ->on("casino_info_list.id = rel_casino_info_list.related_item_id");

        $casino_info_logo_smaller_field_id = DatabaseService::getField('casino_info', 'casino_info_logo_smaller');
        $query
            ->leftJoin(DatabaseService::REL_TABLE, 'rel_casino_info_list_casino_info_logo_smaller')
            ->on("rel_casino_info_list_casino_info_logo_smaller.field_id = $casino_info_logo_smaller_field_id AND rel_casino_info_list_casino_info_logo_smaller.item_id = casino_info_list.id");
        $query
            ->leftJoin('wp_posts', 'casino_info_list_casino_info_logo_smaller')
            ->on("casino_info_list_casino_info_logo_smaller.id = rel_casino_info_list_casino_info_logo_smaller.related_item_id");

        $query
            ->where()
            ->equal('casino_info_list.id', $casino_info_id);

        $query
            ->orderBy('t.name', 'ASC')
            ->orderBy('t.id', 'ASC')
            ->limit(1);

        $casino_logo_id = (new DatabaseService($query, DatabaseService::GET_ITEM, 'casino_info_logo_id'))->build() ?: false;
    }

    return $casino_logo_id;
}


/**
 * Get BIG casino logo ID.
 */
function scg_casino_trackers_get_big_casino_logo_id($casino_info_id)
{
    if (!empty($casino_info_id)) {
        $fields = array(
            'casino_info_list.casino_info_logo.id AS casino_info_logo_id',
        );
        $fields = implode(', ', $fields);
        $args = array(
            'select' => $fields,
            'where' =>
                'casino_info_list.id = "' . $casino_info_id . '"',
            'distinct' => FALSE,
        );

        $pods = pods('casino', $args);

        $casino_logo_id = FALSE;

        if ($pods->total_found()) {
            while ($pods->fetch()) {
                $casino_logo_id = (int)$pods->field('casino_info_logo_id');
            }
        }

    }

    return $casino_logo_id;
}

/**
 * Get casino restricted countries.
 */
function scg_casino_trackers_get_casino_restricted_countries($casino_id)
{
    global $wpdb;
    $casino_restricted_countries = array();

    if (!empty($casino_id)) {
//    $args = array(
//      'where' =>
//        't.id = "' . $casino_id . '"',
//      'distinct' => FALSE,
//    );

        $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
        $casino_restricted_countries_field_id = DatabaseService::getField('casino', 'casino_restricted_countries');

        $sql = "SELECT related_item_id FROM wp_podsrel WHERE wp_podsrel.pod_id = $casino_pod_id AND wp_podsrel.item_id = $casino_id AND wp_podsrel.field_id = $casino_restricted_countries_field_id";

        $data = $wpdb->get_results($sql);


        if ($data) {
            foreach ($data as $item) {
                $casino_restricted_countries[] = (int)$item->related_item_id;
            }
        }
    }

    return $casino_restricted_countries;
}


/**
 * Get casino review data by casino ID,
 * country name and language.
 */
function scg_casino_trackers_get_casino_review_data($casino_id, $country_id, $language)
{
    // var_dump($country_id);
    $country_name = scg_get_country_name_from_id($country_id);
    // var_dump($country_name);

    $country_ids = scg_get_country_ids_based_on_bonus_country($country_name);
    $country_id = !empty($country_ids) ? $country_ids['country_id'] : FALSE;
    $original_country_id = !empty($country_ids) ? (int)$country_ids['original_country_id'] : FALSE;
    $original_country_code = scg_get_country_code_from_id($original_country_id);

    // Get casino name.
    $casino_name = scg_get_casino_name_from_id($casino_id);
    $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

    // $bonus_countries = scg_casino_trackers_list_casino_bonus_countries($casino_id);
    // $bonus_countries_count = count($bonus_countries);
    // // d($bonus_countries);
    // // d($bonus_countries_count);

    // $country_id = $bonus_countries_count == 1 && !empty($bonus_countries) ? reset($bonus_countries) : $country_id;
    // $country_name = $bonus_countries_count == 1 && !empty($bonus_countries) ? scg_get_country_name_from_id(reset($bonus_countries)) : $country_name;

    // d($country_id);

    $casino = get_transient('scg_casino_review_data_' . $casino_name_lower . '_' . $country_id . '_' . $language);

    // d('scg_casino_review_data_' . $casino_name_lower . '_' . $country_id . '_' . $language);

    if (empty($casino) && !empty($casino_id) && !empty($country_id) && !empty($language)) {
        $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name, $casino_id);
        $casino_info_enabled = scg_casino_trackers_check_if_casino_info_is_enabled($casino_id, $language);
        $have_this_country_rating = scg_casino_trackers_check_if_casino_rating_country_imported($casino_id, $country_id, $language);

        // d($have_this_country_bonuses);
        // d($casino_info_enabled);
        // d($have_this_country_rating);

        if (!empty($have_this_country_bonuses) && !empty($casino_info_enabled) && !empty($have_this_country_rating)) {
            // Get country ID using pods query.
            $country_id = $country_id;
        } else {
            // Use "All Countries" country.
            $country_id = scg_get_country_id_from_name('All Countries');

            // Use 'EN' language if current language does not exist.
            $language = 'en';
        }

        // Limit and offset by $top1 variable.
        $limit = 1;

        $rating_total_non_empty_filter = 'casino_review_list.rating_total > 0.00 AND ';

        // NEW
        // Using Country Relation.
        $query = new SelectQuery;
        $query
            ->select([
                't.casino_id AS casino_id',
                'casino.name AS casino_name',

                'main_country_field.related_item_id AS country',
                'main_country.name AS country_name',
                
                'info.id AS casino_info_id',
                'info.casino_info_enabled AS casino_info_enabled',
                'info.casino_info_new_casino AS casino_new_casino',

                'main_info.id AS casino_main_info_id',
                'main_info.owner_name AS owner_name',
                'main_info.established_date AS established_date',
                'main_info.vpn_allowed AS vpn_allowed',

                'currency_field.related_item_id AS currency',

                'main_info.minimum_deposit AS minimum_deposit',
                'main_info.withdrawal_limit_month AS withdrawal_limit_month',
                'main_info.average_visitors AS avg_visitors',

                'bonus_country.bonus_text AS casino_bonus_text',
                'bonus_country.bonus_sum AS casino_bonus_sum',

                'bonus_currency.currency_sign AS casino_bonus_currency',

                'bonus_country.bonus_list_affiliate_link AS casino_link',
                'bonus_country.bonus_list_button_text AS casino_link_button_text',
                'bonus_country.bonus_list_regulation_text AS casino_link_regulation_text',
                'bonus_country.bonus_shortcode_button_text AS casino_shortcode_button_text',
                'bonus_country.bonus_shortcode_regulation_text AS casino_shortcode_regulation_text',

                'bonus_country.bonus_review_button_text AS casino_bonus_review_button_text',
                'bonus_country.bonus_review_bonus_text AS casino_review_bonus_text',
                'bonus_country.bonus_review_affiliate_link AS casino_bonus_review_affiliate_link',
                'bonus_country.bonus_review_regulation_text AS casino_bonus_review_regulation_text',

                'main_review.rating_total AS casino_top_rating',
            ])
            ->from('wp_pods_country_relation', 't');
        $query
            ->leftJoin('wp_pods_casino', 'casino')
            ->on('t.casino_id = casino.id');
        $query
            ->leftJoin('wp_pods_casino_review', 'main_review')
            ->on('t.main_review_id = main_review.id');
        $query
            ->leftJoin('wp_pods_casino_info', 'info')
            ->on('t.casino_id = info.casino_id');
        $query
            ->leftJoin('wp_pods_casino_bonus', 'bonus_country')
            ->on('t.bonus_id = bonus_country.id');
        $query
            ->leftJoin('wp_pods_casino_main_info', 'main_info')
            ->on('t.casino_id = main_info.casino_id');
        $query
            ->leftJoin('wp_pods_country_relation', 'link_country_rel')
            ->on('t.casino_id = link_country_rel.casino_id AND t.country_id = link_country_rel.country_id');
        $query
            ->leftJoin('wp_pods_casino_link', 'link')
            ->on('link_country_rel.link_id = link.id');

        $currency_field_id = DatabaseService::getField('casino_main_info', 'currency');

        $query
          ->leftJoin('wp_podsrel', 'currency_field')
          ->on("main_info.id = currency_field.item_id AND currency_field.field_id = $currency_field_id");

        $query
            ->leftJoin('wp_pods_currency', 'bonus_currency')
            ->on('bonus_country.currency_id = bonus_currency.id');

        $main_country_field_id = DatabaseService::getField('casino_main_info', 'country');

        $query
          ->leftJoin('wp_podsrel', 'main_country_field')
          ->on("main_info.id = main_country_field.item_id AND main_country_field.field_id = $main_country_field_id");

        $query
            ->leftJoin('wp_pods_country', 'main_country')
            ->on('main_country_field.related_item_id = main_country.id');

        $query
            ->where()
            ->equal('t.country_id', $country_id)
            ->equal('t.casino_id', $casino_id)
            ->isNot('t.main_review_id', NULL)
            ->isNot('t.bonus_id', NULL)
            ->equal('casino.casino_enabled', 1)
            ->equal('casino.casino_restricted_enabled', 1)
            ->equal('bonus_country.bonus_enabled', 1)
            ->equal('info.casino_info_enabled', 1)
            ->equal('link.casino_link_status', 1);

        // Sort by.
        $query
            ->groupBy('t.casino_id')
            ->orderBy('casino_new_casino', 'DESC')
            ->orderBy('casino_top_rating', 'DESC')
            ->orderBy('t.name', 'ASC');

        $data = (new DatabaseService($query))->build();
        $data = !empty($data) ? reset($data) : FALSE;

        if ($data) {
            $casino_id = (int)$data->casino_id;
            $casino_info_id = (int)$data->casino_info_id;

            // Bonus text.
            $casino_review_bonus_text = $data->casino_review_bonus_text;

            // Bonus button text.
            $casino_review_bonus_button_text = $data->casino_bonus_review_button_text;

            $casino_bonus_review_affiliate_link = $data->casino_bonus_review_affiliate_link;
            $casino_bonus_review_regulation_text = $data->casino_bonus_review_regulation_text;
            $casino_top_rating = (float)$data->casino_top_rating;

            $casino_main_info_id = (int)$data->casino_main_info_id;
            $casino_owner_name = $data->owner_name;
            $casino_established_date = $data->established_date;
            $casino_vpn_allowed = (bool)$data->vpn_allowed;
            $casino_vpn_allowed_text = !empty($casino_vpn_allowed) ? 'Allowed' : 'Prohibited';
            $casino_currency = (int)$data->currency;
            $casino_currency_sign = scg_get_currency_sign_from_currency_id($casino_currency);
            $casino_minimum_deposit = !empty($data->minimum_deposit) ? $data->minimum_deposit : 0;
            $casino_minimum_deposit = rtrim($casino_minimum_deposit, 0);
            $casino_minimum_deposit = rtrim($casino_minimum_deposit, '.');

            // Minimum Deposit - USD display: '$ 5'.
            $casino_minimum_deposit_text = !empty($casino_currency_sign) ? $casino_currency_sign . ' ' . $casino_minimum_deposit : $casino_minimum_deposit;

            // Minimum Deposit - EUR override: '5 €'.
            $casino_minimum_deposit_text = !empty($casino_currency_sign) && $casino_currency_sign == '€' ? $casino_minimum_deposit . ' ' . $casino_currency_sign : $casino_minimum_deposit_text;

            // Minimum Deposit - BTC override: '0.02 BTC'.
            $casino_minimum_deposit_text = !empty($casino_currency_sign) && $casino_currency_sign == 'BTC' ? $casino_minimum_deposit . ' ' . $casino_currency_sign : $casino_minimum_deposit_text;

            // Withdrawal Limit Month.
            $casino_withdrawal_limit_month = (int)$data->withdrawal_limit_month;
            $casino_withdrawal_limit_month = !empty($casino_withdrawal_limit_month) ? scg_custom_number_format($casino_withdrawal_limit_month, 1) : FALSE;
            $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && !empty($casino_currency_sign) ? $casino_currency_sign . ' ' . $casino_withdrawal_limit_month : $casino_withdrawal_limit_month;

            // Text based on domain: LT or EN.
            $urlparts = parse_url(home_url());
            $domain = $urlparts['host'];
            $is_sbingg = strstr($domain, 'bingoguide') !== FALSE || strstr($domain, 'sbingg') !== FALSE;

            // Withdrawal Limit - '0' override: 'No Limit'.
            if ($is_sbingg) {
                $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && $casino_withdrawal_limit_month == '0' ? 'No Limit' : $casino_withdrawal_limit_month_text;
            } else {
                $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && $casino_withdrawal_limit_month == '0' ? 'Nėra limito' : $casino_withdrawal_limit_month_text;
            }

            // Withdrawal Limit - '-1' override: 'N/A'.
            $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && $casino_withdrawal_limit_month == '-1' ? 'N/A' : $casino_withdrawal_limit_month_text;

            // Withdrawal Limit - USD override: '$ 5'.
            $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && !empty($casino_currency_sign) && $casino_currency_sign == '$' ? $casino_withdrawal_limit_month . ' ' . $casino_currency_sign : $casino_withdrawal_limit_month_text;

            // Withdrawal Limit - EUR override: '5 €'.
            $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && !empty($casino_currency_sign) && $casino_currency_sign == '€' ? $casino_withdrawal_limit_month . ' ' . $casino_currency_sign : $casino_withdrawal_limit_month_text;

            // Withdrawal Limit - BTC override: '1 BTC'.
            $casino_withdrawal_limit_month_text = !empty($casino_withdrawal_limit_month) && !empty($casino_currency_sign) && $casino_currency_sign == 'BTC' ? $casino_withdrawal_limit_month . ' ' . $casino_currency_sign : $casino_withdrawal_limit_month_text;

            $casino_country_name = $data->country_name;

            $casino_avg_visitors_raw = (int)$data->avg_visitors;
            $casino_avg_visitors = !empty($casino_avg_visitors_raw) ? number_format($casino_avg_visitors_raw / 1000000, 2) . ' M' : FALSE;

            $casino_main_info = scg_casino_trackers_get_casino_main_info($casino_id);

            // Remove slots if its bingo site.
            if (Guru_Trackers_IS_BINGO) {
                $casino_slots = FALSE;
            } else {
                $casino_slots = scg_casino_trackers_get_casino_slots($casino_id);
            }

            $casino = array(
                'casino_id' => $casino_id,
                'casino_info_id' => $casino_info_id,
                'casino_link' => $casino_bonus_review_affiliate_link,
                'casino_bonus_text' => $casino_review_bonus_text,
                'casino_button_text' => $casino_review_bonus_button_text,
                'casino_regulation_text' => $casino_bonus_review_regulation_text,
                'casino_rating' => $casino_top_rating,
                'casino_owner_name' => $casino_owner_name,
                'casino_established_date' => $casino_established_date,
                'casino_country_name' => $casino_country_name,
                'casino_vpn_allowed' => $casino_vpn_allowed,
                'casino_vpn_allowed_text' => $casino_vpn_allowed_text,
                'casino_currency' => $casino_currency,
                'casino_currency_sign' => $casino_currency_sign,
                'casino_minimum_deposit' => $casino_minimum_deposit,
                'casino_minimum_deposit_text' => $casino_minimum_deposit_text,
                'casino_withdrawal_limit_month' => $casino_withdrawal_limit_month,
                'casino_withdrawal_limit_month_text' => $casino_withdrawal_limit_month_text,
                'casino_avg_visitors' => $casino_avg_visitors,
                'casino_main_info' => $casino_main_info,
                'casino_slots' => $casino_slots,
                'casino_compare' => FALSE,
            );

            // var_dump($casino);
            // var_dump($country_id);
            // var_dump($original_country_id);
            // var_dump($original_country_code);

            // 25 hours - actually it's not expiring as we
            // re-generate transients every 24 hours after
            // all casino trackers import (in the morining).
            set_transient('scg_casino_review_data_' . $casino_name_lower . '_' . $country_id . '_' . $language, $casino, 25 * 60 * 60);
        }

    }

    return $casino;
}



/**
* Get bookmaker review data by bookmaker ID,
* country name and language.
*/
function sbg_betting_trackers_get_bookmaker_review_data($bookmaker_id, $country_name) {
  $bookmaker = FALSE;

  if (!empty($bookmaker_id) && !empty($country_name)) {

    $have_this_country_bonuses = sbg_betting_trackers_check_if_bookmaker_bonus_country_imported($country_name, $bookmaker_id);
    // var_dump($have_this_country_bonuses);

    $country_id = sbg_get_country_id_from_name(trim($country_name));
    $original_country_id = $country_id;

    // var_dump($have_this_country_bonuses);
    // var_dump($country_id);

    if (!empty($have_this_country_bonuses)) {
      // Get country ID using pods query.
      $country_id = $country_id;
    } else {
      // Use "All Countries" country.
      $country_id = sbg_get_country_id_from_name('All Countries');
    }

    // Get bookmaker name.
    $bookmaker_name = sbg_get_bookmaker_name_from_id($bookmaker_id);
    $bookmaker_name_lower = strtolower(str_replace(' ', '_', $bookmaker_name));

    // $bookmaker = get_transient('sbg_bookmaker_review_data_' . $bookmaker_name_lower . '_' . $country_id);
    // var_dump($bookmaker);

    // Return early if bookmaker review data is found.
    // if (!empty($bookmaker)) {
    //   return $bookmaker;
    // }

    // Limit and offset by $top1 variable.
    $limit = 1;

    $fields = array(
      't.id AS bookmaker_id',
      'bookmaker_info_list.id AS bookmaker_info_id',
      'bookmaker_review_list.rating_total AS bookmaker_top_rating',

      'bookmaker_bonuses.bonus_review_bonus_text AS bookmaker_review_bonus_text',
      'bookmaker_bonuses.bonus_review_button_text AS bookmaker_bonus_review_button_text',
      'bookmaker_bonuses.bonus_review_affiliate_link AS bookmaker_bonus_review_affiliate_link',
      'bookmaker_bonuses.bonus_review_regulation_text AS bookmaker_bonus_review_regulation_text',

      'bookmaker_main_info.id AS bookmaker_main_info_id',
      'bookmaker_main_info.owner_name AS owner_name',
      'bookmaker_main_info.established_date AS established_date',
      'bookmaker_main_info.country.name AS country_name',
      'bookmaker_main_info.vpn_allowed AS vpn_allowed',
      'bookmaker_main_info.currency.id AS currency',
      'bookmaker_main_info.minimum_deposit AS minimum_deposit',
      'bookmaker_main_info.withdrawal_limit_month AS withdrawal_limit_month',
      'bookmaker_main_info.average_visitors AS avg_visitors',
    );
    $fields = implode( ', ', $fields );
    $args = array(
      'select' => $fields,
      'limit' => $limit,
      'where' =>
        't.id = "' . $bookmaker_id . '" AND 
        t.bookmaker_enabled = 1 AND
        t.bookmaker_restricted_enabled = 1 AND 
        bookmaker_info_list.bookmaker_info_enabled = 1 AND 
        bookmaker_links_list.bookmaker_link_countries.id = "' . $country_id . '" AND 
        bookmaker_links_list.bookmaker_link_status = 1 AND  
        bookmaker_bonuses.bonus_countries.id = "' . $country_id . '" AND 
        bookmaker_bonuses.bonus_enabled = 1',
        // bookmaker_review_list.rating_countries.id = "' . $country_id . '" AND
      'distinct' => FALSE,
    );

    // $pods = pods( 'bookmaker', $args );

    // var_dump($pods->sql);


    // + t.bookmaker_enabled = 1 AND
    // + t.bookmaker_restricted_enabled = 1 AND 
    // + bookmaker_info_list.bookmaker_info_enabled = 1 AND 
    // + bookmaker_links_list.bookmaker_link_countries.id = "' . $country_id . '" AND 
    // + bookmaker_links_list.bookmaker_link_status = 1 AND  
    // + bookmaker_bonuses.bonus_countries.id = "' . $country_id . '" AND 
    // + bookmaker_bonuses.bonus_enabled = 1',


    // NEW
    // Using Country Relation.
    $query = new SelectQuery;
    $query
        ->select([
            't.bookmaker_id AS bookmaker_id',
            'bookmaker.name AS bookmaker_name',

            'main_country_field.related_item_id AS country',
            'main_country.name AS country_name',
            
            'info.id AS bookmaker_info_id',
            'info.bookmaker_info_enabled AS bookmaker_info_enabled',
            'info.bookmaker_info_new_bookmaker AS bookmaker_new_bookmaker',

            'main_info.id AS bookmaker_main_info_id',
            'main_info.owner_name AS owner_name',
            'main_info.established_date AS established_date',
            'main_info.vpn_allowed AS vpn_allowed',

            'currency_field.related_item_id AS currency',

            'main_info.minimum_deposit AS minimum_deposit',
            'main_info.withdrawal_limit_month AS withdrawal_limit_month',
            'main_info.average_visitors AS avg_visitors',

            'bonus_country.bonus_text AS bookmaker_bonus_text',
            'bonus_country.bonus_sum AS bookmaker_bonus_sum',

            'bonus_currency.currency_sign AS bookmaker_bonus_currency',

            'bonus_country.bonus_list_affiliate_link AS bookmaker_link',
            'bonus_country.bonus_list_button_text AS bookmaker_link_button_text',
            'bonus_country.bonus_list_regulation_text AS bookmaker_link_regulation_text',
            'bonus_country.bonus_shortcode_button_text AS bookmaker_shortcode_button_text',
            'bonus_country.bonus_shortcode_regulation_text AS bookmaker_shortcode_regulation_text',

            'bonus_country.bonus_review_button_text AS bookmaker_bonus_review_button_text',
            'bonus_country.bonus_review_bonus_text AS bookmaker_review_bonus_text',
            'bonus_country.bonus_review_affiliate_link AS bookmaker_bonus_review_affiliate_link',
            'bonus_country.bonus_review_regulation_text AS bookmaker_bonus_review_regulation_text',

            'main_review.rating_total AS bookmaker_top_rating',
        ])
        ->from('wp_pods_country_relation', 't');
    $query
        ->leftJoin('wp_pods_bookmaker', 'bookmaker')
        ->on('t.bookmaker_id = bookmaker.id');
    $query
        ->leftJoin('wp_pods_bookmaker_review', 'main_review')
        ->on('t.main_review_id = main_review.id');
    $query
        ->leftJoin('wp_pods_bookmaker_info', 'info')
        ->on('t.bookmaker_id = info.bookmaker_id');
    $query
        ->leftJoin('wp_pods_bookmaker_bonus_country', 'bonus_country')
        ->on('t.bonus_id = bonus_country.id');
    $query
        ->leftJoin('wp_pods_bookmaker_main_info', 'main_info')
        ->on('t.bookmaker_id = main_info.bookmaker_id');
    $query
        ->leftJoin('wp_pods_country_relation', 'link_country_rel')
        ->on('t.bookmaker_id = link_country_rel.bookmaker_id AND t.country_id = link_country_rel.country_id');
    $query
        ->leftJoin('wp_pods_bookmaker_link', 'link')
        ->on('link_country_rel.link_id = link.id');

    $currency_field_id = DatabaseService::getField('bookmaker_main_info', 'currency');

    $query
      ->leftJoin('wp_podsrel', 'currency_field')
      ->on("main_info.id = currency_field.item_id AND currency_field.field_id = $currency_field_id");

    $query
        ->leftJoin('wp_pods_currency', 'bonus_currency')
        ->on('bonus_country.currency_id = bonus_currency.id');

    $main_country_field_id = DatabaseService::getField('bookmaker_main_info', 'country');

    $query
      ->leftJoin('wp_podsrel', 'main_country_field')
      ->on("main_info.id = main_country_field.item_id AND main_country_field.field_id = $main_country_field_id");

    $query
        ->leftJoin('wp_pods_country', 'main_country')
        ->on('main_country_field.related_item_id = main_country.id');

    $query
        ->where()
        ->equal('t.country_id', $country_id)
        ->equal('t.bookmaker_id', $bookmaker_id)
        ->isNot('t.main_review_id', NULL)
        ->isNot('t.bonus_id', NULL)
        ->equal('bookmaker.bookmaker_enabled', 1)
        ->equal('bookmaker.bookmaker_restricted_enabled', 1)
        ->equal('bonus_country.bonus_enabled', 1)
        ->equal('info.bookmaker_info_enabled', 1)
        ->equal('link.bookmaker_link_status', 1);

    // Sort by.
    $query
        ->groupBy('t.bookmaker_id')
        ->orderBy('bookmaker_new_bookmaker', 'DESC')
        ->orderBy('bookmaker_top_rating', 'DESC')
        ->orderBy('t.name', 'ASC');

    $data = (new DatabaseService($query))->build();
    $data = !empty($data) ? reset($data) : FALSE;

    if ($data) {
        $bookmaker_id = (int) $data->bookmaker_id;
        $bookmaker_info_id = (int) $data->bookmaker_info_id;
        $bookmaker_top_rating = (float) $data->bookmaker_top_rating;

        // Bonus text.
        $bookmaker_review_bonus_text = $data->bookmaker_review_bonus_text;

        $bookmaker_bonus_review_button_text = $data->bookmaker_bonus_review_button_text;
        $bookmaker_bonus_review_affiliate_link = $data->bookmaker_bonus_review_affiliate_link;
        $bookmaker_bonus_review_regulation_text = $data->bookmaker_bonus_review_regulation_text;
        $bookmaker_restricted_countries = !empty($bookmaker_id) ? sbg_betting_trackers_get_bookmaker_restricted_countries($bookmaker_id) : array();

        $bookmaker_main_info_id = (int) $data->bookmaker_main_info_id;
        $owner_name = $data->owner_name;
        $established_date = (int) $data->established_date;
        $country_name = $data->country_name;
        $vpn_allowed = (bool) $data->vpn_allowed;
        $vpn_allowed_text = !empty($vpn_allowed) ? 'Allowed' : 'Prohibited';
        $currency = (int) $data->currency;
        $currency_sign = sbg_get_currency_sign_from_currency_id($currency);
        $minimum_deposit = (float) $data->minimum_deposit;
        $minimum_deposit_text = !empty($currency_sign) ? $minimum_deposit . ' ' . $currency_sign : $minimum_deposit_text;

        $withdrawal_limit_month = (int) $data->withdrawal_limit_month;
        $withdrawal_limit_month = isset($withdrawal_limit_month) ? sbg_custom_number_format($withdrawal_limit_month, 1) : FALSE;
        $withdrawal_limit_month_no_limit = $withdrawal_limit_month == '0';
        $withdrawal_limit_month_text = isset($withdrawal_limit_month) && !empty($currency_sign) ? $currency_sign . ' ' . $withdrawal_limit_month : $withdrawal_limit_month;
        $withdrawal_limit_month_text = isset($withdrawal_limit_month) && !empty($currency_sign) && $currency_sign == '€' ? $withdrawal_limit_month . ' ' . $currency_sign : $withdrawal_limit_month_text;
        $withdrawal_limit_month_text = !empty($withdrawal_limit_month_no_limit) ? 'No limit' : $withdrawal_limit_month_text;

        $avg_visitors_raw = (int) $data->avg_visitors;
        $avg_visitors = !empty($avg_visitors_raw) ? number_format($avg_visitors_raw / 1000000, 2) . ' M' : FALSE;

        $bookmaker_main_info = sbg_betting_trackers_get_bookmaker_main_info($bookmaker_id);

        $bookmaker = array(
          'bookmaker_id'      => $bookmaker_id,
          'bookmaker_info_id' => $bookmaker_info_id,
          'bookmaker_rating'  => $bookmaker_top_rating,
          'bookmaker_link'    => $bookmaker_bonus_review_affiliate_link,
          'bookmaker_bonus_text' => $bookmaker_review_bonus_text,
          'bookmaker_button_text'     => $bookmaker_bonus_review_button_text,
          'bookmaker_regulation_text' => $bookmaker_bonus_review_regulation_text,

          'bookmaker_rating' => $bookmaker_top_rating,
          'bookmaker_owner_name' => $owner_name,
          'bookmaker_established_date' => $established_date,
          'bookmaker_country_name' => $country_name,
          'bookmaker_vpn_allowed' => $vpn_allowed,
          'bookmaker_vpn_allowed_text' => $vpn_allowed_text,
          'bookmaker_currency' => $currency,
          'bookmaker_currency_sign' => $currency_sign,
          'bookmaker_minimum_deposit' => $minimum_deposit,
          'bookmaker_minimum_deposit_text' => $minimum_deposit_text,
          'bookmaker_withdrawal_limit_month' => $withdrawal_limit_month,
          'bookmaker_withdrawal_limit_month_text' => $withdrawal_limit_month_text,
          'bookmaker_avg_visitors' => $avg_visitors,
          'bookmaker_main_info' => $bookmaker_main_info,
        );

        // 25 hours - actually it's not expiring as we
        // re-generate transients every 24 hours after
        // all bookmaker trackers import (in the morining).
        // set_transient('sbg_bookmaker_review_data_' . $bookmaker_name_lower . '_' . $country_id, $bookmaker, 25 * 60 * 60);
    }

    // if ( isset($pods) && $pods->total_found() ) {
    //   while( $pods->fetch() ) {
    //     $bookmaker_id = (int) $pods->field('bookmaker_id');
    //     $bookmaker_info_id = (int) $pods->field('bookmaker_info_id');
    //     $bookmaker_top_rating = (float)$pods->field('bookmaker_top_rating');

    //     // Bonus text.
    //     $bookmaker_review_bonus_text = $pods->field('bookmaker_review_bonus_text');

    //     $bookmaker_bonus_review_button_text = $pods->field('bookmaker_bonus_review_button_text');
    //     $bookmaker_bonus_review_affiliate_link = $pods->field('bookmaker_bonus_review_affiliate_link');
    //     $bookmaker_bonus_review_regulation_text = $pods->field('bookmaker_bonus_review_regulation_text');
    //     $bookmaker_restricted_countries = !empty($bookmaker_id) ? sbg_betting_trackers_get_bookmaker_restricted_countries($bookmaker_id) : array();

    //     $bookmaker_main_info_id = (int)$pods->field('bookmaker_main_info_id');
    //     $owner_name = $pods->field('owner_name');
    //     $established_date = (int)$pods->field('established_date');
    //     $country_name = $pods->field('country_name');
    //     $vpn_allowed = (bool)$pods->field('vpn_allowed');
    //     $vpn_allowed_text = !empty($vpn_allowed) ? 'Allowed' : 'Prohibited';
    //     $currency = (int)$pods->field('currency');
    //     $currency_sign = sbg_get_currency_sign_from_currency_id($currency);
    //     $minimum_deposit = (float)$pods->field('minimum_deposit');
    //     $minimum_deposit_text = !empty($currency_sign) ? $minimum_deposit . ' ' . $currency_sign : $minimum_deposit_text;

    //     $withdrawal_limit_month = (int)$pods->field('withdrawal_limit_month');
    //     $withdrawal_limit_month = isset($withdrawal_limit_month) ? sbg_custom_number_format($withdrawal_limit_month, 1) : FALSE;
    //     $withdrawal_limit_month_no_limit = $withdrawal_limit_month == '0';
    //     $withdrawal_limit_month_text = isset($withdrawal_limit_month) && !empty($currency_sign) ? $currency_sign . ' ' . $withdrawal_limit_month : $withdrawal_limit_month;
    //     $withdrawal_limit_month_text = isset($withdrawal_limit_month) && !empty($currency_sign) && $currency_sign == '€' ? $withdrawal_limit_month . ' ' . $currency_sign : $withdrawal_limit_month_text;
    //     $withdrawal_limit_month_text = !empty($withdrawal_limit_month_no_limit) ? 'No limit' : $withdrawal_limit_month_text;

    //     $avg_visitors_raw = (int)$pods->field('avg_visitors');
    //     $avg_visitors = !empty($avg_visitors_raw) ? number_format($avg_visitors_raw / 1000000, 2) . ' M' : FALSE;

    //     $bookmaker_main_info = sbg_betting_trackers_get_bookmaker_main_info($bookmaker_id);

    //     $bookmaker = array(
    //       'bookmaker_id'      => $bookmaker_id,
    //       'bookmaker_info_id' => $bookmaker_info_id,
    //       'bookmaker_rating'  => $bookmaker_top_rating,
    //       'bookmaker_link'    => $bookmaker_bonus_review_affiliate_link,
    //       'bookmaker_bonus_text' => $bookmaker_review_bonus_text,
    //       'bookmaker_button_text'     => $bookmaker_bonus_review_button_text,
    //       'bookmaker_regulation_text' => $bookmaker_bonus_review_regulation_text,

    //       'bookmaker_rating' => $bookmaker_top_rating,
    //       'bookmaker_owner_name' => $owner_name,
    //       'bookmaker_established_date' => $established_date,
    //       'bookmaker_country_name' => $country_name,
    //       'bookmaker_vpn_allowed' => $vpn_allowed,
    //       'bookmaker_vpn_allowed_text' => $vpn_allowed_text,
    //       'bookmaker_currency' => $currency,
    //       'bookmaker_currency_sign' => $currency_sign,
    //       'bookmaker_minimum_deposit' => $minimum_deposit,
    //       'bookmaker_minimum_deposit_text' => $minimum_deposit_text,
    //       'bookmaker_withdrawal_limit_month' => $withdrawal_limit_month,
    //       'bookmaker_withdrawal_limit_month_text' => $withdrawal_limit_month_text,
    //       'bookmaker_avg_visitors' => $avg_visitors,
    //       'bookmaker_main_info' => $bookmaker_main_info,
    //     );

    //     // 25 hours - actually it's not expiring as we
    //     // re-generate transients every 24 hours after
    //     // all bookmaker trackers import (in the morining).
    //     set_transient('sbg_bookmaker_review_data_' . $bookmaker_name_lower . '_' . $country_id, $bookmaker, 25 * 60 * 60);

    //   }
    // }

  }

  // var_dump($bookmaker);

  return $bookmaker;
}




/**
 * Get casino slots.
 */
function scg_casino_trackers_get_casino_slots($casino_id)
{
    $fields = array(
        // 't.id AS id',
        // 't.name AS name',
    );

    $fields = implode(', ', $fields);
    $args = array(
        'select' => $fields,
        'limit' => $limit,
        'where' =>
            'games_casinos.id = "' . $casino_id . '"',

        'distinct' => FALSE,
    );

    // var_dump($args);

    $pods = pods('vegashero_games', $args);

    $slots_array = FALSE;
    $slots = array();
    $slot_ids = array();

    if ($pods->total_found()) {
        while ($pods->fetch()) {
            $slot_id = (int)$pods->field('id');
            $slot_link = get_permalink($slot_id);

            $slot_ids[] = $slot_id;

            $slots[] = array(
                'slot_id' => $slot_id,
                'slot_link' => $slot_link,
            );
        }
    }

    if (!empty($slots)) {
        // global $wpdb;
        // $wpdb->get_results("SELECT * FROM ".$this->wpdb->users." WHERE status = 'active'", ARRAY_A);

        $slot_ids_sql = implode(',', $slot_ids);
        global $wpdb;
        $querystr = "
      SELECT p.ID, p.post_title, game_img.meta_value game_img, yuzoviews.views
      FROM {$wpdb->posts} p
      JOIN {$wpdb->postmeta} game_img
      ON p.ID = game_img.post_id AND game_img.meta_key = 'game_img'
      LEFT JOIN wp_yuzoviews yuzoviews ON p.ID = yuzoviews.post_id
      WHERE p.post_status = 'publish'
      AND p.post_type = 'vegashero_games'
      AND p.ID IN ($slot_ids_sql)
      ORDER BY yuzoviews.views DESC
      LIMIT 0, 10
    ";

        $slots_array = $wpdb->get_results($querystr, OBJECT);

        // $today = time();
        // $args = array(
        //   'posts_per_page' => -1,
        //   'post_type'   => 'video_review',
        //   'post_status' => 'publish',
        //   'orderby' => 'date',
        //   'order'   => 'ASC',
        //   'post__in' => array($post->ID),
        // );

        // $listings = new WP_Query($args);
    }

    return $slots_array;
}


/**
 * Get shortcode translated title by post_id, post, type,
 * language and shortcode_type. Fixes shortcode translation problem.
 */
function scg_casino_trackers_get_shortcode_translated_title($post_id, $post_type, $language, $game_type, $shortcode_type)
{
    $post_id = apply_filters('wpml_object_id', $post_id, $post_type, FALSE, 'en');
    $trid = apply_filters('wpml_element_trid', NULL, $post_id, 'post_' . $post_type);
    $translations = apply_filters('wpml_get_element_translations', NULL, $trid, 'post_' . $post_type);

    $shortcode_title = FALSE;
    $translated_post_id = FALSE;

    foreach ($translations as $translation) {
        if ($language == $translation->language_code && $translation->post_status == 'publish') {
            $translated_post_id = $translation->element_id;
        }
    }

    if (!empty($translated_post_id)) {
        $translated_post = get_post($translated_post_id);

        $post_content = !empty($translated_post) ? $translated_post->post_content : FALSE;

        if (!empty($post_content)) {

            if ($shortcode_type == 'top') {
                $shortcode_regex = '/\[(top-rated-casino|top-rated-casino-by-game).*"' . $game_type . '".*\]/i';
            }

            if ($shortcode_type == 'other') {
                $shortcode_regex = '/\[(other-rated-casinos|other-rated-casinos-by-game).*"' . $game_type . '".*\]/i';
            }

            preg_match($shortcode_regex, $post_content, $matches);

            if (!empty($matches)) {
                $shortcode = $matches[0];
                $shortcode_atts = preg_match_all('/\[*title="(.*?)"[^\]]*]/uis', $shortcode, $shortcode_matches);

                if (!empty($shortcode_matches)) {
                    $shortcode_title = wp_strip_all_tags($shortcode_matches[1][0]);
                }
            }

        }

    }

    return $shortcode_title;
}


/**
 * Check casino review and info, casino game review existance.
 */
function scg_casino_trackers_check_casino_rating_and_info($casino_id, $language_code, $game_type = FALSE)
{
    $casino_info_exists = FALSE;
    $casino_review_exists = FALSE;
    $casino_game_review_exists = FALSE;

    // Prepare main data.
    if (!empty($casino_id) && !empty($language_code)) {
        $casino_existance_info = array(
            'casino_id' => $casino_id,
            'casino_language' => $language_code,
        );
    }

    // Let's check if casino info exists.
    if (!empty($casino_id) && !empty($language_code) && empty($game_type)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'casino_info_casino.id = "' . $casino_id . '"' . ' AND ' . 'language = "' . $language_code . '"' . ' AND ' . 'casino_info_enabled = 1',
            'limit' => 1,
        );

        $casino_info = pods('casino_info');
        $casino_info->find($params);
        $casino_info_data = $casino_info->data();

        if (!empty($casino_info_data) && !empty($casino_info_data[0])) {
            $casino_info_exists = !empty($casino_info_data[0]->id);

            $casino_existance_info['casino_info_exists'] = $casino_info_exists;
        }
    }

    // Let's check if casino review exists.
    if (!empty($casino_id) && !empty($language_code) && empty($game_type)) {
        $params = array(
            'distinct' => FALSE,
            'where' =>
                'rating_casino.id = "' . $casino_id . '"' . ' AND ' . 'language = "' . $language_code . '"',
            'limit' => 1,
        );

        $casino_review = pods('casino_review');
        $casino_review->find($params);
        $casino_review_data = $casino_review->data();

        if (!empty($casino_review_data) && !empty($casino_review_data[0])) {
            $casino_review_exists = !empty($casino_review_data[0]->id);

            $casino_existance_info['casino_review_exists'] = $casino_review_exists;
        }
    }

    // Check if casino review by game exists.
    if (!empty($casino_id) && !empty($language_code) && !empty($game_type)) {
        $game_type_id = scg_get_casino_game_type_id_from_name($game_type);

        $params = array(
            'distinct' => FALSE,
            'where' =>
                'casino_game_review_casino.id = "' . $casino_id . '"' . ' AND ' . 'language = "' . $language_code . '"' . ' AND ' . 'casino_game_review_game.id = "' . $game_type_id . '"',
            'limit' => 1,
        );

        $casino_game_review = pods('casino_game_review');
        $casino_game_review->find($params);
        $casino_game_review_data = $casino_game_review->data();

        if (!empty($casino_game_review_data) && !empty($casino_game_review_data[0])) {
            $casino_game_review_exists = !empty($casino_game_review_data[0]->id);
        }

        $casino_existance_info['casino_game_type'] = $game_type;
        $casino_existance_info['casino_game_review_exists'] = $casino_game_review_exists;
    }

    return $casino_existance_info;
}


/**
 * Send email summary on finished import.
 */
function scg_send_email_summary_on_finished_import($hash_code, $import_log_id)
{
    // Check if import log exists first.
    $params = array(
        'distinct' => FALSE,
        'where' =>
            't.id = "' . $import_log_id . '" AND ' . 't.import_log_code = "' . $hash_code . '"',
    );

    $import_log = pods('import_log');
    $import_log->find($params);
    $data = $import_log->data();
    $import_log_data = !empty($data) ? $data[0] : FALSE;

    $import_log_entries = $import_log->field('import_log_items');
    $import_log_type = $import_log->field('import_log_type');

    $sheet_names = array();
    $import_log_entries_count = 0;
    $casinos_added = array();
    $casinos_added_count = 0;

    foreach ($import_log_entries as $log_entry) {
        $sheet_added = $log_entry['log_entry_reason'] === "Naujas Sheet'as";
        $casino_added = $log_entry['log_entry_reason'] === "Naujas kazino";

        if ($sheet_added) {
            $sheet_names[] = $log_entry['log_entry_sheet_name'];
        }

        if ($casino_added) {
            $casinos_added[] = $log_entry['log_entry_text'];
            $casinos_added_count++;
        }

        if (!$sheet_added && !$casino_added) {
            $import_log_entries_count++;
        }
    }

    if (empty($import_log_entries_count)) {
        $import_log_entries_count = 'Woop, woop! Nėra klaidų!';
    }

    $to = 'jovita.valuzyte@gmail.com';
    $subject = 'Importuoti Kazino Tracker\'iai - ' . $import_log_data->import_log_datetime;

    if ($import_log_type == 'restricted_countries') {
        $subject = 'Importuota Restricted Countries - ' . $import_log_data->import_log_datetime;
    }

    $headers = array();
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'Bcc: paulius.pazdrazdys@gmail.com';

    $site_url = get_site_url();
    $import_logs_link = $site_url . '/wp-admin/admin.php?page=scg-casino-trackers';

    if ($import_log_type == 'restricted_countries') {
        $import_logs_link = $site_url . '/wp-admin/admin.php?page=scg-restricted-countries';
    }

    if ($import_log_type == 'casino_trackers') {
        $import_log_template_name = 'import-log-email-summary.php';
    }

    if ($import_log_type == 'restricted_countries') {
        $import_log_template_name = 'import-log-email-restricted-countries-summary.php';
    }

    ob_start();
    include(plugin_dir_path(__FILE__) . '/templates/' . $import_log_template_name);
    $template = ob_get_contents();
    ob_get_clean();

    $message = $template;

    // Send import log summary.
    if (WP_ENV == 'production') {
        wp_mail($to, $subject, $message, $headers);
    }
}


/**
 * Add casino pods post save hook.
 */
function scg_pods_post_save_import_log_finished($pieces, $is_new_item, $id)
{
    $import_log_finished = $pieces['fields']['import_log_finished']['value'];
    $import_log_type = $pieces['fields']['import_log_type']['value'];
    $hash = $pieces['fields']['import_log_code']['value'];
    $restricted_progress = get_option('scg-restricted-countries-import-progress');
    $trackers_progress = get_option('scg-casino-trackers-import-progress');
    $email_sent = get_option('scg-import-email-sent');

    if ($import_log_finished == 1 && !empty($hash) && (($import_log_type == 'casino_trackers' && $restricted_progress == 'complete') || ($import_log_type == 'casino_trackers' && $trackers_progress == 'complete')) && empty($email_sent)) {
        // Send email when import log finished
        scg_send_email_summary_on_finished_import($hash, $id);

        update_option('scg-import-email-sent', 1);
    }

    return $pieces;
}

add_action('pods_api_post_save_pod_item_import_log', 'scg_pods_post_save_import_log_finished', 99, 3);


/**
 * AJAX callback for import progress.
 */
function scg_trackers_import_progress_callback()
{
    global $wpdb;

    // var_dump($_POST);
    // die();

    if ($_POST['progress'] == 0 && $_POST['counter'] == 0) {
        // Insert variables for new import.
        scg_trackers_import_start_import_insert_variables();
    }

    $progress = get_option('scg-casino-trackers-import-progress');

    echo $progress;

    wp_die();
}

add_action('wp_ajax_scg_trackers_import_progress', 'scg_trackers_import_progress_callback');


/**
 * Callback to insert variables for new import.
 */
function scg_trackers_import_start_import_insert_variables($type = '')
{
    $progress = update_option('scg-casino-trackers-import-progress', 0);

    // Add hash for later use.
    $hash = password_hash(time(), PASSWORD_DEFAULT);

    // Add new import hash option when starting the import.
    add_option('scg-casino-trackers-import-hash', $hash, '', 'yes');

    // Reset email sent setting on import start.
    update_option('scg-import-email-sent', 0);

    $import_log_data = array(
        'first_save' => TRUE,
        'import_log_type' => 'casino_trackers',
    );

    if ($type == 'cron') {
        $import_log_data['author_cron'] = TRUE;
    }

    scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);
}


/**
 * Remove countries from casino.
 */
function scg_remove_countries_from_casino($casino_id)
{
    global $wpdb;

    // Get dynamic variables.
    $pod_id = DatabaseService::getField('casino', 'pod_id');
    $field_id_1 = DatabaseService::getField('casino', 'casino_restricted_countries');
    $field_id_2 = DatabaseService::getField('casino', 'casino_accepting_countries');

    $delete_sql = "DELETE FROM " . "wp_podsrel WHERE wp_podsrel.pod_id = $pod_id AND wp_podsrel.item_id = %d AND (wp_podsrel.field_id = $field_id_1 OR wp_podsrel.field_id = $field_id_2)";

    try {
        $wpdb->query(
            $wpdb->prepare(
                $delete_sql,
                array($casino_id)
            )
        );

        return TRUE;
    } catch (Exception $e) {
        return error_log('Error removing countries from Casino! ID: ' . $casino_id . ', Error: ' . $wpdb->last_error);
    }
}


/**
 * Add countries to caasino.
 */
function scg_add_countries_to_casino($data)
{
    global $wpdb;

    // 13550 - Field ID - Restricted Countries
    // 13551 - Field ID - Accepting Countries

    $values = array();

    // We're preparing each DB item on it's own. Makes the code cleaner.
    foreach ($data as $key => $value) {
        $values[] = $wpdb->prepare("(%d, %d, %d, 0, 0, %d, %d)", $value['pod_id'], $value['field_id'], $value['item_id'], $value['related_item_id'], $key);
    }

    $query = "INSERT INTO wp_podsrel (pod_id, field_id, item_id, related_pod_id, related_field_id, related_item_id, weight) VALUES ";
    $query .= implode(",\n", $values);

    // error_log('<pre>' . print_r($query, TRUE) . '<pre>');

    try {
        $wpdb->query(
            $query
        );

        return TRUE;
    } catch (Exception $e) {
        return error_log('Error adding countries for Casino! ID: ' . $casino_id . ', Error: ' . $wpdb->last_error);
    }
}


/**
 * Import casino restricted countries and accepting countries.
 */
function scg_casino_trackers_import_casino_restricted_countries($casino_column_id, $casino_id, $casino_name, $casino_restricted_enabled, $casino_enabled, $casino_closed)
{
    // Sheet name for further use.
    $sheet_name = 'Restricted Countries';

    // Get hash of current import.
    $hash = get_option('scg-restricted-countries-import-hash');

    // Prepare log entry data for further use.
    // $log_entry_data = array(
    //     'log_entry_casino_id' => $casino_id,
    //     'log_entry_sheet_name' => $sheet_name,
    //     'log_entry_type' => 'restricted_countries',
    // );

    $casino_matches = scg_check_if_casino_and_id_matches($casino_id, $casino_name);

    if (!$casino_matches) {
        // Log entry data for bad casino name/ID.
        // $log_entry_data['log_entry_reason'] = 'Neatitinka kazino';
        // $log_entry_data['log_entry_text'] = 'Blogas kazino ID arba pavadinimas';

        // // Insert log entry about bad casino name/ID.
        // scg_casino_trackers_create_log_entry($hash, $log_entry_data);

        // $cell_info = scg_google_sheet_get_row_info_by_casino_value($casino_name, $sheet_name);
        // $cell_row = $cell_info['row'];
        // $cell_column = $cell_info['column_index'];
        // $mark_type = 'Casino ID/Name';

        // if (isset($cell_row) && isset($cell_column)) {
        //     $mark_cell_red = scg_google_sheet_red_color_cell($cell_row, $cell_column, $sheet_name, $mark_type);
        // }

        // Stop executing further code if no match is found.
        return FALSE;
    }

    // Log entry data to mark importing casino.
    // $log_entry_data = array(
    //     'log_entry_sheet_name' => $sheet_name,
    //     'log_entry_reason' => 'Naujas kazino',
    //     'log_entry_text' => $casino_name,
    //     'log_entry_type' => 'restricted_countries',
    // );

    // // Insert log entry about importing casino.
    // scg_casino_trackers_create_log_entry($hash, $log_entry_data);

    // Remove accepting and restricted countries.
    scg_remove_countries_from_casino($casino_id);

    // Change restricted enabled value based on Sheet value.
    global $wpdb;
    $casino_restricted_enabled = 1;
    $data_update = array(
        'casino_restricted_enabled' => $casino_restricted_enabled,
        'casino_enabled'            => $casino_enabled,
        'casino_closed'             => $casino_closed,
    );
    $data_where = array('id' => $casino_id);
    $wpdb->update('wp_pods_casino', $data_update, $data_where);

    $data = get_transient('scg-restricted-countries-data-' . $casino_id . '-' . $casino_name);
    $casino_restricted_countries = $data['casino_restricted_countries'];

    $restricted_countries_to_import = $casino_restricted_countries;
    $restricted_countries_ids = array();

    foreach ($restricted_countries_to_import as $country) {
        $country_id = scg_get_country_id_from_country_code($country['country_code']);
        $country_name = scg_get_country_name_from_id($country_id);
        // echo "<pre>" . print_r($country['country_code'], true) . "</pre>";
        // echo "<pre>" . print_r($country_id, true) . "</pre>";
        // echo "<pre>" . print_r($country_name, true) . "</pre>";

        $restricted_countries_ids[] = $country_id;
    }

    // Remove countries (Regions right now).
    $restricted_countries_ids = array_filter($restricted_countries_ids);

    // echo "<pre>" . print_r($restricted_countries_ids, true) . "</pre>";

    // die();
    // array_filter(array_map('array_filter'

    // Add accepting countries only when we have
    // at least one restricted country ID.
    if (!empty($restricted_countries_ids)) {
        // Accepting Countries logic.
        $query = new SelectQuery;
        $query
            ->from('wp_pods_country', 't')
            ->select(['t.id'])
            ->orderBy('name', 'ASC');

        $all_countries_list = (new DatabaseService($query, DatabaseService::GET_DATA))->build();

        $all_countries_ids = array();

        if ($all_countries_list) {
            foreach ($all_countries_list as $entry) {
                $all_countries_ids[] = (int)$entry->id;
            }
        }

        $allowed_countries_ids = $all_countries_ids;

        // Remove "All Countries" Country.
        $all_countries_country_id = scg_get_country_id_from_name('All Countries');
        $all_countries_country_key = array_search($all_countries_country_id, $allowed_countries_ids);
        unset($allowed_countries_ids[$all_countries_country_key]);

        // Re-index array.
        $allowed_countries_ids = array_values($allowed_countries_ids);

        // echo "<pre>" . print_r($restricted_countries_ids, true) . "</pre>";
        // echo "<pre>" . print_r($allowed_countries_ids, true) . "</pre>";

        // Sort restricted countries array by aplhabetical
        // order based on allowed countries array keys.
        $ordered_restricted_countries_ids = array();
        foreach($allowed_countries_ids as $key => $value) {

           if (in_array($value, $restricted_countries_ids)) {
              $ordered_restricted_countries_ids[$key] = $value;
           }
        }
        $ordered_restricted_countries_ids = array_values($ordered_restricted_countries_ids);

        foreach ($ordered_restricted_countries_ids as $restricted_id) {
            $key = array_search($restricted_id, $allowed_countries_ids, $strict = TRUE);

            // echo "<pre>" . print_r($restricted_id, true) . "</pre>";
            // echo "<pre>" . print_r($key, true) . "</pre>";

            // Remove restricted countries IDs.
            if (!is_null($key)) {
                // echo "<pre>" . print_r($restricted_id, true) . "</pre>";
                // echo "<pre>" . print_r($key, true) . "</pre>";
                // die();

                unset($allowed_countries_ids[$key]);
            }
        }

        // Re-index array.
        $allowed_countries_ids = array_values($allowed_countries_ids);

        // echo "<pre>" . print_r($restricted_countries_ids, true) . "</pre>";
        // echo "<pre>" . print_r($allowed_countries_ids, true) . "</pre>";
        // die();
        // echo "<pre>" . print_r($allowed_countries_ids, true) . "</pre>";
        // echo "<pre>" . print_r(count($allowed_countries_ids), true) . "</pre>";

        // echo "<pre>" . print_r($allowed_countries_ids, true) . "</pre>";
        // echo "<pre>" . print_r($ordered_restricted_countries_ids, true) . "</pre>";
        // die();

        // 13550 - Field ID - Restricted Countries
        // 13551 - Field ID - Accepting Countries
        $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
        $restricted_countries_field_id = DatabaseService::getField('casino', 'casino_restricted_countries');
        $accepting_countries_field_id  = DatabaseService::getField('casino', 'casino_accepting_countries');

        // Execute if there is restricted countries found.
        if (!empty($ordered_restricted_countries_ids)) {
            $restricted_countries_data = array();

            foreach ($ordered_restricted_countries_ids as $restricted_country_id) {
                $restricted_countries_data[] = array(
                    'pod_id' => $casino_pod_id,
                    'field_id' => $restricted_countries_field_id,
                    'item_id' => $casino_id,
                    'related_item_id' => $restricted_country_id,
                );
            }

            if (!empty($restricted_countries_data)) {
                scg_add_countries_to_casino($restricted_countries_data);
            }
        }

        // Add Allowed Countries to Casino.
        if (!empty($allowed_countries_ids)) {
            $allowed_countries_data = array();

            foreach ($allowed_countries_ids as $country_id) {
                $allowed_countries_data[] = array(
                    'pod_id' => $casino_pod_id,
                    'field_id' => $accepting_countries_field_id,
                    'item_id' => $casino_id,
                    'related_item_id' => $country_id,
                );
            }

            if (!empty($allowed_countries_data)) {
                scg_add_countries_to_casino($allowed_countries_data);
            }
        }
    }
}

add_action('scg_restricted_countries_batch_casino_import', 'scg_casino_trackers_import_casino_restricted_countries', 10, 6);



function ok_testing_now() {
  // $data_to_save = array(
  //   'rating_casino'     => 18,
  //   'review_id'         => 204,
  //   'rating_countries'  => array(
  //     60
  //   ),
  // );

    $casino_sheet_name = 'FI';
    $casino_name = 'NordicBet';
    $casino_id = 18;
    $review_id = 204;
    $country_ids = array(60);
    $casino_rating = 6.89;

    $sheet_name_lower = str_replace(' ', '-', strtolower($casino_sheet_name));
    $casino_name_lower = str_replace(' ', '-', strtolower($casino_name));
    $permalink = $casino_name_lower . '-' . $sheet_name_lower;
    $current_timestamp = current_time('mysql');

    // Prepare data for insertion/update.
    $data_to_save = array(
        'name'                  => $casino_name . ' - ' . $casino_sheet_name,
        'rating_casino'         => $casino_id,
        // 'rating_bookmaker_info' => $bookmaker_info_id,
        'rating_countries'      => $country_ids,
        // 'rating_sport_ratings'  => $bookmaker_sport_ratings,
        'rating_total'          => $casino_rating,
        'review_id'             => $review_id,
        'permalink'             => $permalink,
        'modified'              => $current_timestamp,
    );

  scg_casino_main_rating_country_relation_insert_or_update($data_to_save);

  // scg_generate_casinos_transients_cache();
}

// add_action('init', 'ok_testing_now');

function scg_casino_main_rating_country_relation_insert_or_update($data_to_save) {
  // Custom Country Relation.
  $current_timestamp = current_time('mysql');
  $permalink = str_replace('---', '-', strtolower($custom_name));
  $permalink = str_replace(' ', '', strtolower($custom_name));

  foreach ($data_to_save['rating_countries'] as $country_id) {
    $country_id = (int) $country_id;

    // d($country_id);
    // die();

    // Let's check if this rating exists first.
    $query = new SelectQuery;
    $query
        ->from('wp_pods_country_relation', 't')
        ->select(['id', 'country_id'])
        ->limit(1);

    $query->where()
        ->equal('t.casino_id', $data_to_save['rating_casino'])
        ->equal('t.main_review_id', $data_to_save['review_id'])
        ->equal('t.country_id', $country_id);

    $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $existing_country_rel_id = !empty($results) && $results->country_id == $country_id ? (int) $results->id : FALSE;

    // d($existing_country_rel_id);
    // die();

    $data_to_update = [
        'id'            => $existing_country_rel_id,
        'name'          => $data_to_save['name'],
        'permalink'     => $data_to_save['permalink'],
        'country_id'    => $country_id,
        'modified'      => $current_timestamp,
        'casino_id'     => $data_to_save['rating_casino'],
        'main_review_id' => $data_to_save['review_id'],
    ];

    if (empty($existing_country_rel_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // d($data_to_update);
    // die();

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_country_relation',
        [
            'id' => $existing_country_rel_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $country_rel_id = !empty($existing_country_rel_id) ? $existing_country_rel_id : $last_insert_id;
    // var_dump($country_rel_id);
    // die();
  }
}


function scg_game_type_rating_country_relation_insert_or_update($data_to_save) {

    // $data_to_save = array(
    //   'review_id'           => $review_id,
    //   'game_type_review_id' => $game_type_review_id,
    //   'casino_id'           => $casino_id,
    //   'name'                => $game_type,
    //   'permalink'           => $permalink,
    //   'game_type_id'        => $game_type_id,
    //   'country_ids'         => $country_ids,
    // );

  // Custom Country Relation.
  $current_timestamp = current_time('mysql');

  foreach ($data_to_save['country_ids'] as $country_id) {
    $country_id = (int) $country_id;

    // Let's check if this rating exists first.
    $query = new SelectQuery;
    $query
        ->from('wp_pods_country_relation', 't')
        ->select(['id', 'country_id'])
        ->limit(1);

    $query->where()
        ->equal('t.casino_id', $data_to_save['casino_id'])
        ->equal('t.main_review_id', $data_to_save['review_id'])
        ->equal('t.game_type_review_id', $data_to_save['game_type_review_id'])
        ->equal('t.game_type_id', $data_to_save['game_type_id'])
        ->equal('t.country_id', $country_id);

    $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
    $existing_country_rel_id = !empty($results) && $results->country_id == $country_id ? (int) $results->id : FALSE;

    // var_dump($existing_sport_review_id);
    // die();

    $data_to_update = [
        'id'             => $existing_country_rel_id,
        'name'           => $data_to_save['name'],
        'permalink'      => $data_to_save['permalink'],
        'country_id'     => $country_id,
        'modified'       => $current_timestamp,
        'casino_id'      => $data_to_save['casino_id'],
        'main_review_id' => $data_to_save['review_id'],
        'game_type_id'   => $data_to_save['game_type_id'],
        'game_type_review_id' => $data_to_save['game_type_review_id'],
    ];

    if (empty($existing_country_rel_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_country_relation',
        [
            'id' => $existing_country_rel_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $country_rel_id = !empty($existing_country_rel_id) ? $existing_country_rel_id : $last_insert_id;
    // var_dump($country_rel_id);
    // die();
  }
}


function just_some_testing() {
  $bookmaker = array();
  $bookmaker['bookmaker_name'] = '1xbit';
  $bonus_sum_currency_text = '100 $';
  $country_code = 'IN/IT';
  $country_ids = array(112, 105);
  $bookmaker_id = 10;
  $bookmaker_bonus_id = 3;

  $current_timestamp = current_time('mysql');
  $country_rel_name = $bookmaker['bookmaker_name'] . ' - ' . $bonus_sum_currency_text . ' - ' . $country_code;
  $permalink = str_replace('---', '-', strtolower($country_rel_name));
  $permalink = str_replace(' ', '', strtolower($permalink));

  if (!empty($country_ids) && !empty($bookmaker_id)) {
    foreach ($country_ids as $country_id) {
      // Let's check if this rating exists first.
      $query = new SelectQuery;
      $query
          ->from('wp_pods_country_relation', 't')
          ->select(['id', 'country_id'])
          ->limit(1);

      $query->where()
          ->equal('t.bookmaker_id', $bookmaker_id)
          ->equal('t.bonus_id', $bookmaker_bonus_id)
          ->equal('t.country_id', $country_id);

      $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
      $existing_country_rel_id = !empty($results) && $results->country_id == $country_id ? (int) $results->id : FALSE;

      // var_dump($existing_sport_review_id);
      // die();

      $data_to_update = [
          'id'            => $existing_country_rel_id,
          'name'          => $country_rel_name,
          'permalink'     => $permalink,
          'country_id'    => $country_id,
          'modified'      => $current_timestamp,
          'bookmaker_id'  => $bookmaker_id,
          'bonus_id'      => $bookmaker_bonus_id,
      ];

      if (empty($existing_country_rel_id)) {
        $data_to_update['created'] = $current_timestamp;
      }    

      // Insert/update review item.
      (new DatabaseService)->createOrUpdate(
          'wp_pods_country_relation',
          [
              'id' => $existing_country_rel_id,
          ],
          $data_to_update
      );

      global $wpdb;
      $last_insert_id = $wpdb->insert_id;
      $country_rel_id = !empty($existing_country_rel_id) ? $existing_country_rel_id : $last_insert_id;
      var_dump($country_rel_id);
      // die();
    }
  }
}

// add_action('admin_init', 'just_some_testing');



add_action('scg_batch_import_casino_main_info', 'scg_casino_trackers_import_main_info', 10, 1);

add_action('scg_batch_import_casino_licenses', 'scg_casino_trackers_import_licenses', 10, 1);

add_action('scg_batch_import_casino_deposit_methods', 'scg_casino_trackers_import_casino_deposit_methods', 10, 1);

add_action('scg_batch_import_casino_crypto_deposit_methods', 'scg_casino_trackers_import_casino_crypto_deposit_methods', 10, 1);

add_action('scg_batch_import_casino_withdrawal_methods', 'scg_casino_trackers_import_casino_withdrawal_methods', 10, 1);

add_action('scg_batch_import_casino_crypto_withdrawal_methods', 'scg_casino_trackers_import_casino_crypto_withdrawal_methods', 10, 1);

add_action('scg_batch_import_casino_game_providers', 'scg_casino_trackers_import_casino_game_providers', 10, 1);

add_action('scg_batch_import_all_casino_main_info_data', 'scg_casino_trackers_import_all_casinos_all_info_data', 10, 3);


/**
 * Import casino extra data.
 */
function scg_casino_trackers_import_casino_data($casino_main_info_data, $data_array_name, $pod_name, $field_name)
{
    global $wpdb;

    $casino_name = $casino_main_info_data['Casino/Bookmaker'];
    $casino_id = !empty($casino_name) ? scg_get_casino_id_from_name($casino_name) : FALSE;
    unset($data['Casino/Bookmaker']);

    $data = !empty($casino_main_info_data[$data_array_name]) ? $casino_main_info_data[$data_array_name] : FALSE;

    $data_ids = array();

    if (!empty($data) && !empty($casino_name) && !empty($casino_id)) {
      foreach ($data as $key_name => $entry) {
          // Get main info fields info.
          $name_escaped = str_replace("'", "\'", $entry['name']);
          $data_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_$pod_name WHERE name = '$name_escaped'");

          // Add payment method if it's not found.
          if (!$data_id) {
              $data_pods = pods($pod_name);

              $data = array(
                  'name'        => $entry,
                  // 'description' => $entry['description'],
              );

              if (!empty($data)) {
                  $data_id = $data_pods->add($data);
              }
          }

          if (!empty($data_id) && !empty($entry)) {
              $table_name = $wpdb->prefix . 'pods_' . $pod_name;

              $data_update = array(
                  'name'        => $entry,
                  // 'description' => $entry['description'],
              );

              $data_where = array('id' => $data_id);
              $wpdb->update($table_name, $data_update, $data_where);

              $data_ids[] = $data_id;
          }
      }
    }

    // var_dump($data);
    // var_dump($data_ids);

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $data_field = $casino_main_info_pod->fields($field_name);

    $data_field_info = array(
        'pod_id' => $data_field['pod_id'],
        'field_id' => $data_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($data_field_info) && !empty($data_ids)) {
        scg_remove_referenced_data_from_pod($data_field_info);

        $new_data = array();
        foreach ($data_ids as $data_item_id) {
            $new_data[] = array(
                'pod_id' => $data_field['pod_id'],
                'field_id' => $data_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $data_item_id,
            );
        }

        if (!empty($new_data)) {
            scg_add_reference_data_to_pod($new_data);
        }
    }
}



function stuff_is_great() {
    // Parse data from separate CSV files.
    // $all_info_data   = scg_casino_trackers_parse_casinos_all_info_csv_file('All Info');
    // $deposit_methods = scg_casino_trackers_parse_casinos_all_info_csv_file('Deposit Methods');
    // $licenses        = scg_casino_trackers_parse_casinos_all_info_csv_file('Licenses');
    // $game_providers  = scg_casino_trackers_parse_casinos_all_info_csv_file('Game Providers');

    // echo "<pre>" . print_r($all_info_data, true) . "</pre>";
    // die();

    // $casino_name = 'Bingo.com';
    // scg_casino_trackers_import_main_info($casino_name);
    // die();

    // scg_casino_trackers_get_all_info_csv_files();
    // guru_trackers_and_ratings_get_google_drive_file();


    // Get all CSV files from SxG - All Info - Restricted Countries file.
    // scg_casino_trackers_get_all_info_csv_files();

    $restricted_countries_csv_data = scg_casino_trackers_parse_casinos_all_info_csv_file('Restricted Countries');


    $single_casino_id = 1;
    $single_casino_name = '888ladies';

    $single_casino_info = array(
        'casino_id' => $single_casino_id,
        'casino_name' => $single_casino_name,
    );

    // Only import user selected Casino Restricted Countries.
    // $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name, $single_casino_info);
    $casinos = scg_casino_trackers_parse_restricted_countries_csv_file($restricted_countries_csv_data, $single_casino_info);
    echo "<pre>" . print_r($casinos, true) . "</pre>";
    die();


    $sheet_name = 'ALL';
    // $trackers = guru_trackers_parse_file($sheet_name);
    $ratings = guru_ratings_parse_file($sheet_name);
    // echo "<pre>" . print_r($trackers, true) . "</pre>";
    echo "<pre>" . print_r($ratings, true) . "</pre>";
    die();

}

// add_action('admin_init', 'stuff_is_great');


function scg_casino_trackers_import_all_casinos_all_info_data($import = TRUE, $casino_id = FALSE, $import_logos = FALSE) {
    // $import_status = get_option('scg-casinos-main-info-data-import-done');
    $selected_casino_name = !empty($casino_id) ? scg_get_casino_name_from_id($casino_id) : FALSE;

    // if ($import_status == 'done') return;

    $import_logos = !empty($import_logos) && $import_logos == 'yes' ? TRUE : FALSE;

    // Download new file.
    // scg_casino_trackers_get_restricted_countries_file();
    scg_casino_trackers_get_all_info_csv_files();

    // $all_info_data = scg_casino_trackers_parse_casinos_all_info_file();

    // Parse data from separate CSV files.
    $all_info_data   = scg_casino_trackers_parse_casinos_all_info_csv_file('All Info');
    $deposit_methods = scg_casino_trackers_parse_casinos_all_info_csv_file('Deposit Methods');
    $licenses        = scg_casino_trackers_parse_casinos_all_info_csv_file('Licenses');
    $game_providers  = scg_casino_trackers_parse_casinos_all_info_csv_file('Game Providers');

    if (!empty($all_info_data)) {
        foreach ($all_info_data as $entry) {
            if ($entry['sheet_name'] == 'All Info' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if (!empty($casino_all_info['Casino/Bookmaker'])) {
                        $data = set_transient('scg-main-infos-import-data-' . $casino_all_info['Casino/Bookmaker'], $casino_all_info, 1 * 24 * 60 * 60);

                        // Skip executing if Casino ID 
                        // provided and it doesn't match.
                        if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] != $selected_casino_name) {
                            continue;
                        }

                        // Create a "schedule action" for the data
                        as_schedule_single_action(
                            time(),
                            'scg_batch_import_casino_main_info',
                            array('casino_main_info_data' => $casino_all_info['Casino/Bookmaker']),
                            'all_info_data'
                        );

                        // Break foreach execution if we found
                        // our Casino ID in array.
                        if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] == $selected_casino_name) {
                            break;
                        }

                        // scg_casino_trackers_import_main_info($casino_all_info);
                    }
                }
            }
        }

        if (!empty($licenses)) {
            foreach ($licenses as $entry) {
                if ($entry['sheet_name'] == 'Licenses' && !empty($entry['data'])) {
                    foreach ($entry['data'] as $casino_all_info) {
                        if (!empty($casino_all_info['Casino/Bookmaker'])) {
                            $data = set_transient('scg-main-infos-import-data-licenses-' . $casino_all_info['Casino/Bookmaker'], $casino_all_info, 1 * 24 * 60 * 60);

                            // Skip executing if Casino ID 
                            // provided and it doesn't match.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] != $selected_casino_name) {
                                continue;
                            }

                            as_schedule_single_action(
                                time(),
                                'scg_batch_import_casino_licenses',
                                array('casino_main_info_data' => $casino_all_info['Casino/Bookmaker']),
                                'all_info_data'
                            );

                            // Break foreach execution if we found
                            // our Casino ID in array.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] == $selected_casino_name) {
                                break;
                            }

                            // scg_casino_trackers_import_licenses($casino_all_info);
                        }
                    }
                }
            }
        }

        if (!empty($deposit_methods)) {
            foreach ($deposit_methods as $entry) {
                if ($entry['sheet_name'] == 'Deposit Methods' && !empty($entry['data'])) {
                    // var_dump($entry['data']);
                    foreach ($entry['data'] as $casino_all_info) {
                        // Import logos first if needed.
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'Logo URL (Google Drive logo image link)') !== FALSE) {
                            if ($import_logos) {
                                // Import Payment method logos.
                                scg_casino_trackers_import_payment_method_logos($casino_all_info);

                                // Import Cryptos logos.
                                scg_casino_trackers_import_cryptos_logos($casino_all_info);
                            }

                            // // Stop foreaching if found.
                            // break;
                        }

                        // Import Deposit Methods secondly.
                        if (!empty($casino_all_info['Casino/Bookmaker'])) {
                            $data = set_transient('scg-main-infos-import-data-deposit-methods-' . $casino_all_info['Casino/Bookmaker'], $casino_all_info, 1 * 24 * 60 * 60);

                            // Skip executing if Casino ID 
                            // provided and it doesn't match.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] != $selected_casino_name) {
                                continue;
                            }

                            as_schedule_single_action(
                                time(),
                                'scg_batch_import_casino_deposit_methods',
                                array('data' => $casino_all_info['Casino/Bookmaker']),
                                'all_info_data'
                            );

                            $data = set_transient('scg-main-infos-import-data-crypto-deposit-methods-' . $casino_all_info['Casino/Bookmaker'], $casino_all_info, 1 * 24 * 60 * 60);

                            as_schedule_single_action(
                                time(),
                                'scg_batch_import_casino_crypto_deposit_methods',
                                array('data' => $casino_all_info['Casino/Bookmaker']),
                                'all_info_data'
                            );

                            // Break foreach execution if we found
                            // our Casino ID in array.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] == $selected_casino_name) {
                                break;
                            }

                            // scg_casino_trackers_import_casino_deposit_methods($casino_all_info);
                            // scg_casino_trackers_import_casino_crypto_deposit_methods($casino_all_info);
                        }
                    }
                }
            }
        }


        if (!empty($game_providers)) {
            foreach ($game_providers as $entry) {
                if ($entry['sheet_name'] == 'Game Providers' && !empty($entry['data'])) {
                    foreach ($entry['data'] as $casino_all_info) {
                        // Import logos first if needed.
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'Logo URL (Google Drive logo image link)') !== FALSE) {
                            if ($import_logos) {
                                // Import Game Provider Logos.
                                scg_casino_trackers_import_game_provider_logos($casino_all_info);
                            }

                            // // Stop foreaching if found.
                            // break;
                        }


                        // Import Game Providers secondly.
                        if (!empty($casino_all_info['Casino/Bookmaker'])) {
                            $data = set_transient('scg-main-infos-import-data-game-providers-' . $casino_all_info['Casino/Bookmaker'], $casino_all_info, 1 * 24 * 60 * 60);

                            // Skip executing if Casino ID 
                            // provided and it doesn't match.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] != $selected_casino_name) {
                                continue;
                            }

                            as_schedule_single_action(
                                time(),
                                'scg_batch_import_casino_game_providers',
                                array('data' => $casino_all_info['Casino/Bookmaker']),
                                'all_info_data'
                            );

                            // Break foreach execution if we found
                            // our Casino ID in array.
                            if (!empty($selected_casino_name) && $selected_casino_name != 'all' && $casino_all_info['Casino/Bookmaker'] == $selected_casino_name) {
                                break;
                            }

                            // scg_casino_trackers_import_casino_game_providers($casino_all_info);
                        }
                    }
                }
            }
        }

        // $casino_name = scg_get_casino_name_from_id($casino_id);
        // $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

        // $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_review_data_$casino_name_lower_%')");

        // Remove old transient cache items after import.
        global $wpdb;

        if ($casino_id == 'all') {
            // Remove all casinos transient cache.
            $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_review_data_%')");
            $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_main_info_%')");
        } else {
            $casino_name = scg_get_casino_name_from_id($casino_id);
            $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

            // Remove one casino transient cache.
            $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_review_data_$casino_name_lower%')");
            $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_main_info_$casino_id%')");
        }
    }



    // Check if have casinos to import.
    // if (!empty($casinos)) {
    //     foreach ($casinos as $item) {
    //         // Create a "schedule action" for the data
    //         $data = set_transient('scg-restricted-countries-data-' . $item['casino_id'] . '-' . $item['casino_name'], $item, 31 * 24 * 60 * 60);

    //         as_schedule_single_action(
    //             time(), 
    //             'scg_restricted_countries_batch_casino_import',
    //             array(
    //                 'casino_column_id'          => $item['casino_column_id'],
    //                 'casino_id'                 => $item['casino_id'],
    //                 'casino_name'               => $item['casino_name'],
    //                 'casino_restricted_enabled' => $item['casino_restricted_enabled'],
    //             ),
    //             'restricted_countries'
    //         );
    //     }
    
    //     // Generate transients cache.
    //     // $sheet_select = 'all_sheets';
    //     // scg_generate_casinos_transients_cache($sheet_select);
    // }

    /*
    * @todo %scg_casino_main_info_% transient delete on import.
    */

    // Generate transients cache.
    // $sheet_select = 'all_sheets';
    // scg_generate_casinos_transients_cache($sheet_select);

    // update_option('scg-casinos-main-info-data-import-done', 'done');
}

// function scg_schedule_all_main_infos_import() {
//     // var_dump(get_transient('scg-main-infos-import-data-licenses-36'));

//     // $import_status = get_option('scg-casinos-main-info-data-import-done');

//     // if ($import_status == 'done') return;

//     // Download new file.
//     scg_casino_trackers_get_restricted_countries_file();

//     // Create a "schedule action" for the data
//     // as_schedule_single_action(
//     //     time(),
//     //     'scg_batch_import_all_casino_main_info_data',
//     //     array('import' => TRUE),
//     //     'all_info_data'
//     // );

//     // update_option('scg-casinos-main-info-data-import-done', 'done');
// }

// add_action('admin_init', 'scg_schedule_all_main_infos_import');


function scg_manual_schedule_casino_all_info_import_action() {
    $casino = $_POST['casino_all_info_import_select'];
    $casino_id = !empty($casino) ? $casino : FALSE;
    $import_logos_raw = $_POST['casino_all_info_import_logos'];
    $import_logos = !empty($import_logos_raw) ? $import_logos_raw : FALSE;

    // Create a "schedule action" for the data
    as_schedule_single_action(
        time(),
        'scg_batch_import_all_casino_main_info_data',
        array(
            'import'        => TRUE,
            'casino_id'     => $casino_id,
            'import_logos'  => $import_logos,
        ),
        'all_info_data'
    );

    echo TRUE;
  
    wp_die();
}

add_action( 'wp_ajax_scg_manual_schedule_casino_all_info_import_action', 'scg_manual_schedule_casino_all_info_import_action' );


/**
 * Parse casinos restricted countries file.
 */
function scg_casino_trackers_parse_casinos_all_info_csv_file($selected_sheet_name, $single_casino_info = FALSE)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = scg_get_xlsx_uploads_directory_path();

    $filename = strtolower(str_replace(' ', '-', $selected_sheet_name)) . '.csv';
    $file_path = $xlsx_directory_path . $filename;

    // Check modified time of downloaded file.
    $modified_time = filemtime($file_path);
    $current_timestamp = current_time('timestamp');

    $time_difference = ($current_timestamp - $modified_time) / 60 / 60;

    // If modified time is more than 5,
    // then use cached version.
    $all_info_data = !empty($time_difference) && $time_difference > 5 ? get_transient('scg-all-info-casinos-data') : FALSE;

    // if (!empty($all_info_data)) {
    //     return $all_info_data;
    // }

    $allRows = [];
    $all_info_data = array();
    $data = array();

    try {
        $reader = ReaderEntityFactory::createCSVReader();
        $reader->open($file_path);

        $sheets_to_import = array(
            $selected_sheet_name
        );

        // d($sheets_to_import);
        // die();

        // foreach ($reader->getSheetIterator() as $sheetIndex => $sheet) {
        //     foreach ($sheet->getRowIterator() as $rowIndex => $row) {
        //         $allRows[] = $row->toArray();
        //     }
        // }

        foreach ($reader->getSheetIterator() as $sheet) {
            $sheet_name = $sheet->getName();

            // if ($sheet_name == 'Restricted Countries') {
            //     continue;
            // }

           /* Currently used memory */
           // $mem_usage = memory_get_usage();
           
           /* Peak memory usage */
           // $mem_peak = memory_get_peak_usage();

           // echo 'The script is now using: <strong>' . round($mem_usage / 1024 / 1024) . ' MB</strong> of memory.<br>';
           // echo 'Peak usage: <strong>' .round($mem_peak /1024/1024) . '</strong> MB memory.<br><br>';
           // die();

            // if ($sheet_name == $selected_sheet_name) {

                $i = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    // if ($i > 4) {
                    //   continue;
                    // }

                    $value = $row->toArray();
                    // echo "<pre>".print_r($value,true)."</pre>";
                    // var_dump($value);
                    // die();
                    // d($value);

                    // if ($i == 400) {
                        // d($value);

                       /* Peak memory usage */
                       // $mem_peak = memory_get_peak_usage();

                       // echo 'The script is now using: <strong>' . round($mem_usage / 1024 / 1024) . ' MB</strong> of memory.<br>';
                       // echo 'Peak usage: <strong>' .round($mem_peak /1024/1024) . '</strong> MB memory.<br><br>';
                       // die();
                    // }

                    $name = $value[0];

                    $data[] = $value;

                    // var_dump($name);
                    // var_dump($value);
                    // var_dump($value);

                    // $i++;
                }

                // No need to transpose Restricted Countries
                // array now, for that reason: return early.
                if ($selected_sheet_name == 'Restricted Countries') {
                    return $data;
                }

                $transposed_data = scg_transpose($data);
                $transposed_data = array_values($transposed_data);

                // echo "<pre>" . print_r($transposed_data, true) . "</pre>";

                $i = 0;
                foreach ($transposed_data as $key => $value) {
                    // if ($i > 3) {
                    //   continue;
                    // }

                    // var_dump($value);
                    // var_dump($key);

                    $next_is_web_languages = FALSE;
                    $next_is_chat_languages = FALSE;
                    $next_is_email_languages = FALSE;
                    $next_is_licenses = FALSE;
                    $next_is_products = FALSE;

                    if ($selected_sheet_name == 'All Info') {
                        $transposed_data[$key]['Website languages'] = array();
                        $transposed_data[$key]['Chat languages'] = array();
                        $transposed_data[$key]['Email languages'] = array();
                        $transposed_data[$key]['Products'] = array();
                        $transposed_data[$key]['SBG Bookmaker type'] = array();
                        $transposed_data[$key]['SBG Additional betting selections'] = array();
                        $transposed_data[$key]['Wagering requirements'] = array();
                        $transposed_data[$key]['Bonus requirements'] = array();
                    }

                    if ($selected_sheet_name == 'Licenses') {
                        // $transposed_data[$key]['Licenses'] = array();
                    }

                    foreach ($value as $subkey => $subvalue) {
                        if ($selected_sheet_name == 'All Info') {
                            // Products
                            if ($subkey == 'Products') {
                                $next_is_web_languages = FALSE;
                                $next_is_chat_languages = FALSE;
                                $next_is_email_languages = FALSE;
                                $next_is_licenses = FALSE;
                                $next_is_products = TRUE;
                            }

                            if ($next_is_products) {
                              if (!empty($subvalue)) {
                                $transposed_data[$key]['Products'][$transposed_data[$key][$subkey]] = $subkey;

                                // var_dump($subkey);
                                // var_dump($subvalue);
                              }

                              unset($transposed_data[$key][$subkey]);

                              if (!empty($transposed_data[$key]['Products'])) {
                                ksort($transposed_data[$key]['Products']);
                              }

                              // if ($subkey == 'Website Languages') {
                              //   unset($transposed_data[$key]['Products'][$subkey]);
                              // }
                            }

                            if($subkey == 'SBG Bookmaker type' || $subkey == 'SBG Additional betting selections'){
                                $next_is_products = FALSE;
                                continue;
                            }

                            // Website Languages.
                            if ($subkey == 'Website languages') {
                                $next_is_web_languages = TRUE;
                                $next_is_products = FALSE;
                            }

                            if ($next_is_web_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Website languages'][] = str_replace(' - WEB', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }

                            if ($subkey == 'Chat languages') {
                                $next_is_web_languages = FALSE;
                                $next_is_chat_languages = TRUE;
                                $next_is_products = FALSE;
                            }

                            if ($next_is_chat_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Chat languages'][] = str_replace(' - CHAT', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }

                            if ($subkey == 'Email languages') {
                                $next_is_web_languages = FALSE;
                                $next_is_chat_languages = FALSE;
                                $next_is_email_languages = TRUE;
                                $next_is_products = FALSE;
                            }

                            if ($next_is_email_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Email languages'][] = str_replace(' - EMAIL', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }

                            if($subkey == 'Wagering requirements' || $subkey == 'Bonus requirements'){
                                continue;
                            }
                        }

                        if ($selected_sheet_name == 'Licenses') {
                            // var_dump($subkey);

                            if (empty($subvalue)) {
                                unset($transposed_data[$key][$subkey]);
                            }

                            // if ($subkey != 'Casino' || $subkey != 'Casino ID') {
                            //   $next_is_licenses = TRUE;
                            // }

                            // if ($next_is_licenses) {
                            //   if (!empty($subvalue)) {
                            //     $transposed_data[$key]['Licenses'][] = $subkey;
                            //   }

                            //   unset($transposed_data[$key][$subkey]);
                            // }
                        }

                        // var_dump($subkey);
                        // var_dump($subvalue);
                    }

                    $i++;
                }

                $all_info_data[] = array(
                    'sheet_name' => $selected_sheet_name,
                    'data' => $transposed_data,
                );

            // }

        }

        $reader->close();

        // echo "<pre>" . print_r($data, true) . "</pre>";

        // $all_info_data[] = array(
        //     'sheet_name' => $sheet_name,
        //     'data' => $allRows,
        // );


    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    $count = 0;
    $casinos = array();

    if (!empty($single_casino_info)) {
        foreach ($casinos as $casino) {
            if (
                $casino['casino_id'] == $single_casino_info['casino_id'] ||
                $casino['casino_name'] == $single_casino_info['casino_name']
            ) {
                $single_casino = $casino;

                continue;
            }
        }

        $casinos = array();

        // Import single casino.
        if (!empty($single_casino)) {
            $casinos[] = $single_casino;
        }
    }

    $data = set_transient('scg-all-info-casinos-data', $all_info_data, 1 * 24 * 60 * 60);

    return $all_info_data;
}


/**
 * Parse casinos restricted countries file.
 */
function scg_casino_trackers_parse_casinos_all_info_file($single_casino_info = FALSE)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = scg_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'casinos-restricted-countries.xlsx';

    // Check modified time of downloaded file.
    $modified_time = filemtime($file_path);
    $current_timestamp = current_time('timestamp');

    $time_difference = ($current_timestamp - $modified_time) / 60 / 60;

    // If modified time is more than 5,
    // then use cached version.
    $all_info_data = !empty($time_difference) && $time_difference > 5 ? get_transient('scg-all-info-casinos-data') : FALSE;

    if (!empty($all_info_data)) {
        return $all_info_data;
    }

    try {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($file_path);

        $sheets_to_import = array(
            'All Info',
            'Deposit Methods',
            'Withdrawal Methods',
            'Game Providers',
            'Licenses',
        );

        $all_info_data = array();

        foreach ($reader->getSheetIterator() as $sheet) {
            $sheet_name = $sheet->getName();

            if ($sheet_name == 'Restricted Countries') {
                continue;
            }

            $data = array();
            if ($sheet_name == 'All Info' || $sheet_name == 'Deposit Methods' || $sheet_name == 'Withdrawal Methods' || $sheet_name == 'Game Providers' || $sheet_name == 'Licenses') {

                $i = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    // if ($i > 4) {
                    //   continue;
                    // }

                    $value = $row->toArray();
                    $name = $value[0];

                    $data[] = $value;

                    // var_dump($name);
                    // var_dump($value);
                    // var_dump($value);

                    $i++;
                }

                $transposed_data = scg_transpose($data);

                if ($sheet_name != 'Deposit Methods') {
                    unset($transposed_data[0]);
                }

                $transposed_data = array_values($transposed_data);

                $i = 0;
                foreach ($transposed_data as $key => $value) {
                    // if ($i > 3) {
                    //   continue;
                    // }

                    // var_dump($value);
                    // var_dump($key);

                    $next_is_web_languages = FALSE;
                    $next_is_chat_languages = FALSE;
                    $next_is_email_languages = FALSE;
                    $next_is_licenses = FALSE;

                    if ($sheet_name == 'All Info') {
                        $transposed_data[$key]['Website languages'] = array();
                        $transposed_data[$key]['Chat languages'] = array();
                        $transposed_data[$key]['Email languages'] = array();
                    }

                    if ($sheet_name == 'Licenses') {
                        // $transposed_data[$key]['Licenses'] = array();
                    }

                    foreach ($value as $subkey => $subvalue) {
                        if ($sheet_name == 'All Info') {
                            if ($subkey == 'Website languages') {
                                $next_is_web_languages = TRUE;
                            }

                            if ($next_is_web_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Website languages'][] = str_replace(' - WEB', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }

                            if ($subkey == 'Chat languages') {
                                $next_is_web_languages = FALSE;
                                $next_is_chat_languages = TRUE;
                            }

                            if ($next_is_chat_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Chat languages'][] = str_replace(' - CHAT', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }

                            if ($subkey == 'Email languages') {
                                $next_is_web_languages = FALSE;
                                $next_is_chat_languages = FALSE;
                                $next_is_email_languages = TRUE;
                            }

                            if ($next_is_email_languages) {
                                if (!empty($subvalue)) {
                                    $transposed_data[$key]['Email languages'][] = str_replace(' - EMAIL', '', $subkey);
                                }

                                unset($transposed_data[$key][$subkey]);
                            }
                        }

                        if ($sheet_name == 'Licenses') {
                            // var_dump($subkey);

                            if (empty($subvalue)) {
                                unset($transposed_data[$key][$subkey]);
                            }

                            // if ($subkey != 'Casino' || $subkey != 'Casino ID') {
                            //   $next_is_licenses = TRUE;
                            // }

                            // if ($next_is_licenses) {
                            //   if (!empty($subvalue)) {
                            //     $transposed_data[$key]['Licenses'][] = $subkey;
                            //   }

                            //   unset($transposed_data[$key][$subkey]);
                            // }
                        }

                        // var_dump($subkey);
                        // var_dump($subvalue);
                    }

                    $i++;
                }

                $all_info_data[] = array(
                    'sheet_name' => $sheet_name,
                    'data' => $transposed_data,
                );
            }

        }

        // var_dump($all_info_data);

        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        // // $reader->setLoadSheetsOnly([$sheet_name]);
        // $reader->setReadDataOnly(true);
        // $reader->setReadEmptyCells(false);
        // $spreadsheet = $reader->load($file_path);

        // $worksheetData = $reader->listWorksheetInfo($file_path);

        // $sheetCount = $spreadsheet->getSheetCount();

        // $all_sheets = $spreadsheet->getSheetNames();
        // $all_sheets_count = count($all_sheets);

        // foreach ($all_sheets as $key => $sheet) {
        //   // Remove restricted countries sheet.
        //   if ($sheet == 'Restricted Countries') {
        //     unset($all_sheets[$key]);
        //   }
        // }

        // foreach ($all_sheets as $key => $sheet) {
        //   $sheet = $spreadsheet->getSheet($key);
        //   $sheetData = $sheet->toArray(null, true, true, true);
        //   $sheetData_array = array_filter(array_map('array_filter', $sheetData));

        //   $keys = array_shift($sheetData);

        //   $result = array_map(function($values) use ($keys) {
        //     return array_combine($keys, $values); }, $sheetData);

        //   if ($key == 1) {
        //     var_dump($result);
        //   }
        // }

        // for ($i = 0; $i < $sheetCount; $i++) {
        //   $sheet = $spreadsheet->getSheet($i);
        //   $active_sheet = $spreadsheet->getActiveSheet();

        //   if ($i == 1) {
        //     var_dump($all);
        //     var_dump($active_sheet->title);
        //     // var_dump($active_sheet->getTitle());
        //     // var_dump($active_sheet->title());
        //   }

        //   //var_dump($sheet);

        //   // $sheetData = $sheet->toArray(null, true, true, true);

        //   // var_dump($sheetData);
        // }

        // foreach ($worksheetData as $worksheet) {
        //   if ($worksheet['worksheetName'] == $sheet_name) {
        //     $sheet_lastColumnIndex = $worksheet['lastColumnIndex'];
        //     $sheet_lastColumnLetter = $worksheet['lastColumnLetter'];

        //     $sheet_lastRange = $sheet_lastColumnLetter . $sheet_lastColumnIndex;
        //   }
        // }

        // $active_sheet = $spreadsheet->getActiveSheet();

        // $all_info_data = $active_sheet->rangeToArray(
        //   'A1:' . $sheet_lastColumnLetter . '300'
        // );

        // // Filter array to only non-empty items.
        // $all_info_data_array = array_filter(array_map('array_filter', $all_info_data));

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    $count = 0;
    $casinos = array();

    // foreach ($all_info_data_array as $key => $restricted_country) {

    //   // Prepare Casino Data in first row.
    //   if ($key == 0) {
    //     foreach ($restricted_country as $row_key => $entry) {

    //       if ($row_key > 1) {
    //         $casino_column_id = $row_key;

    //         $casino_id = $restricted_countries_data_array[1][$casino_column_id];
    //         $casino_name = $restricted_country[$casino_column_id];
    //         $casino_restricted_enabled_raw = trim($restricted_countries_data_array[2][$casino_column_id]);
    //         $casino_restricted_enabled = $casino_restricted_enabled_raw != 'Ne';

    //         $casinos[$casino_column_id] = array(
    //           'casino_column_id' => $casino_column_id,
    //           'casino_id' => $casino_id,
    //           'casino_name' => $casino_name,
    //           'casino_restricted_enabled' => $casino_restricted_enabled,
    //           'casino_restricted_countries' => array(),
    //         );

    //       }
    //     }
    //   }

    //   if ($key > 2) {
    //     $country_name = $restricted_country[0];
    //     $country_code = $restricted_country[1];

    //     foreach ($casinos as $casino_key => $casino) {
    //       $country_is_restricted = !empty($restricted_country[$casino['casino_column_id']]);

    //       if ($country_is_restricted) {
    //         $casinos[$casino_key]['casino_restricted_countries'][] = array(
    //           'country_name' => $country_name,
    //           'country_code' => $country_code,
    //         );
    //       }
    //     }
    //   }

    //   $count++;
    // }

    if (!empty($single_casino_info)) {
        foreach ($casinos as $casino) {
            if (
                $casino['casino_id'] == $single_casino_info['casino_id'] ||
                $casino['casino_name'] == $single_casino_info['casino_name']
            ) {
                $single_casino = $casino;

                continue;
            }
        }

        $casinos = array();

        // Import single casino.
        if (!empty($single_casino)) {
            $casinos[] = $single_casino;
        }
    }


    $data = set_transient('scg-all-info-casinos-data', $all_info_data, 1 * 24 * 60 * 60);

    // return $casinos;
    return $all_info_data;
}


/**
 * Transpose array helper function.
 */
function scg_transpose($array_one)
{
    $array_two = [];
    foreach ($array_one as $key => $item) {
        foreach ($item as $subkey => $subitem) {
            // $array_two[] = array($array_one[$key][0] => $subitem);
            $array_two[$subkey][$array_one[$key][0]] = $subitem;
        }
    }

    return $array_two;
}


/**
 * Testing import on stage.
 */
function testing_like_its_good()
{
    // $testing_done = get_option('scg-casino-trackers-testing-done');

    // if ($testing_done == 'done') return;

    // update_option('scg-casino-trackers-testing-done', 'done');

    // // Download new file.
    // scg_casino_trackers_get_restricted_countries_file();

    // Casino ID for imort.
    $casino_id_import = 8;
    $import_is_active = TRUE;

    // Import data settings.
    $import_main_info = FALSE;
    $import_licenses = FALSE;
    $import_deposit_methods = FALSE;
    $import_withdrawal_methods = FALSE;
    $import_game_providers = TRUE;

    // Import logo file.
    $import_game_provider_logos = FALSE;
    $import_payment_method_logos = TRUE;
    $import_crypto_logos = FALSE;

    $all_info_data = scg_casino_trackers_parse_casinos_all_info_file();

    if (!empty($all_info_data)) {
        foreach ($all_info_data as $entry) {
            if ($entry['sheet_name'] == 'All Info' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_main_info && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_main_info($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Licenses' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_licenses && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_licenses($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Deposit Methods' && !empty($entry['data'])) {
                // var_dump($entry['data']);
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_deposit_methods && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_casino_deposit_methods($casino_all_info);
                        scg_casino_trackers_import_casino_crypto_deposit_methods($casino_all_info);
                    }

                    if (strpos($casino_all_info['Casino'], 'Logo URL') !== FALSE) {
                        if ($import_is_active && $import_payment_method_logos) {
                            scg_casino_trackers_import_payment_method_logos($casino_all_info);
                        }

                        if ($import_is_active && $import_crypto_logos) {
                            scg_casino_trackers_import_cryptos_logos($casino_all_info);
                        }
                    }
                }
            }

            if ($entry['sheet_name'] == 'Withdrawal Methods' && !empty($entry['data'])) {
                // var_dump($entry['data']);
                foreach ($entry['data'] as $casino_all_info) {
                    // Import withdrawal methods.
                    if ($import_is_active && $import_withdrawal_methods && $casino_all_info['CG Casino'] == $casino_id_import) {
                        // var_dump($casino_all_info);
                        scg_casino_trackers_import_casino_withdrawal_methods($casino_all_info);
                        scg_casino_trackers_import_casino_crypto_withdrawal_methods($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Game Providers' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_game_providers && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_casino_game_providers($casino_all_info);
                    }

                    if (strpos($casino_all_info['Casino'], 'Logo URL') !== FALSE) {
                        if ($import_is_active && $import_game_provider_logos) {
                            scg_casino_trackers_import_game_provider_logos($casino_all_info);
                        }
                    }
                }
            }

        }

        // Remove old transient cache items after import.
        global $wpdb;
        $casino_id = $casino_id_import;
        $casino_name = scg_get_casino_name_from_id($casino_id);
        $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

        $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_review_data_$casino_name_lower_%')");
    }

    die();
}

// add_action('admin_init', 'testing_like_its_good');


function restricted_testing() {
    $casino_column_id = 281;
    $casino_id        = 58;
    $casino_name      = 'Tsars';
    $casino_restricted_enabled = 1;
    $casino_enabled = 1;
    $casino_closed = 0;

    // scg_remove_countries_from_casino($casino_id);
    // scg_casino_trackers_import_casino_restricted_countries($casino_column_id, $casino_id, $casino_name, $casino_restricted_enabled, $casino_enabled, $casino_closed);
    // die();

  // $casino_id = 11;
  // $game_type_id = 1;
  // $country_id = 1;
  // $language = 'en';

  // $review = scg_casino_trackers_get_casino_game_type_review($casino_id, $game_type_id, $country_id, $language);

  // var_dump($review);
}

// add_action('admin_init', 'restricted_testing');

/**
 * Import casino all info.
 */
function scg_casino_trackers_import_casino_all_info($casino_id, $import_logos = FALSE)
{
    // Download new file.
    scg_casino_trackers_get_restricted_countries_file();

    // Casino ID for imort.
    $casino_id_import = $casino_id;
    $import_is_active = TRUE;

    // Import data settings.
    $import_main_info = TRUE;
    $import_licenses = TRUE;
    $import_deposit_methods = TRUE;
    $import_withdrawal_methods = TRUE;
    $import_game_providers = TRUE;

    $all_info_data = scg_casino_trackers_parse_casinos_all_info_file();

    if (!empty($all_info_data)) {
        foreach ($all_info_data as $entry) {
            if ($entry['sheet_name'] == 'All Info' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_main_info && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_main_info($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Licenses' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_licenses && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_licenses($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Deposit Methods' && !empty($entry['data'])) {
                // var_dump($entry['data']);
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_deposit_methods && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_casino_deposit_methods($casino_all_info);
                        scg_casino_trackers_import_casino_crypto_deposit_methods($casino_all_info);
                    }

                    if (strpos($casino_all_info['Casino'], 'Logo URL') !== FALSE) {
                        if ($import_is_active && $import_logos) {
                            scg_casino_trackers_import_payment_method_logos($casino_all_info);
                        }

                        if ($import_is_active && $import_logos) {
                            scg_casino_trackers_import_cryptos_logos($casino_all_info);
                        }
                    }
                }
            }

            if ($entry['sheet_name'] == 'Withdrawal Methods' && !empty($entry['data'])) {
                // var_dump($entry['data']);
                foreach ($entry['data'] as $casino_all_info) {
                    // Import withdrawal methods.
                    if ($import_is_active && $import_withdrawal_methods && $casino_all_info['CG Casino'] == $casino_id_import) {
                        // var_dump($casino_all_info);
                        scg_casino_trackers_import_casino_withdrawal_methods($casino_all_info);
                        scg_casino_trackers_import_casino_crypto_withdrawal_methods($casino_all_info);
                    }
                }
            }

            if ($entry['sheet_name'] == 'Game Providers' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {
                    if ($import_is_active && $import_game_providers && $casino_all_info['CG Casino'] == $casino_id_import) {
                        scg_casino_trackers_import_casino_game_providers($casino_all_info);
                    }

                    if (strpos($casino_all_info['Casino'], 'Logo URL') !== FALSE) {
                        if ($import_is_active && $import_logos) {
                            scg_casino_trackers_import_game_provider_logos($casino_all_info);
                        }
                    }
                }
            }

        }

        // Remove old transient cache items after import.
        global $wpdb;
        $casino_name = scg_get_casino_name_from_id($casino_id);
        $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

        $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casino_review_data_$casino_name_lower_%')");
    }
}


/**
 * Import casino deposit methods.
 */
function scg_casino_trackers_import_casino_deposit_methods($data)
{
    $casino_name = $data;
    $casino_id = scg_get_casino_id_from_name($casino_name);

    $data_info = get_transient('scg-main-infos-import-data-deposit-methods-' . $data);
    $data = !empty($data_info) ? $data_info : $data;

    global $wpdb;

    if (Guru_Trackers_IS_BINGO) {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['SBIG Bingo'] != 'Taip' || empty($casino_id)) {
            return;
        }
    } else {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['CG Casino'] != 'Taip' || empty($casino_id)) {
            return;
        }
    }

    unset($data['Casino/Bookmaker']);
    unset($data['SCG Casino']);
    unset($data['CG Casino']);
    unset($data['SBG Bookmaker']);
    unset($data['LG Bookmaker']);
    unset($data['SBIG Bingo']);

    $deposit_methods_ids = array();

    $deposit_methods = scg_casino_trackers_parse_casinos_all_info_csv_file('Deposit Methods');
    $deposit_methods_urls = FALSE;

    if (!empty($deposit_methods)) {
        foreach ($deposit_methods as $entry) {
            if ($entry['sheet_name'] == 'Deposit Methods' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {

                    if (Guru_Trackers_IS_BINGO) {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'SBIG Bingo rating link') !== FALSE) {
                            $deposit_methods_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    } else {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'CG rating link') !== FALSE) {
                            $deposit_methods_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    }

                }
            }
        }
    }

    foreach ($data as $key_name => $entry) {
        // Trim data.
        $entry = trim($entry);

        // URL.
        $full_url = !empty($deposit_methods_urls) ? $deposit_methods_urls[$key_name] : FALSE;
        $page_id = scg_casino_trackers_get_top_rating_page_id_from_url($full_url);

        // echo "<pre>" . print_r($deposit_methods_urls, true) . "</pre>";
        // echo "<pre>" . print_r($key_name, true) . "</pre>";
        // echo "<pre>" . print_r($full_url, true) . "</pre>";
        // echo "<pre>" . print_r($page_id, true) . "</pre>";

        // Get main info fields info.
        $key_name_escaped = str_replace("'", "\'", $key_name);
        $payment_method_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_payment_method WHERE name = '$key_name_escaped'");

        if ($key_name == 'Cryptos') {
            break;
        }

        // Add payment method if it's not found.
        if (!$payment_method_id) {
            $payment_method_pods = pods('payment_method');

            $data = array(
                'name' => $key_name,
            );

            if (!empty($data['name'])) {
                $payment_method_id = $payment_method_pods->add($data);
            }
        }

        if (!empty($payment_method_id) && !empty($entry)) {
            $deposit_methods_ids[] = array(
                'id' => $payment_method_id,
                'top_rating_page' => $page_id,
            );
        }
    }

    // echo "<pre>" . print_r($deposit_methods_ids, true) . "</pre>";
    // die();

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $deposit_methods_field = $casino_main_info_pod->fields('deposit_methods');

    // Payment method.
    $payment_method_pod = pods('payment_method');
    $top_rating_page_field = $payment_method_pod->fields('top_rating_page');

    // Deposit methods.
    $deposit_methods_field_info = array(
        'pod_id' => $deposit_methods_field['pod_id'],
        'field_id' => $deposit_methods_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($deposit_methods_field_info) && !empty($deposit_methods_ids)) {
        scg_remove_referenced_data_from_pod($deposit_methods_field_info);

        $deposit_methods_data = array();
        foreach ($deposit_methods_ids as $deposit_method_id) {
            $deposit_methods_data[] = array(
                'pod_id' => $deposit_methods_field['pod_id'],
                'field_id' => $deposit_methods_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $deposit_method_id['id'],
            );
        }

        if (!empty($deposit_methods_data)) {
            scg_add_reference_data_to_pod($deposit_methods_data);
        }
    }


    // TOP rating page.
    if (!empty($deposit_methods_ids)) {
        $top_rating_page_data = array();
        foreach ($deposit_methods_ids as $deposit_method_id) {
            $top_rating_page_field_info = array(
                'pod_id' => $top_rating_page_field['pod_id'],
                'field_id' => $top_rating_page_field['id'],
                'item_id' => $deposit_method_id['id'],
            );

            scg_remove_referenced_data_from_pod($top_rating_page_field_info);

            if (!empty($deposit_method_id['top_rating_page'])) {
                $top_rating_page_data[] = array(
                    'pod_id' => $top_rating_page_field['pod_id'],
                    'field_id' => $top_rating_page_field['id'],
                    'item_id' => $deposit_method_id['id'],
                    'related_item_id' => $deposit_method_id['top_rating_page'],
                );
            }
        }

        if (!empty($top_rating_page_data)) {
            scg_add_reference_data_to_pod($top_rating_page_data);
        }
    }
}


/**
 * Import casino crypto deposit methods.
 */
function scg_casino_trackers_import_casino_crypto_deposit_methods($data)
{
    $casino_name = $data;
    $casino_id = scg_get_casino_id_from_name($casino_name);

    $data_info = get_transient('scg-main-infos-import-data-crypto-deposit-methods-' . $data);
    $data = !empty($data_info) ? $data_info : $data;

    global $wpdb;

    if (Guru_Trackers_IS_BINGO) {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['SBIG Bingo'] != 'Taip' || empty($casino_id)) {
            return;
        }
    } else {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['CG Casino'] != 'Taip' || empty($casino_id)) {
            return;
        }
    }

    unset($data['Casino/Bookmaker']);
    unset($data['SCG Casino']);
    unset($data['CG Casino']);
    unset($data['SBG Bookmaker']);
    unset($data['LG Bookmaker']);
    unset($data['SBIG Bingo']);

    $crypto_deposits_ids = array();

    $crypto_deposit_methods = scg_casino_trackers_parse_casinos_all_info_csv_file('Deposit Methods');
    $crypto_deposit_methods_urls = FALSE;

    if (!empty($crypto_deposit_methods)) {
        foreach ($crypto_deposit_methods as $entry) {
            if ($entry['sheet_name'] == 'Deposit Methods' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {

                    if (Guru_Trackers_IS_BINGO) {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'SBIG Bingo rating link') !== FALSE) {
                            $crypto_deposit_methods_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    } else {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'CG rating link') !== FALSE) {
                            $crypto_deposit_methods_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    }

                }
            }
        }
    }

    $is_cryptos = FALSE;
    foreach ($data as $key_name => $entry) {
        // Trim data.
        $entry = trim($entry);

        if ($key_name == 'Cryptos') {
            $is_cryptos = TRUE;
        }

        if (!$is_cryptos || $key_name == 'Cryptos' || strpos($key_name, 'REVIEW') !== false) {
            continue;
        }

        // URL.
        $full_url = !empty($crypto_deposit_methods_urls) ? $crypto_deposit_methods_urls[$key_name] : FALSE;
        $page_id = !empty($full_url) ? scg_casino_trackers_get_top_rating_page_id_from_url($full_url) : FALSE;

        // echo "<pre>" . print_r($full_url, true) . "</pre>";
        // echo "<pre>" . print_r($page_id, true) . "</pre>";

        // Get main info fields info.
        $key_name_escaped = str_replace("'", "\'", $key_name);
        $payment_method_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_cryptocurrency WHERE name = '$key_name_escaped'");

        // Add payment method if it's not found.
        if (!empty($is_cryptos) && !$payment_method_id) {
            $payment_method_pods = pods('cryptocurrency');

            $data = array(
                'name' => $key_name,
            );

            if (!empty($data['name'])) {
                $payment_method_id = $payment_method_pods->add($data);
            }
        }

        if (!empty($payment_method_id) && !empty($entry)) {
            $crypto_deposits_ids[] = array(
                'id' => $payment_method_id,
                'top_rating_page' => $page_id,
            );
        }
    }

    // echo "<pre>" . print_r($crypto_deposits_ids, true) . "</pre>";
    // die();

//  var_dump($casino_name);
//  var_dump($casino_id);
//  var_dump($crypto_deposits_ids);

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $crypto_deposits_field = $casino_main_info_pod->fields('crypto_deposits');

    // Crypto TOP rating page.
    $crypto_pod = pods('cryptocurrency');
    $top_rating_page_field = $crypto_pod->fields('top_rating_page');

    // Deposit methods.
    $crypto_deposits_field_info = array(
        'pod_id' => $crypto_deposits_field['pod_id'],
        'field_id' => $crypto_deposits_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($crypto_deposits_field_info) && !empty($crypto_deposits_ids)) {
        scg_remove_referenced_data_from_pod($crypto_deposits_field_info);

        $deposit_methods_data = array();
        foreach ($crypto_deposits_ids as $deposit_method_id) {
            $crypto_deposits_data[] = array(
                'pod_id' => $crypto_deposits_field['pod_id'],
                'field_id' => $crypto_deposits_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $deposit_method_id['id'],
            );
        }

        if (!empty($crypto_deposits_data)) {
            scg_add_reference_data_to_pod($crypto_deposits_data);
        }
    }

    // TOP rating page.
    if (!empty($crypto_deposits_ids)) {
        $top_rating_page_data = array();
        foreach ($crypto_deposits_ids as $deposit_method_id) {
            $top_rating_page_field_info = array(
                'pod_id' => $top_rating_page_field['pod_id'],
                'field_id' => $top_rating_page_field['id'],
                'item_id' => $deposit_method_id['id'],
            );

            scg_remove_referenced_data_from_pod($top_rating_page_field_info);

            if (!empty($deposit_method_id['top_rating_page'])) {
                $top_rating_page_data[] = array(
                    'pod_id' => $top_rating_page_field['pod_id'],
                    'field_id' => $top_rating_page_field['id'],
                    'item_id' => $deposit_method_id['id'],
                    'related_item_id' => $deposit_method_id['top_rating_page'],
                );
            }
        }

        if (!empty($top_rating_page_data)) {
            scg_add_reference_data_to_pod($top_rating_page_data);
        }
    }
}


/**
 * Import casino game providers.
 */
function scg_casino_trackers_import_casino_game_providers($data)
{
    $casino_name = $data;
    $casino_id = scg_get_casino_id_from_name($casino_name);

    $data_info = get_transient('scg-main-infos-import-data-game-providers-' . $data);
    $data = !empty($data_info) ? $data_info : $data;

    global $wpdb;

    if (Guru_Trackers_IS_BINGO) {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['SBIG Bingo'] != 'Taip' || empty($casino_id)) {
            return;
        }
    } else {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['CG Casino'] != 'Taip' || empty($casino_id)) {
            return;
        }
    }

    unset($data['Casino/Bookmaker']);
    unset($data['SCG Casino']);
    unset($data['CG Casino']);
    unset($data['SBG Bookmaker']);
    unset($data['LG Bookmaker']);
    unset($data['SBIG Bingo']);

    $game_providers_ids = array();

    // error_log('<pre>' . print_r($data, TRUE) . '</pre>');

    // var_dump($data);

    $game_providers = scg_casino_trackers_parse_casinos_all_info_csv_file('Game Providers');
    $game_providers_urls = FALSE;

    if (!empty($game_providers)) {
        foreach ($game_providers as $entry) {
            if ($entry['sheet_name'] == 'Game Providers' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {

                    if (Guru_Trackers_IS_BINGO) {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'SBIG Bingo rating link') !== FALSE) {
                            $game_providers_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    } else {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'CG rating link') !== FALSE) {
                            $game_providers_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    }

                }
            }
        }
    }

    // echo "<pre>" . print_r($game_providers_urls, true) . "</pre>";
    // die();

    foreach ($data as $key_name => $entry) {
        // Remove spaces.
        $entry = trim($entry);

        // URL.
        $full_url = !empty($game_providers_urls) ? $game_providers_urls[$key_name] : FALSE;
        $page_id = scg_casino_trackers_get_top_rating_page_id_from_url($full_url);

        // Pass when there is no name.
        if (empty($key_name)) continue;

        // Get main info fields info.
        $key_name_escaped = str_replace("'", "\'", $key_name);
        $provider_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_game_provider WHERE name = '$key_name_escaped'");

        // var_dump($key_name_escaped);
        // var_dump("SELECT id FROM {$wpdb->prefix}pods_casino_game_provider WHERE name = '$key_name_escaped");
        // var_dump($provider_id);

        // Add game provider if it's not found.
        if (!$provider_id && (!empty($key_name) || $key_name != ' ')) {
            $game_provider_pods = pods('casino_game_provider');

            $data = array(
                'name' => $key_name,
            );

            // var_dump($data);

            if (!empty($data['name'])) {
                $provider_id = $game_provider_pods->add($data);
            }
        }

        if (!empty($provider_id) && !empty($entry)) {
            $game_providers_ids[] = array(
                'id' => $provider_id,
                'top_rating_page' => $page_id,
            );
        }
    }

    // echo "<pre>" . print_r($game_providers_ids, true) . "</pre>";
    // die();

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $game_providers_field = $casino_main_info_pod->fields('game_providers');

    // Main Info - Game Providers field info.
    $game_providers_field_info = array(
        'pod_id' => $game_providers_field['pod_id'],
        'field_id' => $game_providers_field['id'],
        'item_id' => $casino_main_info_id,
    );

    // Game Providers.
    $game_provider_pod = pods('casino_game_provider');
    $top_rating_page_field = $game_provider_pod->fields('top_rating_page');

    // echo "<pre>" . print_r($game_providers_field_info, true) . "</pre>";

    // Main Info - Game Providers attachment logic.
    if (!empty($game_providers_field_info) && !empty($game_providers_ids)) {
        scg_remove_referenced_data_from_pod($game_providers_field_info);

        $game_providers_data = array();
        foreach ($game_providers_ids as $provider_id) {
            $game_providers_data[] = array(
                'pod_id' => $game_providers_field['pod_id'],
                'field_id' => $game_providers_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $provider_id['id'],
            );
        }

        // echo "<pre>" . print_r($game_providers_data, true) . "</pre>";

        if (!empty($game_providers_data)) {
            scg_add_reference_data_to_pod($game_providers_data);
        }
    }

    // TOP rating page.
    if (!empty($game_providers_ids)) {
        $top_rating_page_data = array();
        foreach ($game_providers_ids as $provider_id) {
            $top_rating_page_field_info = array(
                'pod_id' => $top_rating_page_field['pod_id'],
                'field_id' => $top_rating_page_field['id'],
                'item_id' => $provider_id['id'],
            );

            scg_remove_referenced_data_from_pod($top_rating_page_field_info);

            if (!empty($provider_id['top_rating_page'])) {
                $top_rating_page_data[] = array(
                    'pod_id' => $top_rating_page_field['pod_id'],
                    'field_id' => $top_rating_page_field['id'],
                    'item_id' => $provider_id['id'],
                    'related_item_id' => $provider_id['top_rating_page'],
                );
            }
        }

        if (!empty($top_rating_page_data)) {
            scg_add_reference_data_to_pod($top_rating_page_data);
        }
    }
}


/**
 * @param $arr
 * @return mixed
 *
 * Update uploads folder path.
 */
function scg_update_casino_info_logos_uploads_folder_path($arr)
{
    $folder = "/casinos-logos"; // No trailing slash at the end.

    $arr['path'] = $arr['basedir'] . $folder;
    $arr['url'] = $arr['baseurl'] . $folder;
    $arr['subdir'] = $folder;

    // Create folder if doesn't exist
    $uploads_dir = trailingslashit($arr['basedir']) . "casinos-logos";
    wp_mkdir_p($uploads_dir);

    return $arr;
}


/**
 * Save logo image file.
 */
function scg_save_casino_info_logos_image_file_to_folder($logo)
{
    add_filter('upload_dir', 'scg_update_casino_info_logos_uploads_folder_path');

    // add_filter('upload_dir', 
    //     function($upload_dir_set) use ($user_language) {
    //     $lang_set = $user_language;
    //     return $lang_set;
    // });

    $image_title = $logo['casino_name'] . '-' . 'logo' . '-' . $logo['casino_id'] . '-' . $logo['image_dimensions'];
    $image_name = strtolower(str_replace(' ', '-', $image_title));
    $image_name = strtolower(str_replace('/', '-', $image_name));
    $image_url = $logo['image_url'];
    $full_path = wp_upload_dir()['path'] . '/' . "$image_name.png";
    $image_file_id = $logo['image_file_id'];

    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();

    // Set the scopes required for the API.
    $client->addScope(Google_Service_Drive::DRIVE);

    // Make Drive Service.
    $service = new Google_Service_Drive($client);

    $file = $service->files->get($image_file_id, array(
        'alt' => 'media'));

    $file_data = $file->getBody()->getContents();

    if (!$file_data) {
        return 0;
    }

    $google_file_size = $file->getBody()->getSize();
    $local_file_size = filesize($full_path);

    // Needed to search existing attachments.
    $upload_dir = wp_upload_dir();
    $image_file_path = ltrim($upload_dir['subdir'], '/') . '/' . "$image_name.png";
    $image_file_url = $upload_dir['url'] . '/' . "$image_name.png";
    $attachment_id_raw = attachment_url_to_postid($image_file_url);
    $attachment_id = apply_filters('wpml_object_id', $attachment_id_raw, 'attachment', true);

    // Skip uploading if file size matches.
    if ($google_file_size == $local_file_size) {
        return $attachment_id;
    }

    // Delete file first.
    wp_delete_file($full_path);

    global $wpdb;
    $sql = $wpdb->prepare(
        "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_wp_attached_file' AND meta_value = %s",
        $image_file_path
    );

    $results = $wpdb->get_results($sql);

    // Remove old attachments now to prevent
    // duplicate records with same file.
    if (!empty($results)) {
        foreach ($results as $entry) {
            $post_id = (int)$entry->post_id;

            wp_delete_attachment($post_id, $force_delete = true);
        }
    }

    file_put_contents($full_path, $file_data);

    $stat = stat(dirname($full_path));
    $perms = $stat['mode'] & 0000666;
    @chmod($full_path, $perms);

    $wp_filetype = wp_check_filetype($full_path);

    if (!$wp_filetype['type'] || !$wp_filetype['ext']) {
        return 0;
    }

    $uploads_dir = wp_upload_dir();
    $uploads_url = $uploads_dir['url'];

    // Disable thumbnail generation.
    add_filter('intermediate_image_sizes', '__return_empty_array');

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'guid' => $uploads_url . '/' . $image_name . '.' . $wp_filetype['ext'],
        'post_parent' => null,
        'post_title' => $image_title,
        'post_content' => '',
    );

    // var_dump($attachment);
    // var_dump($game_provider);

    $attachment_id = wp_insert_attachment($attachment, $full_path);

    if (is_wp_error($attachment_id)) {
        return 0;
    }

    require_once ABSPATH . 'wp-admin/includes/media.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';

    wp_update_attachment_metadata($attachment_id, wp_generate_attachment_metadata($attachment_id, $full_path));

    remove_filter('upload_dir', 'scg_update_casino_info_logos_uploads_folder_path');

    // Re-enable thumbnail generation.
    remove_filter('intermediate_image_sizes', '__return_empty_array');

    return $attachment_id;
}


/**
 * Import casino info logo.
 */
function scg_import_casino_info_logo($logo)
{
    $attachment_id = scg_save_casino_info_logos_image_file_to_folder($logo);

    // Add attachment ID to payment method pod.
    if (!empty($attachment_id)) {

        // Get payment method fields info.
        $casino_info_pod = pods('casino_info');
        $logo_field = $casino_info_pod->fields('casino_info_logo');

        $top_rating_logo_field = $casino_info_pod->fields('casino_info_logo');
        $shortcode_logo_field = $casino_info_pod->fields('casino_info_logo_smaller');
        $square_logo_field = $casino_info_pod->fields('casino_info_logo_square');

        // Dynamically use certain field
        // based on image dimensions argument.
        if ($logo['image_dimensions'] == '470x246') {
            $logo_field = $top_rating_logo_field;
        } elseif ($logo['image_dimensions'] == '370x128') {
            $logo_field = $shortcode_logo_field;
        } elseif ($logo['image_dimensions'] == '50x50') {
            $logo_field = $square_logo_field;
        }

        // Logo stuff.
        $logo_field_info = array(
            'pod_id' => $logo_field['pod_id'],
            'field_id' => $logo_field['id'],
            'item_id' => $logo['casino_info_id'],
        );

        if (!empty($logo_field_info) && !empty($logo['casino_info_id'])) {
            scg_remove_referenced_data_from_pod($logo_field_info);

            $logo_data = array();
            $logo_data[] = array(
                'pod_id' => $logo_field['pod_id'],
                'field_id' => $logo_field['id'],
                'item_id' => $logo['casino_info_id'],
                'related_item_id' => $attachment_id,
            );

            if (!empty($logo_data)) {
                scg_add_reference_data_to_pod($logo_data);
            }
        }

    }
}

add_action('scg_batch_import_casino_info_logo', 'scg_import_casino_info_logo', 10, 1);


function scg_match_google_drive_image_url($url) {
    // Parse ID from Google Drive URL.
    // Match two patterns:
    // ..drive.com/open?id=1MXSv8SzKREFA1OfJqjx3dDiHq6MJltfB
    // ..drive.com/file/d/1cSFtiY2LLU_3Aoo1zyzkXPpQcrX_sz0y/view?usp=sharing
    $pattern = '/^(https:\/\/drive\.google\.com\/)(file\/d\/|open\?id=)([^\/]+)/';
    preg_match($pattern, $url, $matches);
    $image_file_id = !empty($matches[3]) ? $matches[3] : FALSE;
    $image_url_final = !empty($image_file_id) ? 'https://drive.google.com/uc?id=' . $image_file_id : FALSE;

    return array(
        'image_url'     => $image_url_final,
        'image_file_id' => $image_file_id,
    );
}


/**
 * Import casino main info.
 */
function scg_casino_trackers_import_main_info($casino_main_info_data)
{
    $casino_name = $casino_main_info_data;
    $data = get_transient('scg-main-infos-import-data-' . $casino_name);
    $casino_main_info_data = !empty($data) ? $data : $casino_main_info_data;

    global $wpdb;

    $casino_name = $casino_main_info_data['Casino/Bookmaker'];
    $casino_id = scg_get_casino_id_from_name($casino_name);

    // echo "<pre>" . print_r($casino_name, true) . "</pre>";
    // echo "<pre>" . print_r($casino_id, true) . "</pre>";
    // echo "<pre>" . print_r($casino_main_info_data['CG Casino'], true) . "</pre>";

    if (Guru_Trackers_IS_BINGO) {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($casino_main_info_data['SBIG Bingo'] != 'Taip' || empty($casino_id)) {
            return;
        }
    } else {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($casino_main_info_data['CG Casino'] != 'Taip' || empty($casino_id)) {
            return;
        }
    }

    // echo "<pre>" . print_r($casino_main_info_data, true) . "</pre>";
    // die();

    // var_dump($casino_main_info_data);

    // $casino_matches = scg_check_if_casino_and_id_matches($casino_id, $casino_name);

    // if (!$casino_matches) {
    //     return FALSE;
    // }

    $owner_name = trim($casino_main_info_data['Owner (company name)']);
    $established_date = trim($casino_main_info_data['Casino/Bookmaker Established year']);
    $vpn_allowed = trim($casino_main_info_data['VPN Allowed']) == 'Y';
    $minimum_deposit = !empty($casino_main_info_data['Minimum Deposit']) ? $casino_main_info_data['Minimum Deposit'] : 0;
    $withdrawal_limit_month = isset($casino_main_info_data['Withdrawal Limit/Month']) ? $casino_main_info_data['Withdrawal Limit/Month'] : 0;
    $avg_visitors_similar = trim($casino_main_info_data['AVG visitors/month (SimilarWeb.com)']);
    // $avg_visitors_ahrefs = trim($casino_main_info_data['AVG visitors/month - Ahrefs.com']);
    $avg_visitors_final = !empty($avg_visitors_similar) ? scg_convert_shortened_text_to_number($avg_visitors_similar) : FALSE;
    $desktop_friendly = TRUE;
    $tablet_friendly = trim($casino_main_info_data['Tablet friendly']) == 'Yes';
    $mobile_friendly = trim($casino_main_info_data['Mobile friendly']) == 'Yes';
    $mobile_app = trim($casino_main_info_data['App']) == 'Yes';

    $casino_country_raw = trim($casino_main_info_data['Casino/Bookmaker Office']);
    $casino_country = $casino_country_raw == 'Unknown' || $casino_country_raw == 'unknown' || $casino_country_raw == 'Unknown' || $casino_country_raw == 'unknown' ? 'All Countries' : $casino_country_raw;
    $casino_country_id = (int)scg_get_country_id_from_name($casino_country);
    $main_currency = trim($casino_main_info_data['Main Currency']);
    $main_currency_id = (int)scg_get_currency_id_from_currency_name($main_currency);

    $top_5_countries_raw = trim($casino_main_info_data['TOP 5 Countries (SimilarWeb.com)']);
    $top_5_countries = explode(';', $top_5_countries_raw);

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $country_field = $casino_main_info_pod->fields('country');
    $currency_field = $casino_main_info_pod->fields('currency');
    $top_5_countries_field = $casino_main_info_pod->fields('top_countries');
    $website_languages_field = $casino_main_info_pod->fields('websites_languages');
    $chat_languages_field = $casino_main_info_pod->fields('chat_languages');
    $email_languages_field = $casino_main_info_pod->fields('email_support_languages');

    // var_dump($casino_country);
    // var_dump($casino_country_id);
    // var_dump($main_currency);
    // var_dump($main_currency_id);

    // Country field - setting data.
    if (!empty($country_field) && !empty($casino_country_id)) {
        $casino_country_field_info = array(
            'pod_id' => $country_field['pod_id'],
            'field_id' => $country_field['id'],
            'item_id' => $casino_main_info_id,
            'related_item_id' => $casino_country_id,
        );

        scg_remove_referenced_data_from_pod($casino_country_field_info);

        $casino_country_data = array();
        $casino_country_data[] = $casino_country_field_info;

        // Add country field data.
        scg_add_reference_data_to_pod($casino_country_data);
    }

    // Currency field - setting data.
    if (!empty($currency_field) && !empty($main_currency_id)) {
        $main_currency_field_info = array(
            'pod_id' => $currency_field['pod_id'],
            'field_id' => $currency_field['id'],
            'item_id' => $casino_main_info_id,
            'related_item_id' => $main_currency_id,
        );

        scg_remove_referenced_data_from_pod($main_currency_field_info);

        $casino_currency_data = array();
        $casino_currency_data[] = $main_currency_field_info;

        // Add currency field data.
        scg_add_reference_data_to_pod($casino_currency_data);
    }

    $website_languages = $casino_main_info_data['Website languages'];
    $chat_languages = $casino_main_info_data['Chat languages'];
    $email_languages = $casino_main_info_data['Email languages'];


    $table_name = $wpdb->prefix . 'pods_casino_main_info';

    if (Guru_Trackers_IS_BINGO) {
        $video_review_link = trim($casino_main_info_data['SBIG Video Review Link']);
    } else {
        $video_review_link = trim($casino_main_info_data['CG Video Review Link']);
    }

    $background_color = trim($casino_main_info_data['Promo Block Background Color']);

    // Logos related.
    $top_rating_logo_link   = trim($casino_main_info_data['TOP Rating Logo (470x246) link']);
    $shortcode_logo_link    = trim($casino_main_info_data['Short code Logo (370x128) link']);
    $square_logo_link       = trim($casino_main_info_data['Square Logo (50x50) link']);

    // Buttons color related.
    $top_rating_button_bg_color     = trim($casino_main_info_data['TOP Rating button Background Color']);
    $top_rating_button_text_color   = trim($casino_main_info_data['TOP Rating button Text Color']);

    // Casino info related.
    $casino_info_id = scg_casino_trackers_get_casino_info_id($casino_id, $language_code = 'en');

    // TOP rating logo data.
    $top_rating_image_data = scg_match_google_drive_image_url($top_rating_logo_link);
    $top_rating_image_file_id = !empty($top_rating_image_data) ? $top_rating_image_data['image_file_id'] : FALSE;
    $top_rating_image_file_url = !empty($top_rating_image_data) ? $top_rating_image_data['image_url'] : FALSE;

    // Shortcode rating logo data.
    $shortcode_image_data = scg_match_google_drive_image_url($shortcode_logo_link);
    $shortcode_image_file_id = !empty($shortcode_image_data) ? $shortcode_image_data['image_file_id'] : FALSE;
    $shortcode_image_file_url = !empty($shortcode_image_data) ? $shortcode_image_data['image_url'] : FALSE;

    // Square logo data.
    $square_image_data = scg_match_google_drive_image_url($square_logo_link);
    $square_image_file_id = !empty($square_image_data) ? $square_image_data['image_file_id'] : FALSE;
    $square_image_file_url = !empty($square_image_data) ? $square_image_data['image_url'] : FALSE;

    // echo "<pre>" . print_r($top_rating_logo_link, true) . "</pre>";
    // echo "<pre>" . print_r($top_rating_image_data, true) . "</pre>";
    // echo "<pre>" . print_r($top_rating_image_file_url, true) . "</pre>";

    $logo_data = array();
    
    if (!empty($top_rating_image_file_id) && !empty($top_rating_image_file_url) && !empty($casino_info_id)) {
        $logo_data[] = array(
            'casino_name' => $casino_name,
            'casino_id' => $casino_id,
            'casino_info_id' => $casino_info_id,
            'image_file_id' => $top_rating_image_file_id,
            'image_url' => $top_rating_image_file_url,
            'image_dimensions' => '470x246',
        );
    }

    if (!empty($shortcode_image_file_id) && !empty($shortcode_image_file_url) && !empty($casino_info_id)) {
        $logo_data[] = array(
            'casino_name' => $casino_name,
            'casino_id' => $casino_id,
            'casino_info_id' => $casino_info_id,
            'image_file_id' => $shortcode_image_file_id,
            'image_url' => $shortcode_image_file_url,
            'image_dimensions' => '370x128',
        );
    }

    if (!empty($square_image_file_id) && !empty($square_image_file_url) && !empty($casino_info_id)) {
        $logo_data[] = array(
            'casino_name' => $casino_name,
            'casino_id' => $casino_id,
            'casino_info_id' => $casino_info_id,
            'image_file_id' => $square_image_file_id,
            'image_url' => $square_image_file_url,
            'image_dimensions' => '50x50',
        );
    }

    // Schedule casino info
    // logos import.
    if (!empty($logo_data)) {
        $count = 0;
        foreach ($logo_data as $logo) {
            // Create a "schedule action" for the data
            as_schedule_single_action(
                time() - 100,
                'scg_batch_import_casino_info_logo',
                array('logo' => $logo),
                'all_info_data'
            );

            $count++;
        }
    }

    $data_update = array(
        'owner_name' => $owner_name,
        'established_date' => $established_date,
        'vpn_allowed' => $vpn_allowed,
        'minimum_deposit' => $minimum_deposit,
        'withdrawal_limit_month' => $withdrawal_limit_month,
        'average_visitors' => $avg_visitors_final,
        'desktop_friendly' => $desktop_friendly,
        'tablet_friendly' => $tablet_friendly,
        'mobile_friendly' => $mobile_friendly,
        'mobile_app' => $mobile_app,
        'video_review_link' => $video_review_link,
        'background_color' => $background_color,
    );

    $main_rating = trim($casino_main_info_data['Main Rating']);
    $main_rating_id = !empty($main_rating) ? (int) scg_get_casino_game_type_id_from_name($main_rating) : FALSE;

    $data_where = array('casino_id' => $casino_id);
    $wpdb->update($table_name, $data_update, $data_where);

    // Update Casino Info color fields.
    $data_update = array(
        'casino_info_color'             => $top_rating_button_bg_color,
        'casino_info_button_text_color' => $top_rating_button_text_color,
    );
    $data_where = array('casino_id' => $casino_id);
    $wpdb->update($table_name = 'wp_pods_casino_info', $data_update, $data_where);

    $top_5_countries_field_id = $top_5_countries_field['id'];
    $website_languages_field_id = $website_languages_field['id'];
    $chat_languages_field_id = $chat_languages_field['id'];
    $email_languages_field_id = $email_languages_field['id'];

    $main_rating_field = $casino_main_info_pod->fields('main_rating');

    // Main Rating field - setting data.
    if (!empty($main_rating_field) && !empty($main_rating_id)) {
        $main_rating_field_info = array(
            'pod_id' => $main_rating_field['pod_id'],
            'field_id' => $main_rating_field['id'],
            'item_id' => $casino_main_info_id,
            'related_item_id' => $main_rating_id,
        );

        scg_remove_referenced_data_from_pod($main_rating_field_info);

        $main_rating_data = array();
        $main_rating_data[] = $main_rating_field_info;

        // error_log('<pre>' . print_r($main_rating_data, TRUE) . '</pre>');

        // Add main rating field data.
        scg_add_reference_data_to_pod($main_rating_data);
    }

    $field_names = array(
        'top_countries',
        'websites_languages',
        'chat_languages',
        'email_support_languages',
    );

    $top_5_countries_field_info = array(
        'pod_id' => $top_5_countries_field['pod_id'],
        'field_id' => $top_5_countries_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($top_5_countries)) {
        $top_5_country_ids = array();
        foreach ($top_5_countries as $country_name) {
            $country_name = trim($country_name);
            $country_id = scg_get_country_id_from_name($country_name);

            $top_5_country_ids[] = (int)$country_id;
        }
    }

    if (!empty($top_5_countries_field_info) && !empty($top_5_country_ids)) {
        scg_remove_referenced_data_from_pod($top_5_countries_field_info);

        $top_countries_data = array();
        foreach ($top_5_country_ids as $country_id) {
            $top_countries_data[] = array(
                'pod_id' => $top_5_countries_field['pod_id'],
                'field_id' => $top_5_countries_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $country_id,
            );
        }

        if (!empty($top_countries_data)) {
            scg_add_reference_data_to_pod($top_countries_data);
        }
    }


    // Website languages.
    $website_languages_field_info = array(
        'pod_id' => $website_languages_field['pod_id'],
        'field_id' => $website_languages_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($website_languages)) {
        $website_languages_ids = array();
        foreach ($website_languages as $language) {
            $language_id = scg_get_language_id_from_name($language);

            $website_languages_ids[] = (int)$language_id;
        }
    }

    if (!empty($website_languages_field_info) && !empty($website_languages_ids)) {
        scg_remove_referenced_data_from_pod($website_languages_field_info);

        $website_languages_data = array();
        foreach ($website_languages_ids as $language_id) {
            $website_languages_data[] = array(
                'pod_id' => $website_languages_field['pod_id'],
                'field_id' => $website_languages_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $language_id,
            );
        }

        if (!empty($website_languages_data)) {
            scg_add_reference_data_to_pod($website_languages_data);
        }
    }


    // Chat languages.
    $chat_languages_field_info = array(
        'pod_id' => $chat_languages_field['pod_id'],
        'field_id' => $chat_languages_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($chat_languages)) {
        $chat_languages_ids = array();
        foreach ($chat_languages as $language) {
            $language_id = scg_get_language_id_from_name($language);

            $chat_languages_ids[] = (int)$language_id;
        }
    }

    if (!empty($chat_languages_field_info) && !empty($chat_languages_ids)) {
        scg_remove_referenced_data_from_pod($chat_languages_field_info);

        $chat_languages_data = array();
        foreach ($chat_languages_ids as $language_id) {
            $chat_languages_data[] = array(
                'pod_id' => $chat_languages_field['pod_id'],
                'field_id' => $chat_languages_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $language_id,
            );
        }

        if (!empty($chat_languages_data)) {
            scg_add_reference_data_to_pod($chat_languages_data);
        }
    }


    // Email languages.
    $email_languages_field_info = array(
        'pod_id' => $email_languages_field['pod_id'],
        'field_id' => $email_languages_field['id'],
        'item_id' => $casino_main_info_id,
    );

    if (!empty($email_languages)) {
        $email_languages_ids = array();
        foreach ($email_languages as $language) {
            $language_id = scg_get_language_id_from_name($language);

            $email_languages_ids[] = (int)$language_id;
        }
    }

    if (!empty($email_languages_field_info) && !empty($email_languages_ids)) {
        scg_remove_referenced_data_from_pod($email_languages_field_info);

        $email_languages_data = array();
        foreach ($email_languages_ids as $language_id) {
            $email_languages_data[] = array(
                'pod_id' => $email_languages_field['pod_id'],
                'field_id' => $email_languages_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $language_id,
            );
        }

        if (!empty($email_languages_data)) {
            scg_add_reference_data_to_pod($email_languages_data);
        }
    }

    // Products
    $data_array_name = 'Products';
    $pod_name = 'product';
    $field_name = 'products';
    $products = $casino_main_info_data;
    scg_casino_trackers_import_casino_data($casino_main_info_data, $data_array_name, $pod_name, $field_name);
}

/**
 * Import licenses.
 */
function scg_casino_trackers_import_licenses($data)
{
    $casino_name = $data;
    $casino_id = scg_get_casino_id_from_name($casino_name);

    $data_info = get_transient('scg-main-infos-import-data-licenses-' . $data);
    $data = !empty($data_info) ? $data_info : $data;

    global $wpdb;

    // echo "<pre>" . print_r($data, true) . "</pre>";

    if (Guru_Trackers_IS_BINGO) {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['SBIG Bingo'] != 'Taip' || empty($casino_id)) {
            return;
        }
    } else {
        // SCG Casino has to be 'Taip' or Casino ID has to be not empty.
        if ($data['CG Casino'] != 'Taip' || empty($casino_id)) {
            return;
        }
    }

    // var_dump($casino_main_info_data);
    // $casino_matches = scg_check_if_casino_and_id_matches($casino_id, $casino_name);

    // if (!$casino_matches) {
    //     return FALSE;
    // }


    $licenses = scg_casino_trackers_parse_casinos_all_info_csv_file('Licenses');
    $licenses_urls = FALSE;

    if (!empty($licenses)) {
        foreach ($licenses as $entry) {
            if ($entry['sheet_name'] == 'Licenses' && !empty($entry['data'])) {
                foreach ($entry['data'] as $casino_all_info) {

                    if (Guru_Trackers_IS_BINGO) {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'SBIG Bingo rating link') !== FALSE) {
                            $licenses_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    } else {
                        if (strpos($casino_all_info['Casino/Bookmaker'], 'CG rating link') !== FALSE) {
                            $licenses_urls = $casino_all_info;

                            // Stop foreaching if found.
                            break;
                        }
                    }

                }
            }
        }
    }

    // echo "<pre>" . print_r($licenses_urls, true) . "</pre>";

    // Get main info fields info.
    $casino_main_info_id = (int)$wpdb->get_var("SELECT id FROM {$wpdb->prefix}pods_casino_main_info WHERE casino_id = $casino_id");

    $casino_main_info_pod = pods('casino_main_info');
    $license_field = $casino_main_info_pod->fields('licenses');

    $licenses_array = $data;

    unset($licenses_array['Casino/Bookmaker']);
    unset($licenses_array['SCG Casino']);
    unset($licenses_array['CG Casino']);
    unset($licenses_array['SBG Bookmaker']);
    unset($licenses_array['LG Bookmaker']);
    unset($licenses_array['SBIG Bingo']);
    
    $licenses_tmp = array();

    if (!empty($licenses_array)) {
        $i = 0;
        foreach ($licenses_array as $key => $license) {
            // URL.
            $full_url = !empty($licenses_urls) ? $licenses_urls[$key] : FALSE;
            $page_id = scg_casino_trackers_get_top_rating_page_id_from_url($full_url);

            $licenses_tmp[$i] = array(
                'name' => $key,
                'top_rating_page' => $page_id,
            );

            $i++;
        }
    }

    // echo "<pre>" . print_r($licenses_tmp, true) . "</pre>";
    // die();

    // Licenses.
    $license_field_info = array(
        'pod_id' => $license_field['pod_id'],
        'field_id' => $license_field['id'],
        'item_id' => $casino_main_info_id,
    );

    // License Pod.
    $license_pod = pods('license');
    $top_rating_page_field = $license_pod->fields('top_rating_page');

    if (!empty($licenses_tmp)) {
        $license_ids = array();
        foreach ($licenses_tmp as $license) {
            $license_id = scg_get_or_create_license_id_from_name($license['name']);

            $license_ids[] = array(
                'id' => (int) $license_id,
                'top_rating_page' => $license['top_rating_page'],
            );
        }
    }

    // var_dump($license_ids);

    if (!empty($license_field_info) && !empty($license_ids)) {
        scg_remove_referenced_data_from_pod($license_field_info);

        $licenses_data = array();
        foreach ($license_ids as $license_id) {
            $licenses_data[] = array(
                'pod_id' => $license_field['pod_id'],
                'field_id' => $license_field['id'],
                'item_id' => $casino_main_info_id,
                'related_item_id' => $license_id['id'],
            );
        }

        if (!empty($licenses_data)) {
            scg_add_reference_data_to_pod($licenses_data);
        }
    }

    // TOP rating page.
    if (!empty($license_ids)) {
        $top_rating_page_data = array();
        foreach ($license_ids as $license_id) {
            $top_rating_page_field_info = array(
                'pod_id' => $top_rating_page_field['pod_id'],
                'field_id' => $top_rating_page_field['id'],
                'item_id' => $license_id['id'],
            );

            scg_remove_referenced_data_from_pod($top_rating_page_field_info);

            if (!empty($license_id['top_rating_page'])) {
                $top_rating_page_data[] = array(
                    'pod_id' => $top_rating_page_field['pod_id'],
                    'field_id' => $top_rating_page_field['id'],
                    'item_id' => $license_id['id'],
                    'related_item_id' => $license_id['top_rating_page'],
                );
            }
        }

        if (!empty($top_rating_page_data)) {
            scg_add_reference_data_to_pod($top_rating_page_data);
        }
    }
}


/**
 * Import payment method logos.
 */
function scg_casino_trackers_import_payment_method_logos($casino_all_info)
{
    $payment_methods_tmp = $casino_all_info;

    unset($payment_methods_tmp['Casino/Bookmaker']);
    unset($payment_methods_tmp['SCG Casino']);
    unset($payment_methods_tmp['CG Casino']);
    unset($payment_methods_tmp['SBG Bookmaker']);
    unset($payment_methods_tmp['LG Bookmaker']);
    unset($payment_methods_tmp['SBIG Bingo']);

    $payment_methods = array();
    $is_cryptos = FALSE;

    foreach ($payment_methods_tmp as $key => $method) {
        if (empty($method) || strpos($method, 'gyazo') !== FALSE) {
            unset($payment_methods_tmp[$key]);
        }

        if ($key == 'Cryptos') {
            $is_cryptos = TRUE;
        }

        if (!empty($is_cryptos)) {
            continue;
        }

        // Parse ID from Google Drive URL.
        // Match two patterns:
        // ..drive.com/open?id=1MXSv8SzKREFA1OfJqjx3dDiHq6MJltfB
        // ..drive.com/file/d/1cSFtiY2LLU_3Aoo1zyzkXPpQcrX_sz0y/view?usp=sharing
        $pattern = '/^(https:\/\/drive\.google\.com\/)(file\/d\/|open\?id=)([^\/]+)/';
        preg_match($pattern, $method, $method_matches);
        $method_image_file_id = !empty($method_matches[3]) ? $method_matches[3] : FALSE;
        $method_image_url_final = !empty($method_image_file_id) ? 'https://drive.google.com/uc?id=' . $method_image_file_id : FALSE;
        $payment_method_id = scg_get_payment_method_id_from_name($key);

        $payment_methods[] = array(
            'name' => $key,
            'id' => $payment_method_id,
            'image_url' => $method_image_url_final,
            'image_file_id' => $method_image_file_id,
        );
    }

    // var_dump($payment_methods_tmp);
    // var_dump($payment_methods);

    if (!empty($payment_methods)) {
        $count = 0;
        foreach ($payment_methods as $payment_method) {
            // if ($count > 20) {
            //   break;
            // }

            if (!empty($payment_method['id']) && !empty($payment_method['image_file_id'])) {
                // Create a "schedule action" for the data
                as_schedule_single_action(
                    time(),
                    'scg_batch_import_payment_method_logo',
                    array('payment_method' => $payment_method),
                    'all_info_data'
                );
            }

            $count++;
        }
    }
}


/**
 * Import crypto logos.
 */
function scg_casino_trackers_import_cryptos_logos($casino_all_info)
{
    $cryptos_tmp = $casino_all_info;

    unset($cryptos_tmp['Casino/Bookmaker']);
    unset($cryptos_tmp['SCG Casino']);
    unset($cryptos_tmp['CG Casino']);
    unset($cryptos_tmp['SBG Bookmaker']);
    unset($cryptos_tmp['LG Bookmaker']);
    unset($cryptos_tmp['SBIG Bingo']);

    $cryptos = array();
    $is_cryptos = FALSE;

    foreach ($cryptos_tmp as $key => $method) {
        if (empty($method) || strpos($method, 'gyazo') !== FALSE) {
            unset($cryptos_tmp[$key]);
        }

        if ($key == 'Cryptos') {
            $is_cryptos = TRUE;
        }

        if (empty($is_cryptos)) {
            continue;
        }

        // Parse ID from Google Drive URL.
        // Match two patterns:
        // ..drive.com/open?id=1MXSv8SzKREFA1OfJqjx3dDiHq6MJltfB
        // ..drive.com/file/d/1cSFtiY2LLU_3Aoo1zyzkXPpQcrX_sz0y/view?usp=sharing
        $pattern = '/^(https:\/\/drive\.google\.com\/)(file\/d\/|open\?id=)([^\/]+)/';
        preg_match($pattern, $method, $method_matches);
        $method_image_file_id = !empty($method_matches[3]) ? $method_matches[3] : FALSE;

        $method_image_url_final = !empty($method_image_file_id) ? 'https://drive.google.com/uc?id=' . $method_image_file_id : FALSE;
        $crypto_id = scg_get_crypto_id_from_name($key);

        $cryptos[] = array(
            'name' => $key,
            'id' => $crypto_id,
            'image_url' => $method_image_url_final,
            'image_file_id' => $method_image_file_id,
        );
    }

    // var_dump($payment_methods_tmp);
    // var_dump($cryptos);

    if (!empty($cryptos)) {
        $count = 0;
        foreach ($cryptos as $crypto) {
            // if ($count > 5) {
            //   break;
            // }

            if (!empty($crypto['id']) && !empty($crypto['image_file_id'])) {
                // Create a "schedule action" for the data
                as_schedule_single_action(
                    time(),
                    'scg_batch_import_crypto_logo',
                    array('crypto' => $crypto),
                    'all_info_data'
                );
            }

            $count++;
        }
    }
}


/**
 * Import game provider logos.
 */
function scg_casino_trackers_import_game_provider_logos($casino_all_info)
{
    $game_providers_tmp = $casino_all_info;

    unset($game_providers_tmp['Casino/Bookmaker']);
    unset($game_providers_tmp['SCG Casino']);
    unset($game_providers_tmp['CG Casino']);
    unset($game_providers_tmp['SBG Bookmaker']);
    unset($game_providers_tmp['LG Bookmaker']);
    unset($game_providers_tmp['SBIG Bingo']);

    $game_providers = array();
    $is_cryptos = FALSE;

    foreach ($game_providers_tmp as $key => $method) {
        if (empty($method) || strpos($method, 'gyazo') !== FALSE) {
            unset($game_providers_tmp[$key]);
        }

        // Parse ID from Google Drive URL.
        // Match two patterns:
        // ..drive.com/open?id=1MXSv8SzKREFA1OfJqjx3dDiHq6MJltfB
        // ..drive.com/file/d/1cSFtiY2LLU_3Aoo1zyzkXPpQcrX_sz0y/view?usp=sharing
        $pattern = '/^(https:\/\/drive\.google\.com\/)(file\/d\/|open\?id=)([^\/]+)/';
        preg_match($pattern, $method, $method_matches);
        $method_image_file_id = !empty($method_matches[3]) ? $method_matches[3] : FALSE;

        // $method_image_file_id = $method_image['id'];
        $method_image_url_final = !empty($method_image_file_id) ? 'https://drive.google.com/uc?id=' . $method_image_file_id : FALSE;
        $provider_id = scg_get_game_provider_id_from_name($key);

        $game_providers[] = array(
            'name' => $key,
            'id' => $provider_id,
            'image_url' => $method_image_url_final,
            'image_file_id' => $method_image_file_id,
        );
    }

    // error_log('<pre>' . print_r($game_providers, TRUE) . '</pre>');

    if (!empty($game_providers)) {
        $count = 0;
        foreach ($game_providers as $game_provider) {
            // if ($count > 1) {
            //   break;
            // }

            if (!empty($game_provider['id']) && !empty($game_provider['image_file_id'])) {
                // Create a "schedule action" for the data
                as_schedule_single_action(
                    time(),
                    'scg_batch_import_game_provider_logo',
                    array('game_provider' => $game_provider),
                    'all_info_data'
                );
            }

            $count++;
        }
    }
}


/**
 * Import payment method logo.
 */
function scg_import_payment_method_logo($payment_method)
{
    $attachment_id = scg_save_image_file_to_folder($payment_method);

    // Add attachment ID to payment method pod.
    if (!empty($attachment_id)) {

        // Get payment method fields info.
        $payment_method_pod = pods('payment_method');
        $logo_field = $payment_method_pod->fields('logo');

        // Licenses.
        $logo_field_info = array(
            'pod_id' => $logo_field['pod_id'],
            'field_id' => $logo_field['id'],
            'item_id' => $payment_method['id'],
        );

        if (!empty($logo_field_info) && !empty($payment_method['id'])) {
            scg_remove_referenced_data_from_pod($logo_field_info);

            $logo_data = array();
            $logo_data[] = array(
                'pod_id' => $logo_field['pod_id'],
                'field_id' => $logo_field['id'],
                'item_id' => $payment_method['id'],
                'related_item_id' => $attachment_id,
            );

            if (!empty($logo_data)) {
                scg_add_reference_data_to_pod($logo_data);
            }
        }

    }
}

add_action('scg_batch_import_payment_method_logo', 'scg_import_payment_method_logo', 10, 1);


/**
 * Import game provider logo.
 */
function scg_import_game_provider_logo($game_provider)
{
    $attachment_id = scg_save_game_provider_image_file_to_folder($game_provider);

    // Add attachment ID to payment method pod.
    if (!empty($attachment_id)) {

        // Get payment method fields info.
        $game_provider_pod = pods('casino_game_provider');
        $logo_field = $game_provider_pod->fields('logo');

        // Licenses.
        $logo_field_info = array(
            'pod_id' => $logo_field['pod_id'],
            'field_id' => $logo_field['id'],
            'item_id' => $game_provider['id'],
        );

        if (!empty($logo_field_info) && !empty($game_provider['id'])) {
            scg_remove_referenced_data_from_pod($logo_field_info);

            $logo_data = array();
            $logo_data[] = array(
                'pod_id' => $logo_field['pod_id'],
                'field_id' => $logo_field['id'],
                'item_id' => $game_provider['id'],
                'related_item_id' => $attachment_id,
            );

            if (!empty($logo_data)) {
                scg_add_reference_data_to_pod($logo_data);
            }
        }

    }
}

add_action('scg_batch_import_game_provider_logo', 'scg_import_game_provider_logo', 10, 1);


/**
 * Import crypto logo.
 */
function scg_import_crypto_logo($crypto)
{
    $attachment_id = scg_save_crypto_image_file_to_folder($crypto);

    // echo "<pre>" . print_r($attachment_id, true) . "</pre>";

    // Add attachment ID to payment method pod.
    if (!empty($attachment_id)) {

        // Get payment method fields info.
        $crypto_pod = pods('cryptocurrency');
        $logo_field = $crypto_pod->fields('logo');

        // Licenses.
        $logo_field_info = array(
            'pod_id' => $logo_field['pod_id'],
            'field_id' => $logo_field['id'],
            'item_id' => $crypto['id'],
        );


        // echo "<pre>" . print_r($logo_field_info, true) . "</pre>";

        if (!empty($logo_field_info) && !empty($crypto['id'])) {
            scg_remove_referenced_data_from_pod($logo_field_info);

            $logo_data = array();
            $logo_data[] = array(
                'pod_id' => $logo_field['pod_id'],
                'field_id' => $logo_field['id'],
                'item_id' => $crypto['id'],
                'related_item_id' => $attachment_id,
            );

            // echo "<pre>" . print_r($logo_data, true) . "</pre>";

            if (!empty($logo_data)) {
                scg_add_reference_data_to_pod($logo_data);
            }
        }

    }
}

add_action('scg_batch_import_crypto_logo', 'scg_import_crypto_logo', 10, 1);


/**
 * Save payment method image file.
 */
function scg_save_image_file_to_folder($payment_method)
{
    add_filter('upload_dir', 'scg_update_methods_uploads_folder_path');

    $image_title = $payment_method['name'];
    $image_name = strtolower(str_replace(' ', '-', $payment_method['name']));
    $image_name = strtolower(str_replace('/', '-', $image_name));
    $image_url = $payment_method['image_url'];
    $full_path = wp_upload_dir()['path'] . '/' . "$image_name.png";
    $image_file_id = $payment_method['image_file_id'];

    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();

    // Set the scopes required for the API.
    $client->addScope(Google_Service_Drive::DRIVE);

    // Make Drive Service.
    $service = new Google_Service_Drive($client);

    $file = $service->files->get($image_file_id, array(
        'alt' => 'media'));

    $file_data = $file->getBody()->getContents();

    if (!$file_data) {
        return 0;
    }

    $google_file_size = $file->getBody()->getSize();
    $local_file_size = filesize($full_path);

    // Needed to search existing attachments.
    $upload_dir = wp_upload_dir();
    $image_file_path = ltrim($upload_dir['subdir'], '/') . '/' . "$image_name.png";
    $image_file_url = $upload_dir['url'] . '/' . "$image_name.png";
    $attachment_id = attachment_url_to_postid($image_file_url);
    // $attachment_id = apply_filters('wpml_object_id', $attachment_id_raw, 'attachment', true);

    // Skip uploading if file size matches.
    if ($google_file_size == $local_file_size && !empty($attachment_id)) {
        return $attachment_id;
    }

    // Delete file first.
    wp_delete_file($full_path);

    global $wpdb;
    $sql = $wpdb->prepare(
        "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_wp_attached_file' AND meta_value = %s",
        $image_file_path
    );

    $results = $wpdb->get_results($sql);

    // Remove old attachments now to prevent
    // duplicate records with same file.
    if (!empty($results)) {
        foreach ($results as $entry) {
            $post_id = (int)$entry->post_id;

            wp_delete_attachment($post_id, $force_delete = true);
        }
    }

    file_put_contents($full_path, $file_data);

    $stat = stat(dirname($full_path));
    $perms = $stat['mode'] & 0000666;
    @chmod($full_path, $perms);

    $wp_filetype = wp_check_filetype($full_path);

    if (!$wp_filetype['type'] || !$wp_filetype['ext']) {
        return 0;
    }

    $uploads_dir = wp_upload_dir();
    $uploads_url = $uploads_dir['url'];

    // Disable thumbnail generation.
    add_filter('intermediate_image_sizes', '__return_empty_array');

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'guid' => $uploads_url . '/' . $image_name . '.' . $wp_filetype['ext'],
        'post_parent' => null,
        'post_title' => $image_title,
        'post_content' => '',
    );

    // var_dump($attachment);
    // var_dump($payment_method);

    $attachment_id = wp_insert_attachment($attachment, $full_path);

    if (is_wp_error($attachment_id)) {
        return 0;
    }

    require_once ABSPATH . 'wp-admin/includes/media.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';

    wp_update_attachment_metadata($attachment_id, wp_generate_attachment_metadata($attachment_id, $full_path));

    remove_filter('upload_dir', 'scg_update_methods_uploads_folder_path');

    // Re-enable thumbnail generation.
    remove_filter('intermediate_image_sizes', '__return_empty_array');

    return $attachment_id;
}


/**
 * Save crypto image file to folder.
 */
function scg_save_crypto_image_file_to_folder($crypto)
{
    add_filter('upload_dir', 'scg_update_crypto_logos_uploads_folder_path');

    $image_title = $crypto['name'];
    $image_name = strtolower(str_replace(' ', '-', $crypto['name']));
    $image_name = strtolower(str_replace('/', '-', $image_name));
    $image_url = $crypto['image_url'];
    $full_path = wp_upload_dir()['path'] . '/' . "$image_name.png";
    $image_file_id = $crypto['image_file_id'];

    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();

    // Set the scopes required for the API.
    $client->addScope(Google_Service_Drive::DRIVE);

    // Make Drive Service.
    $service = new Google_Service_Drive($client);

    $file = $service->files->get($image_file_id, array(
        'alt' => 'media'));

    $file_data = $file->getBody()->getContents();

    if (!$file_data) {
        return 0;
    }

    $google_file_size = $file->getBody()->getSize();
    $local_file_size = filesize($full_path);

    // Needed to search existing attachments.
    $upload_dir = wp_upload_dir();
    $image_file_path = ltrim($upload_dir['subdir'], '/') . '/' . "$image_name.png";
    $image_file_url = $upload_dir['url'] . '/' . "$image_name.png";
    $attachment_id = attachment_url_to_postid($image_file_url);
    // $attachment_id = apply_filters('wpml_object_id', $attachment_id_raw, 'attachment', true);

    // Skip uploading if file size matches.
    if ($google_file_size == $local_file_size && !empty($attachment_id)) {
        return $attachment_id;
    }

    // Delete file first.
    wp_delete_file($full_path);

    global $wpdb;
    $sql = $wpdb->prepare(
        "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_wp_attached_file' AND meta_value = %s",
        $image_file_path
    );

    $results = $wpdb->get_results($sql);

    // Remove old attachments now to prevent
    // duplicate records with same file.
    if (!empty($results)) {
        foreach ($results as $entry) {
            $attachment_id = (int)$entry->post_id;

            wp_delete_attachment($attachment_id, $force_delete = true);
        }
    }

    file_put_contents($full_path, $file_data);

    $stat = stat(dirname($full_path));
    $perms = $stat['mode'] & 0000666;
    @chmod($full_path, $perms);

    $wp_filetype = wp_check_filetype($full_path);

    if (!$wp_filetype['type'] || !$wp_filetype['ext']) {
        return 0;
    }

    $uploads_dir = wp_upload_dir();
    $uploads_url = $uploads_dir['url'];

    // Disable thumbnail generation.
    add_filter('intermediate_image_sizes', '__return_empty_array');

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'guid' => $uploads_url . '/' . $image_name . '.' . $wp_filetype['ext'],
        'post_parent' => null,
        'post_title' => $image_title,
        'post_content' => '',
    );

    // var_dump($attachment);
    // var_dump($crypto);

    $attachment_id = wp_insert_attachment($attachment, $full_path);

    if (is_wp_error($attachment_id)) {
        return 0;
    }

    require_once ABSPATH . 'wp-admin/includes/media.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';

    wp_update_attachment_metadata($attachment_id, wp_generate_attachment_metadata($attachment_id, $full_path));

    remove_filter('upload_dir', 'scg_update_crypto_logos_uploads_folder_path');

    // Re-enable thumbnail generation.
    remove_filter('intermediate_image_sizes', '__return_empty_array');

    return $attachment_id;
}


/**
 * Save game provider image file.
 */
function scg_save_game_provider_image_file_to_folder($game_provider)
{
    add_filter('upload_dir', 'scg_update_game_provider_logos_uploads_folder_path');

    $image_title = $game_provider['name'];
    $image_name = strtolower(str_replace(' ', '-', $game_provider['name']));
    $image_name = strtolower(str_replace('/', '-', $image_name));
    $image_url = $game_provider['image_url'];
    $full_path = wp_upload_dir()['path'] . '/' . "$image_name.png";
    $image_file_id = $game_provider['image_file_id'];

    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();

    // Set the scopes required for the API.
    $client->addScope(Google_Service_Drive::DRIVE);

    // Make Drive Service.
    $service = new Google_Service_Drive($client);

    $file = $service->files->get($image_file_id, array(
        'alt' => 'media'));

    $file_data = $file->getBody()->getContents();

    if (!$file_data) {
        return 0;
    }

    $google_file_size = $file->getBody()->getSize();
    $local_file_size = filesize($full_path);

    // Needed to search existing attachments.
    $upload_dir = wp_upload_dir();
    $image_file_path = ltrim($upload_dir['subdir'], '/') . '/' . "$image_name.png";
    $image_file_url = $upload_dir['url'] . '/' . "$image_name.png";
    $attachment_id = attachment_url_to_postid($image_file_url);
    // $attachment_id = apply_filters('wpml_object_id', $attachment_id_raw, 'attachment', true);

    // Skip uploading if file size matches.
    if ($google_file_size == $local_file_size && !empty($attachment_id)) {
        return $attachment_id;
    }

    // Delete file first.
    wp_delete_file($full_path);

    global $wpdb;
    $sql = $wpdb->prepare(
        "SELECT post_id, meta_value FROM $wpdb->postmeta WHERE meta_key = '_wp_attached_file' AND meta_value = %s",
        $image_file_path
    );

    $results = $wpdb->get_results($sql);

    // Remove old attachments now to prevent
    // duplicate records with same file.
    if (!empty($results)) {
        foreach ($results as $entry) {
            $post_id = (int)$entry->post_id;

            wp_delete_attachment($post_id, $force_delete = true);
        }
    }

    file_put_contents($full_path, $file_data);

    $stat = stat(dirname($full_path));
    $perms = $stat['mode'] & 0000666;
    @chmod($full_path, $perms);

    $wp_filetype = wp_check_filetype($full_path);

    if (!$wp_filetype['type'] || !$wp_filetype['ext']) {
        return 0;
    }

    $uploads_dir = wp_upload_dir();
    $uploads_url = $uploads_dir['url'];

    // Disable thumbnail generation.
    add_filter('intermediate_image_sizes', '__return_empty_array');

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'guid' => $uploads_url . '/' . $image_name . '.' . $wp_filetype['ext'],
        'post_parent' => null,
        'post_title' => $image_title,
        'post_content' => '',
    );

    // var_dump($attachment);
    // var_dump($game_provider);

    $attachment_id = wp_insert_attachment($attachment, $full_path);

    if (is_wp_error($attachment_id)) {
        return 0;
    }

    require_once ABSPATH . 'wp-admin/includes/media.php';
    require_once ABSPATH . 'wp-admin/includes/image.php';

    wp_update_attachment_metadata($attachment_id, wp_generate_attachment_metadata($attachment_id, $full_path));

    remove_filter('upload_dir', 'scg_update_game_provider_logos_uploads_folder_path');

    // Re-enable thumbnail generation.
    remove_filter('intermediate_image_sizes', '__return_empty_array');

    return $attachment_id;
}


/**
 * @param $arr
 * @param $type
 * @return mixed
 *
 * Update uploads folder path based on type (team-logos or league-logos)
 */
function scg_update_methods_uploads_folder_path($arr)
{
    $folder = "/casinos-data/payment-methods"; // No trailing slash at the end.

    $arr['path'] = $arr['basedir'] . $folder;
    $arr['url'] = $arr['baseurl'] . $folder;
    $arr['subdir'] = $folder;

    // Create folder if doesn't exist
    $uploads_dir = trailingslashit($arr['basedir']) . "casinos-data/payment-methods";
    wp_mkdir_p($uploads_dir);

    return $arr;
}


/**
 * @param $arr
 * @param $type
 * @return mixed
 *
 * Update uploads folder path based on type (team-logos or league-logos)
 */
function scg_update_crypto_logos_uploads_folder_path($arr)
{
    $folder = "/casinos-data/cryptos"; // No trailing slash at the end.

    $arr['path'] = $arr['basedir'] . $folder;
    $arr['url'] = $arr['baseurl'] . $folder;
    $arr['subdir'] = $folder;

    // Create folder if doesn't exist
    $uploads_dir = trailingslashit($arr['basedir']) . "casinos-data/cryptos";
    wp_mkdir_p($uploads_dir);

    return $arr;
}


/**
 * @param $arr
 * @param $type
 * @return mixed
 *
 * Update uploads folder path based on type (team-logos or league-logos)
 */
function scg_update_game_provider_logos_uploads_folder_path($arr)
{
    $folder = "/casinos-data/game-providers"; // No trailing slash at the end.

    $arr['path'] = $arr['basedir'] . $folder;
    $arr['url'] = $arr['baseurl'] . $folder;
    $arr['subdir'] = $folder;

    // Create folder if doesn't exist
    $uploads_dir = trailingslashit($arr['basedir']) . "casinos-data/cryptos";
    wp_mkdir_p($uploads_dir);

    return $arr;
}


/**
 * Convert shortened text to number.
 */
function scg_convert_shortened_text_to_number($number)
{
    $abbrevs = array(
        12 => "T",
        9 => "B",
        6 => "M",
        3 => "K",
        0 => ""
    );

    foreach ($abbrevs as $exponent => $abbrev) {
        if (strtoupper(substr($number, -1)) == $abbrev) {
            return substr_replace($number, "", -1) * pow(10, $exponent);
        }
    }
}


/**
 * Add referenced data to selected pod.
 */
function scg_add_reference_data_to_pod($data)
{
    global $wpdb;

    $values = array();

    // We're preparing each DB item on it's own. Makes the code cleaner.
    foreach ($data as $key => $value) {
        $values[] = $wpdb->prepare("(%d, %d, %d, 0, 0, %d, %d)", $value['pod_id'], $value['field_id'], $value['item_id'], $value['related_item_id'], $key);
    }

    $query = "INSERT INTO wp_podsrel (pod_id, field_id, item_id, related_pod_id, related_field_id, related_item_id, weight) VALUES ";
    $query .= implode(",\n", $values);

    // error_log('<pre>' . print_r($query, TRUE) . '<pre>');

    try {
        $wpdb->query(
            $query
        );

        return TRUE;
    } catch (Exception $e) {
        return error_log('Error adding data for pod! ID: ' . $data['item_id'] . ', Error: ' . $wpdb->last_error);
    }
}


/**
 * Remove referenced data from pod.
 */
function scg_remove_referenced_data_from_pod($data)
{
    global $wpdb;

    $delete_sql = 'DELETE FROM ' . 'wp_podsrel WHERE wp_podsrel.pod_id = ' . $data['pod_id'] . ' AND wp_podsrel.item_id = %d AND wp_podsrel.field_id = ' . $data['field_id'];

    try {
        $wpdb->query(
            $wpdb->prepare(
                $delete_sql,
                array($data['item_id'])
            )
        );

        return TRUE;
    } catch (Exception $e) {
        return error_log('Error removing data from pod! ID: ' . $data['item_id'] . ', Error: ' . $wpdb->last_error);
    }
}


function scg_casino_rating_insert_or_update($data_to_save) {
    // $data_to_save = array(
    //     'name' => $casino_name . ' - ' . $casino_rating,

    //     'language' => $language_code,
    //     'rating_casino' => $casino_id,
    //     'rating_casino_info' => $casino_info_id,
    //     'rating_countries' => $country_id,
    //     'rating_total' => $casino_rating,
    // );

    $current_timestamp = current_time('mysql');
    $rating_total = !empty($data_to_save['rating_total']) ? $data_to_save['rating_total'] : 0.00;

    $data_to_update = [
        'id'            => $data_to_save['review_id'],
        'name'          => $data_to_save['name'],
        'permalink'     => $data_to_save['permalink'],
        'rating_total'  => $rating_total,
        'modified'      => $data_to_save['modified'],
        'casino_id'     => $data_to_save['rating_casino'],
    ];

    if (empty($data_to_save['review_id'])) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_casino_review',
        [
            'id' => $data_to_save['review_id'],
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $review_id = !empty($data_to_save['review_id']) ? $data_to_save['review_id'] : $last_insert_id;

    // d($review_id);
    // die();

    // TODO update/insert on Casino review:
    // 1. rating_casino_info
    // + 2. other rating fields - 0.00
    // + 3. created, modified, permalink
    // 4. attach game_type_ratings (after adding/updating them)

    if (!empty($data_to_save['rating_countries'])) {
      $rating_casino_info_field_id = DatabaseService::getField('casino_review', 'rating_casino_info');
      $rating_casino_field_id = DatabaseService::getField('casino_review', 'rating_casino');
      $rating_countries_field_id = DatabaseService::getField('casino_review', 'rating_countries');
      $casino_review_pod_id = DatabaseService::getField('casino_review', 'pod_id');
      $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
      $casino_reviews_field_id = DatabaseService::getField('casino', 'casino_review_list');

      // d($casino_review_pod_id);
      // d($casino_pod_id);
      // die();

      // Casino reviews.
      $casino_reviews_field_info = array(
          'pod_id' => $casino_pod_id,
          'field_id' => $casino_reviews_field_id,
          'item_id' => $data_to_save['rating_casino'],
          'related_item_id' => $review_id,
      );

      if (!empty($casino_reviews_field_info) && !empty($data_to_save['rating_casino'])) {
          // sbg_remove_referenced_data_from_pod($bookmaker_reviews_field_info);

          $casino_reviews_data = array();
          $casino_reviews_data[] = $casino_reviews_field_info;

          if (!empty($casino_reviews_data)) {
            // error_log('<pre>' . print_r($bookmaker_reviews_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_reviews_data);
          }
      }

      // Rating Casino info.
      $rating_casino_info_field_info = array(
          'pod_id' => $casino_review_pod_id,
          'field_id' => $rating_casino_info_field_id,
          'item_id' => $review_id,
          'related_item_id' => $data_to_save['rating_casino_info'],
      );

      if (!empty($rating_casino_info_field_info) && !empty($data_to_save['rating_casino_info'])) {
          // sbg_remove_referenced_data_from_pod($rating_bookmaker_info_field_info);

          $casino_info_data = array();
          $casino_info_data[] = $rating_casino_info_field_info;

          if (!empty($casino_info_data)) {
            // error_log('<pre>' . print_r($bookmaker_info_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_info_data);
          }
      }

      // Rating Casino.
      $rating_casino_field_info = array(
          'pod_id' => $casino_review_pod_id,
          'field_id' => $rating_casino_field_id,
          'item_id' => $review_id,
          'related_item_id' => $data_to_save['rating_casino'],
      );

      if (!empty($rating_casino_field_info) && !empty($data_to_save['rating_casino'])) {
          // sbg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $casino_data = array();
          $casino_data[] = $rating_casino_field_info;

          if (!empty($casino_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_data);
          }
      }

      // Rating Countries.
      $rating_countries_field_info = array(
          'pod_id' => $casino_review_pod_id,
          'field_id' => $rating_countries_field_id,
          'item_id' => $review_id,
      );

      if (!empty($rating_countries_field_info) && !empty($data_to_save['rating_countries'])) {
          scg_remove_referenced_data_from_pod($rating_countries_field_info);

          $countries_data = array();
          foreach ($data_to_save['rating_countries'] as $country_id) {
              $countries_data[] = array(
                  'pod_id' => $casino_review_pod_id,
                  'field_id' => $rating_countries_field_id,
                  'item_id' => $review_id,
                  'related_item_id' => $country_id,
              );
          }

          if (!empty($countries_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($countries_data);
          }
      }
    }

    // error_log('<pre>' . print_r($data_to_save['rating_bookmaker'] . ' - ' . $review_id, TRUE) . '</pre>');

    $data_to_save['review_id'] = $review_id;

    // Custom Country relation. 
    $country_rel_id = scg_casino_main_rating_country_relation_insert_or_update($data_to_save);

    return $review_id;
}

function scg_casino_game_type_rating_insert_or_update($review_id, $casino_id, $casino_name, $casino_sheet_name, $game_type, $game_type_rating, $country_ids, $language_code) {
    // $params = array(
    //     'distinct' => FALSE,
    //     'where' =>
    //         'casino_game_review_casino.id = "' . $casino_id . '"' . ' AND ' .
    //         'casino_game_review_countries.id IN (' . $country_id . ')' . ' AND ' .
    //         "casino_game_review_game.id = " . $game_type_id . "" . ' AND ' .
    //         'language = "' . $language_code . '"',
    // );

    $casino_sheet_name = str_replace('-Rate', '', $casino_sheet_name);

    $current_timestamp = current_time('mysql');
    $existing_sport_review_id = FALSE;

    $game_type_rating_casino_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_casino');
    $game_type_rating_countries_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_countries');
    $game_type_rating_game_type_field_id = DatabaseService::getField('casino_game_review', 'casino_game_review_game');

    // d($game_type_rating_casino_field_id);
    // d($game_type_rating_countries_field_id);
    // d($game_type_rating_game_type_field_id);

    $game_type_id = scg_get_casino_game_type_id_from_name($game_type);

    // Let's check if this rating exists first.
    if (!empty($country_ids) && !empty($casino_id) && !empty($game_type_id)) {
        $query = new SelectQuery;
        $query
            ->from('wp_pods_casino_game_review', 't')
            ->select(['t.id'])
            ->orderBy('t.id', 'DESC')
            ->limit(1);

        $query
            ->join(DatabaseService::REL_TABLE, 'game_type_rating_casino')
            ->on("game_type_rating_casino.field_id = $game_type_rating_casino_field_id AND game_type_rating_casino.item_id = t.id");

        $query
            ->join(DatabaseService::REL_TABLE, 'game_type_rating_countries')
            ->on("game_type_rating_countries.field_id = $game_type_rating_countries_field_id AND game_type_rating_countries.item_id = t.id");

        $query
            ->join(DatabaseService::REL_TABLE, 'game_type_rating_game_type')
            ->on("game_type_rating_game_type.field_id = $game_type_rating_game_type_field_id AND game_type_rating_game_type.item_id = t.id");

        $query->where()
            ->equal('game_type_rating_casino.related_item_id', $casino_id);

        $query->where()
            ->equal('game_type_rating_game_type.related_item_id', $game_type_id);

        $query->where()
            ->in('game_type_rating_countries.related_item_id', $country_ids);

        // $query->where()
        //     ->equal('t.language', $language_code);

        $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
        $existing_game_type_review_id = !empty($results) ? (int) $results->id : FALSE;

        // d($existing_game_type_review_id);
        // die();
    }

    $sheet_name_lower = str_replace(' ', '-', strtolower($casino_sheet_name));
    $casino_name_lower = str_replace(' ', '-', strtolower($casino_name));
    $game_type_lower = str_replace(' ', '-', strtolower($game_type));
    $permalink = $game_type_lower . '-' . $casino_name_lower . '-' . $sheet_name_lower;
    $current_timestamp = current_time('mysql');
    $game_type_rating_name = $game_type . ' - ' . $game_type_rating . ' (' . $casino_name . ') - ' . $casino_sheet_name;

    $data_to_update = [
        'id'            => $existing_game_type_review_id,
        'name'          => $game_type_rating_name,
        'permalink'     => $permalink,
        'casino_game_review_rating'  => $game_type_rating,
        'modified'      => $current_timestamp,
        'casino_id'     => $casino_id,
        'game_type_id'  => $game_type_id,
        'game_type_name' => $game_type,
    ];

    // if ($game_type_id == 13) {
    //     d($data_to_update);
    //     die();
    // }

    if (empty($existing_game_type_review_id)) {
      $data_to_update['created'] = $current_timestamp;
    }    

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_casino_game_review',
        [
            'id' => $existing_game_type_review_id,
        ],
        $data_to_update
    );

    global $wpdb;
    $last_insert_id = $wpdb->insert_id;
    $game_type_review_id = !empty($existing_game_type_review_id) ? $existing_game_type_review_id : $last_insert_id;

    // error_log('<pre>' . print_r($review_id, TRUE) . '</pre>');

    // + Name:                   name 
    // + Countries:              bookmaker_sport_review_countries
    // + Lažybų bendrovė:        bookmaker_sport_review_bookmaker
    // + Sporto šaka:            bookmaker_sport_review_sport_branch
    // + Sporto šakos reitingas: bookmaker_sport_review_rating

    // + $sport_rating_countries_field_id
    // + $sport_rating_bookmaker_field_id
    // + $sport_rating_sport_branch_field_id

    // // TODO update/insert on Bookmaker review:
    // // 1. rating_bookmaker_info
    // // + 2. other rating fields - 0.00
    // // + 3. created, modified, permalink
    // // + 4. attach sport_type_ratings (after adding/updating them)

    if (!empty($country_ids)) {
      $casino_review_pod_id = DatabaseService::getField('casino_review', 'pod_id');
      $casino_game_type_review_pod_id = DatabaseService::getField('casino_game_review', 'pod_id');
      $casino_pod_id = DatabaseService::getField('casino', 'pod_id');
      $casino_game_type_reviews_field_id = DatabaseService::getField('casino', 'casino_game_review_list');

      // Attach - Game Type reviews.
      $casino_game_type_reviews_field_info = array(
          'pod_id' => $casino_pod_id,
          'field_id' => $casino_game_type_reviews_field_id,
          'item_id' => $casino_id,
          'related_item_id' => $game_type_review_id,
      );

      if (!empty($casino_game_type_reviews_field_info)) {
          // scg_remove_referenced_data_from_pod($bookmaker_reviews_field_info);

          $casino_game_type_reviews_data = array();
          $casino_game_type_reviews_data[] = $casino_game_type_reviews_field_info;

          if (!empty($casino_game_type_reviews_data)) {
            // error_log('<pre>' . print_r($bookmaker_reviews_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_game_type_reviews_data);
          }
      }

      // Game Type rating - Countries.
      $rating_countries_field_info = array(
          'pod_id' => $casino_game_type_review_pod_id,
          'field_id' => $game_type_rating_countries_field_id,
          'item_id' => $game_type_review_id,
      );

      if (!empty($rating_countries_field_info)) {
          // scg_remove_referenced_data_from_pod($rating_countries_field_info);

          $countries_data = array();
          foreach ($country_ids as $country_id) {
              $countries_data[] = array(
                  'pod_id' => $casino_game_type_review_pod_id,
                  'field_id' => $game_type_rating_countries_field_id,
                  'item_id' => $game_type_review_id,
                  'related_item_id' => $country_id,
              );
          }

          if (!empty($countries_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($countries_data);
          }
      }

      // Game Type rating - Casino.
      $rating_casino_field_info = array(
          'pod_id' => $casino_game_type_review_pod_id,
          'field_id' => $game_type_rating_casino_field_id,
          'item_id' => $game_type_review_id,
          'related_item_id' => $casino_id,
      );

      if (!empty($rating_casino_field_info)) {
          // scg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $casino_data = array();
          $casino_data[] = $rating_casino_field_info;

          if (!empty($casino_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($casino_data);
          }
      }

      // Game Type rating - Game type.
      $rating_game_type_field_info = array(
          'pod_id' => $casino_game_type_review_pod_id,
          'field_id' => $game_type_rating_game_type_field_id,
          'item_id' => $game_type_review_id,
          'related_item_id' => $game_type_id,
      );

      if (!empty($rating_game_type_field_info) && !empty($game_type_id)) {
          // scg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $rating_game_type_data = array();
          $rating_game_type_data[] = $rating_game_type_field_info;

          if (!empty($rating_game_type_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($rating_game_type_data);
          }
      }

      // Game type review - attach to main Review.
      $rating_casino_game_type_ratings_field_id = DatabaseService::getField('casino_review', 'rating_casino_game_reviews');

      $rating_sport_type_ratings_field_info = array(
          'pod_id' => $casino_review_pod_id,
          'field_id' => $rating_casino_game_type_ratings_field_id,
          'item_id' => $review_id,
          'related_item_id' => $game_type_review_id,
      );

      if (!empty($rating_sport_type_ratings_field_info)) {
          // scg_remove_referenced_data_from_pod($rating_bookmaker_field_info);

          $game_type_ratings_data = array();
          $game_type_ratings_data[] = $rating_sport_type_ratings_field_info;

          if (!empty($game_type_ratings_data)) {
            // error_log('<pre>' . print_r($countries_data, TRUE) . '</pre>');
            scg_add_reference_data_to_pod($game_type_ratings_data);
          }
      }
    }

    $data_to_save = array(
      'review_id'           => $review_id,
      'game_type_review_id' => $game_type_review_id,
      'casino_id'           => $casino_id,
      'name'                => $game_type . ' - ' . $game_type_rating . ' (' . $casino_name . ') - ' . $casino_sheet_name,
      'permalink'           => $permalink,
      'game_type_id'        => $game_type_id,
      'country_ids'         => $country_ids,
    );

    // if ($game_type_id == 13) {
    //     d($casino_game_type_reviews_data);
    //     d($countries_data);
    //     d($casino_data);
    //     d($game_type_ratings_data);
    //     d($review_id);
    //     d($data_to_save);
    //     die();
    // }

    // Custom Country Relation.
    scg_game_type_rating_country_relation_insert_or_update(
        $data_to_save
    );

    return $game_type_review_id;
}


function new_ratings_import_testing() {
  // $casino_id = 
  // $casino_name = 
  // $casino_sheet_name = 
  // $casino_rating = 
  // $casino_country_1 = 
  // $casino_games_ratings = 

  // scg_casino_ratings_get_google_drive_file();

  $sheet_name = 'NO';
  $casino_ratings = guru_ratings_parse_file($sheet_name);

  // $top_rating = scg_casino_trackers_get_top_rating('Slots', 'All Countries', 'en');
  // d($top_rating);

  // Check if have casinos to import.
  if (!empty($casino_ratings)) {
      $count = 0;
      foreach ($casino_ratings as $item) {
          if ($count < 5) {
            $casino_id            = $item['casino_id'];
            $casino_name          = $item['casino_name'];
            $casino_sheet_name    = $item['casino_sheet_name'];
            $casino_rating        = $item['casino_rating'];
            $casino_country_1     = $item['casino_country_1'];
            $casino_games_ratings = $item['casino_games_ratings'];

            scg_casino_ratings_import_casino_ratings($casino_id, $casino_name, $casino_sheet_name, $casino_rating, $casino_country_1, $casino_games_ratings);

          }

          $count++;
      }
  }
}

// add_action('init', 'new_ratings_import_testing');


/**
 * Import casino ratings data.
 */
function scg_casino_ratings_import_casino_ratings($casino_id, $casino_name, $casino_sheet_name, $casino_rating, $casino_country_1, $casino_games_ratings)
{
    // Get hash of current import.
    $hash = get_option('scg-casino-trackers-import-hash');

    // Sheet name for further use.
    $sheet_name = trim($casino_sheet_name);
    $sheet_name = str_replace('-Rate', '', $sheet_name);

    // Prepare log entry data for further use.
    $log_entry_data = array(
        'log_entry_casino_id' => $casino_id,
        'log_entry_sheet_name' => $casino_sheet_name,
        'log_entry_type' => 'casino_trackers',
    );

    $casino_matches = scg_check_if_casino_and_id_matches($casino_id, $casino_name);

    if (!$casino_matches) {
        // Stop executing further code if no match is found.
        return FALSE;
    }

    $casino_info_id = FALSE;

    // Get country IDs.
    $country_ids = scg_get_country_ids_from_country_names($casino_country_1);
    $country_ids_count = count($country_ids);

    if ($country_ids_count == 1) {
      $country_id = reset($country_ids);
      $country_code = scg_get_country_code_from_country_id($country_id);
    }

    if ($country_ids_count > 1) {
      $country_id = $country_ids;
      $country_code = $sheet_name;
    }

    // Get language code by country code.
    $language_code = scg_get_language_code_by_country_code($country_code);

    // d($country_ids);
    // d($country_id);
    // d($country_code);
    // die();

    // Let's check if this rating exists first.
    if (!empty($country_ids) && !empty($casino_id)) {
        $rating_casino_field_id = DatabaseService::getField('casino_review', 'rating_casino');
        $rating_countries_field_id = DatabaseService::getField('casino_review', 'rating_countries');
        $rating_language_field_id = DatabaseService::getField('casino_review', 'language');

        $query = new SelectQuery;
        $query
            ->from('wp_pods_casino_review', 't')
            ->select(['t.id'])
            ->limit(1);

        $query
            ->join(DatabaseService::REL_TABLE, 'rating_casino')
            ->on("rating_casino.field_id = $rating_casino_field_id AND rating_casino.item_id = t.id");

        $query
            ->join(DatabaseService::REL_TABLE, 'rating_countries')
            ->on("rating_countries.field_id = $rating_countries_field_id AND rating_countries.item_id = t.id");

        $query->where()
            ->equal('rating_casino.related_item_id', $casino_id);
            // ->equal('t.language', $language_code);

        $query->where()
            ->in('rating_countries.related_item_id', $country_ids);

        $results = (new DatabaseService($query, DatabaseService::GET_ITEM))->build();
        $existing_casino_review_id = !empty($results) ? (int) $results->id : FALSE;
        $casino_info_id = scg_casino_trackers_get_casino_info_id($casino_id, $language_code);

        // d($existing_casino_review_id);
        // // d($casino_info_id);
        // // d($country_ids);
        // die();
    }

    $sheet_name_lower = str_replace(' ', '-', strtolower($casino_sheet_name));
    $casino_name_lower = str_replace(' ', '-', strtolower($casino_name));
    $permalink = $casino_name_lower . '-' . $sheet_name_lower;
    $current_timestamp = current_time('mysql');

    // Float rating
    $casino_rating = (float) $casino_rating;

    // Prepare data for insertion/update.
    $data_to_save = array(
        'name'                  => $casino_name . ' - ' . $casino_sheet_name,
        'rating_casino'         => $casino_id,
        'rating_casino_info'    => $casino_info_id,
        'rating_countries'      => $country_ids,
        'rating_game_ratings'   => $casino_games_ratings,
        'rating_total'          => $casino_rating,
        'review_id'             => $existing_casino_review_id,
        'permalink'             => $permalink,
        'modified'              => $current_timestamp,
        'language'              => $language_code,
    );

    // d($data_to_save);
    // die();

    // Insert / update wp_pods_casino_review record.
    $review_id = scg_casino_rating_insert_or_update($data_to_save);

    // Deal with multiple casino games ratings.
    // error_log('<pre>' . print_r($casino, TRUE) . '</pre>');

    if (!empty($casino_games_ratings)) {
        foreach ($casino_games_ratings as $casino_game_rating) {
            $casino_id = $casino_game_rating['casino_id'];
            $casino_name = $casino_name;
            $game_type = trim($casino_game_rating['game_type']);
            $game_type_rating = (float) $casino_game_rating['rating'];
            $country_id = $country_id;

            if (isset($game_type_rating)) {
                // Set game type ratings from sheet data
                // and attach to parent casino review pod.
                // $casino_game_type_rating_id = scg_set_casino_game_type_ratings_from_sheet(
                //     $casino_review_id,
                //     $casino_id,
                //     $casino_name,
                //     $game_type,
                //     $game_type_rating,
                //     $country_id,
                //     $language_code
                // );

                $casino_game_type_rating_id = scg_casino_game_type_rating_insert_or_update(
                  $review_id,
                  $casino_id,
                  $casino_name,
                  $casino_sheet_name,
                  $game_type,
                  $game_type_rating,
                  $country_ids,
                  $language_code
                );

            }
        }
    }

    // $casino_ratings_actions_count = scg_get_casino_ratings_actions_count();

    // if ($casino_ratings_actions_count == 1) {
    //     // Re-generate casinos transients cache.
    //     scg_generate_casinos_transients_cache();
    // }

    return TRUE;
}

add_action('scg_casino_ratings_batch_casino_import', 'scg_casino_ratings_import_casino_ratings', 10, 6);


/**
 * Find vegashero game pods ID by slot name.
 */
function scg_find_vegashero_game_pods_id($slot_name)
{
    global $wpdb;

    $slot_name = trim($slot_name);

    $slot_search = $wpdb->get_results($wpdb->prepare("
    SELECT * FROM $wpdb->posts
      WHERE post_type = 'vegashero_games'
      AND post_status = 'publish'
      AND post_title = '%s'",
        $wpdb->esc_like($slot_name)
    ));

    $slot_id = !empty($slot_search) ? (int)$slot_search[0]->ID : FALSE;

    return $slot_id;
}

/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function scg_restricted_countries_import_update_status($post_id, $post, $update)
{
    $post_type = get_post_type($post_id);
    $post_status = get_post_status($post_id);

    // If this isn't a 'scheduled-action' post, skip execution.
    if ('scheduled-action' != $post_type) return;

    if ($post_status == 'publish') {

        // Get hash of current import.
        $hash = get_option('scg-restricted-countries-import-hash');

        $log_type = 'restricted_countries';

        $import_log_info = scg_get_import_log_info_by_hash($hash, $log_type);
        $import_log_start_time = !empty($import_log_info) ? strtotime($import_log_info->import_log_datetime) - 10850 : FALSE;

        if (!empty($import_log_start_time)) {
            $progress = scg_get_scheduled_actions_by_group($log_type, $import_log_start_time);
        }

        if (!empty($progress) && $progress == 100) {
            $mark_complete = scg_restricted_countries_import_mark_complete($hash);
        }

    }
}

add_action('save_post', 'scg_restricted_countries_import_update_status', 10, 3);


/**
 * Mark restricted countries import as Complete.
 */
function scg_restricted_countries_import_mark_complete($hash)
{
    // Mark this import as complete.
    update_option('scg-restricted-countries-import-progress', 'complete');

    // Get hash of current import.
    $hash = get_option('scg-restricted-countries-import-hash');

    // Log data for finished data.
    $import_log_data = array(
        'finished' => TRUE,
        'import_log_type' => 'restricted_countries',
    );

    // // Mark import complete for import log.
    scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);

    // Remove hash option.
    // delete_option('scg-restricted-countries-import-hash');
}


/**
 * Get restricted countries actions count.
 */
function scg_get_restricted_countries_actions_count()
{
    global $wpdb;
    $last_import_log_info = $wpdb->get_row("SELECT * FROM wp_pods_import_log ORDER BY id DESC LIMIT 0, 1");
    $last_import_log_id = !empty($last_import_log_info) ? (int)$last_import_log_info->id : FALSE;
    $last_import_log_hash = !empty($last_import_log_info) ? $last_import_log_info->import_log_code : FALSE;
    $last_import_log_start_datetime = !empty($last_import_log_info) ? $last_import_log_info->import_log_datetime : FALSE;

    // Compare with last import log start
    // datetime minus 180 minutes.
    $compare_datetime = !empty($import_log_info) ? strtotime($last_import_log_start_datetime) - 10850 : FALSE;

    $args = array(
        'hook' => 'scg_restricted_countries_batch_casino_import',
        // 'date' => $compare_datetime,
        // 'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_PENDING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $actions = array();

    $pending_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($pending_actions)) {
        foreach ($pending_actions as $action) {
            $actions[] = $action;
        }
    }

    // Get running actions.
    $args = array(
        'hook' => 'scg_restricted_countries_batch_casino_import',
        // 'date' => $compare_datetime,
        // 'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_RUNNING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $running_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($running_actions)) {
        foreach ($running_actions as $action) {
            $actions[] = $action;
        }
    }

    $actions_count = count($actions);

    return $actions_count;
}


/**
 * Get casino ratings actions count.
 */
function scg_get_casino_ratings_actions_count()
{
    global $wpdb;

    $last_import_log_info = $wpdb->get_row("SELECT * FROM wp_pods_import_log ORDER BY id DESC LIMIT 0, 1");
    $last_import_log_id = !empty($last_import_log_info) ? (int)$last_import_log_info->id : FALSE;
    $last_import_log_hash = !empty($last_import_log_info) ? $last_import_log_info->import_log_code : FALSE;
    $last_import_log_start_datetime = !empty($last_import_log_info) ? $last_import_log_info->import_log_datetime : FALSE;

    // Compare with last import log start
    // datetime minus 180 minutes.
    $compare_datetime = !empty($import_log_info) ? strtotime($last_import_log_start_datetime) - 10850 : FALSE;

    $args = array(
        'hook' => 'scg_casino_ratings_batch_casino_import',
        // 'date' => $compare_datetime,
        // 'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_PENDING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $actions = array();

    $pending_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($pending_actions)) {
        foreach ($pending_actions as $action) {
            $actions[] = $action;
        }
    }

    // Get running actions.
    $args = array(
        'hook' => 'scg_casino_ratings_batch_casino_import',
        // 'date' => $compare_datetime,
        // 'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_RUNNING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $running_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($running_actions)) {
        foreach ($running_actions as $action) {
            $actions[] = $action;
        }
    }

    // Get failed actions.
    $args = array(
        'hook' => 'scg_casino_ratings_batch_casino_import',
        // 'date' => $compare_datetime,
        // 'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_FAILED,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $failed_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($failed_actions)) {
        foreach ($failed_actions as $action) {
            $actions[] = $action;
        }
    }

    // Get pending actions - transients cache.
    $args = array(
        'hook' => 'scg_batch_prime_casinos_transient_cache',
        'status' => ActionScheduler_Store::STATUS_PENDING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $pending_transient_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($pending_transient_actions)) {
        foreach ($pending_transient_actions as $action) {
            $actions[] = $action;
        }
    }

    $actions_count = count($actions);

    return $actions_count;
}


/**
 * Get scheduled actions by group and date.
 */
function scg_get_scheduled_actions_by_group($group, $date)
{
    $args = array(
        'group' => $group,
        'date' => $date,
        'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_PENDING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $actions = array();

    $pending_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($pending_actions)) {
        foreach ($pending_actions as $action) {
            $actions[] = $action;
        }
    }

    // Get running actions.
    $args = array(
        'group' => $group,
        'date' => $date,
        'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_RUNNING,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $running_actions = as_get_scheduled_actions($args, $return_format);

    if (!empty($running_actions)) {
        foreach ($running_actions as $action) {
            $actions[] = $action;
        }
    }

    $actions_count = count($actions);

    // Get complete actions.
    $args = array(
        'group' => $group,
        'date' => $date,
        'date_compare' => '>=',
        'status' => ActionScheduler_Store::STATUS_COMPLETE,
        'per_page' => 0,
    );

    $return_format = 'ids';

    $complete_actions = as_get_scheduled_actions($args, $return_format);

    $complete_actions_count = count($complete_actions);
    $all_actions_count = $actions_count + $complete_actions_count;

    $progress_percentage = $complete_actions_count / $all_actions_count * 100;

    return $progress_percentage;
}


/**
 * Get import log info by hash.
 */
function scg_get_import_log_info_by_hash($hash_code, $log_type)
{
    // Check if import log exists first.
    $params = array(
        'distinct' => FALSE,
        'where' =>
            't.import_log_code = "' . $hash_code . '"' . ' AND ' .
            't.import_log_type = "' . $log_type . '"',
    );

    $import_log = pods('import_log');
    $import_log->find($params);
    $data = $import_log->data();

    $import_log_info = FALSE;

    if (!empty($data) && !empty($data[0])) {
        $import_log_info = $data[0];
    }

    return $import_log_info;
}


/**
 * Batch start for restricted countries import action scheduler.
 */
function scg_restricted_countries_batch_start($type = FALSE)
{
    if (isset($_POST['action']) && $_POST['action'] == 'scg_restricted_countries_import' || $type == 'cron') {

        // Don't execute further if nonce is not verified.
        // if ($type != 'cron' && !wp_verify_nonce($_POST['restricted_countries_security'], 'scg_restricted_countries_import')) {
        //     die('-2');
        // }

        // Get latest Restricted Countries Google Drive file.
        // scg_casino_trackers_get_restricted_countries_file();

        // Get all CSV files from SxG - All Info - Restricted Countries file.
        scg_casino_trackers_get_all_info_csv_files();

        // Prepare main variables.
        $sheet_name = 'Restricted Countries';

        $casinos = array();
        $casinos_data = array();

        $restricted_countries_csv_data = scg_casino_trackers_parse_casinos_all_info_csv_file('Restricted Countries');

        if (isset($_POST['restricted_countries_import_select']) && $_POST['restricted_countries_import_select'] != 'all') {
            $single_casino_id = $_POST['restricted_countries_import_select'];
            $single_casino_name = !empty($single_casino_id) ? scg_get_casino_name_from_id($single_casino_id) : FALSE;

            $single_casino_info = array(
                'casino_id' => $single_casino_id,
                'casino_name' => $single_casino_name,
            );

            // Only import user selected Casino Restricted Countries.
            // $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name, $single_casino_info);
            $casinos = scg_casino_trackers_parse_restricted_countries_csv_file($restricted_countries_csv_data, $single_casino_info);
        } else {
            // Import All Casinos Restricted Countries.
            // $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name);
            $casinos = scg_casino_trackers_parse_restricted_countries_csv_file($restricted_countries_csv_data);
        }

        // Check if have casinos to import.
        if (!empty($casinos)) {
            foreach ($casinos as $item) {
                // Create a "schedule action" for the data

                // Preserve Casino ID variable, as we are 
                // NOT storing it in Google Sheet anymore.
                $item['casino_id'] = scg_get_casino_id_from_name($item['casino_name']);
                $data = set_transient('scg-restricted-countries-data-' . $item['casino_id'] . '-' . $item['casino_name'], $item, 31 * 24 * 60 * 60);

                as_schedule_single_action(
                    time(), 
                    'scg_restricted_countries_batch_casino_import',
                    array(
                        'casino_column_id'          => $item['casino_column_id'],
                        'casino_id'                 => $item['casino_id'],
                        'casino_name'               => $item['casino_name'],
                        'casino_restricted_enabled' => $item['casino_restricted_enabled'],
                        'casino_enabled'            => $item['casino_enabled'],
                        'casino_closed'             => $item['casino_closed'],
                    ),
                    'restricted_countries'
                );
            }
        
            // Generate transients cache.
            $sheet_select = 'all_sheets';
            scg_generate_casinos_transients_cache($sheet_select);
        }

    }
}


/**
 * AJAX callback for import progress.
 */
function scg_restricted_countries_import_progress_callback()
{
    global $wpdb;

    if ($_POST['progress'] == 0 && $_POST['counter'] == 0) {
        // Insert variables for new import.
        scg_restricted_countries_start_import_insert_variables();
    }

    // Get hash of current import.
    $hash = get_option('scg-restricted-countries-import-hash');

    $log_type = 'restricted_countries';

    $import_log_info = scg_get_import_log_info_by_hash($hash, $log_type);
    $import_log_start_time = !empty($import_log_info) ? strtotime($import_log_info->import_log_datetime) - 10850 : FALSE;

    if (!empty($import_log_start_time)) {
        $progress = scg_get_scheduled_actions_by_group($log_type, $import_log_start_time);
    }

    echo $progress;

    wp_die();
}

add_action('wp_ajax_scg_restricted_countries_import_progress', 'scg_restricted_countries_import_progress_callback');


/**
 * AJAX callback for import progress.
 */
function scg_restricted_countries_import_callback()
{
    global $wpdb;

    // Batch start.
    scg_restricted_countries_batch_start();

    wp_die();
}

add_action('wp_ajax_scg_restricted_countries_import', 'scg_restricted_countries_import_callback');


/**
 * Callback to insert variables for new restricted countries import.
 */
function scg_restricted_countries_start_import_insert_variables($type = '')
{
    $progress = update_option('scg-restricted-countries-import-progress', 0);

    // Add hash for later use.
    $hash = password_hash(time(), PASSWORD_DEFAULT);

    // Add new import hash option when starting the import.
    update_option('scg-restricted-countries-import-hash', $hash, '', 'yes');

    $import_log_data = array(
        'first_save' => TRUE,
        'import_log_type' => 'restricted_countries',
    );

    if ($type == 'cron') {
        $import_log_data['author_cron'] = TRUE;
    }

    // Reset email sent setting on import start.
    update_option('scg-import-email-sent', 0);

    scg_casino_trackers_create_or_update_import_log($hash, $import_log_data);
}


/**
 * Get casinos transient sheet name.
 */
function scg_casino_trackers_get_casinos_transient_sheet_name($country_name)
{
    $sheets_with_countries = scg_casino_trackers_get_sheets_with_countries_list();
    $transient_sheet_name = FALSE;

    // d($sheets_with_countries);

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            foreach ($sheet['countries'] as $sheet_country) {
                if ($country_name == $sheet_country) {
                    $transient_sheet_name = strtolower(str_replace(' ', '_', trim($sheet['title'])));
                }
            }
        }
    }

    if (empty($transient_sheet_name) || $transient_sheet_name == 'all') {
        $transient_sheet_name = 'all_countries';
    }

    return $transient_sheet_name;
}

/**
 * Get sheets with countries list from casino trackers file.
 */
function scg_casino_trackers_get_sheets_with_countries_list()
{
    $selected_sheets = get_option('scg-casino-trackers-settings-selected-sheets');
    $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');

    if (empty($sheets_with_countries)) {
        // Get latest Google Drive file.
        guru_trackers_and_ratings_get_google_drive_file();

        if (!empty($selected_sheets)) {
            foreach ($selected_sheets as $sheet) {
                $sheet_countries = scg_casino_trackers_get_countries_from_sheet($sheet['title']);

                $sheets_with_countries[] = array(
                    'title' => $sheet['title'],
                    'sheetId' => $sheet['sheetId'],
                    'countries' => $sheet_countries,
                );
            }
        }

        if (!empty($sheets_with_countries)) {
            update_option('scg-casino-trackers-sheets-with-countries', $sheets_with_countries, false);
        }
    }

    return $sheets_with_countries;
}


/**
 * Get countries from concrete sheet.
 */
function scg_casino_trackers_get_countries_from_sheet($sheet_name)
{
    // Get proper XLSX directory path.
    $xlsx_directory_path = scg_get_xlsx_uploads_directory_path();
    $file_path = $xlsx_directory_path . 'guru-trackers-ratings.xlsx';

    try {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setLoadSheetsOnly([$sheet_name]);
        $reader->setReadEmptyCells(false);
        $spreadsheet = $reader->load($file_path);

        $worksheetData = $reader->listWorksheetInfo($file_path);

        foreach ($worksheetData as $worksheet) {
            if ($worksheet['worksheetName'] == $sheet_name) {
                $sheet_lastColumnIndex = $worksheet['lastColumnIndex'];
                $sheet_lastColumnLetter = $worksheet['lastColumnLetter'];

                $sheet_lastRange = $sheet_lastColumnLetter . $sheet_lastColumnIndex;
            }
        }

        $active_sheet = $spreadsheet->getActiveSheet();
        $maxCell = $active_sheet->getHighestRowAndColumn();

        $highestRow = $active_sheet->getHighestRow();
        $highestCol = $active_sheet->getHighestColumn();

        // Get last column that has countries
        // starting from second row.
        $casino_countries_range_array = $active_sheet->rangeToArray(
            $highestCol . '2:' . $highestCol . $highestRow
        );

        // Filter array to only non-empty items.
        $casino_countries = array_filter(call_user_func_array('array_merge', $casino_countries_range_array));

    } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        error_log('An error occurred while parsing XLS file: ' . $e->getMessage());
    }

    return $casino_countries;
}


/**
 * Get casino game types list.
 */
function scg_get_casino_game_types_list()
{
    $query = new SelectQuery;

    $query
        ->select(['t.*'])
        ->from('wp_pods_casino_game', 't');

    $query
        ->orderBy('t.name', 'ASC');

    $data = (new DatabaseService($query))->build();

    $game_types = [];

    if ($data) {
        foreach ($data as $item) {
            $game_types[] = array(
                'id' => (int)$item->id,
                'name' => $item->name,
            );
        }
    }

    return $game_types;
}


/**
 * Get countries list.
 */
function sbg_get_countries_list()
{
    $countries_pods = pods('country');
    $params = array(
        'distinct' => FALSE,
        'limit' => 0,
        'orderby' => 't.name ASC',
    );
    $countries_pods->find($params);
    $countries_list = $countries_pods->data();

    $countries = array();

    if ($countries_list) {
        foreach ($countries_list as $entry) {
            $countries[] = array(
                'id' => (int)$entry->id,
                'name' => $entry->name,
            );
        }
    }

    return $countries;
}


/**
 * Prime casinos transient cache.
 */
function scg_batch_prime_casinos_transient_cache($country_name)
{
    $country_code = scg_get_country_code_from_name($country_name);

    // Get language code by country code.
    $language_code = scg_get_language_code_by_country_code($country_code);

    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    // If we dont have this country bonuses, then show
    // casinos from 'All Countries' (English) rating.
    if (!$have_this_country_bonuses) {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses, then show
    // casinos from this country rating and language.
    if ($have_this_country_bonuses) {
        $casino_review_language = strtolower($country_code);
    }

    // If we have this country bonuses and user's country
    // is SE, then show casinos from Sweden and in English.
    if ($have_this_country_bonuses && $country_code == 'SE') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'TR') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'BG') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is UK, then show casinos from UK and in English.
    if ($have_this_country_bonuses && $country_code == 'GB') {
        $casino_review_language = 'uk';
    }

    // Map EE (Estonia) code to our ET code.
    if ($have_this_country_bonuses && $country_code == 'EE') {
        $casino_review_language = 'et';
    }

    // Set English language as default.
    if (empty($casino_review_language)) {
        $casino_review_language = 'en';
    }

    // Prime TOP rating casinos - Best Online casinos
    // without any game type filter.
    $top_casinos_main = scg_casino_trackers_get_top_rating(FALSE, $country_name, $casino_review_language);

    $game_types = scg_get_casino_game_types_list();

    $count = 1;
    foreach ($game_types as $game_type) {
        // Get casino review language and user country name.
        // $casino_review_language = scg_casino_trackers_get_casino_review_language($game_type['name']);

        // scg_casino_trackers_get_limited_casinos_by_game($game_type['name'], $country_name, $casino_review_language);

        // Prime TOP rating casinos.
        // $language_code = $country_name == 'Lithuania' ? 'en' : $casino_review_language;

        scg_casino_trackers_get_top_rating($game_type['name'], $country_name, $casino_review_language);

        $count++;
    }
}

add_action('scg_batch_prime_casinos_transient_cache', 'scg_batch_prime_casinos_transient_cache', 10, 3);


/**
 * Re-generate casinos transient cache.
 */
function scg_generate_casinos_transients_cache($sheet_select = FALSE)
{
    $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');

    global $wpdb;

    // Remove old transient cache items.
    $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casinos_filter_%')");

    $sheets_countries = array();

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $country_count = 1;
            foreach ($sheet['countries'] as $country) {
                if ($country_count == 1) {
                    if ($country == 'All Countries') {
                        $country_name = 'Lithuania';
                    } else {
                        $country_name = $country;
                    }

                    // Stop executing if user selected other sheet.
                    if (isset($sheet_select) && $sheet_select != 'all_sheets' && $sheet_select != $sheet['title']) {
                        continue;
                    }

                    $sheets_countries[] = $country_name;
                }

                $country_count++;
            }
        }
    }

    $count = 1;
    $current_datetime = current_time('mysql');
    $gmt_date = get_gmt_from_date($current_datetime);
    $gmt_timestamp = strtotime($gmt_date);

    $time_with_1_min = $gmt_timestamp + 60;
    $time_with_3_mins = $gmt_timestamp + 180;

    $count = 1;
    foreach ($sheets_countries as $country_name) {
        // This make trigger transient cache and makes it fast loaded.
        as_schedule_single_action(
            $time_with_3_mins,
            'scg_batch_prime_casinos_transient_cache',
            array(
                'country_name' => $country_name,
            ),
            'scg_import_sheets_data'
        );

        $count++;
    }
}


/**
 * Get [single-casino] used shortcodes and unique casinos.
 */
function scg_get_single_casinos_from_shortcodes()
{
    $shortcodes = array();

    scg_get_shortcodes_in_content('page', __('Post'), $shortcodes);
    scg_get_shortcodes_in_content('post', __('Page'), $shortcodes);

    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $types = get_post_types($args);
    foreach ($types as $type) {
        scg_get_shortcodes_in_content($type, '', $shortcodes);
    }

    $single_casino_shortcodes = array();
    if (!empty($shortcodes)) {
        foreach ($shortcodes as $key => $shortcode) {
            if ($key == 'single-casino') {
                $single_casino_shortcodes[] = $shortcode;
            }
        }
    }

    if (!empty($single_casino_shortcodes[0])) {
        $single_casinos = array();
        foreach ($single_casino_shortcodes[0] as $key => $shortcode) {
            $casino_name = trim($shortcode['params']['name'], '"');
            $shortcode_code = $shortcode['code'];
            $shortcode_atts = !empty($shortcode_code) ? shortcode_parse_atts($shortcode_code) : FALSE;
            $game_type = isset($shortcode_atts['game-type']) ? $shortcode_atts['game-type'] : FALSE;

            $single_casinos[] = array(
                'casino_name' => $casino_name,
                'game_type' => $game_type,
            );
        }
    }

    $single_casinos = array_map("unserialize", array_unique(array_map("serialize", $single_casinos)));

    return $single_casinos;
}


/**
 * Re-generate all single casinos transient cache.
 */
function scg_generate_all_single_casinos_cache()
{
    $single_casinos = scg_get_single_casinos_from_shortcodes();

    if (!empty($single_casinos)) {
        foreach ($single_casinos as $casino) {
            scg_generate_single_casino_cache($casino['casino_name']);
        }
    }
}


/**
 * Re-generate single casino transient cache.
 */
function scg_generate_single_casino_cache($casino_name)
{
    $sheets_with_countries = get_option('scg-casino-trackers-sheets-with-countries');

    $single_casinos = scg_get_single_casinos_from_shortcodes();

    $single_casino_used = FALSE;
    if (!empty($single_casinos)) {
        foreach ($single_casinos as $key => $casino) {
            if ($casino_name == $casino['casino_name']) {
                $single_casino_used = TRUE;
            } else {
                unset($single_casinos[$key]);
            }
        }
        $single_casinos = array_values($single_casinos);
    }

    // Return early if this casino's [single-casino] shortcode is not used.
    if (!$single_casino_used) {
        return;
    }

    global $wpdb;

    $casino_id = scg_get_casino_id_from_name($casino_name);
    $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

    // Remove old transient cache items.
    if (!empty($casino_name_lower)) {
        $wpdb->query("DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_scg_casinos_filter_single_'" . $casino_name_lower . "_%')");
    }

    if ($casino_id) {
        return;
    }

    $sheets_countries = array();

    if (!empty($sheets_with_countries)) {
        foreach ($sheets_with_countries as $sheet) {
            $country_count = 1;
            foreach ($sheet['countries'] as $country) {
                if ($country_count == 1) {
                    if ($country == 'All Countries') {
                        $country_name = 'Lithuania';
                    } else {
                        $country_name = $country;
                    }

                    $sheets_countries[] = $country_name;
                }

                $country_count++;
            }
        }
    }

    $count = 1;

    $languages = scg_get_languages_list_from_pod();

    // var_dump($languages);
    // var_dump($sheets_countries);

    foreach ($single_casinos as $single_casino) {
        foreach ($sheets_countries as $country_name) {

            if (!empty($languages)) {
                foreach ($languages as $language_code) {
                    // Casino info ID.
                    $casino_info_id = scg_casino_trackers_get_casino_info_id($casino_id, $language_code);

                    // var_dump($language_code);
                    // var_dump($casino_info_id);

                    if (!empty($casino_info_id)) {
                        // Trigger function and generate cache by game type.
                        // This make trigger transient cache and makes it fast loaded.
                        as_schedule_single_action(
                            time() + 10,
                            'scg_batch_prime_single_casino_transient_cache',
                            array(
                                'casino_name' => $casino_name,
                                'game_type' => $single_casino['game_type'],
                                'country_name' => $country_name,
                                'language_code' => $language_code,
                            ),
                            'single_casino_transient_cache'
                        );
                    }
                }
            }

            $count++;
        }
    }
}


/**
 * Re-generate single casino transient cache.
 */
function scg_update_casino_info_casino_id_column()
{
  if (empty(get_option('new-casino-info-casino-id-set'))) {
    $languages = scg_get_languages_list_from_pod();
    $casinos_list = scg_get_current_casinos_list($enabled_only = FALSE);

    foreach ($casinos_list as $casino) {
        $casino_id = $casino['id'];
        $casino_name = $casino['name'];

        foreach ($languages as $language_code) {
            // Casino info ID.
            $casino_info_id = scg_casino_trackers_get_casino_info_id_old($casino_id, $language_code);

            if (!empty($casino_info_id)) {
                // Trigger function and generate cache by game type.
                // This make trigger transient cache and makes it fast loaded.
                as_schedule_single_action(
                    time(),
                    'scg_batch_set_casino_info_casino_id',
                    array(
                        'casino_info_id'  => $casino_info_id,
                        'casino_id'       => $casino_id,
                        'language_code'   => $language_code,
                    ),
                    'casino_info_set_casino_id'
                );
            }

        }
    }


    update_option('new-casino-info-casino-id-set', TRUE);
  }

}

// add_action('init', 'scg_update_casino_info_casino_id_column');


function scg_set_casino_info_casino_id($casino_info_id, $casino_id) {
    $current_timestamp = current_time('timestamp');

    $data_to_update = [
        'id'             => $casino_info_id,
        'modified'       => $current_timestamp,
        'casino_id'      => $casino_id,
    ];

    // Insert/update review item.
    (new DatabaseService)->createOrUpdate(
        'wp_pods_casino_info',
        [
            'id' => $casino_info_id,
        ],
        $data_to_update
    );
}

add_action('scg_batch_set_casino_info_casino_id', 'scg_set_casino_info_casino_id', 10, 2);



/**
 * Get shortcodes in every content of "type_name" type. List results by shortcode.
 * Allow to pass an existing $shortcodes array to fill with results.
 */
function scg_get_shortcodes_in_content($type_name, $type_title = '', &$shortcodes = array())
{

    if ($type_title == '') $type_title = ucfirst($type_name);

    $args = array(
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'orderby' => 'date',
        'post_type' => $type_name,
        'order' => 'DESC'
    );
    $posts = get_posts($args);
    scg_getShortcodesByShortcode($posts, $type_title, $shortcodes);
}


/**
 * Get shortcodes recursive.
 */
function scg_getShortcodesByShortcodeRecursive($content, $post, $type_title, $pattern, &$shortcodes)
{

    if (preg_match_all($pattern, $content, $matches) && array_key_exists(2, $matches)) {

        foreach ($matches[0] as $key => $value) {
            $shortcode = array();

            // replace space with '&' for parse_str() function
            parse_str(str_replace(" ", "&", $matches[3][$key]), $params);

            $shortcode = array('name' => $matches[2][$key], 'params' => $params, 'params_raw' => $matches[3][$key], 'content' => $matches[5][$key], 'code' => $value,
                'post' => array('ID' => $post->ID, 'title' => $post->post_title, 'type' => (isset($type_title) ? $type_title : $post->post_type), 'guid' => $post->guid));

            $shortcodes[$matches[2][$key]][] = $shortcode;

            // Recursivity
            scg_getShortcodesByShortcodeRecursive($matches[5][$key], $post, $type_title, $pattern, $shortcodes);
        }
    }
}


/**
 * Get shortcodes by shortcode.
 */
function scg_getShortcodesByShortcode($posts, $type_title, &$shortcodes = array())
{

    $pattern = '/' . get_shortcode_regex() . '/s';

    foreach ($posts as $post) {
        scg_getShortcodesByShortcodeRecursive($post->post_content, $post, $type_title, $pattern, $shortcodes);
    }
}


/**
 * Prime single casino transient cache.
 */
function scg_batch_prime_single_casino_transient_cache($casino_name, $game_type, $country_name, $language_code)
{
    $country_code = scg_get_country_code_from_name($country_name);

    // Get language code by country code.
    $language_code = scg_get_language_code_by_country_code($country_code);

    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    // If we dont have this country bonuses, then show
    // casinos from 'All Countries' (English) rating.
    if (!$have_this_country_bonuses) {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses, then show
    // casinos from this country rating and language.
    if ($have_this_country_bonuses) {
        $casino_review_language = strtolower($country_code);
    }

    // If we have this country bonuses and user's country
    // is SE, then show casinos from Sweden and in English.
    if ($have_this_country_bonuses && $country_code == 'SE') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'TR') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is TR, then show casinos from Turkey and in English.
    if ($have_this_country_bonuses && $country_code == 'BG') {
        $casino_review_language = 'en';
    }

    // If we have this country bonuses and user's country
    // is UK, then show casinos from UK and in English.
    if ($have_this_country_bonuses && $country_code == 'GB') {
        $casino_review_language = 'uk';
    }

    // Map EE (Estonia) code to our ET code.
    if ($have_this_country_bonuses && $country_code == 'EE') {
        $casino_review_language = 'et';
    }

    // Set English language as default.
    if (empty($casino_review_language)) {
        $casino_review_language = 'en';
    }

    // Trigger getting single casino.
    $single_casino_by_game = scg_casino_trackers_get_single_casino($casino_name, $game_type, $country_name, $casino_review_language);
}

add_action('scg_batch_prime_single_casino_transient_cache', 'scg_batch_prime_single_casino_transient_cache', 10, 4);


/**
 * Action scheduler claims a batch of actions to process in each request. It keeps the batch
 * fairly small (by default, 25) in order to prevent errors, like memory exhaustion.
 *
 * This method increases it so that more actions are processed in each queue, which speeds up the
 * overall queue processing time due to latency in requests and the minimum 1 minute between each
 * queue being processed.
 *
 * For more details, see: https://actionscheduler.org/perf/#increasing-batch-size
 */
function ashp_increase_queue_batch_size( $batch_size ) {
  return $batch_size * 2;
}

add_filter( 'action_scheduler_queue_runner_batch_size', 'ashp_increase_queue_batch_size' );


/** 
 * Action scheduler processes queues of actions in parallel to speed up the processing of large numbers
 * If each queue takes a long time, this will result in multiple PHP processes being used to process actions,
 * which can prevent PHP processes being available to serve requests from visitors. This is why it defaults to
 * only 5. However, on high volume sites, this can be increased to speed up the processing time for actions.
 *
 * This method hextuples the default so that more queues can be processed concurrently. Use with caution as doing
 * this can take down your site completely depending on your PHP configuration.
 *
 * For more details, see: https://actionscheduler.org/perf/#increasing-concurrent-batches
 */
function ashp_increase_concurrent_batches( $concurrent_batches ) {
    return $concurrent_batches * 2;
}
add_filter( 'action_scheduler_queue_runner_concurrent_batches', 'ashp_increase_concurrent_batches' );


/**
 * Check if casino trackers import is currently active.
 */
function scg_casino_trackers_import_is_active()
{
    $progress = get_option('scg-casino-trackers-import-progress');

    $is_active = $progress != '100' && $progress != 'complete';

    return $is_active;
}


/**
 * Check if casino trackers import is currently active.
 */
function scg_restricted_countries_import_is_active()
{
    $progress = get_option('scg-restricted-countries-import-progress');

    $is_active = $progress != '100' && $progress != 'complete';

    return $is_active;
}


/**
 * Add weekly CRON schedule.
 */
function scg_cron_schedule_add_weekly($schedules)
{
    $schedules['weekly'] = array(
        'interval' => 604800,
        'display' => __('Once Weekly')
    );

    return $schedules;
}

add_filter('cron_schedules', 'scg_cron_schedule_add_weekly');


function scg_casino_trackers_cron_trigger_bg_process_night()
{
    // new SCG_Trackers_Import('cron');
}


/**
 * Restricted Countries CRON trigger.
 */
function scg_restricted_countries_cron_trigger_import()
{
    // Insert new import variables.
    scg_restricted_countries_start_import_insert_variables('cron');

    // Batch start the import.
    scg_restricted_countries_batch_start('cron');
}


function sbg_auto_schedule_ratings_trackers_import_action($import) {

  $sheet_select = 'all_sheets';

  // Schedule wrapper import action.
  as_schedule_single_action(
      time(),
      'scg_batch_schedule_wrapper_import_action',
      array(
        'sheet_select' => $sheet_select
      ),
      'scg_import_sheets_data'
  );

}

add_action('sbg_batch_schedule_ratings_trackers_import_action', 'sbg_auto_schedule_ratings_trackers_import_action', 10, 1);


function sbg_auto_schedule_restricted_countries_import_action($import) {

    // Get latest Restricted Countries Google Drive file.
    scg_casino_trackers_get_restricted_countries_file();

    // Prepare main variables.
    $sheet_name = 'Restricted Countries';

    $casinos = array();
    $casinos_data = array();

    // Import All Casinos Restricted Countries.
    $casinos = scg_casino_trackers_parse_restricted_countries_file($sheet_name);

    // Check if have casinos to import.
    if (!empty($casinos)) {
        foreach ($casinos as $item) {
            // Create a "schedule action" for the data
            $data = set_transient('scg-restricted-countries-data-' . $item['casino_id'] . '-' . $item['casino_name'], $item, 31 * 24 * 60 * 60);

            as_schedule_single_action(
                time(), 
                'scg_restricted_countries_batch_casino_import',
                array(
                    'casino_column_id'          => $item['casino_column_id'],
                    'casino_id'                 => $item['casino_id'],
                    'casino_name'               => $item['casino_name'],
                    'casino_restricted_enabled' => $item['casino_restricted_enabled'],
                    'casino_enabled'            => $item['casino_enabled'],
                ),
                'restricted_countries'
            );
        }
    
        // Generate transients cache.
        $sheet_select = 'all_sheets';
        scg_generate_casinos_transients_cache($sheet_select);
    }

}

add_action('sbg_batch_schedule_restricted_countries_import_action', 'sbg_auto_schedule_restricted_countries_import_action', 10, 1);

/**
 * Register cron events.
 * Schedule an action if it's not already scheduled.
 * Will repeat daily at 03:00 (UTC+3) - import casino trackers.
 * Also repeat weekly at Monday 04:00 (UTC+3) - import restricted countries.
 */
function scg_register_cron_events()
{

    if (false === as_next_scheduled_action('sbg_batch_schedule_ratings_trackers_import_action')) {

        as_schedule_recurring_action(
            time(),
            604800,
            'sbg_batch_schedule_ratings_trackers_import_action',
            ['import' => TRUE],
            "scg_import_sheets_data"
        );
    }


    // Let's schedule Restricted Countries Import
    // on 04:00 AM Monday and repeat weekly.
    $current_timestamp = current_time('timestamp');
    $is_today_monday = date('w', $current_timestamp) === '1';
    $is_now_past_4_am = date('H') > 01;

    // if (!wp_next_scheduled('scg_restricted_countries_import_cron_night') && $is_today_monday && $is_now_past_4_am) {
    //     wp_schedule_event(strtotime('01:00:00'), 'weekly', 'scg_restricted_countries_import_cron_night');
    // }

    if (false === as_next_scheduled_action('sbg_batch_schedule_restricted_countries_import_action')) {

        as_schedule_recurring_action(
            time(),
            604800,
            'sbg_batch_schedule_restricted_countries_import_action',
            ['import' => TRUE],
            "restricted_countries"
        );
    }
}

// add_action('init', 'scg_register_cron_events');


/**
 * Cron actions.
 */
// add_action('scg_casino_trackers_import_cron_night', 'scg_casino_trackers_cron_trigger_bg_process_night');
// add_action('scg_restricted_countries_import_cron_night', 'scg_restricted_countries_cron_trigger_import');


/**
 * Resave casino_info pods for
 * bi-directional relationship with casino pods type.
 */
function scg_casino_trackers_resave_casino_info_pods()
{
    // Execute only if option is empty.
    if (empty(get_option('pods-casino-info-resaved'))) {
        $casino_info_pods = pods('casino_info');
        $params = array(
            'limit' => 0,
        );
        $casino_info_pods->find($params);
        $casino_info_list = $casino_info_pods->data();

        $casino_info_ids = array();

        if (!empty($casino_info_list)) {
            foreach ($casino_info_list as $entry) {
                $casino_info_ids[] = (int)$entry->id;
            }

            if (!empty($casino_info_ids)) {
                foreach ($casino_info_ids as $casino_info) {
                    $params = array(
                        'distinct' => false,
                        'where' => 't.id = "' . $casino_info . '"',
                    );

                    $single_casino_info_pods = pods('casino_info', $params);
                    $single_casino_info_casino_id = $single_casino_info_pods->field('casino_info_casino');

                    if (!empty($single_casino_info_casino_id)) {
                        $data = array(
                            'casino_info_casino' => $single_casino_info_casino_id,
                            'casino_id'          => $single_casino_info_casino_id,
                        );
                    }

                    // Batch resave all casino infos to
                    // attach reference to casino pods type.
                    if (!empty($data)) {
                        $single_casino_info_pods->save($data);
                    }
                }
            }
        }

        // Mark done.
        add_option('pods-casino-info-resaved', TRUE);
    }
}

// add_action('init', 'scg_casino_trackers_resave_casino_info_pods');


/**
 * Resave casino_review pods for
 * bi-directional relationship with casino pods type.
 */
function scg_casino_trackers_resave_casino_review_pods()
{
    // Execute only if option is empty.
    if (empty(get_option('pods-casino-review-resaved'))) {
        $casino_review_pods = pods('casino_review');
        $params = array(
            'limit' => 0,
        );
        $casino_review_pods->find($params);
        $casino_review_list = $casino_review_pods->data();

        $casino_review_ids = array();

        if (!empty($casino_review_list)) {
            foreach ($casino_review_list as $entry) {
                $casino_review_ids[] = (int)$entry->id;
            }

            if (!empty($casino_review_ids)) {
                foreach ($casino_review_ids as $casino_review) {
                    $params = array(
                        'distinct' => false,
                        'where' => 't.id = "' . $casino_review . '"',
                    );

                    $single_casino_review_pods = pods('casino_review', $params);
                    $single_casino_review_casino_id = $single_casino_review_pods->field('rating_casino');

                    if (!empty($single_casino_review_casino_id)) {
                        $data = array(
                            'rating_casino' => $single_casino_review_casino_id,
                        );
                    }

                    // Batch resave all casino reviews to
                    // attach reference to casino pods type.
                    if (!empty($data)) {
                        $single_casino_review_pods->save($data);
                    }
                }
            }
        }

        // Mark done.
        add_option('pods-casino-review-resaved', TRUE);
    }
}

// add_action('init', 'scg_casino_trackers_resave_casino_review_pods');


/**
 * Resave casino_game_review pods for
 * bi-directional relationship with casino pods type.
 */
function scg_casino_trackers_resave_casino_game_review_pods()
{
    // Execute only if option is empty.
    if (empty(get_option('pods-casino-game-review-resaved'))) {
        $casino_game_review_pods = pods('casino_game_review');
        $params = array(
            'limit' => 0,
        );
        $casino_game_review_pods->find($params);
        $casino_game_review_list = $casino_game_review_pods->data();

        $casino_game_review_ids = array();

        if (!empty($casino_game_review_list)) {
            foreach ($casino_game_review_list as $entry) {
                $casino_game_review_ids[] = (int)$entry->id;
            }

            if (!empty($casino_game_review_ids)) {
                foreach ($casino_game_review_ids as $casino_game_review) {
                    $params = array(
                        'distinct' => false,
                        'where' => 't.id = "' . $casino_game_review . '"',
                    );

                    $single_casino_game_review_pods = pods('casino_game_review', $params);
                    $single_casino_game_review_casino_id = $single_casino_game_review_pods->field('casino_game_review_casino');

                    if (!empty($single_casino_game_review_casino_id)) {
                        $data = array(
                            'casino_game_review_casino' => $single_casino_game_review_casino_id,
                        );
                    }

                    // Batch resave all casino game reviews to
                    // attach reference to casino pods type.
                    if (!empty($data)) {
                        $single_casino_game_review_pods->save($data);
                    }
                }
            }
        }

        // Mark done.
        add_option('pods-casino-game-review-resaved', TRUE);
    }
}

// add_action('init', 'scg_casino_trackers_resave_casino_game_review_pods');


/**
 * Get vegashero_games synonyms.
 */
function scg_get_vegashero_games_synonyms($synonym_title)
{
    $synonym_single_quote = str_replace('\'', '’', $synonym_title);
    $synonym_without_quotes = str_replace(array('\'', '’', '-'), array('', '', ''), $synonym_title);
    $synonyms_array = array();
    $synonyms_array[] = $synonym_without_quotes;

    // Add to synonyms array only if they differ.
    if ($synonym_single_quote != $synonym_without_quotes) {
        $synonyms_array[] = $synonym_single_quote;
    }

    $synonyms_list = implode('|', $synonyms_array);

    return $synonyms_list;
}


/**
 * Resave vegashero_games synonyms.
 */
function scg_generate_vegashero_games_synonyms($post_id)
{
    $single_post = get_post($post_id);
    $single_vegashero_games_pods = pods('vegashero_games', $post_id);

    $pods_slot_short_title = $single_vegashero_games_pods->field('games_short_title');

    $synonym_title = !empty($pods_slot_short_title) ? strtolower($pods_slot_short_title) : strtolower($single_post->post_title);

    $synonyms_list = scg_get_vegashero_games_synonyms($synonym_title);
    $games_synonyms = $synonyms_list;

    $data = array();

    if (!empty($games_synonyms)) {
        $data['games_synonyms'] = $games_synonyms;
    }

    if (empty($pods_slot_short_title)) {
        $data['games_short_title'] = $single_post->post_title;
    }

    // Batch resave.
    if (!empty($data)) {
        $single_vegashero_games_pods->save($data);
    }

    return $data;
}

/**
 * Resaves vegashero_games synonyms.
 *
 * @param array $args
 * @param array $assoc_args
 *
 * Usage: `wp scg-save-vegashero-games-synonyms`
 */
function scg_save_vegashero_games_synonyms($args = array(), $assoc_args = array())
{
    $args = array(
        'post_type' => 'vegashero_games',
        'numberposts' => -1,
    );
    $all_posts = get_posts($args);
    $games_count = count($all_posts);

    foreach ($all_posts as $single_post) {
        // Generate vegashero_games_synonyms.
        scg_generate_vegashero_games_synonyms($single_post->ID);
    }

    // Show success message.
    WP_CLI::success('Updated ' . $games_count . ' vegashero_games successfully.');
}

// Add the command.
if (defined('WP_CLI') && WP_CLI) {
    WP_CLI::add_command('scg-save-vegashero-games-synonyms', 'scg_save_vegashero_games_synonyms');
}


/**
 * Add vegashero_games pod pre save hook.
 */
function scg_pods_pre_save_vegashero_games($pieces, $is_new_item, $id)
{
    $single_post = get_post($id);
    $pods_slot_short_title = $pieces['fields']['games_short_title']['value'];

    $synonym_title = !empty($pods_slot_short_title) ? strtolower($pods_slot_short_title) : strtolower($single_post->post_title);
    $generated_synonyms = scg_get_vegashero_games_synonyms($synonym_title);

    $pieces['fields']['games_synonyms']['value'] = $generated_synonyms;

    return $pieces;
}

add_action('pods_api_pre_save_pod_item_vegashero_games', 'scg_pods_pre_save_vegashero_games', 99, 3);


function scg_get_slot_casinos($slot_id)
{
    $vegashero_games = pods('vegashero_games', $slot_id);
    $casinos = $vegashero_games->field('games_casinos');

    $casino_names = '';
    if (!empty($casinos)) {
        foreach ($casinos as $casino) {
            $casino_names .= $casino['name'] . ', ';
        }
    }

    $casino_names = rtrim($casino_names, ', ');

    return $casino_names;
}

/**
 *
 * Add casino review pods save hook.
 *
 */
// function custom_casino_review_save($pieces, $is_new_item)
// {
//     // If pod is casino_info, warmup transients
//     // cache on every save event to show fresh data
//     // on TOP casinos rating and other pages.
// //    if ($pieces['params']->pod == 'casino_info') {
// //        // Re-generate casinos transients cache.
// //        scg_generate_casinos_transients_cache();
// //
// //        $casino_id = (int)$pieces['fields']['casino_info_casino']['value'];
// //        $casino_name = !empty($casino_id) ? scg_get_casino_name_from_id($casino_id) : FALSE;
// //
// //        // Re-generate single casino transients cache.
// //        if (!empty($casino_name)) {
// //            scg_generate_single_casino_cache($casino_name);
// //        }
// //    }

//     // If pod is casino_review, calculate its rating_total.
//     if ($pieces['params']->pod == 'casino_review') {

//         $rating_reliability = $pieces['fields']['rating_reliability']['value'];
//         $rating_games_sum = $pieces['fields']['rating_games_sum']['value'];
//         $rating_bonuses = $pieces['fields']['rating_bonuses']['value'];
//         $rating_overall = $pieces['fields']['rating_overall']['value'];
//         $rating_extra = $pieces['fields']['rating_extra']['value'];
//         $rating_selected_game = $pieces['fields']['rating_selected_game']['value'];

//         if (empty($pieces['fields']['rating_casino_game']['value'])) {
//             // (Overall + Reliability * 1.2) + Extra + Bonuses + Games Sum / 4.
//             $rating_total = (($rating_overall + $rating_reliability * 1.2) + $rating_extra + $rating_bonuses + $rating_games_sum) / 4;
//         }

//         if (!empty($pieces['fields']['rating_casino_game']['value'])) {
//             // (Overall + Reliability * 1.2) + Selected Game + Extra + Bonuses + Games Sum / 5.
//             $rating_total = (($rating_overall + $rating_reliability * 1.2) + ($rating_selected_game * 1.2) + $rating_extra + $rating_bonuses + $rating_games_sum) / 5;
//         }

//         // $pieces['fields']['rating_total']['value'] = $rating_total;
//     }

//     // If pod is casino_link, auto generate title.
//     if ($pieces['params']->pod == 'casino_link') {
//         $casino_pod_id = $pieces['fields']['casino_link_casino']['value'];
//         $casino_pod = pods('casino', $casino_pod_id);
//         $casino_name = $casino_pod->field('name');

//         $casino_game_pod_id = !empty($pieces['fields']['casino_link_game']['value']) ? $pieces['fields']['casino_link_game']['value'] : FALSE;

//         if ($casino_game_pod_id) {
//             $casino_game_pod = pods('casino_game', $casino_game_pod_id);
//             $casino_game_name = $casino_game_pod->field('name');
//         }

//         $site_url = 'https://play.smartcasinoguide.com';
//         $casino_game = !empty($casino_game_name) ? ' - ' . $casino_game_name : FALSE;
//         $casino_link = $pieces['fields']['casino_link']['value'];
//         $casino_link_path_only = str_replace($site_url, '', $casino_link);
//         $casino_link_status = $pieces['fields']['casino_link_status']['value'] == '1' ? '' : ' - Išjungta';

//         $language = $pieces['fields']['language']['value'];
//         $language_code = !empty($language) ? ' - ' . strtoupper($language) : '';

//         $country_id = $pieces['fields']['casino_link_countries']['value'];
//         $country_name_value = scg_get_country_name_from_id($country_id);
//         $country_name = !empty($country_name_value) ? ' - ' . $country_name_value : '';

//         // Generate title.
//         $title = $casino_link_path_only . $casino_game . $country_name . $casino_link_status;

//         $pieces['fields']['name']['value'] = $title;
//     }

//     // If pod is casino_game_review, auto generate title.
//     if ($pieces['params']->pod == 'casino_game_review') {
//         $casino_game_pod_id = $pieces['fields']['casino_game_review_game']['value'];
//         $casino_game_pod = pods('casino_game', $casino_game_pod_id);
//         $casino_game_name = $casino_game_pod->field('name');

//         $casino_pod_id = $pieces['fields']['casino_game_review_casino']['value'];
//         $casino_pod = pods('casino', $casino_pod_id);
//         $casino_name = ' (' . $casino_pod->field('name') . ')';

//         $params = array(
//             'distinct' => false,
//             'where' => 'rating_casino.id = "' . $casino_pod_id . '"',
//         );
//         $casino_review_pod = pods('casino_review', $params);
//         $casino_rating_total = $casino_review_pod->field('rating_total');
//         $casino_game_rating = $pieces['fields']['casino_game_review_rating']['value'];
//         $rating = ($casino_rating_total + $casino_game_rating) / 2;
//         $rating = round($rating, 2);

//         // Set calculated game rating.
//         // $pieces['fields']['casino_game_review_total_rating']['value'] = $rating;

//         $casino_game_rating = ' - ' . $pieces['fields']['casino_game_review_rating']['value'];
//         $casino_game_calced_rating = ' / ' . $pieces['fields']['casino_game_review_total_rating']['value'];

//         $country_id = $pieces['fields']['casino_game_review_countries']['value'];
//         $country_name_value = scg_get_country_code_from_id($country_id);
//         $country_name = !empty($country_name_value) ? ' - ' . strtoupper($country_name_value) : '';

//         // Generate title.
//         $title = $casino_game_name . $casino_game_rating . $casino_name . $country_name;
//         $pieces['fields']['name']['value'] = $title;
//     }

//     if ($pieces['params']->pod == 'casino_game_node') {
//         $casino_game_name = $pieces['fields']['name']['value'];

//         // Generate shortcode.
//         if (empty($pieces['fields']['casino_game_node_shortcode_top']['value'])) {
//             $shortcode = '[top-rated-casino-by-game game="' . $casino_game_name . '"' . ' title="' . 'TOP 1 Casino to play ' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_node_shortcode_top']['value'] = $shortcode;
//         }

//         if (empty($pieces['fields']['casino_game_node_shortcode_other']['value'])) {
//             $shortcode_other = '[other-rated-casinos-by-game game="' . $casino_game_name . '"' . ' title="' . 'Other Casinos to play ' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_node_shortcode_other']['value'] = $shortcode_other;
//         }

//         if (empty($pieces['fields']['casino_game_node_shortcode_top_rating']['value'])) {
//             $shortcode_top_rating = '[casino-listing game="' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_node_shortcode_top_rating']['value'] = $shortcode_top_rating;
//         }
//     }

//     if ($pieces['params']->pod == 'casino_game') {
//         $casino_game_name = $pieces['fields']['name']['value'];

//         // Generate casino_game related shortcodes.
//         if (empty($pieces['fields']['casino_game_shortcode_top']['value'])) {
//             $shortcode = '[top-rated-casino game-type="' . $casino_game_name . '"' . ' title="' . 'TOP 1 Casino to play ' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_shortcode_top']['value'] = $shortcode;
//         }

//         if (empty($pieces['fields']['casino_game_shortcode_other']['value'])) {
//             $shortcode_other = '[other-rated-casinos game-type="' . $casino_game_name . '"' . ' title="' . 'Other Casinos to play ' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_shortcode_other']['value'] = $shortcode_other;
//         }

//         if (empty($pieces['fields']['casino_game_shortcode_top_rating']['value'])) {
//             $shortcode_top_rating = '[casino-listing game-type="' . $casino_game_name . '"]';
//             $pieces['fields']['casino_game_shortcode_top_rating']['value'] = $shortcode_top_rating;
//         }
//     }

//     return $pieces;
// }

// add_filter('pods_api_pre_save_pod_item', 'custom_casino_review_save', 10, 2);


/**
 * Adds pods post save hook.
 */
function custom_save_casino_pod_item($pieces, $is_new_item, $id) {
    // Casino pod logic.
    if (!empty($is_new_item)) {
        $casino_id = $id;
        $casino_name = $pieces['fields']['name']['value'];

        // // Return early.
        if (empty($casino_id) && empty($casino_name)) {
          return;
        }

        // Check if main info ID exists.
        $params = array(
            'distinct' => FALSE,
            'where' =>
                "casino_id = '" . $casino_id . "'",
        );

        $main_info = pods('casino_main_info');
        $main_info->find($params);
        $data = $main_info->data();

        $main_info_id = FALSE;

        if (!empty($data) && !empty($data[0])) {
            $main_info_id = (int)$data[0]->id;
        }

        // Create main info if ID not found.
        if (empty($main_info_id)) {
            $new_main_info = pods('casino_main_info');

            // To add a new item, let's set the data first
            $data = array(
                'name' => $casino_name . ' - main info',
                'casino' => $casino_id,
                'casino_id' => $casino_id,
            );

            // Add the new item now and get the new ID
            $new_main_info_id = $new_main_info->add($data);

            // Add casino ID to casino Pod.
            if (!empty($casino_id) && !empty($new_main_info_id)) {
                $casino = pods('casino', $casino_id);
                $casino->add_to('casino_main_info', $new_main_info_id);
                $casino->save();
            }
        }
    }
}

add_action('pods_api_post_save_pod_item_casino', 'custom_save_casino_pod_item', 99, 3);


/**
 * Filter to hook into pod creation and override name.
 * Currenly only for casino_link type.
 */
function custom_pods_pre_create_pod_item($pieces)
{
    if ($pieces['params']->pod == 'casino_link') {
        $casino_pod_id = $pieces['fields']['casino_link_casino']['value'];
        $casino_pod = pods('casino', $casino_pod_id);
        $casino_name = $casino_pod->field('name');

        $casino_game_pod_id = !empty($pieces['fields']['casino_link_game']['value']) ? $pieces['fields']['casino_link_game']['value'] : FALSE;

        if ($casino_game_pod_id) {
            $casino_game_pod = pods('casino_game', $casino_game_pod_id);
            $casino_game_name = $casino_game_pod->field('name');
        }

        $site_url = 'https://smartcasinoguide.com';
        $casino_game = !empty($casino_game_name) ? ' - ' . $casino_game_name : FALSE;
        $casino_link = $pieces['fields']['casino_link']['value'];
        $casino_link_path_only = str_replace($site_url, '', $casino_link);
        $casino_link_status = $pieces['fields']['casino_link_status']['value'] == '1' ? '' : ' - Išjungta';

        $language = $pieces['fields']['language']['value'];
        $language_code = !empty($language) ? ' - ' . strtoupper($language) : '';

        $country_id = $pieces['fields']['casino_link_countries']['value'];
        $country_name_value = scg_get_country_name_from_id($country_id);
        $country_name = !empty($country_name_value) ? ' - ' . $country_name_value : '';

        // Generate title.
        $title = $casino_link_path_only . $casino_game . $country_name . $casino_link_status;

        $pieces['fields']['name']['value'] = $title;
    }
}

add_filter('pods_api_pre_create_pod_item', 'custom_pods_pre_create_pod_item', 10, 1);


/**
 *
 * Change pods serial comma value.
 *
 */
// function custom_pods_serial_comma_value($value, $original_value, $params)
// {
//     $value = str_replace(', ', '<br />', $value);
//     $value = str_replace(' ir ', '<br />', $value);
//     $value = str_replace(' and', '<br />', $value);
//     $value = str_replace('and', '<br />', $value);
//     $value = str_replace('<br /><br />', '<br />', $value);

//     return $value;
// }

// add_filter('pods_serial_comma_value', 'custom_pods_serial_comma_value', 10, 3);


/**
 * Check if user is from country.
 */
function scg_user_is_from_country($country_code)
{
    // Make sure IP2Location database is exist.
    if (!is_file(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'))) {
        return;
    }

    if (!class_exists('IP2Location\\Database')) {
        require_once IP2LOCATION_REDIRECTION_ROOT . 'class.IP2Location.php';
    }

    $ip = custom_get_user_ip_address();

    // Create IP2Location object.
    $db = new \IP2Location\Database(IP2LOCATION_REDIRECTION_ROOT . get_option('ip2location_redirection_database'), \IP2Location\Database::FILE_IO);

    // Get geolocation by IP address.
    $response = $db->lookup($ip, \IP2Location\Database::ALL);

    // If user is from UK, force him this rank list.
    if ($response['countryCode'] == $country_code) {
        return TRUE;
    }

    return FALSE;
}


/**
 * Get single casino (by game).
 */
function scg_casino_trackers_get_single_casino($casino_name, $game_name = FALSE, $country_name, $language, $extra_data = array())
{

    $have_this_country_bonuses = scg_casino_trackers_check_if_casino_bonus_country_imported($country_name);

    $country_id = scg_get_country_id_from_name(trim($country_name));
    $original_country_id = $country_id;

    if (!empty($have_this_country_bonuses)) {
        // Get country ID using pods query.
        $country_id = $country_id;
    } else {
        // Use "All Countries" country.
        $country_id = scg_get_country_id_from_name('All Countries');
    }

    // Get casino ID.
    $casino_id = scg_get_casino_id_from_name($casino_name);

    // Use "All Countries" ID when country_id is empty.
    $country_id = empty($country_id) ? 1 : $country_id;
    $original_country_id = empty($original_country_id) ? 1 : $original_country_id;
    $original_country_id = (string)$original_country_id;

    $game_name_lower = strtolower(str_replace(' ', '_', $game_name));

    $transient_sheet_name = scg_casino_trackers_get_casinos_transient_sheet_name($country_name);
    $transient_name = 'scg_casinos_filter_' . $game_name_lower . '_' . $transient_sheet_name;

    $game_name_lower = empty($game_name_lower) ? 'main' : $game_name_lower;
    $casino_name_lower = strtolower(str_replace(' ', '_', $casino_name));

    $casinos = get_transient('scg_casinos_filter_single_' . $casino_name_lower . '_' . $game_name_lower . '_' . $transient_sheet_name);

    if (empty($casinos)) {

        if (!empty($casino_id) && !empty($country_id) && !empty($language)) {

            $casinos = array();

            // NEW Query
            $query = new SelectQuery;

            $query
                ->from('wp_pods_country_relation', 't')
                ->select(
                [
                    't.*', 
                    'info.id AS casino_info_id',
                    'info.casino_info_enabled AS casino_info_enabled',
                ]);

            $query
                ->leftJoin('wp_pods_casino', 'casino')
                ->on('t.casino_id = casino.id');
            $query
                ->leftJoin('wp_pods_casino_review', 'main_review')
                ->on('t.main_review_id = main_review.id');
            $query
                ->leftJoin('wp_pods_casino_info', 'info')
                ->on('t.casino_id = info.casino_id');
            $query
                ->leftJoin('wp_pods_casino_bonus', 'bonus_country')
                ->on('t.bonus_id = bonus_country.id');
            $query
                ->leftJoin('wp_pods_currency', 'currency')
                ->on('bonus_country.currency_id = currency.id');

            $query
                ->limit(1)
                ->where()
                // ->isNot('t.main_review_id', NULL)
                ->isNot('t.bonus_id', NULL)
                ->equal('t.country_id', $country_id)
                ->equal('t.casino_id', $casino_id)
                ->equal('casino.casino_enabled', 1)
                ->equal('casino.casino_restricted_enabled', 1)
                ->equal('bonus_country.bonus_enabled', 1)
                ->equal('info.casino_info_enabled', 1);
                // ->equal('info.language', $language);

              // Sort by.
              $query
                  ->groupBy('t.casino_id');

            $data = (new DatabaseService($query))->build();
            
            // if ($casino_id == 74) {
            //     d($data);
            // }



            if ($data) {
                foreach ($data as $item) {
                    $casino_id = (int)$item->casino_id;
                    $casino_info_id = (int)$item->casino_info_id;
                    $casino_info_enabled = (bool)$item->casino_info_enabled;
                    $casino_info = !empty($casino_info_id) ? scg_casino_trackers_get_casino_info($casino_info_id) : FALSE;
                    $casino_logo_id = scg_casino_trackers_get_casino_logo_id($casino_info_id);

                    $game_type_id = !empty($game_name) ? scg_get_casino_game_type_id_from_name($game_name) : FALSE;

                    if (!empty($game_type_id)) {
                        $casino_rating = scg_get_casino_game_review_by_casino_id_language_country($casino_id, $language, $country_id, $game_type_id);
                    } else {
                        $casino_rating = scg_get_casino_review_by_casino_id_language_country($casino_id, $language, $country_id);
                    }

                    // Get casino link and bonus data
                    // from fast functions separately.
                    $casino_link_data = !empty($game_type_id) ? scg_get_casino_game_link_data_by_casino_id_language_country($casino_id, $language, $country_id, $game_type_id) : FALSE;
                    $casino_bonus_data = scg_get_casino_bonus_by_casino_id_country($casino_id, $country_id);

                    $casino_game_type_link = !empty($casino_link_data['casino_link']) ? $casino_link_data['casino_link'] : FALSE;
                    $casino_top_link = !empty($casino_bonus_data['casino_top_link']) ? $casino_bonus_data['casino_top_link'] : FALSE;
                    $casino_link = !empty($game_name) ? $casino_game_type_link : $casino_top_link;

                    // if ($casino_id == 75) {
                    //     var_dump($game_name);
                    //     var_dump($game_type_id);
                    //     var_dump($casino_rating);
                    //     var_dump($casino_game_type_link);
                    //     var_dump($casino_link);
                    // }

                    $casino_button_text = !empty($casino_link_data['casino_link_button_text']) ? $casino_link_data['casino_link_button_text'] : FALSE;
                    $casino_regulation_text = !empty($casino_link_data['casino_link_regulation_text']) ? $casino_link_data['casino_link_regulation_text'] : FALSE;

                    $casino_bonus_text = !empty($casino_bonus_data['casino_bonus_text']) ? $casino_bonus_data['casino_bonus_text'] : FALSE;
                    $casino_bonus_sum = !empty($casino_bonus_data['casino_bonus_sum']) ? $casino_bonus_data['casino_bonus_sum'] : FALSE;
                    $casino_bonus_currency = !empty($casino_bonus_data['casino_bonus_currency']) ? $casino_bonus_data['casino_bonus_currency'] : FALSE;

                    $casino_top_button_text = !empty($casino_bonus_data['casino_top_button_text']) ? $casino_bonus_data['casino_top_button_text'] : FALSE;
                    $casino_top_regulation_text = !empty($casino_bonus_data['casino_top_regulation_text']) ? $casino_bonus_data['casino_top_regulation_text'] : FALSE;

                    $casino_shortcode_button_text = !empty($casino_bonus_data['casino_shortcode_button_text']) ? $casino_bonus_data['casino_shortcode_button_text'] : FALSE;
                    $casino_shortcode_regulation_text = !empty($casino_bonus_data['casino_shortcode_regulation_text']) ? $casino_bonus_data['casino_shortcode_regulation_text'] : FALSE;

                    $casino_restricted_countries = !empty($casino_id) ? scg_casino_trackers_get_casino_restricted_countries($casino_id) : array();

                    if (!empty($casino_rating) && (!empty($casino_link_data) || !empty($casino_bonus_data))) {
                        $casinos[] = array(
                            'casino_id' => $casino_id,
                            'casino_info_id' => $casino_info_id,
                            'casino_logo_id' => $casino_logo_id,
                            'casino_info' => $casino_info,
                            'casino_rating' => $casino_rating,
                            'casino_link' => $casino_link,
                            'casino_button_text' => $casino_button_text,
                            'casino_regulation_text' => $casino_regulation_text,
                            'casino_top_button_text' => $casino_top_button_text,
                            'casino_top_regulation_text' => $casino_top_regulation_text,
                            'casino_short_button_text' => $casino_shortcode_button_text,
                            'casino_short_regulation_text' => $casino_shortcode_regulation_text,
                            'casino_bonus_text' => $casino_bonus_text,
                            'casino_bonus_sum' => $casino_bonus_sum,
                            'casino_bonus_currency' => $casino_bonus_currency,
                            'casino_restricted_countries' => $casino_restricted_countries,
                        );
                    }
                }
            }

            // 25 hours - actually it's not expiring as we
            // re-generate transients every 24 hours after
            // all casino trackers import (in the morining).
            set_transient('scg_casinos_filter_single_' . $casino_name_lower . '_' . $game_name_lower . '_' . $transient_sheet_name, $casinos, 25 * 60 * 60);
        }

    }

    // var_dump($casinos);

    if (!empty($casinos)) {
        foreach ($casinos as $key => $casino) {
            $casino_restricted = FALSE;
            foreach ($casino['casino_restricted_countries'] as $restricted_country) {
                // Mark restricted casino in this country.
                if ($restricted_country == $original_country_id) {
                    $casino_restricted = TRUE;
                }
            }

            // Remove restricted casino in this country.
            if (!empty($casino_restricted) && $extra_data['show_restricted'] == 'FALSE') {
                unset($casinos[$key]);
            }
        }
    }

    return $casinos;
}


/**
 * Get casino review data (by casino id, language, country).
 */
function scg_get_casino_review_by_casino_id_language_country($casino_id, $language_code, $country_id)
{
    $casino_review_pod = pods('casino_review');
    $casino_field = $casino_review_pod->fields('rating_casino');
    $countries_field = $casino_review_pod->fields('rating_countries');

    if (!empty($casino_id) && !empty($language_code) && !empty($country_id)) {
        $casino_field_info = array(
            'pod_id' => $casino_field['pod_id'],
            'field_id' => $casino_field['id'],
            'related_item_id' => $casino_id,
        );

        $countries_field_info = array(
            'pod_id' => $countries_field['pod_id'],
            'field_id' => $countries_field['id'],
            'related_item_id' => $country_id,
        );

        global $wpdb;
        $casino_reviews_ids_tmp = $wpdb->get_results("
      SELECT r.id, r.name, r.rating_total AS rating, r.language, review_country.related_item_id AS country_id, review_casino.related_item_id AS casino_id
      FROM wp_pods_casino_review r
      LEFT JOIN wp_podsrel AS review_country ON r.id = review_country.item_id
      LEFT JOIN wp_podsrel AS review_casino ON r.id = review_casino.item_id
      WHERE (review_casino.pod_id = " . $casino_field_info['pod_id'] . " AND review_casino.related_item_id = " . $casino_field_info['related_item_id'] . " AND review_casino.field_id = " . $casino_field_info['field_id'] . ") AND
      (review_country.pod_id = " . $countries_field_info['pod_id'] . " AND review_country.related_item_id = " . $countries_field_info['related_item_id'] . " AND review_country.field_id = " . $countries_field_info['field_id'] . ")",
            ARRAY_A);

        $casino_rating = FALSE;
        if (!empty($casino_reviews_ids_tmp)) {
            $casino_rating = $casino_reviews_ids_tmp[0]['rating'];
        }

        return $casino_rating;
    }
}


/**
 * Get casino game review data (by casino id, language, country, game type ID).
 */
function scg_get_casino_game_review_by_casino_id_language_country($casino_id, $language_code, $country_id, $game_type_id)
{
    $game_review_pod = pods('casino_game_review');
    $game_review_countries_field = $game_review_pod->fields('casino_game_review_countries');
    $game_review_casino_field = $game_review_pod->fields('casino_game_review_casino');
    $game_review_game_type_field = $game_review_pod->fields('casino_game_review_game');

    if (!empty($casino_id) && !empty($country_id) && !empty($game_type_id)) {
        $casino_field_info = array(
            'pod_id' => $game_review_casino_field['pod_id'],
            'field_id' => $game_review_casino_field['id'],
            'related_item_id' => $casino_id,
        );

        $countries_field_info = array(
            'pod_id' => $game_review_countries_field['pod_id'],
            'field_id' => $game_review_countries_field['id'],
            'related_item_id' => $country_id,
        );

        $game_type_field_info = array(
            'pod_id' => $game_review_game_type_field['pod_id'],
            'field_id' => $game_review_game_type_field['id'],
            'related_item_id' => $game_type_id,
        );

        global $wpdb;
        $casino_game_reviews_ids_tmp = $wpdb->get_results("
      SELECT r.id, r.name, r.casino_game_review_rating AS rating, review_country.related_item_id AS country_id, review_casino.related_item_id AS casino_id, review_game_type.related_item_id AS game_type_id
      FROM wp_pods_casino_game_review r
      LEFT JOIN wp_podsrel AS review_country ON r.id = review_country.item_id
      LEFT JOIN wp_podsrel AS review_casino ON r.id = review_casino.item_id
      LEFT JOIN wp_podsrel AS review_game_type ON r.id = review_game_type.item_id
      WHERE (review_casino.pod_id = " . $casino_field_info['pod_id'] . " AND review_casino.related_item_id = " . $casino_field_info['related_item_id'] . " AND review_casino.field_id = " . $casino_field_info['field_id'] . ") AND
      (review_country.pod_id = " . $countries_field_info['pod_id'] . " AND review_country.related_item_id = " . $countries_field_info['related_item_id'] . " AND review_country.field_id = " . $countries_field_info['field_id'] . ")" . " AND (review_game_type.pod_id = " . $game_type_field_info['pod_id'] . " AND review_game_type.related_item_id = " . $game_type_field_info['related_item_id'] . " AND review_game_type.field_id = " . $game_type_field_info['field_id'] . ")",
            ARRAY_A);

        $casino_rating = FALSE;
        if (!empty($casino_game_reviews_ids_tmp)) {
            $casino_rating = $casino_game_reviews_ids_tmp[0]['rating'];
        }

        return $casino_rating;
    }
}


/**
 * Get casino bonus data (by casino ID, country).
 */
function scg_get_casino_bonus_by_casino_id_country($casino_id, $country_id)
{
    $casino_bonus_pod = pods('casino_bonus');
    $bonus_casino_field = $casino_bonus_pod->fields('bonus_casino');
    $bonus_countries_field = $casino_bonus_pod->fields('bonus_countries');
    $bonus_currency_field = $casino_bonus_pod->fields('bonus_currency');

    if (!empty($casino_id) && !empty($country_id)) {
        $casino_field_info = array(
            'pod_id' => $bonus_casino_field['pod_id'],
            'field_id' => $bonus_casino_field['id'],
            'related_item_id' => $casino_id,
        );

        $countries_field_info = array(
            'pod_id' => $bonus_countries_field['pod_id'],
            'field_id' => $bonus_countries_field['id'],
            'related_item_id' => $country_id,
        );

        $currency_field_info = array(
            'pod_id' => $bonus_currency_field['pod_id'],
            'field_id' => $bonus_currency_field['id'],
            'related_item_id' => $country_id,
        );

        global $wpdb;
        $bonus_data_tmp = $wpdb->get_results("
      SELECT b.id, b.name, b.bonus_text AS casino_bonus_text, b.bonus_sum AS casino_bonus_sum, b.bonus_list_affiliate_link AS casino_top_link, b.bonus_list_button_text AS casino_top_button_text, b.bonus_list_regulation_text AS casino_top_regulation_text, b.bonus_shortcode_button_text AS casino_shortcode_button_text, b.bonus_shortcode_regulation_text AS casino_shortcode_regulation_text, bonus_country.related_item_id AS country_id, bonus_casino.related_item_id AS casino_id, bonus_currency.related_item_id AS currency_id
      FROM wp_pods_casino_bonus b
      LEFT JOIN wp_podsrel AS bonus_country ON b.id = bonus_country.item_id
      LEFT JOIN wp_podsrel AS bonus_casino ON b.id = bonus_casino.item_id
      LEFT JOIN wp_podsrel AS bonus_currency ON b.id = bonus_currency.item_id
      WHERE (bonus_casino.pod_id = " . $casino_field_info['pod_id'] . " AND bonus_casino.related_item_id = " . $casino_field_info['related_item_id'] . " AND bonus_casino.field_id = " . $casino_field_info['field_id'] . ") AND
      (bonus_country.pod_id = " . $countries_field_info['pod_id'] . " AND bonus_country.related_item_id = " . $countries_field_info['related_item_id'] . " AND bonus_country.field_id = " . $countries_field_info['field_id'] . ")" . " AND (bonus_currency.pod_id = " . $currency_field_info['pod_id'] . " AND bonus_currency.field_id = " . $currency_field_info['field_id'] . ") AND b.bonus_enabled = 1",
            ARRAY_A);

        $bonus_data = FALSE;
        if (!empty($bonus_data_tmp)) {
            $casino_bonus_currency = !empty($bonus_data_tmp[0]['currency_id']) ? scg_get_currency_sign_from_currency_id($bonus_data_tmp[0]['currency_id']) : FALSE;

            $bonus_data = array(
                'casino_bonus_text' => $bonus_data_tmp[0]['casino_bonus_text'],
                'casino_bonus_sum' => $bonus_data_tmp[0]['casino_bonus_sum'],
                'casino_bonus_currency' => $casino_bonus_currency,
                'casino_top_link' => $bonus_data_tmp[0]['casino_top_link'],
                'casino_top_button_text' => $bonus_data_tmp[0]['casino_top_button_text'],
                'casino_top_regulation_text' => $bonus_data_tmp[0]['casino_top_regulation_text'],
                'casino_shortcode_button_text' => $bonus_data_tmp[0]['casino_shortcode_button_text'],
                'casino_shortcode_regulation_text' => $bonus_data_tmp[0]['casino_shortcode_regulation_text'],
            );
        }

        return $bonus_data;
    }
}


/**
 * Get casino game link data (by casino ID, language, country, game type ID).
 */
function scg_get_casino_game_link_data_by_casino_id_language_country($casino_id, $language_code, $country_id, $game_type_id)
{
    $casino_link_pod = pods('casino_link');
    $casino_link_casino_field = $casino_link_pod->fields('casino_link_casino');
    $casino_link_countries_field = $casino_link_pod->fields('casino_link_countries');
    $casino_link_game_field = $casino_link_pod->fields('casino_link_game');

    if (!empty($casino_id) && !empty($country_id) && !empty($game_type_id)) {
        $casino_field_info = array(
            'pod_id' => $casino_link_casino_field['pod_id'],
            'field_id' => $casino_link_casino_field['id'],
            'related_item_id' => $casino_id,
        );

        $countries_field_info = array(
            'pod_id' => $casino_link_countries_field['pod_id'],
            'field_id' => $casino_link_countries_field['id'],
            'related_item_id' => $country_id,
        );

        $game_type_field_info = array(
            'pod_id' => $casino_link_game_field['pod_id'],
            'field_id' => $casino_link_game_field['id'],
            'related_item_id' => $game_type_id,
        );

        global $wpdb;
        $casino_link_data_tmp = $wpdb->get_results("
      SELECT l.id, l.name, l.casino_link, l.casino_link_button_text, l.casino_link_regulation_text, link_country.related_item_id AS country_id, link_casino.related_item_id AS casino_id, link_game_type.related_item_id AS game_type_id
      FROM wp_pods_casino_link l
      LEFT JOIN wp_podsrel AS link_country ON l.id = link_country.item_id
      LEFT JOIN wp_podsrel AS link_casino ON l.id = link_casino.item_id
      LEFT JOIN wp_podsrel AS link_game_type ON l.id = link_game_type.item_id
      WHERE (link_casino.pod_id = " . $casino_field_info['pod_id'] . " AND link_casino.related_item_id = " . $casino_field_info['related_item_id'] . " AND link_casino.field_id = " . $casino_field_info['field_id'] . ") AND
      (link_country.pod_id = " . $countries_field_info['pod_id'] . " AND link_country.related_item_id = " . $countries_field_info['related_item_id'] . " AND link_country.field_id = " . $countries_field_info['field_id'] . ")" . " AND (link_game_type.pod_id = " . $game_type_field_info['pod_id'] . " AND link_game_type.related_item_id = " . $game_type_field_info['related_item_id'] . " AND link_game_type.field_id = " . $game_type_field_info['field_id'] . ")" . " AND (l.casino_link_status = 1)",
            ARRAY_A);

        // l.language = '" . $language_code . "' - TODO.
        // var_dump($casino_link_data_tmp);
        // var_dump($wpdb->last_query);

        $casino_link_data = FALSE;
        if (!empty($casino_link_data_tmp)) {
            $casino_link_data = array(
                'casino_link' => $casino_link_data_tmp[0]['casino_link'],
                'casino_link_button_text' => $casino_link_data_tmp[0]['casino_link_button_text'],
                'casino_link_regulation_text' => $casino_link_data_tmp[0]['casino_link_regulation_text'],
            );
        }

        return $casino_link_data;
    }
}


/**
 * Adds pods post save hook.
 */
function scg_save_casino_info_pod_item($pieces, $is_new_item, $id) {
    $casino_id = $pieces['fields']['casino_info_casino']['value'];

    // Return early.
    if (empty($casino_id)) {
      return;
    }

    // Set Casino ID.
    if (!empty($casino_id)) {
      $pieces['fields']['casino_id']['value'] = $casino_id;
    }

    return $pieces;
}

add_action('pods_api_pre_save_pod_item_casino_info', 'scg_save_casino_info_pod_item', 99, 3);


/**
 * Generate casino main infos.
 */
function scg_generate_casino_main_infos()
{
    // Stop executing if it's done already.
    if (!empty(get_option('pods-casino-main-infos-generated')) && pods('casino_main_info') || !pods('casino_main_info')) {
        return;
    }

    $casinos_list = scg_get_current_casinos_list($enabled_only = FALSE);

    if (!empty($casinos_list)) {
        foreach ($casinos_list as $casino) {
            $casino_id = $casino['id'];
            $casino_name = $casino['name'];

            // Check if main info ID exists.
            $params = array(
                'distinct' => FALSE,
                'where' =>
                    "casino_id = '" . $casino_id . "'",
            );

            $main_info = pods('casino_main_info');
            $main_info->find($params);
            $data = $main_info->data();

            $main_info_id = FALSE;

            if (!empty($data) && !empty($data[0])) {
                $main_info_id = (int)$data[0]->id;
            }

            // Create main info if ID not found.
            if (empty($main_info_id)) {
                $new_main_info = pods('casino_main_info');

                // To add a new item, let's set the data first
                $data = array(
                    'name' => $casino_name . ' - main info',
                    'casino' => $casino_id,
                    'casino_id' => $casino_id,
                );

                // Add the new item now and get the new ID
                $new_main_info_id = $new_main_info->add($data);

                // Add casino ID to casino Pod.
                if (!empty($casino_id) && !empty($new_main_info_id)) {
                    $casino = pods('casino', $casino_id);
                    $casino->add_to('casino_main_info', $new_main_info_id);
                    $casino->save();
                }
            }

        }
    }

    // Mark done.
    update_option('pods-casino-main-infos-generated', TRUE);
}

add_action('init', 'scg_generate_casino_main_infos');


/**
 * Format ordinal number.
 */
function scg_format_ordinal_number($number, $only_suffix)
{
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');

    if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
        if ($only_suffix) {
            return 'th';
        } else {
            return $number . 'th';
        }
    } else {
        return $ends[$number % 10];
        if ($only_suffix) {
            return $ends[$number % 10];
        } else {
            return $number . $ends[$number % 10];
        }
    }
}


/**
 * Number format to 1.5 M or 2.5 B.
 */
function scg_custom_number_format($n, $precision = 3)
{
    if ($n < 1000000) {
        // Anything less than a million
        $n_format = number_format($n);
    } else if ($n < 1000000000) {
        // Anything less than a billion
        $n_format = number_format($n / 1000000, $precision) . ' M';
    } else {
        // At least a billion
        $n_format = number_format($n / 1000000000, $precision) . ' B';
    }

    return $n_format;
}


/**
 * Add taxonomies to pages.
 */
function scg_add_taxonomies_to_pages() {
    register_taxonomy_for_object_type('category', 'page');
}

// add_action('init', 'scg_add_taxonomies_to_pages');


/*
 * Plugin Name: Action Scheduler - Disable Default Queue Runner
 * Plugin URI: https://github.com/Prospress/action-scheduler-disable-default-runner/
 * Description: Disable Action Scheduler's default queue runner, by removing it from the 'action_scheduler_run_queue' hook.
 * Author: Prospress Inc.
 * Author URI: https://prospress.com/
 * License: GPLv3
 * Version: 1.0.0
 * Requires at least: 4.0
 * Tested up to: 4.8
 *
 * GitHub Plugin URI: Prospress/action-scheduler-disable-default-runner
 * GitHub Branch: master
 *
 */

function asdds_disable_default_runner() {
  if ( class_exists( 'ActionScheduler' ) ) {
    remove_action( 'action_scheduler_run_queue', array( ActionScheduler::runner(), 'run' ) );
  }
}
// ActionScheduler_QueueRunner::init() is attached to 'init' with priority 1, so we need to run after that
add_action( 'init', 'asdds_disable_default_runner', 10 );

//Following functions are needed for 'Guru Shortcodes Full' plug in, but are most likely not relecant for CG project.
//Leaving them with commented out code from SCG project, might be needed to be reviewed later on.
function is_page_crypto() {
    // global $post;
    // $current_url = get_permalink(get_the_ID());
  
    // // TOP Cryptos rating page == 5333.
    // $is_crypto_rating = get_the_ID() == 8714;
  
    // $cryptos_page_links_list = sbg_get_cryptos_page_links_list();
  
  
    // $crypto_page_link = false;
    // if (!empty($cryptos_page_links_list)) {
    //     foreach ($cryptos_page_links_list as $link) {
    //         if ($link == $current_url) {
    //             $crypto_page_link = true;
    //         }
    //     }
    // }
  
    // $searchWords = ['crypto','bitcoin','usdt','bnb','eth','tether','nft','blockchain','mining','staking','litecoin','dash','coin','cripto','moneda'];
    // $is_crypto_url = words_contains($current_url, $searchWords); //add check url for crypto, to show crypto rates on blogposts
  
    // //get tags to check is crypto
    // $is_crypto_tag = false;
    // $tags = get_the_tags(get_the_ID()); //get all tags of the post
    // if (!empty($tags)) {
    //     foreach ($tags as $tag) {
    //         if (words_contains($tag->name, $searchWords) || words_contains($tag->slug, $searchWords)) { // check if name or slug has crypto niddle word
    //             $is_crypto_tag = true;
    //             break;
    //         }
    //     }
    // }
  
    // if ($is_crypto_rating || $crypto_page_link || $is_crypto_url || $is_crypto_tag) {
    //     return true;
    // }
  
    return false;
  }
  
  /**
   * Get Cryptos page links list.
   */
  function sbg_get_cryptos_page_links_list() {
    // Get data from transient or
    // make query if it's not there.
    // $cryptos_page_links = get_transient('scg_cryptos_page_links_list');
  
    // if (empty($cryptos_page_links)) {
    //     $params = array(
    //         'distinct' => FALSE,
    //         'where' =>
    //             "top_rating_page.id IS NOT NULL",
    //         'limit' => 0,
    //     );
  
    //     $crypto = pods('cryptocurrency');
    //     $crypto->find($params);
    //     $data = $crypto->data();
  
    //     $cryptos_page_links = array();
  
    //     if (!empty($data) && !empty($data[0])) {
    //         if ($crypto->total_found()) {
    //             while($crypto->fetch()) {
    //                 $crypto_page_object = $crypto->field('top_rating_page');
    //                 $crypto_page_id = !empty($crypto_page_object) ? (int) $crypto_page_object['ID'] : FALSE;
    //                 $crypto_page_link = !empty($crypto_page_id) ? get_permalink($crypto_page_id) : FALSE;
  
    //                 $cryptos_page_links[] = $crypto_page_link;
    //             }
    //         }
    //     }
  
    //     // Set transient to expire after 7 days.
    //     // TODO - make it not expirable with cache clear logic.
    //     set_transient('scg_cryptos_page_links_list', $cryptos_page_links, 24 * 60 * 60 * 7);
    // }
  
    // return $cryptos_page_links;
  }
//   function words_contains($string, Array $search, $caseInsensitive = false) {
//     $exp = '/'
//         . implode('|', array_map('preg_quote', $search))
//         . ($caseInsensitive ? '/i' : '/');
//     return preg_match($exp, $string) ? true : false;
//   }
  /**
   * Get crypto ID from name.
   */
  function sbg_get_crypto_id_from_name($name)
  {
    //   $params = array(
    //       'distinct' => FALSE,
    //       'where' =>
    //           "t.name = '" . $name . "'",
    //   );
  
    //   $crypto = pods('cryptocurrency');
    //   $crypto->find($params);
    //   $data = $crypto->data();
  
    //   $crypto_id = FALSE;
  
    //   if (!empty($data) && !empty($data[0])) {
    //       $crypto_id = (int)$data[0]->id;
    //   }
  
    //   return $crypto_id;
  }
  
  /**
   * Get Crypto name by short name.
   */
  function sbg_get_crypto_name_by_short_name($short_name) {
    // $params = array(
    //     'distinct' => FALSE,
    //     'where' =>
    //         "t.name = '" . $short_name . "'" . ' OR ' .
    //         "t.short_name = '" . $short_name . "'",
    // );
  
    // $country = pods('cryptocurrency');
    // $country->find($params);
    // $data = $country->data();
  
    // $crypto_name = FALSE;
  
    // if (!empty($data) && !empty($data[0])) {
    //     $crypto_name = $data[0]->name;
    // }
  
    // return $crypto_name;
  }
  
  /**
   * Get country code from country name.
   */
  function sbg_get_country_code_from_name($country_name)
  {
    //   $query = new SelectQuery;
  
    //   $query
    //       ->from('wp_pods_country')
    //       ->select(['name', 'country_code'])
    //       ->orderBy('name', 'ASC')
    //       ->orderBy('id', 'ASC')
    //       ->limit(1)
    //       ->where()
    //       ->equal('name', $country_name);
  
    //   return (new DatabaseService($query, DatabaseService::GET_ITEM, 'country_code'))
    //       ->build() ?: false;
  }
/**
 * Get Crypto name by short name.
 */
function scg_get_crypto_name_by_short_name($short_name) {
    // $params = array(
    //     'distinct' => FALSE,
    //     'where' =>
    //         "t.name = '" . $short_name . "'" . ' OR ' .
    //         "t.short_name = '" . $short_name . "'",
    // );

    // $country = pods('cryptocurrency');
    // $country->find($params);
    // $data = $country->data();

    // $crypto_name = FALSE;

    // if (!empty($data) && !empty($data[0])) {
    //     $crypto_name = $data[0]->name;
    // }

    // return $crypto_name;
}